﻿$(function () {
    tree();
});

function tree()
{
    $('.tree li:has(ul)').addClass('parent_li').find(' > span.minus').attr('title', 'Contraer').find(' > i.glyphicon').addClass('glyphicon-plus-sign');
    $('.tree li.parent_li > span.minus').on('click', function (e) { 
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide('fast');
            $(this).attr('title', 'Expandir').find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
        } else {
            children.show('fast');
            $(this).attr('title', 'Contraer').find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
        }
        e.stopPropagation();
    });
}