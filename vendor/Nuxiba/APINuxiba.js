// JavaScript CORE API Methods
Nuxiba = 
{
	init : function(component){
		return this.component = component;
	},
	//username:String, password:String 
	login: function(username, password){
		return this.component.login(username, password);
	},
	//id:uint
	SetOnUnavailableStatus: function(id){
		return this.component.SetOnUnavailableStatus(id);
	},
	SetReady: function(){
		return this.component.SetReady();
	},		
	//Id:uint
	transferCallToAgent: function(Id){
		return this.component.transferCallToAgent(Id);
	},
	//Id:uint
	trasnferCallToACD: function(Id){
		return this.component.trasnferCallToACD(Id);
	},
	//Number:String
	transferCallToPhoneNumber: function(Number){
		return this.component.transferCallToPhoneNumber(Number);
	},
	//callOutID:int, CallID:int, phoneNumber:String, campID:uint
	//phoneNum:String, camID:Number, clientName:String
	makeManualCall: function(phoneNum,campID,clientName){
		return this.component.makeManualCall(phoneNum, campID, clientName);
	},
	//aToAdministrator:String, aMessage:String
	SendChatMessage: function(Administrator, Message){
		return this.component.SendChatMessage(Administrator, Message);
	},
	AVRS_Start: function(){
		return this.component.AVRS_Start();
	},
	AVRS_Stop: function(){
		return this.component.AVRS_Stop();
	},
	AVRS_Delete: function(){
		return this.component.AVRS_Delete();
	},
	GetLastCallData: function(){ //Ce
		return this.component.GetLastCallData();
	},
	getAgentID: function(){ //ui
		return this.component.getAgentID();
	},
	getUsername: function(){ //St
		return this.component.getUsername();
	},
	getExtenstion: function(){ //St
		return this.component.getExtenstion();
	},
		
	holdCall: function(){
		return this.component.holdCall();
	},
	//aDigit:String
	DTMFDigit: function(aDigit){
		return this.component.DTMFDigit(aDigit);
	},
	hangUpCall: function(){
		return this.component.hangUpCall();
	},

	CloseSession: function(){
		return this.component.CloseSession();
	},

	/* **********************************************************************
				REQUESTS 
	********************************************************************** */

	getCallHistory: function(){
		return this.component.getCallHistory();
	},
	getUnavailables: function(){
		return this.component.getUnavailables();
	},
	getunavalibaleHistory: function(){
		return this.component.getunavalibaleHistory();
	},
	//outboundType:Boolean, idACD:uint = 0
	getTransfersOptions: function(outboundType, idACD){
		return this.component.getTransfersOptions(outboundType, idACD);
	},
	getSupervisorsToChat: function(){
		return this.component.getSupervisorsToChat();
	},
	getCampaignsRelated: function(){
		return this.component.getCampaignsRelated();
	},
	//idACD:uint
	getACDDispositions: function(idACD){
		return this.component.getACDDispositions(idACD);
	},
	//idDisposition:uint, callID:uint
	disposeACDCall: function(idDisposition, callID){
		return this.component.disposeACDCall(idDisposition, callID);
	},
	//idCampaign:uint
	getCampaignDispositions: function(idCampaign){
		return this.component.getCampaignDispositions(idCampaign);
	},
	//idCampaign:uint, callOutID:Number
	getCampaignDispositionsAndNumbers: function(idCampaign,callOutID){
		return this.component.getCampaignDispositionsAndNumbers(idCampaign,callOutID);
	},
	//callOutID:Number
	getPhoneNumbers: function(callOutID){
		return this.component.getPhoneNumbers(callOutID);
	},
	//idDiposition:uint, idCampaign:uint, callID:uint
	disposeCampaingCall: function(idDiposition, idCampaign, callID){
		return this.component.disposeCampaingCall(idDiposition, idCampaign, callID);
	},
	//idCampaign:uint, idDisposition:uint, callID:uint, dateCallBack:String, callOutID:int, telephone:String, customNumber:Boolean 
	reprogramCampaignCall: function(idCampaign, idDisposition, callID, dateCallBack, telephone, existingNumber){
		return this.component.reprogramCampaignCall(idCampaign, idDisposition, callID, dateCallBack, telephone, existingNumber);
	},
			
	/// ******** AFFECT BD ************

	//callOutID:int, data1:String, data2:String, data3:String, data4:String, data5:String
	UpdateDataCall: function(callOutID, data1, data2, data3, data4, data5){
		return this.component.UpdateDataCall(callOutID, data1, data2, data3, data4, data5);
	},
	//aCurrentPassword:String, aNewPassword:String
	ChangePassword: function(aCurrentPassword, aNewPassword){
		return this.component.ChangePassword(aCurrentPassword, aNewPassword);
	},
	events: {
		onLogin: [],
		onLogOut: [],
		onUnavailableTypes: [],
		onUnavailableHistory: [],
		onCallHistory: [],
		onAdministrators: [],
		onCampaigns: [],
		onTransferOptions: [],
		onDispositions: [],
		onDispositionsAndNumbers: [],
		onPhoneNumbers: [],
		onCallRecieved: [],
		onCallEnds: [],
		onDataCallUpdated: [],
		onPasswordUpdated: [],
		onDialingNumber: [],
		numberOnDoNotCallList: [],
		wrongNumber: [],
		onReprogramCall: [],
		onDisposeApplied: [],
		onAgentStatusChange: [],
		onCallsOnQueue: [],
		onChatMessage: [],
		onDialResult: [],
		remoteLoginError: [],
		errorOnUpdateDataCall: [],
		errorOnPasswordUpdate: [],
		errorOnCallHistory: [],
		errorOnCampaignsRelated: [],
		errorOnChatAdministratorsList: [],
		errorOnDialProcess: [],
		errorOnDispose: [],
		errorOnDispositionList: [],
		errorOnDispositionSelected: [],
		errorOnDispositionsPhonesList: [],
		errorOnPhoneNumbersList: [],
		errorOnDTMFJS: [],
		onError: [],
		errorOnTransferOptions: [],
		errorOnUnavailableStatus: [],
		errorOnUnavailableStatusHistory: [],
		errorOnDialProcess: [],
		errorOnDispose: [],
		errorOnDispositionList: [],
		errorOnDispositionSelected: [],
	},
	callEvent: function(events, data){
		for (var i = 0; i < events.length; i++) {
			events[i](data);
		}
		console.debug(arguments.callee.caller.name);
		console.debug(data);
		console.debug(events);
	}

}

function onLogin(){
	Nuxiba.callEvent(Nuxiba.events.onLogin, null);
} 

function onLogOut(){
	console.log("onLogOut");
	Nuxiba.callEvent(Nuxiba.events.onLogOut, null);
} 

function onUnavailableTypes(UnavailableStatus){
	//Array -- Elementos de tipo : UnavailableStatus
	//id:uint            //Example: UnavailableStatus[i].id
	//statusName:String
	//nAvaiable:String
	Nuxiba.callEvent(Nuxiba.events.onUnavailableTypes, UnavailableStatus);
}

function onUnavailableHistory(UnavailableStatusHistory){
	//Array -- Elementos de tipo : UnavailableStatusHistory
	//count:uint		//Example: UnavailableStatusHistory[i].count
	//total:String
	//accumulatedTime:String
	//maxTime:String
	//statusName:String
	
	//statusLog:Array
	//time:String		//Example: UnavailableStatusHistory[i].statusLog[j].type
	//hour:String      
	Nuxiba.callEvent(Nuxiba.events.onUnavailableHistory, UnavailableStatusHistory);
}

function onCallHistory(HistoryDataCall){
	//Array -- Elementos de tipo : HistoryDataCall
	//type:String			//Example: HistoryDataCall[i].type
	//hour:String			
	//telephone:String
	//campaignName:String
	//duration:String
	Nuxiba.callEvent(Nuxiba.events.onCallHistory, HistoryDataCall);
}

function onAdministrators(Administrators){
	//Array -- Elementos de tipo : String
	//Example: Administrators[i]
	Nuxiba.callEvent(Nuxiba.events.onAdministrators, Administrators);
}

function onCampaigns(Campaigns){

	//Array -- Elementos de tipo : Campaign
	//id:uint      //Example: Campaigns[i].id
	//name:String 
	Nuxiba.callEvent(Nuxiba.events.onCampaigns, Campaigns);
}

function onTransferOptions(transfersOptions){
	//Array -- Elementos de tipo : TransferAgent
	//transfersOptions.agent
	//id:Number     //Example: transfersOptions.agent[i].id
	//name:String   
	//Array -- Elementos de tipo : TransferPhone
	//transfersOptions.phones
	//id:Number      //Example: transfersOptions.phones[i].id
	//name:String    
	//phone:String  
	//Array -- Elementos de tipo : TransferACD
	//transfersOptions.acds
	//id:Number      //Example: transfersOptions.acds[i].id
	//name:String  
	Nuxiba.callEvent(Nuxiba.events.onTransferOptions, transfersOptions);
}


function onDispositions(CallDisposition){
	//alert(Dispositions);
	//Array -- Elementos de tipo : Disposition
	//Dispositions
	//id:uint       //Example: CallDisposition.dispositions[i].id
	//name:String
	Nuxiba.callEvent(Nuxiba.events.onDispositions, CallDisposition);
	
}

function onDispositionsAndNumbers(CallDisposition){
	//Array -- Elementos de tipo : Disposition
	//CallDisposition.dispositions
	//id:uint       //Example: CallDisposition.dispositions[i].id
	//name:String
	//Array -- Elementos de tipo : Disposition
	//CallDisposition.phoneNumbers    //Example:CallDisposition.phoneNumbers[i]
	Nuxiba.callEvent(Nuxiba.events.onDispositionsAndNumbers, CallDisposition);
}

function onPhoneNumbers(phoneNumbers){
	//Array -- Elementos de tipo : String
	//phoneNumbers    //Example:phoneNumbers[i]
	Nuxiba.callEvent(Nuxiba.events.onPhoneNumbers, phoneNumbers);
}

function onCallRecieved(callData){
		//Number	
		//callData.call_id
		//Number
		//callData.cam_id
		//String
	 	//callData.cal_key
		//Number
	 	//callData.callOut_id
		//String
	 	//callData.DNIS
		//uint
	 	//callData.typeCall
		//uint
	 	//callData.IsQueueCall
		//Number
	 	//callData.holdTime
		//String
	 	//callData.phoneNumber
		//int
	 	//callData.port
		//int
	 	//callData.wrapUpTime
		//Array
	 	//callData.contactData
		
		callData.IDOutIn = "";
		if(callData.typeCall == 1){
			//IN
			callData.IDOutIn = '- DNIS ' + callData.DNIS;
		}else{
			//OUT
			callData.IDOutIn = '- callOut_id ' + callData.callOut_id;
		}
		Nuxiba.callEvent(Nuxiba.events.onCallRecieved, callData);
}

function onCallEnds(callData){
	//Number	
	//callData.dialogTime
	//Number
	//callData.callKey
	Nuxiba.callEvent(Nuxiba.events.onCallEnds, callData);
}


function onDataCallUpdated(){
	Nuxiba.callEvent(Nuxiba.events.onDataCallUpdated, "");
}

function onPasswordUpdated(){
	Nuxiba.callEvent(Nuxiba.events.onPasswordUpdated, "");
}

// MARCANDO NUMERO
function onDialingNumber (callOutData){
	//callout_id:uint
	//callOutData.callout_id
	//call_id:int
	//callOutData.call_id
	//phoneNumber:String
	//callOutData.phoneNumber
	//camID:uint
	//callOutData.camID
	Nuxiba.callEvent(Nuxiba.events.onDialingNumber, callOutData);
}

// Numero en lista negra
function numberOnDoNotCallList(){
	Nuxiba.callEvent(Nuxiba.events.numberOnDoNotCallList, "");
}

// Numero incorrecto
function wrongNumber(phone){
//-- Si se vuelve a invocar makeManualCall con el mismo numero se marcara si pasar por el proceso de verificacion
	Nuxiba.callEvent(Nuxiba.events.wrongNumber, phone);
}

function onReprogramCall(DispositionResult){
	//Array -- Elementos de tipo : DispositionResult
	//startDate:Date; //Example: DispositionResult.startDate
	//endDate:Date;
	//defaultDate:Date

	
	//callBacks:Array
	//DispositionResult.callBacks
	//date:Date				//Example: DispositionResult.callBacks[i].date
	
	//callBacks:Array;		//Example: DispositionResult.callBacks[i].callBacks[j].hour
	//hour:uint;
	//nCallBacks:uint;
	//alert(DispositionResult.callBacks[0].date)
	Nuxiba.callEvent(Nuxiba.events.onReprogramCall, DispositionResult);
}

function onDisposeApplied(){
	Nuxiba.callEvent(Nuxiba.events.onDisposeApplied, "");
}

function onAgentStatusChange(agentStatus){
	agentStatus.descripStatus = ""
	switch(agentStatus.status){
		case 2:  //Unavailable:uint
			agentStatus.descripStatus = "UNAVAILABLE"
		break;
		
		case 3:  //READY:uint
			agentStatus.descripStatus = "READY"
		break;
		
		case 4:  //DIALOG:uint
			agentStatus.descripStatus = "DIALOG"
		break;
		
		case 5:	 //XFER:uint
			agentStatus.descripStatus = "XFER"
		break;
		
		case 6:	 //WRAPUP:uint
			agentStatus.descripStatus = "WRAPUP"
		break;
		
		case 7:	 //OTHER:uint
			agentStatus.descripStatus = "OTHER"
		break;
		
		case 9:	 //RINGING:uint
			agentStatus.descripStatus = "RINGING"
		break;
		
		case 11: //PROBLEM:uint
			agentStatus.descripStatus = "PROBLEM"
		break;
		
		case 21: //CALLOUT:uint
			agentStatus.descripStatus = "CALLOUT"
		break;
		
	}
	Nuxiba.callEvent(Nuxiba.events.onAgentStatusChange, agentStatus);

}

function onCallsOnQueue(CallOnQueue){
	//Array -- Elementos de tipo : CallOnQueue
	//acdID - ID del ACD		//Example: CallOnQueue.acdID
	//nQueue - Numero de personas en QUEUE
	//secondsOnQueue - Segundos maximos en espera
	Nuxiba.callEvent(Nuxiba.events.onCallsOnQueue, CallOnQueue);
}

function onChatMessage(chatMessage){
	//chatMessage.administrator // Nombre del Administrador que envio el mensaje
	//chatMessage.message // Texto del mensaje
	Nuxiba.callEvent(Nuxiba.events.onChatMessage, chatMessage);
}

function onDialResult(callResult){
	//callResult.dialResult // entero >> contiene el resultado de la marcacion manual realizada por el agente.
	//2 ---NUMERO OCUPADO
	//3 --- NUMERO MARCADO NO CONSTESTA
	//4 --- EL NUMERO QUE SE MARCO FUE EL DE UN FAX
	//5 --- NO HAY TONO  DE MARCADO
	//8 --- EL AGENTE ESTA EN OTRO ESTADO
	//10 --- NO HAY SERVICIO DE TELEFONIA
	//11 --- CONTESTO UNA CONSTADORA AUTOMATICA
	//12 --- EL AGENTE ESTA RECIBIENDO UNA TRANSFERENCIA
	//1 -- SE CONTESTO LA LLAMADA 
	switch(callResult.dialResult){
		case 1:  //SE CONTESTO LA LLAMADA 
			callResult.descripResult = "SE CONTESTO LA LLAMADA"
		break;
		case 2:  //NUMERO OCUPADO
			callResult.descripResult = "NUMERO OCUPADO"
		break;
		case 3:  //NUMERO MARCADO NO CONSTESTA
			callResult.descripResult = "NUMERO MARCADO NO CONSTESTA"
		break;
		case 4:  //EL NUMERO QUE SE MARCO FUE EL DE UN FAX
			callResult.descripResult = "EL NUMERO QUE SE MARCO FUE EL DE UN FAX"
		break;
		case 5:	 //NO HAY TONO  DE MARCADO
			callResult.descripResult = "NO HAY TONO  DE MARCADO"
		break;
		case 8:	 //EL AGENTE ESTA EN OTRO ESTADO
			callResult.descripResult = "EL AGENTE ESTA EN OTRO ESTADO"
		break;
		case 10: //NO HAY SERVICIO DE TELEFONIA
			callResult.descripResult = "NO HAY SERVICIO DE TELEFONIA"
		break;
		case 11: //CONTESTO UNA CONSTADORA AUTOMATICA
			callResult.descripResult = "CONTESTO UNA CONSTADORA AUTOMATICA"
		break;
		case 12: //EL AGENTE ESTA RECIBIENDO UNA TRANSFERENCIA
			callResult.descripResult = "EL AGENTE ESTA RECIBIENDO UNA TRANSFERENCIA"
		break;
	}
	Nuxiba.callEvent(Nuxiba.events.onDialResult, callResult);	
}

function remoteLoginError(data){
	Nuxiba.callEvent(Nuxiba.events.remoteLoginError, data);
} 

function errorOnUpdateDataCall(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnUpdateDataCall, data);
}

function errorOnPasswordUpdate(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnPasswordUpdate, data);
}

function errorOnCallHistory(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnCallHistory, data);
}

function errorOnCampaignsRelated(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnCampaignsRelated, data);
}

function errorOnChatAdministratorsList(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnChatAdministratorsList, data);
}

function errorOnDialProcess(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDialProcess, data);
}

function errorOnDispose(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispose, data);
}

function errorOnDispositionList(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispositionList, data);
}

function errorOnDispositionSelected(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispositionSelected, data);
}

function errorOnDispositionsPhonesList(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispositionsPhonesList, data);
}


function errorOnPhoneNumbersList(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnPhoneNumbersList, data);
}

function errorOnDTMFJS(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDTMFJS, data);
}

function onError(data){
	Nuxiba.callEvent(Nuxiba.events.onError, data);
}

function errorOnTransferOptions(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnTransferOptions, data);
}

function errorOnUnavailableStatus(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnUnavailableStatus, data);
}

function errorOnUnavailableStatusHistory(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnUnavailableStatusHistory, data);
}

function errorOnDialProcess(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDialProcess, data);
}

function errorOnDispose(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispose, data);
}
function errorOnDispositionList(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispositionList, data);
}
function errorOnDispositionSelected(data){
	Nuxiba.callEvent(Nuxiba.events.errorOnDispositionSelected, data);
}
