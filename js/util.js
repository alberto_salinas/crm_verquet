var storageEvents = {
    add: function(key, callback){
        if(storageEvents[key] == undefined)
        {
            storageEvents[key] = [];
        }
        storageEvents[key].push(callback);
    }
};
/*navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return ['IE ', (tem[1] || '')];
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\bOPR\/(\d+)/)
        if(tem!= null) return ['Opera ', tem[1]];
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M;
})();

$(document).ready(function() {
    var navegadorValido = false;
    var minVersion = 32;
    try
    {
        if (navigator.sayswho[0].toLowerCase() == "firefox")
        {
            if(parseInt(navigator.sayswho[1]) >= minVersion)
            {
                navegadorValido = true;
            }
        }
    }
    catch(err) {
    }

    if(!navegadorValido)
    {
        $.blockUI({
            message: "<img src='"+img_error.src+"' style='width: 30%;margin-top: 10px;'></br><h4><b>Este navegador no es compatible con la version de CRM</b><h4><h3>Se recomienda el uso de MOZILLA FIREFOX " + minVersion +"</h3><img src='"+img_firefox.src+"' style='width: 20%;'>"
        });
    }
});*/

function validarSalida(e) {
    var e = e || window.event;
    var mensaje = "Se perderan los datos no guardados.";
    // For IE and Firefox
    if (e) {
        e.returnValue = mensaje;
    }
    // For Safari
    return mensaje;
};

function validateAccess(){
    var usuario = $('#username').val();
    var pass = $('#password').val();
    if(usuario && pass){
        showLoading('Iniciando Sesión');
    }
}

function quitarLada(value)
{
    if(value.length > 10 )
    {
        return value.substr(value.length-10,10)
    }
    else
    {
        return value;
    }
}

var addEvent = (function () {
  if (document.addEventListener) {
    return function (el, type, fn) {
      if (el && el.nodeName || el === window) {
        el.addEventListener(type, fn, false);
      } else if (el && el.length) {
        for (var i = 0; i < el.length; i++) {
          addEvent(el[i], type, fn);
        }
      }
    };
  } else {
    return function (el, type, fn) {
      if (el && el.nodeName || el === window) {
        el.attachEvent('on' + type, function () { return fn.call(el, window.event); });
      } else if (el && el.length) {
        for (var i = 0; i < el.length; i++) {
          addEvent(el[i], type, fn);
        }
      }
    };
  }
})();

function storageEvent(event){
    if(storageEvents[event.key] != undefined)
    {
        var events = storageEvents[event.key];
        for (var i = 0; i < events.length; i++) {
            events[i](event);
        }
    }
}

function clearSelect(select)
{
    var childrens = select.children;
    for (var i = childrens.length - 1; i >= 0; i--) {
        if(childrens[i])
        {
            childrens[i].parentNode.removeChild(childrens[i]);
        }
    };
}
function addOption(text, value, select)
{
    var option = document.createElement("option");
    option.textContent = text;
    option.value = value;
    select.appendChild(option);
    return option;
}

function fireEvent(theInput, event)
{
    if ("createEvent" in document) {  //NON IE browsers
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, false, true);
        theInput.dispatchEvent(evt);
      }
      else {  //IE
        var evt = document.createEventObject();
        theInput.fireEvent("on" + event, evt);
      }
}

function showLoading(message, element)
{
    if(element)
    {
        $(element).block({ message: loadingImage.outerHTML + '</br><h3 id="text_load">' + message + '</h3>' });
    }
    else
    {
        $.blockUI({
            message: loadingImage.outerHTML + '</br><h3>' + message + '</h3>'
        });
    }
}
function hideLoading(element)
{
    if(element)
    {
        $(element).unblock();
    }
    else
    {
        $.unblockUI();
    }
}

function showAlert(type,message){
    document.getElementById('alert-message').style.display = 'block';
    $("#alert-message").append('<div id="alert-answare" class="alert alert-'+type+' alert-dismissable">'+
                              '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                              message+'.'+
                            '</div>');
}


function createdate(date){
    var splidate = date.split("/");
    var newdate = new Date(splidate[2], (splidate[1] - 1), splidate[0]);
    return newdate;
}


/*
//DESHABILITA CLICK DERECHO
var message = ""; 

function clickIE(){ 
if (document.all){ 
(message); 
return false; 
} 
} 

function clickNS(e){ 
if (document.layers || (document.getElementById && !document.all)){ 
if (e.which == 2 || e.which == 3){ 
(message); 
return false; 
} 
} 
} 

if (document.layers){ 
document.captureEvents(Event.MOUSEDOWN); 
document.onmousedown = clickNS; 
} else { 
document.onmouseup = clickNS; 
document.oncontextmenu = clickIE; 
} 
document.oncontextmenu = new Function("return false"); 
*/

//VALIDAR TARJETA DE CREDITO CON EL ALGORITMO LUHN
function valid_credit_card(value) {
  // Quita espacios y guiones
    if (/[^0-9-\s]+/.test(value)) return false;
 
    
    var nCheck = 0, nDigit = 0, bEven = false;
    value = value.replace(/\D/g, "");
 
    for (var n = value.length - 1; n >= 0; n--) {
        var cDigit = value.charAt(n),
              nDigit = parseInt(cDigit, 10);
 
        if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
        }
 
        nCheck += nDigit;
        bEven = !bEven;
    }
 
    return (nCheck % 10) == 0;
}

//solo numeros
function solonum(evt) {
    var code = (evt.which) ? evt.which : evt.keyCode;
        if(code==8 || code==9 || code==37 || code==39)
        {
            //backspace
            return true;
        }
        else if(code>=48 && code<=57)
        {
            //is a number
            return true;
        }
        else
        {
            return false;
        }
}

//solo letras
function sololetras(e) {
    key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " abcdefghijklmnñopqrstuvwxyz";

       tecla_especial = false
       if(key==8 || key==9 || key==37 || key==39 || key==46){
                tecla_especial = true;
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
}

function validateNumero(numero){
    var patron = /^\d*$/;
    if(!numero.search(patron)){
        return 1;    
    }else{
        return 0;
    }
}