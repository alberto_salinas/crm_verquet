<?php

namespace MDR\IntranetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use MDR\PuntoVentaBundle\Entity\IntranetComentarios;
/**
 * @Route("/Comentarios")
 */
class ComentariosController extends Controller
{
    /**
     * @Route("/create", name="intranet_comentarios_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $texto = $request->request->get('texto');
        $calificacion = $request->request->get('calificacion');
        $idMenu = $request->request->get('idMenu');

        if(strlen(trim($texto)) == 0)
        {
        	$respuesta['mensaje'] = 'Debe inidicar un texto.';
        	return new JsonResponse($respuesta);
        }
        if($calificacion == 0)
        {
        	$respuesta['mensaje'] = 'Debes indicar una calificación.';
        	return new JsonResponse($respuesta);
        }
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenu);

        if($menu != null)
        {
        	$contenido = $menu->getContenido();
        	if($contenido != null)
        	{
        		if($contenido->getComentariosActivo())
        		{
        			$comentario = new IntranetComentarios();

        			$comentario->setTexto($texto);
        			$comentario->setCalificacion($calificacion);

        			$comentario->setContenido($contenido);
        			$comentario->setUsuario($user);
        			$comentario->setFecha(new \DateTime());

        			$em->persist($comentario);
        			$em->flush();

        			$respuesta['estado'] = 1;
        			$respuesta['mensaje'] = 'Comentario creado exitosamente.';
        			$respuesta['comentario'] = $comentario;
        		} else {
        			$respuesta['mensaje'] = 'Está página no tiene habilitados los comentarios.';
        		}
        	}
        }


        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/update", name="intranet_comentarios_update")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $texto = $request->request->get('texto');
        $calificacion = $request->request->get('calificacion');
        $idComentario = $request->request->get('idComentario');

        if(strlen(trim($texto)) == 0)
        {
            $respuesta['mensaje'] = 'Debe inidicar un texto.';
            return new JsonResponse($respuesta);
        }
        if($calificacion == 0)
        {
            $respuesta['mensaje'] = 'Debes indicar una calificación.';
            return new JsonResponse($respuesta);
        }
        $comentario = $em->getRepository('MDRPuntoVentaBundle:IntranetComentarios')->find($idComentario);

        if($comentario != null)
        {
            if($user->getUNumero() == $comentario->getUsuario()->getUNumero())
            {
                //Validar tiempo para edición/eliminación
                $diff = $comentario->getFecha()->diff(new \DateTime());
                if($diff->format('%a') == 0 && $diff->h == 0 && $diff->i <= 5)
                {
                    $comentario->setTexto($texto);
                    $comentario->setCalificacion($calificacion);

                    $em->persist($comentario);
                    $em->flush();

                    $respuesta['estado'] = 1;
                    $respuesta['mensaje'] = 'Comentario modificado exitosamente.';
                    $respuesta['comentario'] = $comentario;
                } else {
                    $respuesta['mensaje'] = 'Ya no es posible editar este comentario.';
                }
            } else {
                $respuesta['mensaje'] ='Ya no es posible editar este comentario.';
            }
        }

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/delete", name="intranet_comentarios_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $idComentario = $request->request->get('idComentario');
        $comentario = $em->getRepository('MDRPuntoVentaBundle:IntranetComentarios')->find($idComentario);

        if($comentario != null)
        {
            $editable = false;
            $securityContext = $this->container->get('security.context');
            if($securityContext->isGranted('ROLE_SUPERVISOR'))
            {
                $editable = true;
            } else if($user->getUNumero() == $comentario->getUsuario()->getUNumero()) {
                //Validar tiempo para edición/eliminación
                $diff = $comentario->getFecha()->diff(new \DateTime());
                $editable = $diff->format('%a') == 0 && $dif->h == 0 && $diff->i <= 5;
            }
            $this->get('logger')->debug('Comentario editable: '.$editable);

            if($editable)
            {
                $em->remove($comentario);
                $em->flush();

                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Comentario eliminado exitosamente.';
                $respuesta['comentario'] = $comentario;
            } else {
                //$respuesta['mensaje'] = 'El tiempo para modificar esté comentario terminó.';
                $respuesta['mensaje'] = 'Ya no es posible editar este comentario.';
            }
        }

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/Excel/{idContenido}", name="intranet_comentarios_excel")
     * @Method("GET")
     */
    public function ExportExcelAction($idContenido)
    {
        $em = $this->getDoctrine()->getManager();
        $comentarios = $em->getRepository('MDRPuntoVentaBundle:IntranetComentarios')->findBy(['contenido' => $idContenido]);

        set_time_limit(240);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('B1', "Usuario")
                ->setCellValue('C1', "Fecha")
                ->setCellValue('D1', "Texto")
                ->setCellValue('E1', "Calificación")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($comentarios as $comentario)
        {
            $sheet
                ->setCellValue('A'.$numRow, $comentario->getIdComentario())
                ->setCellValue('B'.$numRow, $comentario->getUsuario()->getUClave())
                ->setCellValue('C'.$numRow, $comentario->getFecha()->format('Y/m/d H:i:s'))
                ->setCellValue('D'.$numRow, $comentario->getTexto())
                ->setCellValue('E'.$numRow, $comentario->getCalificacion())
            ;
            $numRow ++;
        }
    
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=Comentarios.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    }

}
