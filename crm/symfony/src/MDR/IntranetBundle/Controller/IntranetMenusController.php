<?php

namespace MDR\IntranetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\IntranetContenidos;
use MDR\PuntoVentaBundle\Entity\IntranetMenus;
use MDR\PuntoVentaBundle\Form\IntranetMenusType;

/**
 * IntranetMenus controller.
 *
 * @Route("/intranetMenus")
 */
class IntranetMenusController extends Controller
{

    /**
     * Lists all IntranetMenus entities.
     *
     * @Route("/", name="intranetmenus")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new IntranetMenus entity.
     *
     * @Route("/", name="intranetmenus_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:IntranetMenus:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new IntranetMenus();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $tipoRequest = $form->get('tipoRequest')->getData();
        $idMenuPadre = $form->get('menuPadre')->getData();

        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = '';

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.context')->getToken()->getUser();
            $menuPadre = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenuPadre);
            $plantilla = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->findOneByTipo(1);

            $contenido = new IntranetContenidos();
            $contenido->setImagen('');
            $contenido->setHtml1('Sin contenido');
            $contenido->setHtml2('Sin contenido');
            $contenido->setHtml3('Sin contenido');
            $contenido->setHtml4('Sin contenido');

            $contenido->setComentariosActivo(true);
            $contenido->setFechaCreacion(new \DateTime());
            $contenido->setUsuarioCrea($user);
            $contenido->setFechaEdicion(new \DateTime());
            $contenido->setUsuarioEdita($user);
            $contenido->setPlantilla($plantilla);
            $em->persist($contenido);

            $entity->setContenido($contenido);
            $entity->setMenuPadre($menuPadre);
            $entity->setUrl($entity->getNombre());
            
            $entity->setFechaCreacion(new \DateTime());
            $entity->setUsuarioCrea($user);
            $entity->setFechaEdicion(new \DateTime());
            $entity->setUsuarioEdita($user);

            $em->persist($entity);
            $em->flush();

            $respuesta['estado'] = 1;
            if($tipoRequest != 'ajax')
            {
                return $this->redirect($this->generateUrl('intranetmenus_show', array('id' => $entity->getId())));
            }
            $respuesta['menu'] = $entity;
        }
        else
        {
            $respuesta['mensaje'] = $form->getErrorsAsString();;
        }

        if($tipoRequest == 'ajax')
        {
            return new JsonResponse($respuesta);
        }
        else
        {
            return array(
                'entity' => $entity,
                'form'   => $form->createView(),
            );
        }
    }

    /**
     * Creates a form to create a IntranetMenus entity.
     *
     * @param IntranetMenus $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(IntranetMenus $entity)
    {
        $form = $this->createForm(new IntranetMenusType(), $entity, array(
            'action' => $this->generateUrl('intranetmenus_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new IntranetMenus entity.
     *
     * @Route("/new", name="intranetmenus_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new IntranetMenus();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a IntranetMenus entity.
     *
     * @Route("/{id}", name="intranetmenus_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IntranetMenus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing IntranetMenus entity.
     *
     * @Route("/{id}/edit", name="intranetmenus_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IntranetMenus entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a IntranetMenus entity.
    *
    * @param IntranetMenus $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(IntranetMenus $entity)
    {
        $form = $this->createForm(new IntranetMenusType(), $entity, array(
            'action' => $this->generateUrl('intranetmenus_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing IntranetMenus entity.
     *
     * @Route("/{id}", name="intranetmenus_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:IntranetMenus:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find IntranetMenus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $tipoRequest = $editForm->get('tipoRequest')->getData();

        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = '';

        if ($editForm->isValid()) {
            $entity->setUrl($entity->getNombre());
            $entity->setFechaEdicion(new \DateTime());
            $entity->setUsuarioEdita($user);
            
            $em->flush();

            $respuesta['estado'] = 1;
            if($tipoRequest != 'ajax')
            {
                return $this->redirect($this->generateUrl('intranetmenus_edit', array('id' => $id)));
            }
            $respuesta['menu'] = $entity;

        } else {
            $respuesta['mensaje'] = $form->getErrorsAsString();;
        }

        if($tipoRequest == 'ajax')
        {
            return new JsonResponse($respuesta);
        }
        else
        {
            return array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            );
        }
    }
    /**
     * Deletes a IntranetMenus entity.
     *
     * @Route("/{id}", name="intranetmenus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find IntranetMenus entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('intranetmenus'));
    }

    /**
     * Creates a form to delete a IntranetMenus entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('intranetmenus_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
