/**
 * Uruguayan spanish translation for bootstrap-wysihtml5
 */
(function($){
    $.fn.wysihtml5.locale["es-ES"] = {
        font_styles: {
              normal: "Texto normal",
              h1: "Título 1",
              h2: "Título 2",
              h3: "Título 3",
              h4: "Título 4",
              h5: "Título 5",
              h6: "Título 6",
        },
        emphasis: {
              bold: "Negrita",
              italic: "Itálica",
              underline: "Subrayado"
        },
        lists: {
              unordered: "Lista desordenada",
              ordered: "Lista ordenada",
              outdent: "Eliminar sangría",
              indent: "Agregar sangría"
        },
        link: {
              insert: "Insertar link",
              cancel: "Cancelar"
        },
        image: {
              insert: "Insertar imagen",
              cancel: "Cancelar"
        },
        html: {
            edit: "Editar HTML"
        },
        colours: {
          black: "Negro",
          silver: "Plata",
          gray: "Gris",
          maroon: "Marrón",
          red: "Rojo",
          purple: "Purpura / Morado",
          green: "Verde",
          olive: "Olivo",
          navy: "Azul Marino",
          blue: "Azul",
          orange: "Naranja"
        }
    };
}(jQuery));