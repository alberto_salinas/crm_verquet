//Modelos
var ComentarioIntranet = Backbone.Model.extend({
	initialize: function(){
		var fecha = new Date(getDateFromFormat(this.get('fecha').date, "yyyy-MM-dd HH:mm:ss"));
		this.set('fecha', fecha);
		if(this.get('usuario').uNumero == idUsuario)
		{
			var diff = new Date() - fecha;
			this.set('editable', (diff / 1000 / 60) <= 5);
		}
	},
	defaults: {
		idComentario: 	contArchivosIntranet++,
		texto: 			'',
		fecha: 			'',
		calificacion: 	0,
		usuario: 		'',
		editable: 		false,
		estatusVista: 	''
	}
});

//Colecciones
var ComentariosIntranetCollection = Backbone.Collection.extend({
	model: ComentarioIntranet,
	getTotales: function(){
		return this.countBy(function(item) {
			return item.get('calificacion') == 1 ? 'likes': 'disLikes';
		});
	}
});

//Instancias
var comentariosIntranet = new ComentariosIntranetCollection();

//Vistas Lógicas
var ComentarioIntranetView = Backbone.View.extend({
	tagName: "div",
	className: "item comentarioIntranet",
	template: comentarioTemplate,

	initialize: function(){
		var that = this;
		this.render();
		//Eventos modelo
		this.listenTo(this.model,  'change', 	this.render);
		this.listenTo(this.model,  'destroy', 	this.destroy);

		if(this.model.get('editable') == true)
		{
			var diff = new Date() - this.model.get('fecha');
			setTimeout(function(){
				that.model.set('editable', false);
			}, 5*60*1000 - diff);
		}
	},

	render: function(){
		this.$el.html(this.template(this.model.toJSON()));

		return this;
	},
	//Eventos vista
	events: {
		'click .editarComentario'	: 	'editarComentario',
		'click .borrarComentario'	: 	'borrarComentario'
	},
	editarComentario: function() {
		this.model.set('estatusVista', 'edit');
	},
	borrarComentario: function() {
		var that = this;
		if(confirm('¿Estas seguro de borrar tu comentario?'))
		{
			var dataSend = {idComentario: this.model.get('idComentario')};
			$.post(urls.intranet.comentarios.borrar, dataSend, function(data, textStatus, xhr) {
				if(data.estado == 1)
				{
					that.model.destroy();
				} else {
					alert(data.mensaje);
				}
			});
		}
	},
	//Eventos modelo
	destroy: function() {
		this.remove();
	}
});

//Vista Principal
var ComentariosIntranetView = Backbone.View.extend({
	events: {
		'keypress #textoComentario': 	'onEnterComentario',
		'click #btnNuevoComentario': 	'creaComentario',
		'click #btnGuardaComentario': 	'guardarComentario',
		'click #btnCancelarComentario': 	'limpiarCampos',
		'click .btnLike': 				'like',
		'click .btnDisLike': 			'disLike'
	},
	//Variables
	calificacion: 0,
	comentarioActual: null,
	//Funciones
	initialize: function() {
		this.setElement($("#ComentariosIntranet"));

		//Elementos vista
		this.$comentariosContainer = $("#comentariosContainer");
		this.$textoComentario = $("#textoComentario");
		this.$btnNuevoComentario = $("#btnNuevoComentario");
		this.$btnGuardaComentario = $("#btnGuardaComentario");
		this.$cancelarComentario = $("#btnCancelarComentario");
		this.$like = $(".btnLike");
		this.$disLike = $(".btnDisLike");
		this.$contLikes = $(".contLikes");
		this.$contDisLikeS = $(".contDisLikes");

		//Listeners modelo
		this.listenTo(comentariosIntranet, 'add',		this.addComentario);
		this.listenTo(comentariosIntranet,  'change', 	this.refreshComentario);
		this.listenTo(comentariosIntranet,  'remove', 	this.showContLikes);

		this.render();
	},
	render: function() {
		this.addAllComentarios(comentariosIntranet);
		
		var alto = $(window).height() - this.$comentariosContainer.position().top - 250;
		this.$comentariosContainer.css('max-height', alto + 'px');

		return this;
	},

	obtenerDatosComentario() {
		var texto = this.$textoComentario.val().trim();
		//Validar
		if(texto.length == 0)
		{
			alert('Debes escribir algo.');
			return false;
		}
		if(this.calificacion == 0)
		{
			alert('Debes indicar una calificación.');
			return false;
		}
		return { idMenu: menuBD.idMenu, texto: texto, calificacion: this.calificacion };
	},
	creaComentario: function() {
		var that = this;
		var data = this.obtenerDatosComentario();
		if(data === false)
		{
			return;
		}
		this.showLoading(true);
		$.post(urls.intranet.comentarios.crear, data, function(data, textStatus, xhr) {
			if(data.estado == 1)
			{
				comentariosIntranet.add(data.comentario);
				that.limpiarCampos();
				that.showContLikes();
			} else {
				alert(data.mensaje);
			}
		}).always(function(){
			that.showLoading(false);
		});
	},
	limpiarCampos: function() {
		this.$textoComentario.val('');
		this.calificacion = 0;
		this.$like.removeClass('active');
		this.$disLike.removeClass('active');
		this.$btnNuevoComentario.show();
		this.$btnGuardaComentario.hide();
		this.$cancelarComentario.hide();
	},
	showContLikes: function() {
		var cont = comentariosIntranet.getTotales();
		this.$contLikes.html(cont.likes !== undefined ? cont.likes : 0);
		this.$contDisLikeS.html(cont.disLikes !== undefined ? cont.disLikes : 0);
	},
	showLoading: function(val) {
		var overlay = this.$el.find('.overlay');
		if(val)
		{
			overlay.show();
		} else {
			overlay.hide();
		}
	},
	editarComentario: function() {
		this.$textoComentario.val(this.comentarioActual.get('texto'));
		this.calificacion = this.comentarioActual.get('calificacion');
		this.setIconLike();
		this.$btnNuevoComentario.hide();
		this.$btnGuardaComentario.show();
		this.$cancelarComentario.show();
	},
	guardarComentario: function() {
		var that = this;
		var dataSend = this.obtenerDatosComentario();
		if(dataSend === false)
		{
			return;
		}
		dataSend.idComentario = this.comentarioActual.get('idComentario');
		this.showLoading(true);
		$.post(urls.intranet.comentarios.update, dataSend, function(data, textStatus, xhr) {
			if(data.estado == 1)
			{
				that.comentarioActual.set('estatusVista', '');
				that.comentarioActual.set('texto', dataSend.texto);
				that.comentarioActual.set('calificacion', dataSend.calificacion);
				that.limpiarCampos();
				that.showContLikes();
			} else {
				alert(data.mensaje);
			}
		}).always(function(){
			that.showLoading(false);
		});
	},
	//Eventos vista
	onEnterComentario: function(e) {
		if (e.keyCode == 13) this.creaComentario();
	},
	like: function() {
		this.calificacion = 1;
		this.setIconLike();
	},
	disLike: function() {
		this.calificacion = -1;
		this.setIconLike();
	},
	setIconLike: function() {
		this.$disLike.removeClass('active');
		this.$like.removeClass('active');
		if(this.calificacion == 1)
		{
			this.$like.addClass('active');
		}
		if(this.calificacion == -1)
		{
			this.$disLike.addClass('active');
		}
	},
	//Eventos modelo
	addComentario: function(item)
	{
		var view = new ComentarioIntranetView({model: item});
  		this.$comentariosContainer.prepend(view.render().el);
	},
	addAllComentarios: function(items)
	{
		items.each(this.addComentario, this);
		this.showContLikes();
	},
	refreshComentario: function(item) {
		var attrs = item.changedAttributes();
		console.log(attrs);
		if(_.keys(attrs).indexOf('estatusVista') >= 0)
		{
			if(attrs.estatusVista == 'edit')
			{
				this.comentarioActual = item;
				this.editarComentario();
			}
		}
	},
	reset: function()
	{
		this.render();
	},
});

var appComentariosIntranet;