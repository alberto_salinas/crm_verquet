var timerBuscar = null;
function buscarMenu()
{
	clearTimeout(timerBuscar);
	timerBuscar = setTimeout(busquedaMenu, 250);
}
function busquedaMenu()
{
	$("[data-idmenu]").hide();
	$("[data-idmenu=0]").show();
	var texto = $("#buscador").val().trim();
	var menusConsulta = [];
	if(texto.length > 2)
	{
		var reg = new RegExp(texto, "i")
		menusConsulta = menus.filter(function(menu) { return reg.test(menu.nombre); });
		_.each(menusConsulta, function(menu){
			$("[data-idmenu]:has([data-idmenu=" + menu.idMenu + "])").show();
			$("[data-idmenu=" + menu.idMenu + "]").show();
			$("[data-idmenu=" + menu.idMenu + "] li").show();
		});
	} else {
		menusConsulta = menus.filter(function(menu) { return menu.idMenuPadre == 0; });
		_.each(menusConsulta, function(menu){
			//$("[data-idmenu]:has([data-idmenu=" + menu.idMenu + "])").show();
			$("[data-idmenu=" + menu.idMenu + "]").show();
			//$("[data-idmenu=" + menu.idMenu + "] li").show();
		});
	}
}

function getBreadCrumb(menu)
{
	if(menu.idMenuPadre > 0)
	{
		var menuPadre = _.find(menus, function(menuItem) { return menuItem.idMenu == menu.idMenuPadre});
		return getBreadCrumb(menuPadre) + '<li>' + menu.nombre + '</li>';
	}
	return '<li>' + menu.nombre + '</li>';
}