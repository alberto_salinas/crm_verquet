var timerBuscar = null;
function buscarMenu()
{
	clearTimeout(timerBuscar);
	timerBuscar = setTimeout(busquedaMenu, 250);
}
function busquedaMenu()
{
	$("[data-idmenu]").hide();
	$("[data-idmenu] ul").removeClass('menu-open');
	var texto = $("#buscador").val().trim();
	var menusConsulta = [];
	if(texto.length > 2)
	{
		var reg = new RegExp(texto, "i")
		menusConsulta = menus.filter(function(menu) { return reg.test(menu.nombre); });
		_.each(menusConsulta, function(menu){
			$("[data-idmenu]:has([data-idmenu=" + menu.idMenu + "])").show().find('>ul').show().addClass('menu-open');
			$("[data-idmenu=" + menu.idMenu + "]").show().find('>ul').show().addClass('menu-open');
			$("[data-idmenu=" + menu.idMenu + "] li").show().find('>ul').show().addClass('menu-open');
		});
	} else {
		$("[data-idmenu]").show();
	}
}