var menuMain = {};
var menuActual = null;
var menus = {};

function GetMenuJSON(isAdmin)
{
	$.post(urls.intranet.menuJSON, {}, function(data, textStatus, xhr) {
		menus = data;
		menuMain = AgrupaMenu();
		HTMLJerarquia(menuMain, isAdmin);
	});
}

function AgrupaMenu(menuPadre)
{
	var idMenu = 0;
	if(menuPadre === undefined)
	{
		menuPadre = _.find(menus, function(menu) { return menu.idMenu == 0});
	}
	idMenu = menuPadre.idMenu;
	menuPadre.menus = [];
	var menuHijos = _.filter(menus, function(menu) { return menu.idMenuPadre == idMenu});
	for (var i = 0; i < menuHijos.length; i++) {
		menuPadre.menus.push(AgrupaMenu(menuHijos[i]));
	}
	return menuPadre;
}

function HTMLJerarquia(item, isAdmin)
{
	var template = _.template($("#ArbolMenuTemplate").html());
	var html = template({item: item, editable: isAdmin});
	$("#arbol").html(html);
	arbol();
	$(".addMenu").click(function(event) {
		var idMenu = $(this).parent().attr('data-idMenu');
		nuevoMenu(idMenu);
	});
	$(".editMenu").click(function(event) {
		var idMenu = $(this).parent().attr('data-idMenu');
		editMenu(idMenu);
	});
}

function nuevoMenu(idMenu)
{
	//Buscar padres
	var template = _.template($("#BreadcrumbMenusTemplate").html());
	var menus = getPadresMenu(idMenu);
	menuActual = null;
	var html = template({menus: menus});
	$("#modalMenu .breadcrumb").html(html);
	$("#intranet_menus_menuPadre").val(idMenu);
	$("#labelMenuAccion").html('Crear');
	$("#modalMenu").modal("show");
}
function editMenu(idMenu)
{
	//Buscar padres
	var template = _.template($("#BreadcrumbMenusTemplate").html());
	var menus = getPadresMenu(idMenu);
	menuActual = menus[menus.length-1];
	var html = template({menus: menus});
	$("#intranet_menus_url").val(menuActual.url);
	$("#intranet_menus_nombre").val(menuActual.nombre);

	$("#modalMenu .breadcrumb").html(html);
	$("#intranet_menus_menuPadre").val(idMenu);
	$("#labelMenuAccion").html('Modificar');
	$("#modalMenu").modal("show");
}
function viewMenu(idMenu)
{
	var url = getUrlMenu(idMenu);
	window.location = url;
}
function getUrlMenu(idMenu, completo)
{
	if (completo === undefined )	completo = true;
	//Buscar padres
	var menus = getPadresMenu(idMenu);
	menus = _.pluck(menus, 'url');
	var urlParcial = menus.join("/");
	if(completo)
	{
		return urls.intranet.index + "#c/" + urlParcial;
	} else {
		return "#" + urlParcial;
	}
}

function saveMenu(isAdmin)
{
	var $form = $("#menu_form");
	var url = '';
	var tipo = '';
	showLoading("Guardando menú");

	if(menuActual == null)
	{
		url = $form.attr('action');
		tipo = 'POST';
	} else {
		url = urls.intranet.menuUpdate.replace('__id__', menuActual.idMenu);
		tipo = 'PUT';
	}
	$.ajax({
		url: url,
		type: tipo,
		
		data: $form.serialize(),
	})
	.done(function(data) {
		if(data.estado == 1 ){
            $form.each (function(){
                this.reset();
            });
            $("#modalMenu").modal("hide");
            var menuPadre = _.find(menus, function(menu) { return menu.idMenu == data.menu.idMenuPadre});
            if(menuActual == null)
            {
                menus.push(data.menu);
                menuPadre.menus = menuPadre.menus || [];
                menuPadre.menus.push(data.menu);
            } else 
            {
            	var menu = _.find(menus, function(menu) { return menu.idMenu == data.menu.idMenu});
            	menu.url = data.menu.url;
            	menu.nombre = data.menu.nombre;
            }
            HTMLJerarquia(menuMain, isAdmin);
        }
        else
        {
        	alert(data.mensaje);
        }
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
    	hideLoading();
	});
    return false;
}

function getPadresMenu(idMenu)
{
	var resMenus = [];
	var menu = _.find(menus, function(menu) { return menu.idMenu == idMenu});
	if(menu === undefined) return [];
	resMenus.push(menu);
	if( menu.idMenuPadre != 0)
	{
		resMenus = _.union(getPadresMenu(menu.idMenuPadre), resMenus);
		return resMenus;
	}
	return resMenus;
}

function goUrl(e, url)
{
	var that = this;
	window.location = url;
	e.stopPropagation();
    e.preventDefault();
}

function sololetras(e) {
    key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = " abcdefghijklmnñopqrstuvwxyz-_";

   tecla_especial = false
   if(key==8 || key==9 || key==37 || key==39 || key==46){
            tecla_especial = true;
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}