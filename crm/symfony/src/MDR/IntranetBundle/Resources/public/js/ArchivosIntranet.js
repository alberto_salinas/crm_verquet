var contArchivosIntranet = 0

//Modelos
var ArchivoIntranet = Backbone.Model.extend({
	initialize: function(){
		this.set({extension: this.getExtension()});
		var tipo = this.getTipo();
		if(tipo === undefined)
		{
			this.destroy();
		} else {
			this.set({icono: tipo.get('icono')});
		}
	},
	defaults: {
		idArchivo: 	contArchivosIntranet++,
		nombre: 	'',
		extension: 	'',
		tamanio: 	0,
		icono: 		'',
		data: 		'',
		archivo: 	null,
		estatus: 	0
	},
	getExtension: function() {
		var partes = this.get('nombre').split('.');
		return partes[partes.length - 1];
	},
	getTipo: function() {
		return tiposArchivoIntranet.getByExtension(this.get("extension"));
	}
});

var TipoArchivoIntranet = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		tipo: 			'',
		icono: 			'',
		extensiones: 	[]
	}
});

//Colecciones
var ArchivosIntranetCollection = Backbone.Collection.extend({
	model: ArchivoIntranet,
	byEstatus: function(estatus) {
		return this.filter(function(item) {
			return item.get('estatus') == estatus;
		});
	},
	byNombre: function(nombre){
		return this.find(function(item) {
			return item.get('nombre').toLowerCase() == nombre.toLowerCase();
		});
	}
});

var TiposArchivoIntranetCollection = Backbone.Collection.extend({
	model: TipoArchivoIntranet,
	getByExtension: function(extension) {
		return this.find(function(item) {
			return 0 <= _.indexOf(item.get("extensiones"), extension)
		});
	}
});

//Instancias
var archivosIntranet = new ArchivosIntranetCollection();
var tiposArchivoIntranet = new TiposArchivoIntranetCollection([
	{
		tipo: 'image',
		icono: '',
		extensiones: ['jpg', 'jpeg', 'png', 'gif']
	},
	{
		tipo: 'video',
		icono: 'fa fa-youtube-play',
		extensiones: ['mp4']
	},
	{
		tipo: 'pdf',
		icono: 'fa fa-file-pdf-o',
		extensiones: ['pdf']
	}
]);

//Vistas Lógicas
var ArchivoIntranetView = Backbone.View.extend({
	tagName: "li",
	className: "document-row box",
	template: archivoListaTemplate,

	initialize: function(){
		this.render();
		this.listenTo(this.model,  'change', 	this.render);
		this.listenTo(this.model,  'destroy', 	this.destroy);
	},

	render: function(){
		//this.el["data-id"] = this.model.get('idArchivo');
		this.$el.html(this.template(this.model.toJSON()));
		//this.btnAdd = this.$("button");

		return this;
	},
	events: {
		'click .btnBorrar'	: 'borrar',
		'click .btnUpload'	: 'upload'
	},
	//Eventos vista
	borrar: function(){
		var that = this;
		if(this.model.get('estatus') == 1) 
		{
			this.model.set({estatus: -1});
			var data = {idMenu: menuBD.idMenu, archivo: this.model.get("nombre")};
			$.post(urls.intranet.contenidoBorrarArchivos, data, function(data, textStatus, xhr) {
				that.model.set({estatus: 1});
				if(data.estado == 1)
				{
					that.model.destroy();
				} else {
					alert(data.mensaje);
				}
			});
		} else 
		{
			alert('Este elemento no ha sido cargado.');
		}
	},
	upload: function(){
		this.model.set({estatus: -1});
		var formData = new FormData();
		formData.append('idMenu', menuBD.idMenu);
		formData.append('idPlantilla', $("#plantilla").val());
		formData.append('archivo', this.model.get('archivo'));
		formData.append('idArchivo', this.model.get('idArchivo'));
		jQuery.ajax({
			url: urls.intranet.contenidoGuardarArchivo,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(data){
				var estatus = 0;
				if(data.estado == 1)
				{
					estatus = 1;
				}
				that.model.set({estatus: estatus});
			}
		}).error(function(){
			that.model.set({estatus: 0});
		});
	},
	//Eventos modelo
	destroy: function() {
		this.remove();
	}
});

//Vista Principal
var ArchivosIntranetView = Backbone.View.extend({
	events: {
		'dragenter #holder': 	'dragEnter',
		'dragleave #holder': 	'dragLeave',
		'dragend #holder': 		'dragEnd',
		'dragover #holder': 	'dragOver',
		'drop #holder': 		'drop',
	},
	//Funciones
	initialize: function() {
		this.setElement($("#ArchivosIntranet"));

		this.$holder = $("#holder");
		console.log('initialize ArchivosIntranetView');
		this.listenTo(archivosIntranet, 'add', 		this.addArchivo);
		this.listenTo(archivosIntranet, 'reset', 	this.resetArchivos);
		this.listenTo(archivosIntranet,  'change', 	this.refreshArchivo);

		this.render();
	},
	render: function() {
		this.addAllArchivos(archivosIntranet);
	},
	readFiles: function(files) {
	    for (var i = 0; i < files.length; i++) {
	    	console.log(files[i])
	    }
	    for (var i = 0; i < files.length; i++) {
	    	this.processFile(files[i]);
	    }
	    var archivosPorSubir = archivosIntranet.byEstatus(0);
	    this.uploadFiles(archivosPorSubir);
	},
	processFile: function(file) {
		//Validar que el archivo no exista en la colección
		var existe = archivosIntranet.byNombre(file.name);
		if(existe !== undefined){
			return;
		}
		var archivo = {
			nombre: file.name,
			tamanio: file.size > 0 ? file.size/1024 : 0,
			data: '',
			archivo: file
		};
		//Si es una imagen obtener el dataURL para mostrarla
		var archivo = archivosIntranet.add(archivo);
		if(archivo.get('icono') == '')
		{
			var reader = new FileReader();
			reader.onload = function (event) {
				archivo.set({data: event.target.result});
			};

			reader.readAsDataURL(file);
		}
	},
	uploadFiles: function(archivos) {
		_.each(archivos, function(item){
			item.set({estatus: -1});
		});
		var formData = new FormData();
		for (var i = 0; i < archivos.length; i++) {
			var archivo = archivos[i];
			formData.append('idMenu', menuBD.idMenu);
			formData.append('idPlantilla', $("#plantilla").val());
			formData.append('archivos[]', archivo.get('archivo'));
			formData.append('idArchivos[]', archivo.get('idArchivo'));
		}
		jQuery.ajax({
			url: urls.intranet.contenidoGuardarArchivos,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(data){
				var estatus = 0;
				if(data.estado == 1)
				{
					estatus = 1;
				}
				_.each(archivos, function(item){
					item.set({estatus: estatus});
				});
			}
		}).error(function(){
			_.each(archivos, function(item){
				item.set({estatus: 0});
			});
		});
	},
	//Eventos vista
	dragEnter: function(e) {
		console.log("Backbone dragEnterFile");
		this.$holder.addClass("dragOver");
		return false;
	},
	dragLeave: function(e) {
		console.log("Backbone dragLeaveFile");
		this.$holder.removeClass("dragOver");
		return false;
	},
	dragOver: function(e) {
		e.stopPropagation();
		e.preventDefault();
		console.log("Backbone dragOver");
		return false;
	},
	dragEnd: function(e) {
		console.log("Backbone dragEnd");
		this.$holder.removeClass("dragOver");
		return false;
	},
	drop: function(e) {
		e.stopPropagation();
		e.preventDefault();
		console.log('Backbone drop');
		this.$holder.removeClass("dragOver");
		var files = [];
		if(e.originalEvent !== undefined)
		{
			files = e.originalEvent.dataTransfer.files;
		} else {
			files = e.dataTransfer.files;
		}
		this.readFiles(files);
	},
	//Eventos modelo
	addArchivo: function(item)
	{
		if(item.getTipo() === undefined)
		{
			item.destroy();
			return;
		}
		if(item.get('nombre').match('__logo[\.]') || item.get('nombre').match('__fondo[\.]'))
		{
			return;
		}
		var view = new ArchivoIntranetView({model: item});
  		this.$holder.prepend(view.render().el);
	},
	addAllArchivos: function(items)
	{
		items.each(this.addArchivo, this);
	},
	resetArchivos: function(items)
	{
		this.$el.html('');
		this.addAllArchivos(items);
	},
	reset: function()
	{
		this.render();
	},
});