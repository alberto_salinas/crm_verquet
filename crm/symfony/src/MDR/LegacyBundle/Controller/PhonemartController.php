<?php

namespace MDR\LegacyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;

/**
 * Phonemart controller.
 *
 * @Route("/Phonemart")
 */

class PhonemartController extends Controller
{

	/**
     * Pagina Consultas de pedido Phonemart
     *
     * @Route("/consultas_phonemart", name="consultas_phonemart")
     * @Method({"GET","POST"})
     * @Template("MDRLegacyBundle:Phonemart:consultas.html.twig")
     */
    public function PConsultasAction(Request $request)
    {
        
    }



}
