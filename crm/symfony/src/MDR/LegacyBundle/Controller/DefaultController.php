<?php

namespace MDR\LegacyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
		$legacyPath = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH');;

    	return $this->redirect($this->container->get('router')->getContext()->getBaseUrl().$legacyPath.'/MDRElastix/software.php');
    }
}
