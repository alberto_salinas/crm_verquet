<?php

namespace MDR\LegacyBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PedidosPhonemartRepository extends EntityRepository
{
    public function findByPedidosFiltro($Pedido,$Cliente,$NTarjeta,$Tipo){

        $em = $this->getEntityManager();
        
        $strQuery = "SELECT pp FROM MDRLegacyBundle:PedidosPhonemart pp LEFT JOIN pp.cobros c";

        if($Pedido){
            $strQuery .= " WHERE pp.pedido = :pedido";
        }

        if($Cliente){
            if($Pedido){
                $strQuery .= " AND pp.nombre LIKE :cliente";
            }else{
                $strQuery .= " WHERE pp.nombre LIKE :cliente";
            }   
            
        }
        if($NTarjeta){
            if($Pedido||$Cliente){
                $strQuery .= " AND c.tarjeta = :tarjeta";
            }else{
                $strQuery .= " WHERE c.tarjeta = :tarjeta";
            }
        }
        if($Tipo){
            if($Pedido||$Cliente||$NTarjeta){
               $strQuery .= " AND pp.Pago = :Tpago";
            }else{
               $strQuery .= " WHERE pp.Pago = :Tpago"; 
            }
        }        
       
        $query = $em->createQuery($strQuery);
        
        if($Pedido){
            $query->setParameter(':pedido', $Pedido); 
        }
        if($Cliente){
             $query->setParameter(':cliente', $Cliente);             
        }
        if($NTarjeta){
            $query->setParameter(':tarjeta', $NTarjeta); 
        }
        if($Tipo){
            $query->setParameter(':Tpago', $Tipo); 
        }

        $entities = $query->getResult();
        return $entities;
    }
  

}
