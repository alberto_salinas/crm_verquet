<?php

namespace MDR\LegacyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * CobrosPhonemart
 */
class CobrosPhonemart implements JsonSerializable
{
    /**
     * @var string
     */
    private $plazo;

    /**
     * @var string
     */
    private $mCobro;

    /**
     * @var string
     */
    private $tarjeta;

    /**
     * @var string
     */
    private $refBanco;

    /**
     * @var integer
     */
    private $idCob;

    /**
     * @var \DateTime
     */
    private $fCobro;

    /**
     * @var string
     */
    private $operador;

    /**
     * @var string
     */
    private $terminal;

    /**
     * @var integer
     */
    private $idCobro;

    /**
     * @var \MDR\LegacyBundle\Entity\PedidosPhonemart
     */
    private $pedido;


    /**
     * Set plazo
     *
     * @param string $plazo
     * @return CobrosPhonemart
     */
    public function setPlazo($plazo)
    {
        $this->plazo = $plazo;

        return $this;
    }

    /**
     * Get plazo
     *
     * @return string 
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set mCobro
     *
     * @param string $mCobro
     * @return CobrosPhonemart
     */
    public function setMCobro($mCobro)
    {
        $this->mCobro = $mCobro;

        return $this;
    }

    /**
     * Get mCobro
     *
     * @return string 
     */
    public function getMCobro()
    {
        return $this->mCobro;
    }

    /**
     * Set tarjeta
     *
     * @param string $tarjeta
     * @return CobrosPhonemart
     */
    public function setTarjeta($tarjeta)
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    /**
     * Get tarjeta
     *
     * @return string 
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    /**
     * Set refBanco
     *
     * @param string $refBanco
     * @return CobrosPhonemart
     */
    public function setRefBanco($refBanco)
    {
        $this->refBanco = $refBanco;

        return $this;
    }

    /**
     * Get refBanco
     *
     * @return string 
     */
    public function getRefBanco()
    {
        return $this->refBanco;
    }

    /**
     * Set idCob
     *
     * @param integer $idCob
     * @return CobrosPhonemart
     */
    public function setIdCob($idCob)
    {
        $this->idCob = $idCob;

        return $this;
    }

    /**
     * Get idCob
     *
     * @return integer 
     */
    public function getIdCob()
    {
        return $this->idCob;
    }

    /**
     * Set fCobro
     *
     * @param \DateTime $fCobro
     * @return CobrosPhonemart
     */
    public function setFCobro($fCobro)
    {
        $this->fCobro = $fCobro;

        return $this;
    }

    /**
     * Get fCobro
     *
     * @return \DateTime 
     */
    public function getFCobro()
    {
        return $this->fCobro;
    }

    /**
     * Set operador
     *
     * @param string $operador
     * @return CobrosPhonemart
     */
    public function setOperador($operador)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * Get operador
     *
     * @return string 
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * Set terminal
     *
     * @param string $terminal
     * @return CobrosPhonemart
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * Get terminal
     *
     * @return string 
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * Get idCobro
     *
     * @return integer 
     */
    public function getIdCobro()
    {
        return $this->idCobro;
    }

    /**
     * Set pedido
     *
     * @param \MDR\LegacyBundle\Entity\PedidosPhonemart $pedido
     * @return CobrosPhonemart
     */
    public function setPedido(\MDR\LegacyBundle\Entity\PedidosPhonemart $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\LegacyBundle\Entity\PedidosPhonemart 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

     public function jsonSerialize()
    {
        return array(
            'plazo'          => $this->plazo,
            'mCobro'           => $this->mCobro,
            'tarjeta'       => $this->tarjeta,
            'refBanco'       => $this->refBanco,
            'idCob'         => $this->idCob,
            'fCobro'         => $this->fCobro,
            'operador'        => $this->operador,
            'terminal'          => $this->terminal,
            'idCobro'          => $this->idCobro,
                       
        );

    }
}
