<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\ListaNegraTarjetas;
use MDR\PuntoVentaBundle\Form\ListaNegraTarjetasType;

/**
 * ListaNegraTarjetas controller.
 *
 * @Route("/listanegratarjetas")
 */
class ListaNegraTarjetasController extends Controller
{

    /**
     * Lists all ListaNegraTarjetas entities.
     *
     * @Route("/", name="listanegratarjetas")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:ListaNegraTarjetas:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();        
        $formExcel = $this->createExcelForm();

        $formExcel->handleRequest($request);

        $entities = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->findAll();
			
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
			
            if($formExcel->isValid()){

                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelLNTDC");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $formExcel['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);
                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $newTDC = trim($sheet->getCell('A'.$row)->getValue()); 

                                 $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->findBynumeroTarjeta($newTDC);

                                if(count($exist)){
                                    //\Doctrine\Common\Util\Debug::dump($exist);
                                        foreach ($exist as $numTDC) {
                                            $numTDC-> setActivo(true);
                                            $em->persist($numTDC);
                                            $em->flush();
                                        } 
                                        $respuesta[] = array('TDC' => $newTDC, 'mensaje'=>'La TDC ya existe, y a sido bloqueado');
                                }else{
                                                try{
                                                        $newTDCln = new ListaNegraTarjetas();
                                                        $newTDCln -> setNumeroTarjeta($newTDC);
                                                        $newTDCln -> setActivo(true);
                                                        $em->persist($newTDCln);
                                                        $em->flush();
                                                        $respuesta[] = array('TDC' => $newTDC, 'mensaje'=>'Se ha registrado con exito');
                                                }catch(\SoapFault $ex){

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA TDC:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }catch(\Exception $ex){   

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA TDC:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }
                                }

                    }
                    $row++;
                }

                return $this->createExcelReportExcelTDC($respuesta);


            }

        return array(
            'entities' => $entities,
            'formExcel' => $formExcel->createView(),
        );
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de tarjetas.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('Enviar', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('listanegratarjetas'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    function createExcelReportExcelTDC($listados, $titulo = 'Reporte-de-Carga_Excel-Lista-Negra-TDC'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "TDC")
                ->setCellValue('B1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, substr($listado['TDC'],0, 6)."-".substr($listado['TDC'],6, 12))
                        ->setCellValue('B'.$numRow, $listado['mensaje'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * Creates a new ListaNegraTarjetas entity.
     *
     * @Route("/", name="listanegratarjetas_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:ListaNegraTarjetas:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ListaNegraTarjetas();
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entity -> setActivo(true);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('listanegratarjetas_show', array('id' => $entity->getIdListaNegra())));

        }

        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ListaNegraTarjetas entity.
     *
     * @param ListaNegraTarjetas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ListaNegraTarjetas $entity)
    {
        $form = $this->createForm(new ListaNegraTarjetasType(), $entity, array(
            'action' => $this->generateUrl('listanegratarjetas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new ListaNegraTarjetas entity.
     *
     * @Route("/new", name="listanegratarjetas_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ListaNegraTarjetas();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ListaNegraTarjetas entity.
     *
     * @Route("/{id}", name="listanegratarjetas_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaNegraTarjetas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ListaNegraTarjetas entity.
     *
     * @Route("/{id}/edit", name="listanegratarjetas_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaNegraTarjetas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ListaNegraTarjetas entity.
    *
    * @param ListaNegraTarjetas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ListaNegraTarjetas $entity)
    {
        $form = $this->createForm(new ListaNegraTarjetasType(), $entity, array(
            'action' => $this->generateUrl('listanegratarjetas_update', array('id' => $entity->getIdListaNegra())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing ListaNegraTarjetas entity.
     *
     * @Route("/{id}", name="listanegratarjetas_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:ListaNegraTarjetas:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaNegraTarjetas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('listanegratarjetas_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ListaNegraTarjetas entity.
     *
     * @Route("/{id}", name="listanegratarjetas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ListaNegraTarjetas entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('listanegratarjetas'));
    }

    /**
     * Creates a form to delete a ListaNegraTarjetas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('listanegratarjetas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }
}
