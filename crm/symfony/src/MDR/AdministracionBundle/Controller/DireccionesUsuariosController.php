<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\DireccionesUsuarios;
use MDR\PuntoVentaBundle\Entity\Usuario;
use MDR\PuntoVentaBundle\Form\DireccionesUsuariosType;

/**
 * DireccionesUsuarios controller.
 *
 * @Route("/direccionesusuarios/{usuario}")
 */
class DireccionesUsuariosController extends Controller
{

    /**
     * Lists all DireccionesUsuarios entities.
     *
     * @Route("/", name="direccionesusuarios")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($usuario)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->findByUsuario($usuario);

        return array(
            'entities' => $entities,
            'usuario' => $usuario,
        );
    }
    /**
     * Creates a new DireccionesUsuarios entity.
     *
     * @Route("/", name="direccionesusuarios_create")
     * @Method("POST")
     * @Template("MDRAdministracionBundle:DireccionesUsuarios:new.html.twig")
     */
    public function createAction(Request $request, $usuario)
    {
        $entity = new DireccionesUsuarios();
        $em = $this->getDoctrine()->getManager();

        
        $idEstado = $_POST['mdr_puntoventabundle_direccionesusuarios']['estado'];
        $idMunicipio = $_POST['mdr_puntoventabundle_direccionesusuarios']['municipio'];
        $idColonia = $_POST['mdr_puntoventabundle_direccionesusuarios']['colonia'];

        $municipios = array();
        $colonias = array();
        $estado = $em->getRepository('MDRPuntoVentaBundle:Estados')->find($idEstado);
        $municipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($idMunicipio);
        if($municipio)
            $municipios[] = $municipio;
        
        $colonia = $em->getRepository('MDRPuntoVentaBundle:Colonias')->find($idColonia);
        if($colonia)
            $colonias[] = $colonia;
        $form = $this->createCreateForm($entity, $usuario, $municipios, $colonias);
        $form->handleRequest($request);

        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $usuario=$em->getRepository('MDRPuntoVentaBundle:Usuario')->find($usuario);
            $entity->setUsuario($usuario);
            $entity->setEstado($entity->getEstado()->getNombre());
            $entity->setMunicipio($entity->getMunicipio()->getNombre());
            $entity->setColonia($entity->getColonia()->getNombre());

            $em->persist($entity);
            $em->flush();


                return $this->redirect($this->generateUrl('direccionesusuarios', array('usuario' => $usuario->getUNumero())));

            return array(
                'entity' => $entity,
                'form'   => $form->createView(),
                'usuario' => $usuario,
            );
        }
            return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'usuario' => $usuario,
        );

    }

    /**
     * Creates a form to create a DireccionesUsuarios entity.
     *
     * @param DireccionesUsuarios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(DireccionesUsuarios $entity, $usuario, $municipios = array(), $colonias = array())
    {
        $form = $this->createForm(new DireccionesUsuariosType(), $entity, array(
            'action' => $this->generateUrl('direccionesusuarios_create', array('usuario' => $usuario)),
            'method' => 'POST',
            'municipios'                => $municipios,
            'colonias'                  => $colonias,
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new DireccionesUsuarios entity.
     *
     * @Route("/new", name="direccionesusuarios_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($usuario)
    {
        $entity = new DireccionesUsuarios();
        $form   = $this->createCreateForm($entity, $usuario);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'usuario' => $usuario,
        );
    }

    /**
     * Finds and displays a DireccionesUsuarios entity.
     *
     * @Route("/{id}", name="direccionesusuarios_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $usuario)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DireccionesUsuarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $usuario);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
           
        );
    }

    /**
     * Displays a form to edit an existing DireccionesUsuarios entity.
     *
     * @Route("/{id}/edit", name="direccionesusuarios_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $usuario)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->find($id, $usuario);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DireccionesUsuarios entity.');
        }

        $editForm = $this->createEditForm($entity, $usuario);
        $deleteForm = $this->createDeleteForm($id, $usuario);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'usuario'     => $usuario,
        );
    }

    /**
    * Creates a form to edit a DireccionesUsuarios entity.
    *
    * @param DireccionesUsuarios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DireccionesUsuarios $entity, $usuario)
    {
        $form = $this->createForm(new DireccionesUsuariosType(), $entity, array(
            'action' => $this->generateUrl('direccionesusuarios_update', array('id' => $entity->getidDireccion())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing DireccionesUsuarios entity.
     *
     * @Route("/{id}", name="direccionesusuarios_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:DireccionesUsuarios:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $usuario)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->find($id, $usuario);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DireccionesUsuarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $usuario);
        $editForm = $this->createEditForm($entity, $usuario);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('direccionesusuarios_edit', array('id' => $id, 'usuario' => $usuario)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a DireccionesUsuarios entity.
     *
     * @Route("/{id}", name="direccionesusuarios_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id, $usuario)
    {
        $form = $this->createDeleteForm($id, $usuario);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DireccionesUsuarios entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('direccionesusuarios', array('usuario' => $usuario)));
    }

    /**
     * Creates a form to delete a DireccionesUsuarios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('direccionesusuarios_delete', array('id' => $id, 'usuario' => $usuario)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
