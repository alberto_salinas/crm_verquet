<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

use MDR\PuntoVentaBundle\Entity\Telefonos;
use MDR\PuntoVentaBundle\Entity\ReglasFraude;
use MDR\PuntoVentaBundle\Entity\ListaNegraDirecciones;
use MDR\PuntoVentaBundle\Entity\ZonasRiesgo;
use MDR\PuntoVentaBundle\Entity\ListaNegraClientes;
use MDR\PuntoVentaBundle\Entity\ListaNegraMontos;

/**
 * CatClientes controller.
 *
 * @Route("/administracion")
 */

class AdministracionController extends Controller
{


    /**
     * Pagina Principal
     *
     * @Route("/reglas", name="reglas_fraude")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:fraude.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->findAll();
        $parametros = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getParametersValue();

        return array(
            'entities' => $entities,
            'parametros' => $parametros,
        );
    }


	 /**
     * Lista Negra Telefonica
     *
     * @Route("/ListaTelefonica", name="lista_negra_telefonica")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:listaTelefonica.html.twig")
     */
    public function listaTelefonicaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();        
        $searchForm = $this->createSearchForm();
        $NewForm = $this->createNewForm();
        $excelForm = $this->createExcelTelefonosForm();
        $entities = array();
        $respuesta = array();

         if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $NewForm->submit($request);
            $excelForm->submit($request);

            if ($searchForm->isValid())
            {
                $Telefono = $searchForm->get('numeroTelefonico')->getData();
                $Estatus = $searchForm->get('Estatus')->getData();
                $digitos = strlen($Telefono);
                if($Telefono){
                	if($digitos > 9){
                		
                		$entities = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findByTelefonosListaNegra($Telefono,$Estatus);
                		if(count($entities)<=0){
			                $this->get('session')->getFlashBag()->add(
			                    'error',  $this->get('translator')->trans('busquedaSinResultados')
			                );
			            }

                	}else{

                		$this->get('session')->getFlashBag()->add(
                    	'error',  'El numero telefonico debe constar de 10 digitos <br> Clave lada + 8 Números'
                		);

                	}
                }else{

                	$entities = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findByTelefonosListaNegra($Telefono,$Estatus);
                	if(count($entities)<=0){
		                $this->get('session')->getFlashBag()->add(
		                    'error',  $this->get('translator')->trans('busquedaSinResultados')
		                );
		            }

                }               
               
            }else if($NewForm->isValid()){

                        $Telefono = $NewForm->get('newNumeroTelefonico')->getData();
                        $digitos = strlen($Telefono);
                        if($digitos > 9){
                        
                        $entities = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findByNumero($Telefono);
                               
                                if(count($entities)>= 1){
                                    $this->get('session')->getFlashBag()->add(
                                        'error',  'El número que intenta registrar ya existe en la base de datos'
                                    );
                                }else{
                                        try{
                                                $newTelefono = new Telefonos();
                                                $newTelefono -> setNumero($Telefono);
                                                $newTelefono -> setEsListaNegra(true);
                                                $em->persist($newTelefono);
                                                $em->flush();

                                                $this->get('session')->getFlashBag()->add(
                                                'success',  'Se ha registrado el número con exito'
                                                );

                                        }catch(\SoapFault $ex){

                                            $this->get('logger')->error('Error LISTA NEGRA TELEFONOS:');
                                            $this->get('logger')->error($ex->getMessage());

                                        }catch(\Exception $ex){   

                                            $this->get('logger')->error('Error LISTA NEGRA TELEFONOS:');
                                            $this->get('logger')->error($ex->getMessage());

                                        }
                                   

                                }

                        }else{

                            $this->get('session')->getFlashBag()->add(
                            'error',  'El numero telefonico debe constar de 10 digitos <br> Clave lada + 8 Números'
                            );

                        }
            }else if($excelForm->isValid()){

                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelLNT");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $excelForm['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);
                


                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $newTel = $sheet->getCell('A'.$row)->getValue(); 
                        $digitos = strlen($newTel);
                        if($digitos > 9){

                                 $exist = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findByExcel($newTel);

                                if(count($exist)>0){
                                    //\Doctrine\Common\Util\Debug::dump($exist);
                                        foreach ($exist as $numteledit) {
                                            $numteledit-> setEsListaNegra(true);
                                            $em->persist($numteledit);
                                            $em->flush();
                                        } 
                                        $respuesta[] = array('numero' => $newTel, 'mensaje'=>'El numero ya existe, y a sido bloqueado');
                                }else{
                                                try{
                                                        $newTelefono = new Telefonos();
                                                        $newTelefono -> setNumero($newTel);
                                                        $newTelefono -> setEsListaNegra(true);
                                                        $em->persist($newTelefono);
                                                        $em->flush();
                                                        $respuesta[] = array('numero' => $newTel, 'mensaje'=>'Se ha registrado con exito');
                                                }catch(\SoapFault $ex){

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA TELEFONOS:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }catch(\Exception $ex){   

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA TELEFONOS:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }
                                }

                        }else{
                                        $respuesta[] = array('numero' => $newTel, 'mensaje'=>'El numero no tiene 10 digitos (lada + 8 números)');
                        }        

                    }
                    $row++;
                }

                return $this->createExcelReportExcelTel($respuesta);
            }
            
        }
       
       
        return $this->render('MDRAdministracionBundle:Administracion:listaTelefonica.html.twig', array(
            'telefonos' => $entities,
            'form_lnt' => $searchForm->createView(),
            'form_new' => $NewForm->createView(),
            'form_excel' => $excelForm->createView(),
        ));
    }

    /**
    * Crear un formulario para buscar numeros telefonicos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('numeroTelefonico', null, array(
                    'label' => 'Número Telefonico',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 10,'onkeypress' => 'return solonum(event)','placeholder' => 'clave lada + 8 digitos'),
                ))
	            ->add('Estatus', 'choice', array(
	            	"required" => false,
	                'choices'     => array(1 => 'Bloqueado', 0 => 'Libre'),
	                'empty_value' => '----------',
	                'attr' => array('class' => "form-control")
                ))                
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')	
                	))
                ->setAction($this->generateUrl('lista_negra_telefonica'))
    			->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
    * Crear un formulario para ingreso de nuevos numeros telefonicos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('newNumeroTelefonico', null, array(
                    'label' => 'Número Telefonico',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10,'onkeypress' => 'return solonum(event)','placeholder' => 'clave lada + 8 digitos'),
                )) 
                ->add(
                    'Guardar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'GuardarNumero()')   
                    ))
                ->add(
                    'Nuevo_numero', 'button' , array(
                    'label' => 'Cargar Excel',   
                    'attr' => array('class' => 'btn btn-success', 'onclick'=>'NuevoNumero()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_telefonica'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de telefonos.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelTelefonosForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('Enviar', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('lista_negra_telefonica'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    function createExcelReportExcelTel($listados, $titulo = 'Reporte de Carga Excel-Lista Negra Telefonica'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Telefono")
                ->setCellValue('B1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado['numero'])
                        ->setCellValue('B'.$numRow, $listado['mensaje'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * Lista Negra Direcciones
     *
     * @Route("/ListaDirecciones", name="lista_negra_direcciones")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:listaDirecciones.html.twig")
     */
    public function listaDireccionesAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();        
        $searchForm = $this->createConsultaDireccionesForm();
        $NewDirForm = $this->createNewFormDir();
        $excelDirForm = $this->createExcelDirForm();
        $entities = array();
        $respuesta = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $excelDirForm->submit($request);

            if ($searchForm->isValid())
            {
                $cp = $searchForm->get('cp')->getData();
                $municipio = $searchForm->get('Municipio')->getData();
                $colonia = $searchForm->get('Colonia')->getData();                  
                $entities = $em->getRepository('MDRPuntoVentaBundle:ListaNegraDirecciones')->findListaNegraDirecciones($cp,$municipio,$colonia);

                if(count($entities)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }
               
            }else if($excelDirForm->isValid()){

                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelLND");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $excelDirForm['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);
                


                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $newCP = $sheet->getCell('A'.$row)->getValue(); 
                        $newMun = $sheet->getCell('B'.$row)->getValue(); 
                        $newCol = $sheet->getCell('C'.$row)->getValue(); 
                       
                       

                                 $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraDirecciones')->findExistDir($newCP,$newMun,$newCol);

                                if(count($exist)){
                                    //\Doctrine\Common\Util\Debug::dump($exist);
                                        foreach ($exist as $ExistDir) {
                                            $ExistDir-> setActivo(true);
                                            $em->persist($ExistDir);
                                            $em->flush();
                                        } 
                                        $respuesta[] = array('Direccion' => $newCP." ".$newMun." ".$newCol , 'mensaje'=>'La Dirección ya existe, y a sido bloqueada');
                                }else{
                                                try{
                                                        $newDireccion = new ListaNegraDirecciones();
                                                        $newDireccion -> setCP($newCP);
                                                        $newDireccion -> setMunicipio($newMun);
                                                        $newDireccion -> setColonia($newCol);
                                                        $newDireccion -> setActivo(true);
                                                        $em->persist($newDireccion);
                                                        $em->flush();
                                                        $respuesta[] = array('Direccion' => $newCP." ".$newMun." ".$newCol , 'mensaje'=>'Se ha registrado con exito');
                                                }catch(\SoapFault $ex){

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA DIRECCIONES:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }catch(\Exception $ex){   

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA DIRECCIONES:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }
                                }

                    }
                    $row++;
                }

                return $this->createExcelReportExcelDir($respuesta);
            }    

            
        }
        
        return $this->render('MDRAdministracionBundle:Administracion:listaDirecciones.html.twig', array(
            'direcciones' => $entities,
            'form_lnd' => $searchForm->createView(),
            'form_newdir' => $NewDirForm->createView(),
            'form_exceldir' => $excelDirForm->createView(),
        ));


    }

    /**
    * Crear un formulario para ingreso de nuevos numeros telefonicos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createConsultaDireccionesForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('cp', null, array(
                    'label' => 'Codigo Postal',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 10),
                ))
                ->add('Municipio', null, array(
                    'label' => 'Municipio',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                )) 
                ->add('Colonia', null, array(
                    'label' => 'Colonia',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))  
                ->add(
                    'ConsultarDir', 'submit' , array(
                    'label' => 'Consultar', 
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'loadPedidos()')   
                    ))
                ->add(
                    'nuevaDir', 'button' , array(                        
                    'label' => 'Nueva Direccion',
                    'attr' => array('class' => 'btn btn-info','onclick' => 'popupNewDireccion()')   
                    ))
                ->add(
                    'ExcelDir', 'button' , array(
                    'label' => 'Carga Excel',                        
                    'attr' => array('class' => 'btn btn-success','onclick' => 'popupLoadExcel()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_direcciones'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


    /**
    * Crear un formulario para ingreso de nuevos numeros telefonicos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewFormDir()
    {
               
        $form = $this->createFormBuilder()
                ->add('newcp', null, array(
                    'label' => 'Codigo Postal',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10, 'onchange' => 'getInfo(1)'),
                ))
                ->add('newMunicipio', 'choice', array(
                    'label' => 'Municipio',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250, 'onchange' => 'getInfo(2)'),
                )) 
                ->add('newColonia', 'choice', array(
                    'label' => 'Colonia',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250, 'onchange' => 'getInfo(3,this)'),
                ))  
                ->add(
                    'Guardar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'newDir()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_direcciones'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de telefonos.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelDirForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('guardar_excel', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('lista_negra_direcciones'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    function createExcelReportExcelDir($listados, $titulo = 'Reporte de Carga Excel-Lista Negra Direcciones'){

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Direccion")
                ->setCellValue('B1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado['Direccion'])
                        ->setCellValue('B'.$numRow, $listado['mensaje'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * @Route("/setListaNEgraDirecciones", name="set_direccion_list")
     * @Template()
     */
    public function setListaNEgraDireccionesAction(Request $request)
    {
        $CP = $request->get('CP');
        $COL = $request->get('COL');
        $MUN = $request->get('MUN');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;

        $municipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($MUN);
        $newMun = $municipio -> getNombre();

        try{

            $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraDirecciones')->findExistDir($CP,$newMun,$COL);
            if(count($exist)>0){
                $Respuesta['mensaje'] = "La direccion ya existe";
            }else{

                $newDireccion = new ListaNegraDirecciones();
                $newDireccion -> setCP($CP);
                $newDireccion -> setMunicipio($newMun);
                $newDireccion -> setColonia($COL);
                $newDireccion -> setActivo(true);
                $em->persist($newDireccion);
                $em->flush();
                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Registrada con exito";
            }

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error REGISTRO MANUAL DE DIRECCION:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] ='Error REGISTRO MANUAL DE DIRECCION:';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error REGISTRO MANUAL DE DIRECCION:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] = 'Error REGISTRO MANUAL DE DIRECCION:';
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
    * @Route("/ADDireccion", name="_ActivarDireccion")
    */
    public function ActivarDireccion(Request $request)
    {
        $id = $request->get('idDireccion');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Direccion = $em->getRepository('MDRPuntoVentaBundle:ListaNegraDirecciones')->find($id);
            if(!$Direccion){
                $respuesta['mensaje'] = 'No se encontro la Dirección en la BD';    
            }else{
                $Direccion->setActivo($valor);
                $em->persist($Direccion);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo correctamente la Dirección '.$Direccion->getCP()."  ".$Direccion->getMunicipio()."  ".$Direccion->getColonia();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE DIRECCION LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE DIRECCION LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE DIRECCION LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE DIRECCION LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lista Negra Zona Riesgo
     *
     * @Route("/ListaZonaRiesgo", name="lista_negra_zona_riesgo")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:listaZonasRiesgo.html.twig")
     */
    public function listaZonaRiesgoAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();        
        $searchForm = $this->createConsultaZonasRiesgoForm();
        $NewZonaForm = $this->createNewFormZona();
        $excelZonaForm = $this->createExcelZonaForm();
        $entities = array();
        $respuesta = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $excelZonaForm->submit($request);

            if ($searchForm->isValid())
            {
                $cp = $searchForm->get('cp')->getData();
                $municipio = $searchForm->get('Municipio')->getData();             
                $entities = $em->getRepository('MDRPuntoVentaBundle:ZonasRiesgo')->findZonasRiesgo($cp,$municipio);

                if(count($entities)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }
               
            }else if($excelZonaForm->isValid()){

                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelLNZR");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $excelZonaForm['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);
                


                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $newCP = $sheet->getCell('A'.$row)->getValue(); 
                        $newMun = $sheet->getCell('B'.$row)->getValue();
                       
                       

                                 $exist = $em->getRepository('MDRPuntoVentaBundle:ZonasRiesgo')->findZonasRiesgo($newCP,$newMun);

                                if(count($exist)){
                                    //\Doctrine\Common\Util\Debug::dump($exist);
                                        foreach ($exist as $ExistZona) {
                                            $ExistZona-> setActivo(true);
                                            $em->persist($ExistZona);
                                            $em->flush();
                                        } 
                                        $respuesta[] = array('Zona' => $newCP." - ".$newMun , 'mensaje'=>'La Zona de Riesgo ya existe, y a sido bloqueada');
                                }else{
                                                try{
                                                        $newZonaRiesgo = new ZonasRiesgo();
                                                        $newZonaRiesgo -> setCP($newCP);
                                                        $newZonaRiesgo -> setMunicipio($newMun);
                                                        $newZonaRiesgo -> setActivo(true);
                                                        $em->persist($newZonaRiesgo);
                                                        $em->flush();
                                                        $respuesta[] = array('Zona' => $newCP." - ".$newMun , 'mensaje'=>'Se ha registrado con exito');
                                                }catch(\SoapFault $ex){

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA ZONA RIESGO:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }catch(\Exception $ex){   

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA ZONA RIESGO:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }
                                }

                    }
                    $row++;
                }

                return $this->createExcelReportExcelZona($respuesta);
            }    

            
        }
        
        return $this->render('MDRAdministracionBundle:Administracion:listaZonasRiesgo.html.twig', array(
            'zonasriesgo' => $entities,
            'form_lnzr' => $searchForm->createView(),
            'form_newzona' => $NewZonaForm->createView(),
            'form_excelzona' => $excelZonaForm->createView(),
        ));


    }

    /**
    * Crear un formulario para consulta de zonas de riesgo.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createConsultaZonasRiesgoForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('cp', null, array(
                    'label' => 'Codigo Postal',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 10),
                ))
                ->add('Municipio', null, array(
                    'label' => 'Municipio',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add(
                    'ConsultarZona', 'submit' , array(
                    'label' => 'Consultar', 
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'loadPedidos()')   
                    ))
                ->add(
                    'nuevaZona', 'button' , array(                        
                    'label' => 'Nueva Zona de Riesgo',
                    'attr' => array('class' => 'btn btn-info','onclick' => 'popupNewZona()')   
                    ))
                ->add(
                    'ExcelZona', 'button' , array(
                    'label' => 'Carga Excel',                        
                    'attr' => array('class' => 'btn btn-success','onclick' => 'popupLoadExcel()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_zona_riesgo'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


    /**
    * Crear un formulario para ingreso de nuevas zonas de riesgo.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewFormZona()
    {
               
        $form = $this->createFormBuilder()
                ->add('newcp', null, array(
                    'label' => 'Codigo Postal',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10, 'onchange' => 'getInfo(1)'),
                ))
                ->add('newMunicipio', 'choice', array(
                    'label' => 'Municipio',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add(
                    'Guardar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'newZona()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_zona_riesgo'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de zonas riesgo.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelZonaForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('guardar_excel', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('lista_negra_zona_riesgo'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    function createExcelReportExcelZona($listados, $titulo = 'Reporte de Carga Excel-Zonas de Riesgo'){

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Zona de Riesgo")
                ->setCellValue('B1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado['Zona'])
                        ->setCellValue('B'.$numRow, $listado['mensaje'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * @Route("/setZonaRiesgo", name="set_zona_riesgo")
     * @Template()
     */
    public function setZonaRiesgoAction(Request $request)
    {
        $CP = $request->get('CP');
        $MUN = $request->get('MUN');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;

        $municipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($MUN);
        $newMun = $municipio -> getNombre();

        try{

            $exist = $em->getRepository('MDRPuntoVentaBundle:ZonasRiesgo')->findZonasRiesgo($CP,$newMun);
            if(count($exist)>0){
                $Respuesta['mensaje'] = "La Zona de Riesgo ya existe";
            }else{

                $newZonaRiesgo = new ZonasRiesgo();
                $newZonaRiesgo -> setCP($CP);
                $newZonaRiesgo -> setMunicipio($newMun);
                $newZonaRiesgo -> setActivo(true);
                $em->persist($newZonaRiesgo);
                $em->flush();
                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Registrada con exito";
            }

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error REGISTRO MANUAL DE ZONA RIESGO:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] ='Error REGISTRO MANUAL DE ZONA RIESGO:';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error REGISTRO MANUAL DE ZONA RIESGO:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] = 'Error REGISTRO MANUAL DE ZONA RIESGO:';
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
    * @Route("/ADZonaRiesgo", name="_ActivarZonaRiesgo")
    */
    public function ActivarZonaRiesgo(Request $request)
    {
        $id = $request->get('idZona');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $ZonaRiesgo = $em->getRepository('MDRPuntoVentaBundle:ZonasRiesgo')->find($id);
            if(!$ZonaRiesgo){
                $respuesta['mensaje'] = 'No se encontro la ZONA DE RIESGO en la BD';    
            }else{
                $ZonaRiesgo->setActivo($valor);
                $em->persist($ZonaRiesgo);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo correctamente la ZONA DE RIESGO '.$ZonaRiesgo->getCP()."  ".$ZonaRiesgo->getMunicipio();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE ZONA RIESGO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE ZONA RIESGO LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE ZONA RIESGO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE ZONA RIESGO LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lista Negra Clientes
     *
     * @Route("/ListaClientes", name="lista_negra_clientes")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:listaClientes.html.twig")
     */
    public function listaClientesAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();        
        $searchForm = $this->createConsultaClientesForm();
        $NewClienteForm = $this->createNewFormCliente();
        $excelClienteForm = $this->createExcelClienteForm();
        $entities = array();
        $respuesta = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $excelClienteForm->submit($request);

            if ($searchForm->isValid())
            {
                $clnombre = $searchForm->get('nombre')->getData(); 
                $clApp = $searchForm->get('App')->getData(); 
                $clApm = $searchForm->get('Apm')->getData();  
                $entities = $em->getRepository('MDRPuntoVentaBundle:ListaNegraClientes')->findLNClientes($clnombre,$clApp,$clApm);

                if(count($entities)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }
               
            }else if($excelClienteForm->isValid()){

                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelLNCL");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $excelClienteForm['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);
                


                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $Nclnombre = $sheet->getCell('A'.$row)->getValue();
                        if(substr($Nclnombre, -1) == " "){
                            $Nclnombre = substr($Nclnombre, 0, - 1);
                        } 
                        $NclApp = $sheet->getCell('B'.$row)->getValue();
                        if(substr($NclApp, -1) == " "){
                            $NclApp = substr($NclApp, 0, - 1);
                        }
                        $NclApm = $sheet->getCell('C'.$row)->getValue();
                        if(substr($NclApm, -1) == " "){
                            $NclApm = substr($NclApm, 0, - 1);
                        }
                        $fullName = $Nclnombre." ".$NclApp." ".$NclApm;
                        $fullName = strtoupper($fullName);
                       
                       

                                 $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraClientes')->findTotalName($fullName);

                                if(count($exist)){
                                    //\Doctrine\Common\Util\Debug::dump($exist);
                                        foreach ($exist as $ExistCliente) {
                                            $ExistCliente-> setActivo(true);
                                            $em->persist($ExistCliente);
                                            $em->flush();
                                        } 
                                        $respuesta[] = array('cliente' => $fullName , 'mensaje'=>'El cliente ya existe, y a sido bloqueado');
                                }else{
                                                try{
                                                        $newCliente = new ListaNegraClientes();
                                                        $newCliente -> setNombreCompleto($fullName);
                                                        $newCliente -> setActivo(true);
                                                        $em->persist($newCliente);
                                                        $em->flush();
                                                        $respuesta[] = array('cliente' => $fullName , 'mensaje'=>'Se ha registrado con exito');
                                                }catch(\SoapFault $ex){

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA CLIENTES:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }catch(\Exception $ex){   

                                                    $this->get('logger')->error('Error EXCEL LISTA NEGRA CLIENTES:');
                                                    $this->get('logger')->error($ex->getMessage());

                                                }
                                }

                    }
                    $row++;
                }

                return $this->createExcelReportExcelClientes($respuesta);
            }    

            
        }
        
        return $this->render('MDRAdministracionBundle:Administracion:listaClientes.html.twig', array(
            'clientes' => $entities,
            'form_lncl' => $searchForm->createView(),
            'form_newcliente' => $NewClienteForm->createView(),
            'form_excelcliente' => $excelClienteForm->createView(),
        ));




    }

    /**
    * Crear un formulario para consulta de clientes.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createConsultaClientesForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('nombre', null, array(
                    'label' => 'Nombre(s)',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 100,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add('App', null, array(
                    'label' => 'Apellido Paterno',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 50,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add('Apm', null, array(
                    'label' => 'Apellido Materno',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 50,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add(
                    'ConsultarCliente', 'submit' , array(
                    'label' => 'Consultar', 
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'loadClientes()')   
                    ))
                ->add(
                    'nuevoCliente', 'button' , array(                        
                    'label' => 'Nuevo Cliente',
                    'attr' => array('class' => 'btn btn-info','onclick' => 'popupNewCliente()')   
                    ))
                ->add(
                    'ExcelCliente', 'button' , array(
                    'label' => 'Carga Excel',                        
                    'attr' => array('class' => 'btn btn-success','onclick' => 'popupLoadExcel()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_clientes'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


    /**
    * Crear un formulario para ingreso de nuevos clientes.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewFormCliente()
    {
               
        $form = $this->createFormBuilder()
                ->add('newnombre', null, array(
                    'label' => 'Nombre(s)',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 100,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add('newApp', null, array(
                    'label' => 'Apellido Paterno',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 50,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add('newApm', null, array(
                    'label' => 'Apellido Materno',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 50,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
                ->add(
                    'Guardar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'newCliente()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_clientes'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de clientes.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelClienteForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('guardar_excel', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('lista_negra_clientes'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }

    function createExcelReportExcelClientes($listados, $titulo = 'Reporte de Carga Excel-Clientes'){

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Cliente")
                ->setCellValue('B1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado['cliente'])
                        ->setCellValue('B'.$numRow, $listado['mensaje'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * @Route("/setlnCliente", name="set_cliente_ln")
     * @Template()
     */
    public function setlnClienteAction(Request $request)
    {
        $fullName = $request->get('cliente');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;


        try{

            $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraClientes')->findTotalName($fullName);
            if(count($exist)>0){
                $Respuesta['mensaje'] = "El cliente ya existe";
            }else{

                $newClienteln = new ListaNegraClientes();
                $newClienteln -> setNombreCompleto($fullName);
                $newClienteln -> setActivo(true);
                $em->persist($newClienteln);
                $em->flush();
                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Registrado con exito";
            }

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error REGISTRO MANUAL DE CLIENTES:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] ='Error REGISTRO MANUAL DE CLIENTES:';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error REGISTRO MANUAL DE CLIENTES:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] = 'Error REGISTRO MANUAL DE CLIENTES:';
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }


    /**
    * @Route("/ADClientes", name="_ActivarCliente")
    */
    public function ActivarCliente(Request $request)
    {
        $id = $request->get('idCliente');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Clientes = $em->getRepository('MDRPuntoVentaBundle:ListaNegraClientes')->find($id);
            if(!$Clientes){
                $respuesta['mensaje'] = 'No se encontro el Cliente en la BD';    
            }else{
                $Clientes->setActivo($valor);
                $em->persist($Clientes);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo correctamente el Cliente '.$Clientes->getNombreCompleto();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE CLIENTE LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE CLIENTE LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE CLIENTE LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE CLIENTE LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lista Negra Montos
     *
     * @Route("/ListaMontos", name="lista_negra_montos")
     * @Method({"GET","POST"})
     * @Template("MDRAdministracionBundle:Administracion:listaMontos.html.twig")
     */
    public function listaMontosAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();        
        $searchForm = $this->createConsultaMontosForm();
        $NewMontoForm = $this->createNewFormMonto();
        $entities = array();
        $respuesta = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);

            if ($searchForm->isValid())
            {
                $producto = $searchForm->get('Producto')->getData();
                $clvproducto = ""; 
                if($producto){
                    $clvproducto = $em->getRepository('MDRPuntoVentaBundle:Productos')->find($producto);
                    $clvproducto = $clvproducto -> getCodigoSAP();
                }
                //\Doctrine\Common\Util\Debug::dump($clvproducto); 
                $entities = $em->getRepository('MDRPuntoVentaBundle:ListaNegraMontos')->findExistProducto($clvproducto);

                if(count($entities)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }
               
            }

            
        }
        
        return $this->render('MDRAdministracionBundle:Administracion:listaMontos.html.twig', array(
            'productos' => $entities,
            'form_lnm' => $searchForm->createView(),
            'form_newmonto' => $NewMontoForm->createView(),
        ));
    }       



    /**
    * Crear un formulario para consulta de productos por pedido.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createConsultaMontosForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('Producto', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Productos',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('class' => "form-control"),
                ))
                ->add(
                    'ConsultarMontos', 'submit' , array(
                    'label' => 'Consultar', 
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'loadMontos()')   
                    ))
                ->add(
                    'nuevoMonto', 'button' , array(                        
                    'label' => 'Nueva Regla de Monto',
                    'attr' => array('class' => 'btn btn-info','onclick' => 'popupNewMonto()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_montos'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


    /**
    * Crear un formulario para ingreso de nuevos productos por pedido.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewFormMonto()
    {
               
        $form = $this->createFormBuilder()
                ->add('newProducto', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Productos',
                    'required' => true,
                    'empty_value' => "-----------",
                    'attr' => array('class' => "form-control"),
                ))
                ->add('newCantidad', null , array(
                    'label' => 'Cantidad',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10,'onkeypress' => 'return solonum(event)'),
                ))
                ->add(
                    'Guardar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary','onclick' => 'newMonto()')   
                    ))
                ->setAction($this->generateUrl('lista_negra_montos'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * @Route("/setlnProducto", name="set_ln_prdt")
     * @Template()
     */
    public function setlnProductoAction(Request $request)
    {
        $producto = $request->get('idProducto');
        $cantidad = $request->get('cantidad');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;


        try{
                    $AProducto = $em->getRepository('MDRPuntoVentaBundle:Productos')->find($producto);
                    $clvproducto = $AProducto -> getCodigoSAP();
                    $nameproducto = $AProducto -> getDescripcion();

            $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraMontos')->findExistProducto($clvproducto);

            if(count($exist)>0){
                $Respuesta['mensaje'] = "La regla ya existe ya existe";
            }else{

                $newProductoln = new ListaNegraMontos();
                $newProductoln -> setNombreProducto($nameproducto);
                $newProductoln -> setMonto($cantidad);
                $newProductoln -> setClaveProducto($clvproducto);
                $newProductoln -> setActivo(true);
                $em->persist($newProductoln);
                $em->flush();
                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Registrado con exito";
            }

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error REGISTRO MANUAL DE REGLAS DE PRODUCTOS:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] ='Error REGISTRO MANUAL DE REGLAS DE PRODUCTOS:';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error REGISTRO MANUAL DE REGLAS DE PRODUCTOS:');
                $this->get('logger')->error($ex->getMessage());
                $Respuesta['mensaje'] = 'Error REGISTRO MANUAL DE REGLAS DE PRODUCTOS:';
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
    * @Route("/ADReglaProducto", name="_ActivarReglaProducto")
    */
    public function ActivarReglaProducto(Request $request)
    {
        $id = $request->get('idMonto');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Producto = $em->getRepository('MDRPuntoVentaBundle:ListaNegraMontos')->find($id);
            if(!$Producto){
                $respuesta['mensaje'] = 'No se encontro el Producto  en la BD';    
            }else{
                $Producto->setActivo($valor);
                $em->persist($Producto);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo correctamente el Producto '.$Producto->getNombreProducto();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA PRODUCTO LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA PRODUCTO LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/getInfoReglaProducto", name="_getReglaProducto")
    */
    public function getInfoReglaProducto(Request $request)
    {
        $id = $request->get('idProducto');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Producto = $em->getRepository('MDRPuntoVentaBundle:ListaNegraMontos')->find($id);
            $respuesta['estado'] = 1;
            $respuesta['producto'] = $Producto;
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error OBTENER INFORMACIÓN DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error OBTENER INFORMACIÓN DE REGLA PRODUCTO LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error OBTENER INFORMACIÓN DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error OBTENER INFORMACIÓN DE REGLA PRODUCTO LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



    /**
    * @Route("/updateReglaProducto", name="_UpdateReglaProducto")
    */
    public function updateReglaProducto(Request $request)
    {
        $id = $request->get('idRegla');
        $cantidad = $request->get('cantidad');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Producto = $em->getRepository('MDRPuntoVentaBundle:ListaNegraMontos')->find($id);
            if(count($Producto)>0){

                $Producto->setMonto($cantidad);
                $em->persist($Producto);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo correctamente el Producto '.$Producto->getNombreProducto();

            }else{
               $respuesta['mensaje'] = 'No se encontro el Producto  en la BD';  
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTUALIZACION DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACION DE REGLA PRODUCTO LN';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTUALIZACION DE REGLA PRODUCTO LN:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACION DE REGLA PRODUCTO LN';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/ParametrosCRM", name="_ActivarParametroCRM")
    */
    public function ParametrosCRM(Request $request)
    {
        $id = $request->get('idParametro');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Regla = $em->getRepository('MDRPuntoVentaBundle:Parametros')->find($id);
            if(!$Regla){
                $respuesta['mensaje'] = 'No se encontro la regla';    
            }else{
                $Regla->setValor($valor);
                $em->persist($Regla);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo la regla: '.$Regla->getDescripcion();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA:';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA:';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


}