<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Parametros;
use MDR\PuntoVentaBundle\Form\ParametrosType;

/**
 * Parametros controller.
 *
 * @Route("/parametros")
 */
class ParametrosController extends Controller
{

    /**
     * Lists all Parametros entities.
     *
     * @Route("/", name="parametros")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Parametros')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Parametros entity.
     *
     * @Route("/", name="parametros_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Parametros:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Parametros();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('parametros_show', array('id' => $entity->getIdParametro())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Parametros entity.
     *
     * @param Parametros $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Parametros $entity)
    {
        $form = $this->createForm(new ParametrosType(), $entity, array(
            'action' => $this->generateUrl('parametros_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Parametros entity.
     *
     * @Route("/new", name="parametros_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Parametros();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Parametros entity.
     *
     * @Route("/{id}", name="parametros_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Parametros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Parametros entity.
     *
     * @Route("/{id}/edit", name="parametros_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Parametros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Parametros entity.
    *
    * @param Parametros $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Parametros $entity)
    {
        $form = $this->createForm(new ParametrosType(), $entity, array(
            'action' => $this->generateUrl('parametros_update', array('id' => $entity->getIdParametro())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Parametros entity.
     *
     * @Route("/{id}", name="parametros_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Parametros:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Parametros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Parametros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('parametros_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Parametros entity.
     *
     * @Route("/{id}", name="parametros_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Parametros')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Parametros entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('parametros'));
    }

    /**
     * Creates a form to delete a Parametros entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('parametros_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
