<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\TipoLlamada;
use MDR\PuntoVentaBundle\Form\TipoLlamadaType;

/**
 * TipoLlamada controller.
 *
 * @Route("/tipollamada")
 */
class TipoLlamadaController extends Controller
{

    /**
     * Lists all TipoLlamada entities.
     *
     * @Route("/", name="tipollamada")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->findAll();
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoLlamada entity.
     *
     * @Route("/", name="tipollamada_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:TipoLlamada:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoLlamada();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipollamada_show', array('id' => $entity->getIdTipoLlamada())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a TipoLlamada entity.
     *
     * @param TipoLlamada $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoLlamada $entity)
    {
        $form = $this->createForm(new TipoLlamadaType(), $entity, array(
            'action' => $this->generateUrl('tipollamada_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoLlamada entity.
     *
     * @Route("/new", name="tipollamada_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoLlamada();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoLlamada entity.
     *
     * @Route("/{id}", name="tipollamada_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLlamada entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoLlamada entity.
     *
     * @Route("/{id}/edit", name="tipollamada_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLlamada entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoLlamada entity.
    *
    * @param TipoLlamada $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoLlamada $entity)
    {
        $form = $this->createForm(new TipoLlamadaType(), $entity, array(
            'action' => $this->generateUrl('tipollamada_update', array('id' => $entity->getIdTipoLlamada())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing TipoLlamada entity.
     *
     * @Route("/{id}", name="tipollamada_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:TipoLlamada:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoLlamada entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipollamada_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoLlamada entity.
     *
     * @Route("/{id}", name="tipollamada_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoLlamada entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipollamada'));
    }

    /**
     * Creates a form to delete a TipoLlamada entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipollamada_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top: -54px;','class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
