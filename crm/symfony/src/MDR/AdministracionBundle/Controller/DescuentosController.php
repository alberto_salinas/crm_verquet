<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Descuentos;
use MDR\PuntoVentaBundle\Entity\DescuentosOrigenVentas;
use MDR\PuntoVentaBundle\Form\DescuentosType;

/**
 * Descuentos controller.
 *
 * @Route("/descuentos")
 */
class DescuentosController extends Controller
{

    /**
     * Lists all Descuentos entities.
     *
     * @Route("/", name="descuentos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->findAll();
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
			
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Descuentos entity.
     *
     * @Route("/origen/{id}", name="descuentos_origen_create")
     * @Method("POST")
     */
    public function createOrigenAction(Request $request, $id)
    {
        $entity = new Descuentos();
        $form = $this->createCreateOrigenForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $oficinaVentas = $form->get('oficinaVentas')->getData();
            $descuentoOrigenVentas = $em->getRepository('MDRPuntoVentaBundle:DescuentosOrigenVentas')->findOneBy(['oficinaVentas' => $oficinaVentas, 'descuento' => $id]);
            if($descuentoOrigenVentas)
            {
                $this->get('session')->getFlashBag()->add(
                    'warning', sprintf('La Oficina de Ventas: %s ya tiene el descuento %s.', $oficinaVentas->getDescripcion(), $entity->getDescuento()->getClave())
                );
            }
            else
            {
                $entity = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);
                $canalDistribucion = $em->getRepository('MDRPuntoVentaBundle:CanalDistribucion')->find('20');
                $organizacionVentas = $em->getRepository('MDRPuntoVentaBundle:OrganizacionVentas')->find('0100');

                $descuentoOrigenVentas = new DescuentosOrigenVentas();
                $descuentoOrigenVentas->setDescuento($entity);
                $descuentoOrigenVentas->setOrganizacionVentas($organizacionVentas);
                $descuentoOrigenVentas->setCanalDistribucion($canalDistribucion);
                $descuentoOrigenVentas->setOficinaVentas($oficinaVentas);

                $entity->addOrigen($descuentoOrigenVentas);

                $em->persist($descuentoOrigenVentas);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success', sprintf('Se agrego la Oficina de Ventas: %s al descuento %s.', $oficinaVentas->getDescripcion(), $entity->getClave())
                );
            }
        }

        return $this->redirect($this->generateUrl('descuentos_edit',  ['id' => $id]));
    }

    /**
     * Creates a new Descuentos entity.
     *
     * @Route("/", name="descuentos_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Descuentos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Descuentos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			if($entity->getActivo() == NULL){
				$entity->setActivo(true); 
			}
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('descuentos_show', array('id' => $entity->getidDescuento())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Descuentos entity.
     *
     * @param Descuentos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Descuentos $entity)
    {
        $form = $this->createForm(new DescuentosType(), $entity, array(
            'action' => $this->generateUrl('descuentos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Descuentos entity.
     *
     * @Route("/new", name="descuentos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Descuentos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
		var_dump($tipoPago);
    }

    /**
     * Finds and displays a Descuentos entity.
     *
     * @Route("/{id}", name="descuentos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Descuentos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Descuentos entity.
     *
     * @Route("/{id}/edit", name="descuentos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Descuentos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        $addOficinaForm = $this->createCreateOrigenForm($id);

        foreach ($entity->getOrigenes() as $key => $origen) {
            $origen->deleteForm = $this->createDeleteOrigenForm($entity->getIdDescuento(), $origen->getOficinaVentas()->getClaveSAP())->createView();
        }

        return array(
            'entity'            => $entity,
            'edit_form'         => $editForm->createView(),
            'delete_form'       => $deleteForm->createView(),
            'addOficinaForm'    => $addOficinaForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Descuentos entity.
    *
    * @param Descuentos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Descuentos $entity)
    {
        $form = $this->createForm(new DescuentosType(), $entity, array(
            'action' => $this->generateUrl('descuentos_update', array('id' => $entity->getidDescuento())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Descuentos entity.
     *
     * @Route("/{id}", name="descuentos_update")
     * @Method("PUT")
     * @Template("MDRAdministracionBundle:Descuentos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
		//accedemos a todas las propiedades de la entidad atravez de $entity
        $entity = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);
		
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Descuentos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
		
        if ($editForm->isValid()) {
			//accedemos a las propiedades que requerimos de la entidad
			$existencias = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->findByClaveTipoPago($entity->getClave(), $entity->getIdDescuento(), $entity->getTipoPago());
			//condicionamos la imprecion del mensage deacuerdo al resultado
			if(count($existencias)==1){
				$this->get('session')->getFlashBag()->add(
					'error', 'La clave '.$entity->getClave().' ya existe con el tipo de pago '.$entity->getTipoPago()
				);	
			}
			else{
				$em->flush();			
				return $this->redirect($this->generateUrl('descuentos_edit', array('id' => $id)));
				}
		}

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Descuentos entity.
     *
     * @Route("/origen/{id}/{idOficinaVentas}", name="descuentos_origen_delete")
     * @Method("DELETE")
     */
    public function deleteOrigenAction(Request $request, $id, $idOficinaVentas)
    {
        $form = $this->createDeleteOrigenForm($id, $idOficinaVentas);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:DescuentosOrigenVentas')->findOneBy(['oficinaVentas' => $idOficinaVentas, 'descuento' => $id]);

            if (!$entity) {
                throw $this->createNotFoundException(sprintf('No se encontro la Oficina de Ventas %s con descuento %s', $idOficinaVentas, $id));
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success', sprintf('Se quito la Oficina de Ventas: %s del descuento %s.', $entity->getOficinaVentas()->getDescripcion(), $entity->getDescuento()->getClave())
            );
        }

        return $this->redirect($this->generateUrl('descuentos_edit',  ['id' => $id]));
    }

    /**
     * Deletes a Descuentos entity.
     *
     * @Route("/{id}", name="descuentos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Descuentos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('descuentos'));
    }

    /**
     * Creates a form to delete a Descuentos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('descuentos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }

    /**
     * Creates a form to delete a DescuentosOrigen entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateOrigenForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('descuentos_origen_create', array('id' => $id)))
            ->setMethod('POST')
            ->add('oficinaVentas', 'entity', array(
                'class'         => 'MDRPuntoVentaBundle:OficinasVentas',
                'label'         => 'Oficina de Ventas',
                'empty_value'   => '-----------',
                'required'      => true,
                'attr' => array('class' => "form-control"),
            ))
            ->add('submit', 'submit', array('label' => 'Agregar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }

    /**
     * Creates a form to delete a DescuentosOrigen entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteOrigenForm($id, $idOficinaVenta)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('descuentos_origen_delete', array('id' => $id, 'idOficinaVentas' => $idOficinaVenta)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }
}
