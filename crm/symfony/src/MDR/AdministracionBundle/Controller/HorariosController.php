<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Horarios;
use MDR\PuntoVentaBundle\Form\HorariosType;

/**
 * Horarios controller.
 *
 * @Route("/horarios")
 */
class HorariosController extends Controller
{

    /**
     * Lists all Horarios entities.
     *
     * @Route("/", name="horarios")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Horarios')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Horarios entity.
     *
     * @Route("/", name="horarios_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Horarios:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Horarios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity -> setActivo(1);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('horarios_show', array('id' => $entity->getidhorario())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Horarios entity.
     *
     * @param Horarios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Horarios $entity)
    {
        $form = $this->createForm(new HorariosType(), $entity, array(
            'action' => $this->generateUrl('horarios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Horarios entity.
     *
     * @Route("/new", name="horarios_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Horarios();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Horarios entity.
     *
     * @Route("/{id}", name="horarios_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Horarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Horarios entity.
     *
     * @Route("/{id}/edit", name="horarios_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Horarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horarios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Horarios entity.
    *
    * @param Horarios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Horarios $entity)
    {
        $form = $this->createForm(new HorariosType(), $entity, array(
            'action' => $this->generateUrl('horarios_update', array('id' => $entity->getidhorario())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Horarios entity.
     *
     * @Route("/{id}", name="horarios_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Horarios:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Horarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Horarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('horarios_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Horarios entity.
     *
     * @Route("/{id}", name="horarios_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Horarios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Horarios entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('horarios'));
    }

    /**
     * Creates a form to delete a Horarios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('horarios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Borrar',                
                'attr' => array('class' => 'btn btn-danger')   
                ))
            ->getForm()
        ;
    }
}
