<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

use MDR\PuntoVentaBundle\Entity\CoberturaEnvio;
use MDR\PuntoVentaBundle\Entity\TipoPagos;
use MDR\PuntoVentaBundle\Form\CoberturaEnvioType;


/**
 * CoberturaEnvio controller.
 *
 * @Route("/coberturaenvio")
 */
class CoberturaEnvioController extends Controller
{

    /**
     * Lists all CoberturaEnvio entities.
     *
     * @Route("/", name="coberturaenvio")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); 
        $form = $this->createSearchForm();
        $excelForm = $this->createExcelForm();
		$entities = array();
        $respuesta = array();


		if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
            $excelForm->handleRequest($request);

            if ($form->isValid()) {
				$arrayQ=array();
				
				if($form['codigoPostal']->getData() != null){
					$arrayQ['cP']= $form['codigoPostal']->getData();
				}
                $entities = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findBy($arrayQ);
		
			     if(count($entities)<=0){
					$this->get('session')->getFlashBag()->add(
						'error',  $this->get('translator')->trans('busquedaSinResultados')
					);
				}
			
		    }else if($excelForm->isValid()){
                set_time_limit(10000);
                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM-ExcelCoberturasEnvio");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $excelForm['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);

                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $Tpago = trim($sheet->getCell('A'.$row)->getValue()); 
                        $CP = trim((string)$sheet->getCell('B'.$row)->getValue());                         
                        $Minimo = $sheet->getCell('C'.$row)->getValue(); 
                        $Maximo = $sheet->getCell('D'.$row)->getValue();
                        //Validar el tipo de pago
                        $tipoPago  = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findTipoPagoDescripcion($Tpago);
                        //\Doctrine\Common\Util\Debug::dump($tipoPago);
                        if(count($tipoPago)>0){
                            $pago =  $tipoPago[0]->getIdTipoPago();
                            //Validar que exista el CP
                            $ExistCP = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($CP); 
                                if(count($ExistCP)>0){
                                        $exist = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findExisteCobertura($pago,$CP);
                                        if(count($exist)>0){
                                            
                                             foreach ($exist as $CoberturasEnvio) {
                                                    $CoberturasEnvio-> setTiempoEntregaMin($Minimo);
                                                    $CoberturasEnvio-> setTiempoEntregaMax($Maximo);
                                                    $em->persist($CoberturasEnvio);
                                                    $em->flush();
                                                } 
                                            $respuesta[] = array('CP' => $CP,'Pago' => $Tpago,'Minimo' => $Minimo,'Maximo' => $Maximo, 'Observaciones'=>'Este registro ya existe, se actualizaron los tiempos de entrega Maximos y Minimos');
                                                
                                        }else{
                                                $CoberturasEnvio = new CoberturaEnvio();
                                                $CoberturasEnvio -> setCP($CP);
                                                $CoberturasEnvio -> setTipoPago($tipoPago[0]);
                                                $CoberturasEnvio -> setIdMensajeria(1);
                                                $CoberturasEnvio -> setTiempoEntregaMin($Minimo);
                                                $CoberturasEnvio -> setTiempoEntregaMax($Maximo);
                                                $em->persist($CoberturasEnvio);
                                                $em->flush();
                                                $respuesta[] = array('CP' => $CP,'Pago' => $Tpago,'Minimo' => $Minimo,'Maximo' => $Maximo, 'Observaciones'=>'Se registro con exito');
                                        }
                                }else{
                                    $respuesta[] = array('CP' => $CP,'Pago' => $Tpago,'Minimo' => $Minimo,'Maximo' => $Maximo, 'Observaciones'=>'El CP no existe en la Base de Datos');
                                }    
                        }else{
                                    $respuesta[] = array('CP' => $CP,'Pago' => $Tpago,'Minimo' => $Minimo,'Maximo' => $Maximo, 'Observaciones'=>'El tipo de pago se ingreso de forma incorrecta, recuerde que los tipos de pagos validos son COD y TDC');
                        }        

                               

                    }
                    $row++;
                }

                return $this->createExcelReportExcelCoberturas($respuesta);
                
            
            }
		}


        return array(
            'entities' => $entities,
			'form' => $form -> createView(),
            'Excelform' => $excelForm -> createView(),
        );
    }

    /**
     * Creates a new CoberturaEnvio entity.
     *
     * @Route("/create", name="coberturaenvio_create")
     * @Method("POST")
     * @Template("MDRAdministracionBundle:CoberturaEnvio:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CoberturaEnvio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager(); 

        if ($form->isValid()) {
            $pago = $entity->getTipoPago()->getIdTipoPago();
            $CP   = $entity->getCP();
            $ExistCP = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($CP); 
            if(count($ExistCP)>0){
                $exist = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findExisteCobertura($pago,$CP);
                if(count($exist)>0){
                    $this->get('session')->getFlashBag()->add(
                            'error',  "El registro que intenta realizar ya existe"
                        );
                }else{
                               
                    $entity->setIdMensajeria(1);
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('coberturaenvio_show', array('id' => $entity->getIdCobertura())));
                }
            }else{
                $this->get('session')->getFlashBag()->add(
                    'error',  "El codigo postal que intenta registrar <b>NO EXISTE EN LA BASE DE DATOS</b>"
                );
            }    
                   
                
            

            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CoberturaEnvio entity.
     *
     * @param CoberturaEnvio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CoberturaEnvio $entity)
    {
        $form = $this->createForm(new CoberturaEnvioType(), $entity, array(
            'action' => $this->generateUrl('coberturaenvio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    function createExcelReportExcelCoberturas($listados, $titulo = 'Reporte_Carga_Coberturas_de_Envio'){
        set_time_limit(10000);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Tipo de Pago")
                ->setCellValue('B1', "CP")
                ->setCellValue('C1', "tiempoEntregaMin")
                ->setCellValue('D1', "tiempoEntregaMax")
                ->setCellValue('E1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado['Pago'])
                        ->setCellValueExplicit('B'.$numRow, $listado['CP'], \PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue('C'.$numRow, $listado['Minimo'])
                        ->setCellValue('D'.$numRow, $listado['Maximo'])
                        ->setCellValue('E'.$numRow, $listado['Observaciones'])
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }


    /**
     * Displays a form to create a new CoberturaEnvio entity.
     *
     * @Route("/new", name="coberturaenvio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CoberturaEnvio();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CoberturaEnvio entity.
     *
     * @Route("/{id}", name="coberturaenvio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CoberturaEnvio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CoberturaEnvio entity.
     *
     * @Route("/{id}/edit", name="coberturaenvio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CoberturaEnvio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CoberturaEnvio entity.
    *
    * @param CoberturaEnvio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CoberturaEnvio $entity)
    {
        $form = $this->createForm(new CoberturaEnvioType(), $entity, array(
            'action' => $this->generateUrl('coberturaenvio_update', array('id' => $entity->getIdCobertura())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing CoberturaEnvio entity.
     *
     * @Route("/{id}", name="coberturaenvio_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:CoberturaEnvio:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CoberturaEnvio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('coberturaenvio_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CoberturaEnvio entity.
     *
     * @Route("/{id}", name="coberturaenvio_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CoberturaEnvio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('coberturaenvio'));
    }

    /**
     * Creates a form to delete a CoberturaEnvio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('coberturaenvio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }
	
	/**
     * Crear formulario para crear combos de seleccion .
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSearchForm()
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()

            ->add('codigoPostal', 'text', array(
                //'class'         => 'MDRPuntoVentaBundle:CoberturaEnvio',
                'label'         => 'Codigo postal',
               // 'empty_value'   => '-----------',
                'required'      => true,
                'attr' => array('class' => "form-control"),
            ))
			            
						
			->add('Enviar', 'submit', array(
                'label'     => 'Buscar',
				'attr' => array('class' => "btn btn-primary"),
                ))

            ->add('Excel', 'button', array(
                'label'     => 'Cargar Excel',
                'attr' => array('class' => "btn btn-success",'onclick' => 'showExcelForm()'),
                )) 

            ->getForm();

        return $form;
    }

    /**
     * Crear formulario para seleccionar Excel para cargar lista negra de telefonos.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createExcelForm(){

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                'attr' => array('class' => 'form-control')
                ))
            ->add('enviar_excel', 'submit', array(
                'label'     => 'Guardar',
                'attr' => array('class' => 'btn btn-success')
                ))
            ->setAction($this->generateUrl('coberturaenvio'))
            ->setMethod('POST')
            ->getForm();

        return $form;
    }
	
}
