<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use MDR\PuntoVentaBundle\Entity\Usuario;
use MDR\PuntoVentaBundle\Entity\DireccionesUsuarios;
use MDR\PuntoVentaBundle\Form\UsuarioType;
use MDR\PuntoVentaBundle\Form\DireccionesUsuariosType;

/**
 * Usuario controller.
 *
 * @Route("/usuario")
 */
class UsuarioController extends Controller
{

    /**
     * Lists all Usuario entities.
     *
     * @Route("/", name="usuario")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createSearchForm();
        $formDireccion = $this->createForm(new DireccionesUsuariosType());
        $entities = array();
        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
             if ($form->isValid()) {
                $arrayQ=array();
                $arrayQ['activo']= $form['activo']->getData();
        $entities = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findBy($arrayQ);

             if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }       
        
             }
        }
        return array(
            'entities'      => $entities,
            'form'          => $form -> createView(),
            'formDireccion' => $formDireccion->createView(),
        );
    }
    /**
     * Creates a new Usuario entity.
     *
     * @Route("/create", name="usuario_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Usuario:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Usuario();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
                                          
                        
                        $usuario = $entity->getUClave();
                        $existUser = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findByUsuario($usuario);
                        $Avatar = $em->getRepository('MDRPuntoVentaBundle:IntranetAvatar')->find(31);
                        if(count($existUser)>0){
                                    $this->get('session')->getFlashBag()->add(
                                            'error', 'El usuario ya existe.'
                                        );           
                            return $this->redirect($this->generateUrl('usuario_new'));
                        }else{
                             if ($form->isValid()) { 

                            $defaultPass = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_INIT_PASS_');

                            $entity->setUPassword(md5(strtoupper($defaultPass)));
                            $entity->setUfechaalta(new \DateTime());
                            $entity->setActivo(1);
                            $entity->setTemaIntranet('skin-blue');
                            $entity->setAvatarIntranet($Avatar);
                            $user = $this->get('security.context')->getToken()->getUser();
                            $entity->setUsuarioModificacion($user);
                            $em->persist($entity);
                            $em->flush();
                            return $this->redirect($this->generateUrl('usuario_show', array('id' => $entity->getId())));
                            } 
                        }  


            return array(
            'entities' => $entities,
            'form' => $form -> createView(),
                );            
    }

    /**
     * Creates a form to create a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuario $entity)
    {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     * @Route("/new", name="usuario_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Usuario();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to change Usuario password.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createChangePassForm()
    {
        $form = $this->createFormBuilder()
        ->add('password', 'repeated', array(
            'type'              => 'password',
            'invalid_message'   => 'Las contraseñas no coinciden',
            'required'          => true,
            'first_options'     => array('label' => 'Contraseña'),
            'second_options'    => array('label' => 'Repetir contraseña'),
        ))
        ->add('Guardar', 'submit')
        
        ->getForm();

        return $form;
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     * @Route("/changePassword", name="usuario_change_pass")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function changePasswordAction(Request $request)
    {
        $searchForm   = $this->createChangePassForm();

        if($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if($searchForm->isValid())
            {
                $user = $this->get('security.context')->getToken()->getUser();
                $user->setUPassword(md5(strtoupper($searchForm->get('password')->getData())));

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success', 'La contraseña se actualizó correctamente.'
                );
            }
        }

        return array(
            'passForm'   => $searchForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     * @Route("/resetPassword/{id}", name="usuario_reset_pass")
     * @Method("GET")
     * @Template()
     */
    public function resetPasswordAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);
        if(!$entity)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'No existe el usuario.'
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }
        $defaultPass = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_INIT_PASS_');
        $entity->setUPassword(md5(strtoupper($defaultPass)));
        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'success', 'Contraseña se ha restablecido: '.$defaultPass.'.'
        );
        return $this->redirect($this->generateUrl('usuario_show', array('id' => $entity->getId())));
    }

    /**
     * Finds and displays a Usuario entity.
     *
     * @Route("/{id}", name="usuario_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }
        $defaultPass = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_INIT_PASS_');

        return array(
            'entity'        => $entity,
            'defaultPass'   => $defaultPass,
        );
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     * @Route("/{id}/edit", name="usuario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Usuario entity.
    *
    * @param Usuario $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usuario $entity)
    {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('usuario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Usuario entity.
     *
     * @Route("/{id}", name="usuario_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Usuario:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }
        $entity->setUfechamodificacion(new \DateTime("now"));
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('usuario_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }
    
    /**
     * Crear formulario para crear combos de seleccion .
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSearchForm()
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            
           ->add('activo', 'choice', array(
                'choices'   => array('1' => 'Activos', '0' => 'Inactivos'),
                'label'         => 'Estado',
                'empty_value'   => '-----------',
                'required'  => true,
                'attr' => array('class' => "form-control"),
            ))
            
            ->add('Enviar', 'submit', array(
                'label'     => 'Buscar',
                'attr' => array('class' => "btn btn-primary"),
                ))
            ->getForm();

        return $form;
    }

    /**
     * @Route("/dirUserJSON", name="dir_user_json")
     * @Template()
     */
    public function getdirUserJSONAction(Request $request)
    {
        $usuario = $request->get('idUser');
        $em = $this->getDoctrine()->getManager();
        $Direcciones = array();
        $Direcciones['estatus'] = 0;
        $Direcciones['Dir'] = '';

        $entities = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->findByUsuario($usuario);
        
        if(count($entities)>0){
            $Direcciones['estatus'] = 1;
            $Direcciones['Dir'] = $entities;
        } 


        $response = new Response(json_encode($Direcciones));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/UserMunJSON", name="user_mun_json")
     * @Template()
     */
    public function UserMunJSONAction(Request $request)
    {
        $edo = $request->get('edo');
        $em = $this->getDoctrine()->getManager();
        $Municipios = array();
        $Municipios['estatus'] = 0;
        $Municipios['municipios'] = '';

        $entities = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findByEstado($edo);
        
        if(count($entities)>0){
            $Municipios['estatus'] = 1;
            $Municipios['municipios'] = $entities;
        } 


        $response = new Response(json_encode($Municipios));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/UserColJSON", name="user_col_json")
     * @Template()
     */
    public function UserColJSONAction(Request $request)
    {
        $mun = $request->get('mun');
        $em = $this->getDoctrine()->getManager();
        $Colonias = array();
        $Colonias['estatus'] = 0;
        $Colonias['colonias'] = '';

        $entities = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByMunicipio($mun);
        
        if(count($entities)>0){
            $Colonias['estatus'] = 1;
            $Colonias['colonias'] = $entities;
        } 


        $response = new Response(json_encode($Colonias));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/deletedirUserJSON", name="delete_dir_user_json")
     * @Template()
     */
    public function deletedirUserJSONAction(Request $request)
    {
        $usuario = $request->get('user');
        $direccion = $request->get('dir');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;

        try{

        $user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($usuario);
        if($user){
            foreach ($user->getDirecciones() as $dir) {
                    if($dir->getIdDireccion() == $direccion){
                       $user->removeDireccion($dir);
                       $em->remove($dir);
                       $em->flush();
                       $Respuesta['estatus'] = 1;
                    }
                }
        }

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error DELETE DIR USER:');
                $this->get('logger')->error($ex->getMessage());

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error DELETE DIR USER:');
                $this->get('logger')->error($ex->getMessage());
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/getInfoDirJSON", name="get_info_dir_json")
     * @Template()
     */
    public function getInfoDirJSONAction(Request $request)
    {
        $type = $request->get('ident');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;

        try{

            if($type == 1){ 

                $CP = $request->get('CP');
                $colonias = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($CP);
                $edo = $colonias[0]->getMunicipio()->getEstado();
                $municipios = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findByEstado($edo);
                $Respuesta['estatus'] = 1;
                $Respuesta['colonias'] = $colonias;
                $Respuesta['estados'] = $edo;
                $Respuesta['municipios'] = $municipios;

            }else if($type == 2){

                $edo = $request->get('EDO');
                $municipios = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findByEstado($edo);
                $Respuesta['estatus'] = 1;
                $Respuesta['municipios'] = $municipios;

            }else if($type == 3){

                $mun = $request->get('MUN');
                $colonias = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByMunicipio($mun);
                $Respuesta['estatus'] = 1;
                $Respuesta['colonias'] = $colonias;

            }

        

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error CONSULTA DE DATOS DIRECCION:');
                $this->get('logger')->error($ex->getMessage());

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error CONSULTA DE DATOS DIRECCION:');
                $this->get('logger')->error($ex->getMessage());
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/MakeDirJSON", name="make_dir_json")
     * @Template()
     */
    public function MakeDirJSONAction(Request $request)
    {
        $type = $request->get('ident');
        $pais = $request->get('pais');
        $estado = $request->get('edo');
        $municipio = $request->get('mun');
        $cp = $request->get('cp');
        $colonia = $request->get('col');
        $calle = $request->get('calle');
        $entreCalles = $request->get('entreCalles');
        $referencia = $request->get('referencia');
        $noExt = $request->get('noExt');
        $noInt = $request->get('noInt');
        $telcasa = $request->get('telcasa');
        $telOficina = "";
        $telCel = $request->get('telCel');
        $user = $request->get('user');
        $em = $this->getDoctrine()->getManager();
        $Respuesta = array();
        $Respuesta['estatus'] = 0;
        
        $nameEdo = $em->getRepository('MDRPuntoVentaBundle:Estados')->find($estado);
        $nameMun = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($municipio);

        try{

            if($type == "N"){ 

                $usuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($user);

                $newDireccion = new DireccionesUsuarios();
                $newDireccion -> setPais($pais);
                $newDireccion -> setEstado($nameEdo);
                $newDireccion -> setMunicipio($nameMun);
                $newDireccion -> setCp($cp);
                $newDireccion -> setColonia($colonia);
                $newDireccion -> setCalle($calle);
                $newDireccion -> setEntreCalles($entreCalles);
                $newDireccion -> setReferencia($referencia);
                $newDireccion -> setNoExt($noExt);
                $newDireccion -> setNoInt($noInt);
                $newDireccion -> setTelcasa($telcasa);
                $newDireccion -> setTelOficina($telOficina);
                $newDireccion -> setTelCel($telCel);
                $newDireccion -> setUsuario($usuario);

                $em->persist($newDireccion);
                $em->flush();

                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Nueva Dirección Registrada";

            }else if($type == "A"){

                $IdDireccion = $request->get('idDir');

                $usuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($user);
                
                $updateDireccion = $em->getRepository('MDRPuntoVentaBundle:DireccionesUsuarios')->find($IdDireccion);
                $updateDireccion -> setPais($pais);
                $updateDireccion -> setEstado($nameEdo);
                $updateDireccion -> setMunicipio($nameMun);
                $updateDireccion -> setCp($cp);
                $updateDireccion -> setColonia($colonia);
                $updateDireccion -> setCalle($calle);
                $updateDireccion -> setEntreCalles($entreCalles);
                $updateDireccion -> setReferencia($referencia);
                $updateDireccion -> setNoExt($noExt);
                $updateDireccion -> setNoInt($noInt);
                $updateDireccion -> setTelcasa($telcasa);
                $updateDireccion -> setTelOficina($telOficina);
                $updateDireccion -> setTelCel($telCel);
                $updateDireccion -> setUsuario($usuario);               

                $em->persist($updateDireccion);
                $em->flush();

                $Respuesta['estatus'] = 1;
                $Respuesta['mensaje'] = "Dirección Actualizada";

            }

        

        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTUALIZAR/NUEVA DIRECCIÓN:');
                $this->get('logger')->error($ex->getMessage());

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTUALIZAR/NUEVA DIRECCIÓN:');
                $this->get('logger')->error($ex->getMessage());
        }

        $response = new Response(json_encode($Respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }
}
