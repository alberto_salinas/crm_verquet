<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Clientes;
use MDR\PuntoVentaBundle\Entity\Telefonos;
use MDR\PuntoVentaBundle\Form\ClientesType;

/**
 * Clientes controller.
 *
 * @Route("/clientes")
 */
class ClientesController extends Controller
{
    /**
     * Lists all Clientes entities.
     *
     * @Route("/", name="clientes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Clientes')->findAll();
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
			
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Clientes entity.
     *
     * @Route("/", name="clientes_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Clientes:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Clientes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $code = 0;
        $telefono = null;
        $tipoRequest = $form->get('tipoRequest')->getData();
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $numero = $form->get('telefono')->getData();
            if($numero)
            {
                $telefono = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findOneByNumero($numero);
                if(!$telefono)
                {
                    $telefono = new Telefonos();
                    $telefono->setNumero($numero);
                    $telefono->setEsListaNegra(false);
                }
                $telefono->addCliente($entity);
                $entity->addTelefono($telefono);
                $em->persist($telefono);
            }
            if($entity->getApellidoMaterno() == null)
            {
                $entity->setApellidoMaterno('');
            }
            $em->persist($entity);
            $em->flush();

            $code = 1;
            $message = '';
            if($tipoRequest != 'ajax')
            {
                return $this->redirect($this->generateUrl('clientes_show', array('id' => $entity->getIdCliente())));
            }
        }
        else
        {
            $message = $form->getErrorsAsString();;
        }

        if($tipoRequest == 'ajax')
        {
            $response = new Response(json_encode(array(
                'code' => $code,
                'message' => $message,
                'cliente' => $entity
                )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }else
        {
            return array(
                'entity' => $entity,
                'form'   => $form->createView(),
            );
        }
    }

    /**
     * Creates a form to create a Clientes entity.
     *
     * @param Clientes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Clientes $entity)
    {
        $form = $this->createForm(new ClientesType(), $entity, array(
            'action' => $this->generateUrl('clientes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Clientes entity.
     *
     * @Route("/new", name="clientes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Clientes();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Clientes entity.
     *
     * @Route("/{id}", name="clientes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clientes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Clientes entity.
     *
     * @Route("/{id}/edit", name="clientes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $tipoRequest = $request->get('tipoRequest');

        $entity = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clientes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->get('tipoRequest')->setData($tipoRequest);
        $deleteForm = $this->createDeleteForm($id);

        if($tipoRequest == 'alone')
        {
            return $this->render('MDRAdministracionBundle:Clientes:editAlone.html.twig',array(
                'edit_form'     => $editForm->createView(),
                'sinMenu'       => true,
            ));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Clientes entity.
    *
    * @param Clientes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Clientes $entity)
    {
        $form = $this->createForm(new ClientesType(), $entity, array(
            'action' => $this->generateUrl('clientes_update', array('id' => $entity->getIdCliente())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Clientes entity.
     *
     * @Route("/{id}", name="clientes_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Clientes:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Clientes entity.');
        }

        $tipoRequest = $_POST['mdr_puntoventabundle_clientes']['tipoRequest'];

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->get('tipoRequest')->setData($tipoRequest);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if($entity->getApellidoMaterno() == null)
            {
                $entity->setApellidoMaterno('');
            }
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success', 'Cliente actualizado correctamente.'
            );

            if($tipoRequest == 'alone')
            {
                return $this->redirect($this->generateUrl('clientes_edit', array('id' => $id)).'?tipoRequest='.$tipoRequest);
            }

            return $this->redirect($this->generateUrl('clientes_edit', array('id' => $id)).'?tipoRequest='.$tipoRequest);
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Clientes entity.
     *
     * @Route("/{id}", name="clientes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Clientes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clientes'));
    }

    /**
     * Creates a form to delete a Clientes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clientes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }

    /**
     * @Route("/direcciones", name="clientes_direcciones")
     * @Method("POST")
     */
    public function direccionesAction(Request $request)
    {
        $idCliente = $request->get('idCliente');
        $em = $this->getDoctrine()->getManager();
        
        $message = '';
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->findByCliente($idCliente);
            $code = 1;

        } catch (Exception $ex) {
            $code = 0;
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(array(
            'code' => $code,
            'message' => $message,
            'value' => $entities
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/telefono/{telefono}", name="clientes_by_telefono")
     * @Method("GET")
     */
    public function getByTelefono(Request $request, $telefono)
    {
        $em = $this->getDoctrine()->getManager();
        if(strlen($telefono) > 10)
        {
            $telefono = substr($telefono, strlen($telefono) -10, 10);
        }

        $entities = $em->getRepository('MDRPuntoVentaBundle:Clientes')->findByTelefono($telefono);

        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => "",
            'entities' => $entities
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/id/{id}", name="clientes_by_id")
     * @Method({"GET", "POST"})
     */
    public function getById(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($id);

        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => "",
            'entity' => $entity
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
