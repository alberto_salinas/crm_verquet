<?php

namespace MDR\SesionBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * @Route("/sesion")
 */
class DefaultController extends Controller
{
    /**
     * Redireccionar a Intranet
     *
     * @Route("/irIntranet", name="ir_intranet")
     * @Method("GET")
     */
    public function irIntranetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $url = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('URL_INTRANET');
        $tempToken = uniqid();

        $user = $this->get('security.context')->getToken()->getUser();
        $user->setTokenSesion($tempToken);
        $em->flush();
        $this->get('logger')->debug($tempToken);

        $env = $this->get('kernel')->getEnvironment();
        if($env == 'prod')
        {
            $env = '';
        } else {
            $env = '/app_'.$env.'.php';
        }
        $url .= $env.'/sesion/enIntranet/'.$tempToken;
        $this->get('logger')->debug($url);
        return $this->redirect($url);
    }

    /**
     * Redireccionar a CRM
     *
     * @Route("/irCRM", name="ir_crm")
     * @Method("GET")
     */
    public function irCRMAction()
    {
        $em = $this->getDoctrine()->getManager();
        $url = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('URL_CRM');

        return $this->redirect($url);
    }

    /**
     * Redireccionar a Intranet
     *
     * @Route("/enIntranet/{token}", name="en_intranet")
     * @Method("GET")
     */
    public function enIntranetAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findOneByTokenSesion($token);

        $token = new UsernamePasswordToken($user, null, "secured_area", $user->getRoles());
	    $this->get("security.context")->setToken($token);
	     
	    //Disparar Login event
	    $request = $this->get("request");
	    $event = new InteractiveLoginEvent($request, $token);
	    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        return $this->redirect($this->generateUrl('main_intranet'));
    }
}
