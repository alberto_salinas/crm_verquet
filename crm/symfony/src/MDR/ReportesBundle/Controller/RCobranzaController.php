<?php

namespace MDR\ReportesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;


/**
 * RCobranza controller.
 *
 * @Route("/cobranza")
 */
class RCobranzaController extends Controller
{

	/**
     * @Route("/rep_cobrados", name="reporte_pedidos_cobrados")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Cobranza:cobrados.html.twig")
     */
    public function RepPedidosCobradosAction(Request $request)
    {
 		$em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepCob();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();

               	$entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByRepPedidosCobrados($fechaInicio, $fechaFin);
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelCobrados($entities);
					}
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Cobranza:cobrados.html.twig', array(
            'cobrados' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de pedidos Cobrados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepCob()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()')	
                	))
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))					
                ->getForm();

        return $form;
    }

function createExcelCobrados($listados, $titulo = 'Cobrados'){

        $user = $this->get('security.context')->getToken()->getUser()->getTipoUsuario()->getIdTipoUsuario();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "OriVta")
				->setCellValue('B1', "OrVta")
                ->setCellValue('C1', "Plazo")
                ->setCellValue('D1', "MCobro")
				->setCellValue('E1', "Tarjeta")
				->setCellValue('F1', "RefBanco")
				->setCellValue('G1', "FCobro")
				->setCellValue('H1', "Id Cob")
				->setCellValue('I1', "Estatus")
				->setCellValue('J1', "Operador")
				->setCellValue('K1', "Falta")
				->setCellValue('L1', "Terminal")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
					            $fechaCobro = '';
								$intentos = 0;
					foreach ($listado->getCobros() as $cobro) {
						$intentos++;
						if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
						{
							$fechaCobro = $cobro->getFechaCobro()->format('d/m/Y H:i:s');
                            if($user == 10){
                                $tarjeta = substr($cobro->getNumeroTarjeta(),0, 6)."-".substr($cobro->getNumeroTarjeta(),6, 12);
                            }else{
							    $tarjeta = substr($cobro->getNumeroTarjeta(),0, 6).'XXXXXXX'.substr($cobro->getNumeroTarjeta(), 12, 14);
                            }
							$autorizacion = $cobro->getNumeroAutorizacion();
						}
					}
					
					foreach ($listado->getPagosTarjeta() as $terminales) {

							$terminal = $terminales->getTerminal()->getDescripcion();
							$promocion = $terminales->getPromocion()->getDescripcion();
					}
					
						
					foreach ($listado->getProductos() as $detalle) {	
						$origenVta = $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion();
					}	        
                    $sheet
                        ->setCellValue('A'.$numRow, $origenVta)
						->setCellValue('B'.$numRow, $listado->getIdPedido())
						->setCellValue('C'.$numRow, $promocion)
						->setCellValue('D'.$numRow, $listado->getTotalPago())
						->setCellValue('E'.$numRow, $tarjeta)
						->setCellValue('F'.$numRow, $autorizacion)
						->setCellValue('G'.$numRow, $fechaCobro) 
						->setCellValue('H'.$numRow, $intentos) 
						->setCellValue('I'.$numRow, $listado->getEstatus()->getDescripcion()) 
						->setCellValue('J'.$numRow, $listado->getUsuario()->getUNombre().' '.$listado->getUsuario()->getUApPaterno().' '.$listado->getUsuario()->getUApMaterno()) 
						->setCellValue('K'.$numRow, $listado->getFecha()->format('d/m/Y H:i:s')) 
						->setCellValue('L'.$numRow, $terminal) 
                    ;
					$numRow ++;	
					
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}
	
		/**
     * @Route("/rep_cobradosIntentos", name="reporte_pedidos_cobrados_intentos")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Cobranza:intentos.html.twig")
     */
    public function RepPedidosCobradosIntentosAction(Request $request)
    {
 		$em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepCobIntentos();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();

               	$entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByRepPedidosCobradosIntentos($fechaInicio, $fechaFin);
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelCobradosIntentos($entities);
					}
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Cobranza:cobrados.html.twig', array(
            'cobrados' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de pedidos Cobrados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepCobIntentos()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()')	
                	))
                ->add(
                	'Excel', 'button' , array(
                	'attr' => array('class' => 'btn btn-success')	
                	))
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))					
                ->getForm();

        return $form;
    }

function createExcelCobradosIntentos($listados, $titulo = 'CobradosIntentos'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "OriVta")
				->setCellValue('B1', "OrVta")
                ->setCellValue('C1', "Plazo")
                ->setCellValue('D1', "MCobro")
				->setCellValue('E1', "Tarjeta")
				->setCellValue('F1', "RefBanco")
				->setCellValue('G1', "FCobro")
				->setCellValue('H1', "Id Cob")
				->setCellValue('I1', "Estatus")
				->setCellValue('J1', "Operador")
				->setCellValue('K1', "Falta")
				->setCellValue('L1', "Terminal")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
					            $fechaCobro = '';
								$intentos = 0;
					foreach ($listado->getCobros() as $cobro) {
						$intentos++;
						if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
						{
							$fechaCobro = $cobro->getFechaCobro()->format('d/m/Y H:i:s');
							$tarjeta = substr($cobro->getNumeroTarjeta(),0, 6).'XXXXXXX'.substr($cobro->getNumeroTarjeta(), 12, 14);
							$autorizacion = $cobro->getNumeroAutorizacion();
						}
					}
					
					foreach ($listado->getPagosTarjeta() as $terminales) {

							$terminal = $terminales->getTerminal()->getDescripcion();
							$promocion = $terminales->getPromocion()->getDescripcion();
					}
					
						
					foreach ($listado->getProductos() as $detalle) {	
						$origenVta = $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion();
					}	        
                    $sheet
                        ->setCellValue('A'.$numRow, $origenVta)
						->setCellValue('B'.$numRow, $listado->getIdPedido())
						->setCellValue('C'.$numRow, $promocion)
						->setCellValue('D'.$numRow, $listado->getTotalPago())
						->setCellValue('E'.$numRow, $tarjeta)
						->setCellValue('F'.$numRow, $autorizacion)
						->setCellValue('G'.$numRow, $fechaCobro) 
						->setCellValue('H'.$numRow, $intentos) 
						->setCellValue('I'.$numRow, $listado->getEstatus()->getDescripcion()) 
						->setCellValue('J'.$numRow, $listado->getUsuario()->getUNombre().' '.$listado->getUsuario()->getUApPaterno().' '.$listado->getUsuario()->getUApMaterno()) 
						->setCellValue('K'.$numRow, $listado->getFecha()->format('d/m/Y H:i:s')) 
						->setCellValue('L'.$numRow, $terminal) 
                    ;
					$numRow ++;	
					
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}

    /**
     * @Route("/rep_cancelados", name="reporte_pedidos_cancelados")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Cobranza:cancelados.html.twig")
     */
    public function RepPedidosCanceladosAction(Request $request)
    {
 		$em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepCancel();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();

               	$entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByRepPedidosCancelados($fechaInicio, $fechaFin);
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelCancelados($entities);
					}               	
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Cobranza:cancelados.html.twig', array(
            'cancelados' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de pedidos Cancelados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepCancel()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()')	
                	))
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))
                ->getForm();

        return $form;
    }

function createExcelCancelados($listados, $titulo = 'Cancelados'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "OriVta")
				->setCellValue('B1', "OrVta")
                ->setCellValue('C1', "Falta")
                ->setCellValue('D1', "TPago")
				->setCellValue('E1', "Mcobro")
				->setCellValue('F1', "Estatus")
				->setCellValue('G1', "Operador")
				->setCellValue('H1', "Cliente")
				->setCellValue('I1', "Cancelacion")
				->setCellValue('J1', "Comentarios")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {	 								
						$comentarios = $listado->getComentariosCancelacion();      
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getPedido()->getIdPedido())
						->setCellValue('B'.$numRow, $listado->getPedido()->__toString())
						->setCellValue('C'.$numRow, $listado->getPedido()->getFecha()->format('d/m/Y H:i:s'))
						->setCellValue('D'.$numRow, $listado->getPedido()->getPago()->getDescripcion())
						->setCellValue('E'.$numRow, $listado->getPedido()->getTotalPago())
						->setCellValue('F'.$numRow, $listado->getPedido()->getEstatus()->getDescripcion())
						->setCellValue('G'.$numRow, $listado->getPedido()->getUsuario()->getUNombre().' '.$listado->getPedido()->getUsuario()->getUApPaterno().' '.$listado->getPedido()->getUsuario()->getUApMaterno()) 
						->setCellValue('H'.$numRow, $listado->getPedido()->getCliente()->__toString()) 
						->setCellValue('I'.$numRow, $listado->getTipoCancelacion()->__toString()) 
						->setCellValue('J'.$numRow, str_replace( array("<b>", "</b>"),' ',$comentarios)) 
                    ;
					$numRow ++;	
					
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}
   
}
