//validar fechas	
	function vd_date(){
		
		var date1 = createdate($('#form_fechaInicio').val());
		var date2 = createdate($('#form_fechaFin').val());

		if (date1 <= date2) {
			$('#form_Consultar').attr("disabled", false);			
		}else if (date1 > date2){
			alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
			$('#form_Consultar').attr("disabled", true);	
		}

	}

//Loading para consulta de operaciones
var img = new Image();
img.src = "../../vendor/img/bubbles_load.gif";

function loadPedidos(){
	showLoading("Cargando Pedidos ...");
}

		$(document).ready(function() {
        	var table = $('#tbl_operaciones').DataTable({
        					"language":DataTables.languaje.es 					
        				});

        	$( "select" ).addClass( "form-control" );
        	$( "input[type='text']" ).addClass( "form-control" );        	
			$( "input[type='search']").addClass( "form-control" );
        	$( "#tbl_operaciones_length" ).addClass( "form-inline" ); 
        	$( "#tbl_operaciones_filter" ).addClass( "form-inline" );
        	$( "#tbl_operaciones_filter>label" ).html('Buscar: <input type="text" id="search_pedido" placeholder="Numero de pedido" class="form-control">');				
 

				$('#search_pedido').on( 'keyup', function () {
						    table
						        .columns( 1 )
						        .search( this.value )
						        .draw();
						} );
			table.order( [ 3, 'desc' ] ).draw();	


        	$('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                    	$target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });	      
    	});

		var table_recuperacion_SAP;	
		if(PedidosSAPJSON.length != 0){
			for(var x=0;x<=PedidosSAPJSON.length-1;x++){
				table_recuperacion_SAP = table_recuperacion_SAP+"<tr><td><input type='checkbox' style='width: 30px;height: 25px;' onclick=check_pf("+PedidosSAPJSON[x].idPedido+")></td><td><button type='button' class='btn btn-info' onclick=Showdetalle("+PedidosSAPJSON[x].idPedido+")><span class='icon-file-settings'></span>  "+PedidosSAPJSON[x].idPedido+"</button></td><td>"+PedidosSAPJSON[x].pago.descripcion+"</td><td>"+PedidosSAPJSON[x].fechaGregorian+"</td><td>$"+parseFloat(PedidosSAPJSON[x].gastosEnvio.precio)+"</td><td>$"+parseFloat(PedidosSAPJSON[x].subTotal)+"</td><td>$"+parseFloat(PedidosSAPJSON[x].totalPago)+"</td></tr>";
				if(x == PedidosSAPJSON.length-1){
					$('#body_tbl_operaciones').html(table_recuperacion_SAP);
				}				
			}
		}

//Botones de seleccion todos o deselecionar todos
var ApedidosSAP = new Array();
var errores = 0;
var contador = 0;
var pedidoserror="<div class='row'>";
		$(function(){
				marcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", true);
				ApedidosSAP.length = 0;
				for(x=0;x<=PedidosSAPJSON.length-1;x++){
						ApedidosSAP.push(PedidosSAPJSON[x].idPedido);	
				}
			}
		

			desmarcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", false);
				ApedidosSAP.length=0;
			}
		});

//Captura de checks de pedidos
		
	function check_pf(idP){
		idpedidos = parseInt(idP);
		var posicion = ApedidosSAP.indexOf(idpedidos);
		if (posicion == -1) {
			ApedidosSAP.push(idpedidos);
		}else{
			ApedidosSAP.splice(posicion,1);
		}
	}

function sendSAP(){
	if (ApedidosSAP.length != 0) {
		showLoading('Enviando Pedido(s) a SAP');
		EnviaSAP(ApedidosSAP,function(respuesta,x){
				contador++;
					if(respuesta.code == 0){
						errores++;
						pedidoserror = pedidoserror+'<div class="col-md-12"><div class="col-md-3"><label>PEDIDO:</label> '+respuesta.pedido+'</div><div class="col-md-9"><label>ERRRO:</label></br>'+respuesta.message+'</div></div></br>';
					}
				if(contador == x){
					if(errores > 0){
						$('#title_operacion').html("No se creo en SAP el/los PEDIDO(s)");
						$('#body_operacion').html(pedidoserror+'</div>');
						hideLoading();
						$('#popup_operacion').modal({
					      backdrop: 'static',
					      keyboard: false
					    });
						$('#popup_operacion').modal('show');
					}else{
						contador = 0;
						errores = 0;
						pedidoserror = "<div class='row'>";
						alert("Pedidos creados con exito en SAP");
						$('#text_load').html("Actualizando pedidos...");
						$('#form_Consultar').click();
					}
				}	
					
				});	
	}else{
		alert("No se a seleccionado ningun pedido");
	}
}	

function EnviaSAP(ApedidosSAP,callback){
	for(var x=0;x<=ApedidosSAP.length-1;x++){
		idPedido = ApedidosSAP[x];
	$.post("../crearPedidoSAP",{
			idPedido:idPedido
	},function(respuesta){
			callback(respuesta,x);
		});

	}
}

function reloadPedidos(){
	showLoading('Actualizando Pedidos');
	contador = 0;
	errores = 0;
	pedidoserror = "<div class='row'>";
	$('#form_Consultar').click();
}


function Showdetalle(idPedido){
	showLoading('Cargando detalle del pedido');
	var bodydetalle ="";
	for(x=0;x<=PedidosSAPJSON.length-1;x++){
		if(PedidosSAPJSON[x].idPedido == idPedido){			

        bodydetalle = bodydetalle + '<div class="row">'+
        								'<div class="col-md-12">'+
                          					'<div class="panel panel-success">'+
		                          				'<div class="panel-heading">Resúmen del pedido: '+idPedido+'</div>'+
		                          				 '<div class="panel-body">'+
		                              				'<div class="col-md-12">'+
		                                  				'<div class="col-md-3" style="font-weight: 700;">Cliente:</div>'+
		                                  				'<div class="col-md-9">'+PedidosSAPJSON[x].cliente.toString+'</div>'+
		                                  				'<div class="col-md-3" style="font-weight: 700;">Dirección:</div>'+
		                                  				'<div class="col-md-9" id="desc_direccion">'+PedidosSAPJSON[x].direccion.toString+'</div>'+
		                                  				'<div class="col-md-3" style="font-weight: 700;">Referencia:</div>'+
		                                  				'<div class="col-md-9">'+PedidosSAPJSON[x].direccion.referencia+'</div>'+
		                                  				'<div class="col-md-3" style="font-weight: 700;">E-mail:</div><div class="col-md-9">';
											              if(PedidosSAPJSON[x].cliente.email){
											                bodydetalle=bodydetalle+'<a href="mailto:'+PedidosSAPJSON[x].cliente.email+'">'+PedidosSAPJSON[x].cliente.email+'</a></div>';
											              }else{
											                bodydetalle=bodydetalle+"SIN CORREO</div>";
											              }
							  bodydetalle = bodydetalle + '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+           
						                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Casa:</div>'+
						                                      '<div class="col-md-12">'+PedidosSAPJSON[x].direccion.telCasa+'</div>'+
						                                  '</div>'+ 
						                                  '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+     
						                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Oficina:</div>'+
						                                      '<div class="col-md-12">'+PedidosSAPJSON[x].direccion.telOficina+'</div>'+
						                                  '</div>'+   
						                                  '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+  
						                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Celular:</div>'+
						                                      '<div class="col-md-12">'+PedidosSAPJSON[x].direccion.telCel+'</div>'+
					                                  	  '</div>'+
					                                  	  	'<div class="col-md-12" style="margin:5px 0px 5px 0px;font-weight: 700;">Productos:</div>'+
							                                '<div class="col-md-12">'+tbl_productos_pedido(idPedido)+'</div>'+                 
                									'</div>'+
                          						'<div>'+
                          					'<div>'+
                          				'<div>'+
                          			'</div>';
		}	
	}


	$('#title_detalle').html('Detalle del Pedido '+idPedido);
	$('#body_detalle').html(bodydetalle);
	$('#popup_detalle').modal({
		backdrop: 'static',
		keyboard: false
	});
	hideLoading();
	$('#popup_detalle').modal('show');
}


function tbl_productos_pedido(idPedido){
	
	var table_dpedido='<table style="width:100%;" class="table table-bordered table-hover"><thead style="text-align: center;font-weight: 700;font-size: 14px;"><tr><td>Cantidad</td><td>Producto</td><td>PU</td><td>Total</td></tr></thead><tbody>';

	 for(x=0;x<=PedidosSAPJSON.length-1;x++){
      if(PedidosSAPJSON[x].idPedido == idPedido){
        for(z=0;z<=PedidosSAPJSON[x].productos.length-1;z++){
        	 table_dpedido=table_dpedido+'<tr style="text-align: center;font-size: 14px;">'+
                                          '<td>'+PedidosSAPJSON[x].productos[z].cantidad+'</td>'+
                                          '<td>'+PedidosSAPJSON[x].productos[z].productoPrecio.producto.toString+'</td>'+
                                          '<td>$'+parseFloat(PedidosSAPJSON[x].productos[z].productoPrecio.precio)+'</td>'+
                                          '<td>$'+parseFloat(PedidosSAPJSON[x].productos[z].productoPrecio.precio)*PedidosSAPJSON[x].productos[z].cantidad+'</td>'+
                                          '</tr>';
            if(z == PedidosSAPJSON[x].productos.length-1){
            
              table_dpedido=table_dpedido+'<tr>'+
                                              '<td colspan="2" rowspan="4" style="text-align: center;"><img src="'+img_MDR.src+'" style="width: 24%;margin: 10px;"></td>'+                                             
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">SubTotal</td>'+
                                              '<td style="text-align: left;font-size: 14px;font-weight: 700;" id="camp_subtotal">$'+parseFloat(PedidosSAPJSON[x].subTotal)+'</td>'+ 
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">Envio</td>'+
                                              '<td style="text-align: left;font-size: 14px;" id="camp_envio">$'+parseFloat(PedidosSAPJSON[x].gastosEnvio.precio)+'</td>'+
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">Total</td>'+
                                              '<td style="text-align: left;font-size: 14px;font-weight: 700;" id="camp_total">$'+parseFloat(PedidosSAPJSON[x].totalPago)+'</td>'+
                                          '</tr></tbody></table>';

            }                              
        }
      }
    }

    return table_dpedido;    	
}