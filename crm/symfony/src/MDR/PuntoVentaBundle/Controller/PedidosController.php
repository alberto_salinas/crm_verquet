<?php

namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use MDR\PuntoVentaBundle\Entity\Clientes;
use MDR\PuntoVentaBundle\Entity\ClienteFlash;
use MDR\PuntoVentaBundle\Entity\Dids;
use MDR\PuntoVentaBundle\Entity\Direcciones;
use MDR\PuntoVentaBundle\Entity\PagoTarjeta;
use MDR\PuntoVentaBundle\Entity\Pedidos;
use MDR\PuntoVentaBundle\Entity\PedidoDescuentos;
use MDR\PuntoVentaBundle\Entity\PedidosCancelados;
use MDR\PuntoVentaBundle\Entity\ProductosPedido;
use MDR\PuntoVentaBundle\Entity\PedidosTickets;
use MDR\PuntoVentaBundle\Entity\Telefonos;
use MDR\PuntoVentaBundle\Entity\ReglasFraude;
use MDR\PuntoVentaBundle\Entity\ListaNegraMontos;

use MDR\PuntoVentaBundle\Form\ClientesType;
use MDR\PuntoVentaBundle\Form\DireccionesType;
use MDR\PuntoVentaBundle\Form\OficinasVentas;
use MDR\PuntoVentaBundle\Form\PagoTarjetaType;
use MDR\PuntoVentaBundle\Form\PedidosType;
use MDR\PuntoVentaBundle\Form\PedidosCanceladosType;


/**
 * Ventas controller.
 *
 * @Route("/ventas")
 */
class PedidosController extends Controller
{
    /**
     * Lists all Direcciones entities.
     *
     * @Route("/", name="ventas")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $searchForm = $this->createSearchForm();

        $entities = [];
        if($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $fechaInicio = $searchForm->get('fechaInicio')->getData();
            $fechaFin = $searchForm->get('fechaFin')->getData();
            $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findAllCompleteByUser($user, $fechaInicio, $fechaFin);
            
            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'entities'      => $entities,
            'searchForm'    => $searchForm->createView(),
        );
    }

    public function createSearchForm()
    {
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));

        $form = $this->createFormBuilder()
        ->add('fechaInicio', 'date', array(
           'label' => 'Inicio',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => $fechaInicio,                    
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('fechaFin', 'date', array(
            'label' => 'Fin',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => new \DateTime(),
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('Buscar', 'submit')
        
        ->getForm();

        return $form;
    }

    /**
     * Lists all Pedidos entities.
     *
     * @Route("/recuperacion", name="recuperacion")
     * @Method({"GET"})
     * @Template()
     */
    public function recuperacionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByUsuarioAsignadoComplete($user);

        return array(
            'entities'      => $entities,
        );
    }

    /**
     * Lists all Pedidos entities.
     *
     * @Route("/asignaRecuperacion", name="asignaRecuperacion")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function asignaRecuperacionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByRecuperacionComplete();

        $form = $this->createAsignaRecuperacionForm($entities);
        if ($request->isMethod('POST')) {
            $form->submit($request);
            $usuarioRecuperacion = $form->get('usuario')->getData();
            $pedidos = $form->get('pedidos')->getData();
            foreach ($pedidos as $pedido) {
                $usuarioRecuperacion->addPedidosRecuperacion($pedido);
                $pedido->addUsuariosRecuperacion($usuarioRecuperacion);
                $em->persist($pedido);
            }
            $em->flush();
            return $this->redirect($this->generateUrl('asignaRecuperacion'));
        }

        return array(
            'entities'      => $entities,
            'formAsigna'    => $form->createView(),
        );
    }

    private function createAsignaRecuperacionForm($pedidos)
    {
        $fechaInicio = new \DateTime();
        $fechaInicio->sub(new \DateInterval('P7D'));

        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findByTipoUsuario(41);
        
        $form = $this->createFormBuilder()
        ->add('usuario', 'entity', array(
            'label' => 'Asignar a',
            'class' => 'MDRPuntoVentaBundle:Usuario',
            'choices' => $usuarios,
            'empty_value' => '----------'
            ))
        ->add('pedidos', 'entity', array(
            'class' => 'MDRPuntoVentaBundle:Pedidos',
            'choices' => $pedidos,
            'multiple'  => true,
            'required'  => true,
            ))
        ->add('Asignar', 'submit')
        
        ->getForm();

        return $form;
    }

	/**
     * Displays a form to create a new CatClientes entity.
     *
     * @Route("/nuevo/{canal}", name="pedidos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request, $canal)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = new Pedidos();

        $AId = $request->get('AId');
        $origenVentas = [];
        $origenVentas['organizacionVentas'] = '';
        $origenVentas['canalDistribucion'] = '';
        $origenVentas['oficinaVentas'] = '';
        $asignaMarcacion = null;
        
        if($AId)
        {
            $asignaMarcacion = $em->getRepository('MDRPuntoVentaBundle:AsignaMarcacion')->find($AId);
            if($asignaMarcacion)
            {
                $origenVentas['organizacionVentas'] = $asignaMarcacion->getOrganizacionVentas()->getClaveSAP();
                $origenVentas['canalDistribucion'] = $asignaMarcacion->getCanalDistribucion()->getClaveSAP();
                $origenVentas['oficinaVentas'] = $asignaMarcacion->getOficinaVentas()->getClaveSAP();
                $asignaMarcacion->setAtendido(true);
                $em->flush();
            }
        }

        $pagoTarjeta = new PagoTarjeta();
        $pagoTarjeta->setPosFechar(0);
        $pagoTarjeta->setFechaCobro(new \DateTime());
        $pagoTarjeta->setVigencia(new \DateTime());
        $entity->addPagosTarjetum($pagoTarjeta);
        $productoPrecios = [];
        $gastosEnvioBD = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->findAll();
        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');

        $gastosEnvio = [];
        $gastosEnvioInit = [];
        foreach ($gastosEnvioBD as $gastoEnvio) {
            if($gastoEnvio->getActivo())
            {
                $gastosEnvio[$gastoEnvio->getTipoPago()->getIdTipoPago()][] = $gastoEnvio;
            }
        }
        $gastosEnvioInit[1] = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('INIT_GASTOS_PREPAGO');
        $gastosEnvioInit[2] = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('INIT_GASTOS_COD');
        $maxDigitosTelCliente = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIGITOS_TEL_CLIENTE');
        $maxDiasPosfecha = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIAS_POSFECHA_CALL');

        $form   = $this->createCreateForm($entity, $productoPrecios, $canal);
        $formOficinaVentas   = $this->createOficinaVentasForm($canal);

        $formClient = $this->createForm(new ClientesType(), new Clientes(), array(
            'action' => $this->generateUrl('clientes_create'),
            'method' => 'POST',
            'attr'   => array(
                'id' => 'clientes_form',
            )
        ));
        $formClient->get('tipoRequest')->setData('ajax');

        $formDireccion = $this->createForm(new DireccionesType(), new Direcciones(), array(
            'action' => $this->generateUrl('direcciones_create', array('idCliente' => 0)),
            'method' => 'POST',
            'maxDigitosTelCliente'      => $maxDigitosTelCliente,
            'attr'   => array(
                'id' => 'direcciones_form',
            )
        ));
        $formDireccion->get('tipoRequest')->setData('ajax');

        $maxTotalCod = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find(10);
        $maxMontoCod = 0;
        if($maxTotalCod->getActivo() == 1){
        	$maxMontoCod = $maxTotalCod->getValor();
        }
        

        $maxTotaltdc = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find(9);
        $maxMontoTdc = 0;
        if($maxTotaltdc->getActivo() == 1){
        	$maxMontoTdc = $maxTotaltdc->getValor();
        }

        return array(
            'title'                 => 'Nueva Venta',
            'entity'                => $entity,
            'productos'             => $productoPrecios,
            'gastosEnvio'           => $gastosEnvio,
            'gastosEnvioInit'       => $gastosEnvioInit,
            'form'                  => $form->createView(),
            'formOficinaVentas'     => $formOficinaVentas->createView(),
            'formClient'            => $formClient->createView(),
            'formDireccion'         => $formDireccion->createView(),
            'maxTotalCod'           => $maxMontoCod,
            'maxTotalTdc'           => $maxMontoTdc,
            'canal'                 => $canal,
            'origenVentas'          => $origenVentas,
            'asignaMarcacion'       => $asignaMarcacion,
            'maxDigitosTelCliente'  => $maxDigitosTelCliente,
            'maxDiasPosfecha'       => $maxDiasPosfecha,
            'env'                   => $env,
        );
    }

    /**
     * Creates a new Pedidos entity.
     *
     * @Route("/crear/{canal}", name="pedidos_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Pedidos:new.html.twig")
     */
    public function createAction(Request $request, $canal)
    {
        $fecha1 = new \Datetime();
        $this->get('logger')->debug('Inicia guardado pedido');
        $entity = new Pedidos();
        $em = $this->getDoctrine()->getManager();
        
        $entity->addPagosTarjetum(new PagoTarjeta());

        $idDireccion = $_POST['mdr_puntoventabundle_pedidos']['direccion'];
        $idCliente = $_POST['mdr_puntoventabundle_pedidos']['cliente'];
        $idsProductoPrecio = $_POST['mdr_puntoventabundle_pedidos']['productos'];
        
        $direcciones = [];
        $clientes = [];
        $direccion = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($idDireccion);
        $cliente = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($idCliente);
        $maxTotalCod = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_TOTAL_COD');
        $productoPrecios = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->findByIds($idsProductoPrecio);
        $mensajeria = $em->getRepository('MDRPuntoVentaBundle:Mensajeria')->find(1);
        $entity->setMensajeria($mensajeria);

        if($direccion != null)
        {
            if($direccion->getCliente()->getIdCliente() == $idCliente)
            {
                $direcciones[] = $direccion;
            }
        }
        if($cliente != null)        $clientes[] = $cliente;
        $form   = $this->createCreateForm($entity, $productoPrecios, $canal, $direcciones, $clientes);
        $this->get('logger')->debug('Creación formulario');

        $form->handleRequest($request);

        $respuesta['code'] = 1;
        $respuesta['message'] = '';

        if ($form->isValid()) {
            $this->get('logger')->debug('Formulario validado');
            $user = $this->get('security.context')->getToken()->getUser();
            $modeloCalificador = $em->getRepository('MDRPuntoVentaBundle:ModeloCalificador')->find(1);
            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(26);
            $entity->setPedidoSAP('');
            $entity->setNumeroGuia('');
            if(strlen($entity->getRfc()) == 0)
            {
                $entity->setRfc('');
            }

            $entity->setFecha(new \DateTime());
            $entity->setModelo($modeloCalificador);
            $entity->setEstatus($estatus);
            $entity->setUsuario($user);
            foreach ($entity->getProductos() as $producto) {
                $producto->setPedido($entity);
                $em->persist($producto);
            }
            $this->get('logger')->debug('Productos agregados');
            
            if($entity->getPago()->getIdTipoPago() == 1)
            {
                $pagoTC = $entity->getPagosTarjeta()[0];
                $pagoTC->setPedido($entity);
                $em->persist($pagoTC);
                if($pagoTC->getTitular() == null)
                {
                    $pagoTC->setTitular('');
                }
                if($pagoTC->getPosFechar() == 1)
                {
                    $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(16);
                    $entity->setEstatus($estatus);
                }
            }
            else
            {
                $entity->removePagosTarjetum($entity->getPagosTarjeta()[0]);
            }
            $numero = $form->get('did')->getData();
            $numeroCliente = $form->get('telefono')->getData();
            $descuentos = $form->get('descuentos')->getData();

            if(count($descuentos) > 0)
            {
                $descuento = $descuentos[0];
                $pedidoDescuento = new PedidoDescuentos();
                $pedidoDescuento->setDescuento($descuento);
                $pedidoDescuento->setPedido($entity);
                $pedidoDescuento->setClave($descuento->getClave());
                $pedidoDescuento->setImporte($descuento->getImporte());
                $entity->addDescuento($pedidoDescuento);
                $em->persist($pedidoDescuento);
            }

            $entity->setDnis($numero);
            $entity->setTelefono($numeroCliente);

            $entity->calculaTotal();

            $em->persist($entity);
            $em->flush();
            $this->get('logger')->debug('Pedido guardado');

            $mensajeCobro = '&nbsp;&nbsp;&nbsp;';
            $productoOfiVtas = $entity->getProductos();
            $ofiventas = $productoOfiVtas[0]->getProductoPrecio()->getOficinaVenta()->getClaveSAP();
            $Fraudes = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findReglasActivas(true);
            
            $this->get('logger')->debug('Validando reglas');
            if($ofiventas == '0023'){

                    $mensajeCobro .= 'Venta realizada en PAUTA SECA, el pedido será liberado posteriormente.';
                    $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(25);
                    $entity->setEstatus($estatus);
                    $em->persist($entity);
                    $em->flush();

            }
            else if(count($Fraudes)>0){
            	$idPedido = $entity->getIdPedido(); //id del pédido 
            	$tipoPago = $entity->getPago()->getIdTipoPago(); //id del pédido 

            	//Loop para depurar reglas que se incluiran con respecto al tipo de pago
            	foreach ($Fraudes as $valFraudes) {
                   if($valFraudes->getIdPago() == $tipoPago){
                   		$Items[] = array('id' => $valFraudes->getIdReglas() , 'valor' => $valFraudes->getValor());
                   }else if($valFraudes->getIdPago() == 3){
                   		$Items[] = array('id' => $valFraudes->getIdReglas() , 'valor' => $valFraudes->getValor());
                   }
                } 

                //\Doctrine\Common\Util\Debug::dump($Items);
				
				$resultadoInvestigacion = $this->RevisionContraFraudes($idPedido,$Items);

				if($resultadoInvestigacion['id']==1){

					$mensajeCobro .= 'El pedido será enviado a <b>INVESTIGACIÓN</b>.';
                    $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(9);
                    $entity->setEstatus($estatus);
                    $em->persist($entity);
                    

                    $EstatusTicket = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->find(1);
                    $NuevoTicket = new PedidosTickets();
                    $NuevoTicket -> setComentarios($resultadoInvestigacion['respuesta']);
                    $NuevoTicket -> setUsuario($user);
                    $NuevoTicket -> setPedido($entity);
                    $NuevoTicket -> setEstatusTicket($EstatusTicket);
                    $NuevoTicket -> setFecha(new \DateTime());
                    $em->persist($NuevoTicket);

                    $em->flush();

                    $this->get('logger')->debug('Investigacion dio 1');
				}else{

					if($entity->getPago()->getIdTipoPago() == 1)
					    {
					        $pagoTC = $entity->getPagosTarjeta()[0];
					       if($pagoTC->getPosFechar() == 1)
					        {
					            $mensajeCobro .= 'Venta post fechada';
					        }
					        elseif($pagoTC->getBanco()->getIdBanco() == 56)
					        {
					            $mensajeCobro .= 'AMEX se cobra en terminal física';
					        }
					        else
					        {
					            $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
					            $webServicesController->setContainer($this->container);
                                $this->get('logger')->debug('TDC - Cobrar pedido');
					            $respuestaCobro = $webServicesController->cobrar($entity);
                                $this->get('logger')->debug('Respuesta de cobro recibida');
					            if($respuestaCobro['code'] == 0)
					            {
					                $mensajeCobro .= $respuestaCobro['message'];
					                $this->get('session')->getFlashBag()->add(
					                    'error', 'Se registro la venta con el no: '.$entity->getIdPedido().' '.$mensajeCobro
					                );
					                $respuesta['url'] = $this->generateUrl('pedidos_valida_tdc', array('id' => $entity->getIdPedido()));

                                    $fecha2 = new \Datetime();
                                    $diff = $fecha2->diff($fecha1);
                                    $this->get('logger')->info('Tiempo: '.$diff->i.'m '.$diff->s.'s');
                                    if($diff->i > 5)
                                    {
                                        $this->get('logger')->error('TiempoExcedido');
                                    }
					                return new JsonResponse($respuesta);
					            }
					            else
					            {
					                $mensajeCobro .= 'Numero de autorización: '.$respuestaCobro['entity']->getNumeroAutorizacion();
					            }
					        }
					    }
					    else
					    {
                            $envioSAP = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('SWITCH_ENVIO_SAP');
                            if($envioSAP == "true"){

    					        $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
    					       	$webServicesController->setContainer($this->container);
                                $this->get('logger')->debug('COD - Crear Pedido en SAP');
    					        $respuestaSAP = $webServicesController->crearPedido($entity);
                                $this->get('logger')->debug('Respuesta de SAP recibida');
    					        if($respuestaSAP['code'] == 0)
    					        {
    					            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
    					            $entity->setEstatus($estatus);
    					            $em->persist($entity);
    					            $em->flush();
    					            $mensajeCobro .= $respuestaSAP['message'];
    					        }
    					        else
    					        {
    					            $mensajeCobro .= 'Pedido creado exitosamente en SAP: '.$respuestaSAP['pedidoSAP'];
    					       	}
                            }else{
                                    $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
                                    $entity->setEstatus($estatus);
                                    $em->persist($entity);
                                    $em->flush();
                            }    

					    }
				}

            }
            else if($entity->getPago()->getIdTipoPago() == 1)
		    {
		        $pagoTC = $entity->getPagosTarjeta()[0];
		       if($pagoTC->getPosFechar() == 1)
		        {
		            $mensajeCobro .= 'Venta post fechada';
		        }
		        elseif($pagoTC->getBanco()->getIdBanco() == 56)
		        {
		            $mensajeCobro .= 'AMEX se cobra en terminal física';
		        }
		        else
		        {
                    $this->get('logger')->debug('TDC - cobrar pedido');
                    $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
                    $webServicesController->setContainer($this->container);
                    $respuestaCobro = $webServicesController->cobrar($entity);
                    $this->get('logger')->debug('Respuesta de cobro recibida');
		            if($respuestaCobro['code'] == 0)
		            {
		                $mensajeCobro .= $respuestaCobro['message'];
		                $this->get('session')->getFlashBag()->add(
		                    'error', 'Se registro la venta con el no: '.$entity->getIdPedido().' '.$mensajeCobro
		                );
		                $respuesta['url'] = $this->generateUrl('pedidos_valida_tdc', array('id' => $entity->getIdPedido()));

                        $fecha2 = new \Datetime();
                        $diff = $fecha2->diff($fecha1);
                        $this->get('logger')->info('Tiempo: '.$diff->i.'m '.$diff->s.'s');
                        if($diff->i > 5)
                        {
                            $this->get('logger')->error('TiempoExcedido');
                        }
		                return new JsonResponse($respuesta);
		            }
		            else
		            {
		                $mensajeCobro .= 'Numero de autorización: '.$respuestaCobro['entity']->getNumeroAutorizacion();
		            }
		        }
		    }
		    else
		    {
		        $envioSAP = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('SWITCH_ENVIO_SAP');
                if($envioSAP == "true"){
                    $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
    		       	$webServicesController->setContainer($this->container);
                    $this->get('logger')->debug('COD - Crear Pedido en SAP');
    		        $respuestaSAP = $webServicesController->crearPedido($entity);
                    $this->get('logger')->debug('Respuesta de SAP recibida');
    		        if($respuestaSAP['code'] == 0)
    		        {
    		            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
    		            $entity->setEstatus($estatus);
    		            $em->persist($entity);
    		            $em->flush();
    		            $mensajeCobro .= $respuestaSAP['message'];
    		        }
    		        else
    		        {
    		            $mensajeCobro .= 'Pedido creado exitosamente en SAP: '.$respuestaSAP['pedidoSAP'];
    		       	}
                }else{
                        $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
                        $entity->setEstatus($estatus);
                        $em->persist($entity);
                        $em->flush();
                }    

		    } 


            $this->get('session')->getFlashBag()->add(
                'success', 'Se registro la venta con el no: '.$entity->getIdPedido().$mensajeCobro
            );
            $respuesta['url'] = $this->generateUrl('pedidos_new', array('canal' => $canal));
        }
        else
        {
            $formError = $form->getErrorsAsString();
            /*if(strlen($formError))
            {
                $this->get('session')->getFlashBag()->add(
                    'error', $formError
                );
            }*/
            $this->get('logger')->info($formError);
            
            $respuesta['code'] = 0;
            $respuesta['message'] = $formError;
        }
        $fecha2 = new \Datetime();
        $diff = $fecha2->diff($fecha1);
        $this->get('logger')->info('Tiempo: '.$diff->i.'m '.$diff->s.'s');
        if($diff->i > 5)
        {
            $this->get('logger')->error('TiempoExcedido');
        }

        return new JsonResponse($respuesta);
    }

    function RevisionContraFraudes($idPedido,$Items){

    	$em = $this->getDoctrine()->getManager();
    	
    	$Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($idPedido);

    	$idCliente = $Pedido->getCliente()->getIdCliente();
    	$Cliente = $Pedido->getCliente()->getNombre()." ".$Pedido->getCliente()->getApellidoPaterno();
        if($Pedido->getCliente()->getApellidoMaterno()){
            $Cliente." ".$Pedido->getCliente()->getApellidoMaterno();
        }
    	$CP = $Pedido->getDireccion()->getCp();
    	$Municipio = utf8_encode($Pedido->getDireccion()->getMunicipio());
    	$Colonia = utf8_encode($Pedido->getDireccion()->getColonia());
    	$Telefono = $Pedido->getTelefono();
    	$esFraude = 0;
    	$mensaje = "<b>EL PEDIDO PRESENTA POSIBLE FRAUDE POR:</b>";

		foreach ($Items as $regla) {

			if($regla['id'] == 1){

				//COTEJO DE LISTA NEGRA CLIENTES
				$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnClientes($Cliente);
				if(count($exist)>0){
    				$esFraude = 1;
    				$mensaje .= "</br>El cliente se encuentra en Lista Negra";
    			}

			}else if($regla['id'] == 2){

				//COTEJO DE LISTA NEGRA DIRECCIONES y DIRECCIONES STAFF
				$lnDir = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnDirecciones($CP,$Municipio,$Colonia);

				if(count($lnDir)>0){
    				$esFraude = 1;
    				$mensaje .= "</br>La direccion se encuentra en Lista Negra";
    			}else{
    				$Staff =  $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findStaffDirecciones($CP,$Municipio,$Colonia);
    				if(count($Staff)>0){
    					$esFraude = 1;
    					$mensaje .= "</br>La direccion tiene coincidencias con la de un miembro(s) de MDR";
    				}
    			}



			}else if($regla['id'] == 3){

				//COTEJO PRODUCTOS C/U POR PEDIDO
				$Productos = $Pedido->getProductos();
				foreach ($Productos as $pindividual) {
					$clvProducto = $pindividual->getProductoPrecio()->getProducto()->getCodigoSAP();
					$cantidad = $pindividual->getCantidad();
					$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findClaveProducto($clvProducto); 
					if(count($exist)>0){
						if($cantidad>$exist[0]->getMonto()){
							$esFraude = 1;
    						$mensaje .= "</br>Se rebaso el limite de pzas. del producto <b>".$pindividual->getProductoPrecio()->getProducto()->getDescripcion()."</b>";
						}
					}
				}

			}else if($regla['id'] == 4){
				
				//COTEJO LISTA NEGRA TARJETAS				
    			$Tarjeta = $Pedido->getPagosTarjeta();
    			$numTarjeta = $Tarjeta[0]->getNumeroTarjeta();
    			$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnTarjetas($numTarjeta); 
    			if(count($exist)>0){
    				$esFraude = 1;
    				$mensaje .= "</br>La tarjeta se encuentra Lista Negra";
    			}

			}else if($regla['id'] == 5){
				
				//COTEJO LISTA NEGRA TELEFONOS
				$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnTelefonos($Telefono); 
				if(count($exist)>0){
    				$esFraude = 1;
    				$mensaje .= "</br>El número Telefonico esta en Lista Negra";
    			}

			}else if($regla['id'] == 7){

				//CANTIDAD DE PRODUCTOS POR PEDIDO
				$Productos = $Pedido->getProductos();
				$totalItems = 0;
				foreach ($Productos as $suma) {
					$totalItems = $totalItems+$suma->getCantidad();
				}
				if($totalItems>$regla['valor']){
					$esFraude = 1;
    				$mensaje .= "</br>Se ha rebazado el limite de productos por pedido";
				}
				
			}else if($regla['id'] == 8){
				
				//COTEJO PEDIDOS POR DIA				
				$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findPedidosDia($idCliente); 
				if(count($exist)>$regla['valor']){
    				$esFraude = 1;
    				$mensaje .= "</br>Se han excedido el maximo de <b>".$regla['valor']."</b> pedido(s) por dia del cliente";
    			}
				

			}else if($regla['id'] == 12){

				//COTEJO ZONAS DE RIESGO
				$exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findZonaRiesgo($CP,$Municipio); 
				if(count($exist)>$regla['valor']){
    				$esFraude = 1;
    				$mensaje .= "</br>La direccion esta en una Zona de Riesgo";
    			}

			}

        }

        return $respuesta[] = array('id' => $esFraude  , 'respuesta' => $mensaje );
    }

    public function nuxibaRemoteInfEditAction()
    {
        $content = $this->renderView('MDRPuntoVentaBundle:Pedidos:REMOTE-INF.xml.twig');
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }


    /**
     * Creates a form to create a Pedidos entity.
     *
     * @param Pedidos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pedidos $entity, $productoPrecios, $canal, $direcciones = array(), $clientes = array())
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new PedidosType(), $entity, array(
            //'action' => 'javascript:showPreview()',
            'method' => 'POST',
            'productoPrecios'   => $productoPrecios,
            'clientes'          => $clientes,
            'direcciones'       => $direcciones,
            'attr'              => array('onsubmit' => 'return false;')
        ));

        $form->add('submit', 'submit', array('label' => 'Terminar'));

        return $form;
    }

    /**
     * Crear formulario para seleccionar Oficina Ventas.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createOficinaVentasForm($canal)
    {
        $em = $this->getDoctrine()->getManager();
        $oficinasVentas = $em->getRepository('MDRPuntoVentaBundle:OficinasVentas')->findByFiltroCanal($canal);

        $form = $this->createFormBuilder()
                ->add('oficinaVentas', 'entity', array(
                    'class'         => 'MDRPuntoVentaBundle:OficinasVentas',
                    'empty_value'   => "-----------",
                    'disabled'      => true,
                    'required'      => false,
                    'choices'       => $oficinasVentas,
                    'attr'          => array('class' => "form-control"),
                ))
                ->getForm();

        return $form;
    }

    /**
    * Creates a form to edit a Pedidos entity.
    *
    * @param Pedidos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Pedidos $entity, $productoPrecios = array())
    {
        if(count($entity->getPagosTarjeta()) > 0)
        {
            
        }
        if($entity->getCliente() != null)
        {
            $em = $this->getDoctrine()->getManager();
            $clientes = $em->getRepository('MDRPuntoVentaBundle:Clientes')->findByTelefono($entity->getTelefono()->getNumero());
            $direcciones = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->findByCliente($entity->getCliente());
        }
        else
        {
            $direcciones = array();
        }
        $form = $this->createForm(new PedidosType(), $entity, array(
            'action'        => $this->generateUrl('pedidos_update', array('id' => $entity->getIdPedido())),
            'method'        => 'PUT',
            'clientes'      => $clientes,
            'direcciones'   => $direcciones,
            'productoPrecios'   => $productoPrecios,
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Creates a form to create a Pedidos entity.
     *
     * @param Pedidos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditPagoTarjetaForm(PagoTarjeta $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new PagoTarjetaType(), $entity, array(
            'action' => $this->generateUrl('pedidos_valida_tdc', array('id' => $entity->getPedido()->getIdPedido())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Creates a form to cancel a Pedidos entity.
     *
     * @param Pedidos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCancelForm(PedidosCancelados $entity)
    {
        $form = $this->createForm(new PedidosCanceladosType(), $entity, array(
            'action' => $this->generateUrl('pedidos_cancelar', array('id' => $entity->getPedido()->getIdPedido())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }
    
    /**
     * Edits an existing Pedidos Pago entity.
     *
     * @Route("/{id}/validaTDC", name="pedidos_valida_tdc")
     * @Method({"GET", "POST"})
     * @Template("MDRPuntoVentaBundle:Pedidos:editPagoTarjeta.html.twig")
     */
    public function validaTDCAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:PagoTarjeta')->findByPedido($id);
        $Intentos = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find(11);
        $maxIntentosCobroTDC = 1;
        if($Intentos->getActivo() == 1){
        	$maxIntentosCobroTDC = $Intentos->getValor();
        }
        $maxDiasPosfecha = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIAS_POSFECHA_CALL');
        $entity = null;
        if(count($entities) > 0)
        {
            $entity = $entities[0];
            if($entity->getPedido()->getEstatus()->getIdEstatus() == 26)
            {
                $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(17);
                $entity->getPedido()->setEstatus($estatus);
            }
            
        }
        if(!$entity)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'No se encontro información de pago con Tarjeta para el pedido: '.$id
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }
        if($entity->getPedido()->getEstatus()->getIdEstatus() != 17)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'El pedido no tiene el estatus correcto para modificar datos de cobro'
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }
        $entities = $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByPedido($id);
        if(count($entities) >= $maxIntentosCobroTDC)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'Limite de intentos de cobro por día superado para el pedido: '.$id
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }

        $pedidoCancelado = new PedidosCancelados();
        $pedidoCancelado->setPedido($entity->getPedido());

        $form               = $this->createEditPagoTarjetaForm($entity);
        $formCancelaPedido  = $this->createCancelForm($pedidoCancelado);

        if ($request->isMethod('POST'))
        {
            $this->get('logger')->info('post');
            $form->submit($request);
            if ($form->isValid() && $entity->getFechaCobro() != null)
            {
                $this->get('logger')->info('formulario valido');
                if($entity->getPosFechar())
                {
                    $this->get('session')->getFlashBag()->add(
                    'success', 'La venta con el no: '.$entity->getPedido()->getIdPedido().' se postfecho.'
                    );

                    return $this->redirect($this->generateUrl('pedidos_new', array('canal' => $entity->getPedido()->getProductos()[0]->getProductoPrecio()->getCanalDistribucion())));
                }
                if($entity->getTitular() == null)
                {
                    $entity->setTitular('');
                }
                $em->flush();
                $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
                $webServicesController->setContainer($this->container);
                $respuestaCobro = $webServicesController->cobrar($entity->getPedido());
                if($respuestaCobro['code'] == 0)
                {
                    $mensajeCobro = $respuestaCobro['message'];

                    $this->get('session')->getFlashBag()->add(
                        'error', $mensajeCobro
                    );
                    return $this->redirect($this->generateUrl('pedidos_valida_tdc', array('id' => $entity->getPedido()->getIdPedido())));
                }
                else
                {
                    $mensajeCobro = 'Se cobro exitosamente, Numero de autorización: '.$respuestaCobro['entity']->getNumeroAutorizacion();
                }

                $this->get('session')->getFlashBag()->add(
                    'success', 'La venta con el no: '.$entity->getPedido()->getIdPedido().' '.$mensajeCobro
                );

                return $this->redirect($this->generateUrl('pedidos_new', array('canal' => $entity->getPedido()->getProductos()[0]->getProductoPrecio()->getCanalDistribucion())));
            }
            else
            {
                $this->get('logger')->info('formulario NO valido');
                if($entity->getFechaCobro() == null)
                {
                    $message = 'Fecha vacía o con formato incorrecto';
                }
                else
                {
                    $message = $form->getErrorsAsString();
                }
                $this->get('logger')->info($message);
                
                $this->get('session')->getFlashBag()->add(
                    'error', $message
                );
            }
        }
        else
        {
            $this->get('logger')->info('no post');
        }

        return array(
            'formPagoTarjeta'   => $form->createView(),
            'formCancelaPedido' => $formCancelaPedido->createView(),
            'pagoTarjeta'       => $entity,
            'maxDiasPosfecha'   => $maxDiasPosfecha,
            );
    }

    /**
     * Cancel an existing Pedidos Pago entity.
     *
     * @Route("/{id}/cancela", name="pedidos_cancelar")
     * @Method({"GET", "POST"})
     * @Template("MDRPuntoVentaBundle:Pedidos:cancel.html.twig")
     */
    public function cancelaAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new PedidosCancelados();
        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        
        if(!$pedido)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'No se encontro el pedido con id: '.$id
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }
        $entity->setPedido($pedido);
        if($entity->getPedido()->getEstatus()->getIdEstatus() != 17)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'El pedido no tiene el estatus correcto para cancelar'
            );
            return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
        }

        $form  = $this->createCancelForm($entity);

        if ($request->isMethod('POST'))
        {
            $this->get('logger')->info('post');
            $form->submit($request);
            if ($form->isValid())
            {
                $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(5);
                $pedido->setEstatus($estatus);
                $entity->setPedidoSAP('');
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'succes', 'El pedido se cancelo correctamente'
                );
                return $this->render('MDRPuntoVentaBundle:Default:index.html.twig');
            }
        }

        return array(
            'form' =>   $form->createView()
        );
    }

    /**
     * Obtener Pedidos por Cliente y exclusión de Estatus.
     *
     * @Route("/clienteNotEstatus", name="pedidos_cliente_not_estatus")
     * @Method({"POST", "GET"})
     * @Template("MDRPuntoVentaBundle:Pedidos:editPagoTarjeta.html.twig")
     */
    public function validarPedidosActivosCliente(Request $request)
    {
        $idCliente = $request->get('cliente');
        $estatus = $request->get('estatus');
        $respuesta = [];
        $respuesta['code'] = 0;
        $respuesta['message'] = '';
        
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByClienteNotEstatus($idCliente, $estatus);
        $respuesta['code'] = 1;
        $respuesta['entities'] = $entities;

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Toma de Flash.
     *
     * @Route("/flash", name="toma_flash")
     * @Method({"GET", "POST"})
     * @Template("MDRPuntoVentaBundle:Pedidos:flash.html.twig")
     */
    public function tomaFlashAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $flashForm = $this->createFlashForm();


        if ($request->isMethod('POST'))
        {
            $flashForm->submit($request);
            if ($flashForm->isValid())
            {
                
            }  
        }
                

        return array(
            'form_fl'   => $flashForm->createView(),
        );



    }


    /**
     * Creates a form to take flash.
     *
     * @param Pedidos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFlashForm(){

        $form = $this->createFormBuilder()
            ->add('nombre', null, array(
                    'required' => true,
                    'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','class' => "form-control",'onkeypress' => 'return sololetras(event)'),
                ))
            ->add('apellidoPaterno',null, array(
                    'required' => true,
                    'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','class' => "form-control",'onkeypress' => 'return sololetras(event)'),
                ))
            ->add('apellidoMaterno', null, array(
                    'required' => false,
                    'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','class' => "form-control",'onkeypress' => 'return sololetras(event)'),
                ))
            ->add('sexo', 'choice', array(
                    'choices'     => array(1 => 'Masculino', 0 => 'Femenino'),
                    'empty_value' => '----------',
                    'attr' => array('class' => "form-control"),
                ))
            ->add('telefono_1', null, array(
                    'label' => 'Telefono',
                    'required' => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10,'onkeypress' => 'return solonum(event)','placeholder' => '8 digitos + clave lada'),
                ))
            ->add('telefono_2', null, array(
                    'label' => 'Telefono adicional',
                    'required' => false,
                    'attr' => array('class' => "form-control",'maxlength' => 10,'onkeypress' => 'return solonum(event)','placeholder' => '8 digitos + clave lada'),
                ))
            ->add('Comentarios', 'text', array(
                    'label' => 'Producto por el cual esta interesado:',
                    'required' => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))

            ->setAction($this->generateUrl('toma_flash'))
            ->setMethod('POST')

            ->getForm();

        return $form;
    }
}
