<?php

namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use MDR\PuntoVentaBundle\Entity\Direcciones;
use MDR\PuntoVentaBundle\Form\DireccionesType;
use MDR\SepomexBundle\Entity\ColoniasSP;

/**
 * Direcciones controller.
 *
 * @Route("/direcciones/{idCliente}")
 */
class DireccionesController extends Controller
{

    /**
     * Lists all Direcciones entities.
     *
     * @Route("/", name="direcciones")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idCliente)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->findByCliente($idCliente);

        return array(
            'entities' => $entities,
			'cliente' => $idCliente,
        );
    }
    /**
     * Creates a new Direcciones entity.
     *
     * @Route("/", name="direcciones_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Direcciones:new.html.twig")
     */
    public function createAction(Request $request, $idCliente)
    {
        $entity = new Direcciones();
        $em = $this->getDoctrine()->getManager();

        $tipoRequest = $_POST['mdr_puntoventabundle_direcciones']['tipoRequest'];
        $idEstado = $_POST['mdr_puntoventabundle_direcciones']['estado'];
        $idMunicipio = $_POST['mdr_puntoventabundle_direcciones']['municipio'];
        $idColonia = $_POST['mdr_puntoventabundle_direcciones']['colonia'];
        if(!($idCliente > 0))
        {
            $idCliente = $_POST['mdr_puntoventabundle_direcciones']['cliente'];
        }

        $this->get('logger')->info('tipoRequest: '.$tipoRequest);

        $municipios = array();
        $colonias = array();
        $estado = $em->getRepository('MDRPuntoVentaBundle:Estados')->find($idEstado);
        $municipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($idMunicipio);
        if($municipio)
            $municipios[] = $municipio;
        
        $colonia = $em->getRepository('MDRPuntoVentaBundle:Colonias')->find($idColonia);
        if($colonia)
            $colonias[] = $colonia;
        $form = $this->createCreateForm($entity, $idCliente, $municipios, $colonias);
        $form->handleRequest($request);

        $code = 0;
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setEstado($entity->getEstado()->getNombre());
            $entity->setMunicipio($entity->getMunicipio()->getNombre());
            $entity->setColonia($entity->getColonia()->getNombre());

            $em->persist($entity);
            $em->flush();

            $code = 1;
            $message = '';
            if($tipoRequest != 'ajax')
            {
                return $this->redirect($this->generateUrl('direcciones_show', array('id' => $entity->getIdDireccion())));
            }

        }
        else
        {
            $message = 'error with form';
        }

        if($tipoRequest == 'ajax')
        {
            $response = new Response(json_encode(array(
                'code' => $code,
                'message' => $message,
                'direccion' => $entity
                )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }else
        {
            return array(
                'entity' => $entity,
                'form'   => $form->createView(),
				'idCliente' => $idCliente,
            );
        }
    }

    /**
     * Creates a form to create a Direcciones entity.
     *
     * @param Direcciones $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Direcciones $entity, $idCliente, $municipios = array(), $colonias = array())
    {
        $em = $this->getDoctrine()->getManager();
        $maxDigitosTelCliente = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIGITOS_TEL_CLIENTE');

        $clientes = [];
        if($idCliente != null)
        {
            $cliente = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($idCliente);
            if($cliente != null)
            {
                $clientes[] = $cliente;
            }
        }

        $form = $this->createForm(new DireccionesType(), $entity, array(
            'action' => $this->generateUrl('direcciones_create', array('idCliente' => $idCliente)),
            'method' => 'POST',
            'clientes'                  => $clientes,
            'municipios'                => $municipios,
            'colonias'                  => $colonias,
            'maxDigitosTelCliente'      => $maxDigitosTelCliente,
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Direcciones entity.
     *
     * @Route("/new", name="direcciones_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idCliente)
    {
        $entity = new Direcciones();
        $form   = $this->createCreateForm($entity, $idCliente);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
			'idCliente' => $idCliente,
        );
    }

    /**
     * Finds and displays a Direcciones entity.
     *
     * @Route("/{id}", name="direcciones_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($idCliente, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Direcciones entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $idCliente);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Direcciones entity.
     *
     * @Route("/{id}/edit", name="direcciones_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $idCliente, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $tipoRequest = $request->get('tipoRequest');

        $entity = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Direcciones entity.');
        }

        $estados = $em->getRepository('MDRPuntoVentaBundle:Estados')->findByNombre($entity->getEstado());
        $municipios = [];
        $colonias = [];
        if(count($estados) > 0)
        {
            $estado = $estados[0];
            $entity->setEstado($estado);
            
            $municipios = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findBy(array('estado' => $estado), array('nombre' => 'ASC'));
            $idMunicipio = 0;
            foreach ($municipios as $municipio) {
                if(!strcasecmp($municipio->getNombre(), $entity->getMunicipio()))
                {
                    $idMunicipio = $municipio->getIdMunicipio();
                    $entity->setMunicipio($municipio);
                }
            }
            $colonias = [];
            if($idMunicipio > 0)
            {
                $idColonia = 0;
                $colonias = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findBy(array('municipio' => $idMunicipio), array('nombre' => 'ASC'));
                foreach ($colonias as $colonia) {
                    if(!strcasecmp($colonia->getNombre(), $entity->getColonia()))
                    {
                        $idColonia = $colonia->getIdColonia();
                        $entity->setColonia($colonia);
                    }
                }
            }
        }

        $editForm = $this->createEditForm($entity, $idCliente, $municipios, $colonias);
        $editForm->get('tipoRequest')->setData($tipoRequest);
        $deleteForm = $this->createDeleteForm($id, $idCliente);
		

        if($tipoRequest == 'alone')
        {
            return $this->render('MDRPuntoVentaBundle:Direcciones:editAlone.html.twig',array(
                'edit_form'     => $editForm->createView(),
                'sinMenu'       => true,
            ));
        }

        return array(
            'entity'        => $entity,
            'edit_form'     => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
			'idCliente' => $idCliente,
        );
    }

    /**
    * Creates a form to edit a Direcciones entity.
    *
    * @param Direcciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Direcciones $entity, $idCliente, $municipios = array(), $colonias = array())
    {
        $em = $this->getDoctrine()->getManager();
        $maxDigitosTelCliente = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIGITOS_TEL_CLIENTE');

        $clientes = [];
        if($idCliente != null)
        {
            $cliente = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($idCliente);
            if($cliente != null)
            {
                $clientes[] = $cliente;
            }
        }

        $form = $this->createForm(new DireccionesType(), $entity, array(
            'action' => $this->generateUrl('direcciones_update', array('idCliente' => $idCliente, 'id' => $entity->getIdDireccion())),
            'method' => 'PUT',
            'clientes'                  => $clientes,
            'municipios'                => $municipios,
            'colonias'                  => $colonias,
            'maxDigitosTelCliente'      => $maxDigitosTelCliente,
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }
    /**
     * Edits an existing Direcciones entity.
     *
     * @Route("/{id}", name="direcciones_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Direcciones:edit.html.twig")
     */
    public function updateAction(Request $request, $idCliente, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Direcciones entity.');
        }

        $tipoRequest = $_POST['mdr_puntoventabundle_direcciones']['tipoRequest'];
        $idEstado = $_POST['mdr_puntoventabundle_direcciones']['estado'];
        $idMunicipio = $_POST['mdr_puntoventabundle_direcciones']['municipio'];
        $idColonia = $_POST['mdr_puntoventabundle_direcciones']['colonia'];

        $municipios = array();
        $colonias = array();
        $estado = $em->getRepository('MDRPuntoVentaBundle:Estados')->find($idEstado);
        $municipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($idMunicipio);
        if($municipio)
            $municipios[] = $municipio;
        
        $colonia = $em->getRepository('MDRPuntoVentaBundle:Colonias')->find($idColonia);
        if($colonia)
            $colonias[] = $colonia;

        $deleteForm = $this->createDeleteForm($id, $idCliente);
        $editForm = $this->createEditForm($entity, $idCliente, $municipios, $colonias);
        $editForm->get('tipoRequest')->setData($tipoRequest);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success', 'Dirección actualizada correctamente.'
            );

            if($tipoRequest == 'alone')
            {
                return $this->redirect($this->generateUrl('direcciones_edit', array('id' => $id, 'idCliente' => $idCliente)).'?tipoRequest='.$tipoRequest);
            }

            return $this->redirect($this->generateUrl('direcciones_edit', array('id' => $id, 'idCliente' => $idCliente)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Direcciones entity.
     *
     * @Route("/{id}", name="direcciones_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $idCliente, $id)
    {
        $form = $this->createDeleteForm($id, $idCliente);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Direcciones entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('direcciones'));
    }

    /**
     * Creates a form to delete a Direcciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($idCliente, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('direcciones_delete', array('idCliente' => $idCliente, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }

    /**
     * @Route("/id/{id}", name="direcciones_by_id")
     * @Method({"GET", "POST"})
     */
    public function getById(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id);

        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => "",
            'entity' => $entity
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}