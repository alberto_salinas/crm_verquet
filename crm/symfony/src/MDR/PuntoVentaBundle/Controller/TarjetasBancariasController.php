<?php

namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MDR\PuntoVentaBundle\Entity\Bin;
use MDR\PuntoVentaBundle\Entity\BinTerminal;
use MDR\PuntoVentaBundle\Entity\Bancos;
use MDR\PuntoVentaBundle\Entity\Terminales;
use MDR\PuntoVentaBundle\Entity\TipoTarjeta;

/**
 * TarjetasBancarias controller.
 *
 * @Route("/tarjetas")
 */
class TarjetasBancariasController extends Controller
{
    public function validar($tc)
    {
        $code = 1;
        $message = '';
        $banco = null;
        $terminal = null;
        $tipoTarjeta = null;
        $promociones = null;
        $this->get('logger')->debug($tc);
        
        if(strlen($tc) < 15)
        {
            $code = 0;
            $message = 'TDC inválida FI';
        }
        else
        {
            if($tc[0] == 3)
            {
                if(strlen($tc) != 15)
                {
                    $code = 0;
                    $message = 'TDC inválida AM';
                }
            }
            else
            {
                if(strlen($tc) != 16)
                {
                    $code = 0;
                    $message = 'TDC inválida V/M';
                }
            }
        }

        $em = $this->getDoctrine()->getManager();
        if($code == 1)
        {
            $prefijo = substr($tc,0, 6);
            $binTerminal = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($prefijo);
            if($binTerminal)
            {
                $banco = $binTerminal->getBanco();
                $terminal = $binTerminal->getTerminal();
                $tipoTarjeta = $binTerminal->getTipoTarjeta();
                $promociones = $binTerminal->getPromociones()->toArray();
            }
            else
            {
                $strTerminal = 'BANCOMER VIRTUAL';
                $tipoT = 'VISA';
                switch ($tc[0]) {
                    case 3:
                        $banco = $em->getRepository('MDRPuntoVentaBundle:Bancos')->findOneByNombre('AMEX');
                        $tipoT = 'AMERICAN EXPRESS';
                        $strTerminal = 'American Express TPV';
                        break;
                    case 4:
                        $tipoT = 'VISA';
                        break;
                    case 5:
                        $tipoT = 'MASTERCARD';
                        break;
                }
                $tipoTarjeta = $em->getRepository('MDRPuntoVentaBundle:TipoTarjeta')->findOneByDescripcion($tipoT);
                $terminal = $em->getRepository('MDRPuntoVentaBundle:Terminales')->findOneByDescripcion($strTerminal);
            }
        }
        $promocionDefault = $em->getRepository('MDRPuntoVentaBundle:Promociones')->find(1);
        if($promociones == null)
        {
            $promociones = array($promocionDefault);
        }
        else
        {
            $promociones[] = $promocionDefault;
        }

        return array(
            'code'          => $code,
            'message'       => $message,
            'banco'         => $banco,
            'terminal'      => $terminal,
            'tipoTarjeta'   => $tipoTarjeta,
            'promociones'   => $promociones,
            );
    }

    /**
     * Validate Tarjeta
     * @Route("/validar", name="tarjetas_validar")
     */
    public function validarAction(Request $request)
    {
        $tc = $request->get('tc');
        
        $respuesta = $this->validar($tc);

    	$response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
