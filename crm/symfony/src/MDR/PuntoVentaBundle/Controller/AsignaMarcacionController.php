<?php

namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\AsignaMarcacion;
use MDR\PuntoVentaBundle\Entity\OficinasVentas;

/**
 * Asigna marcación controller.
 *
 * @Route("/asignaMarcacion")
 */
class AsignaMarcacionController extends Controller
{
    /**
     *
     *
     * @Route("/usuario/{canal}", name="asignados")
     * @Method({"GET"})
     * @Template()
     */
    public function byUsuarioAction(Request $request, $canal)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('MDRPuntoVentaBundle:AsignaMarcacion')->findByUsuario($user->getUClave(), $canal);

        return array(
            'entities'      => $entities,
        );
    }

    /**
     *
     *
     * @Route("/carga/{canal}", name="asigna_marcacion_carga")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function cargaAction(Request $request, $canal)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createOficinaVentasForm();
        $entities = [];

        if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $filePath = sys_get_temp_dir();
                $fileName = $tmpfname = tempnam($filePath, "CRM");
                $fileName = str_replace(strtolower($filePath), '', strtolower($fileName));
                $fileName = str_replace('/','', str_replace('\\', '', $fileName));
                $fileName .= '.xls';
                $form['archivo']->getData()->move($filePath, $fileName);
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filePath.'\\'.$fileName);
                
                $sheet = $phpExcelObject->setActiveSheetIndex(0);

                $organizacionVentas = $em->getRepository('MDRPuntoVentaBundle:OrganizacionVentas')->find('0100');
                $canalDistribucion = $em->getRepository('MDRPuntoVentaBundle:CanalDistribucion')->find($canal);
                $oficinaVentas = $form['oficinaVentas']->getData();
                
                $row = 2;
                $hayDatos = true;

                while ($hayDatos) {
                    $hayDatos = strlen($sheet->getCell('A'.$row)) > 0;
                    if($hayDatos)
                    {
                        $asignaMarcacion = new AsignaMarcacion();
                        $asignaMarcacion->setOrganizacionVentas($organizacionVentas);
                        $asignaMarcacion->setCanalDistribucion($canalDistribucion);
                        $asignaMarcacion->setOficinaVentas($oficinaVentas);
                        $asignaMarcacion->setUsuario($sheet->getCell('A'.$row)->getValue());
                        $asignaMarcacion->setTelefono($sheet->getCell('B'.$row)->getValue());
                        $asignaMarcacion->setTelefono2($sheet->getCell('C'.$row)->getValue());
                        $asignaMarcacion->setProducto($sheet->getCell('D'.$row)->getValue());
                        $asignaMarcacion->setFecha(new \DateTime());
                        try {
                            $fecha = \DateTime::createFromFormat('d/m/Y H:i', $sheet->getCell('E'.$row)->getValue());
                            if($fecha)
                            {
                                $asignaMarcacion->setFecha($fecha);
                            }
                        } catch (\Exception $e) {
                            
                        }
                        $asignaMarcacion->setNombreCliente($sheet->getCell('F'.$row)->getValue());
                        $asignaMarcacion->setComentarios($sheet->getCell('E'.$row)->getValue());
                        $asignaMarcacion->setAtendido(false);

                        $entities[] = $asignaMarcacion;
                        
                        $em->persist($asignaMarcacion);
                        $em->flush();
                    }
                    $row++;
                }
            }
        }

        return array(
            'form'      => $form->createView(),
            'entities'  => $entities,
            );
    }

    /**
     * Crear formulario para seleccionar Oficina Ventas y Excel para cargar.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createOficinaVentasForm()
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('archivo', 'file', array(
                'label'     => 'Excel',
                'required'  => true,
                ))
            ->add('oficinaVentas', 'entity', array(
                'class'         => 'MDRPuntoVentaBundle:OficinasVentas',
                'label'         => 'Oficina de Ventas',
                'empty_value'   => '-----------',
                'required'      => true,
                'attr' => array('class' => "form-control"),
            ))
            ->add('Enviar', 'submit', array(
                'label'     => 'Guardar'
                ))
            ->getForm();

        return $form;
    }
}