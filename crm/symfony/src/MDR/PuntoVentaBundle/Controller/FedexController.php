<?php

namespace MDR\PuntoVentaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MDR\PuntoVentaBundle\Entity\PaqueteriaFedex;

/**
 * Fedex controller.
 *
 * @Route("/fedex")
 */
class FedexController extends Controller
{
    /**
     * @Route("/byCP", name="fedex_by_cp")
     * @Method("POST")
     */
    public function byCPAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	$cp = $request->get('cp');
    	$entities = $em->getRepository('MDRPuntoVentaBundle:PaqueteriaFedex')->findByCp($cp);
    	$code = 1;
    	$message = '';

        $response = new Response(json_encode(array(
                'code' => $code,
                'message' => $message,
                'entities' => $entities
                )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
