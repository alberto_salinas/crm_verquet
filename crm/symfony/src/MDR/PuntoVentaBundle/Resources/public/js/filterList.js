var Descuento = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		idDescuento: 0,
		clave:   '',
		desde:   0,
		hasta:   0,
		importe: 0,
		tipoPago: {idTipoPago: 0, descripcion: ""}
	}
});

var DescuentosCollection = Backbone.Collection.extend({
	model: Descuento
});

var Producto = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		idProducto: 0,
		claveSAP: '',
		descripcion: ''
	}
});

var ProductoPrecio = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		idProductoPrecio: 0,
		precio: 0,
		moneda: "",
		producto: new Producto,
		tipoPago: {idTipoPago: 0, descripcion: ""},
		tipoPromocion: 1
	}
});

var ProductoCompra = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		cantidad: 0,
		item: new ProductoPrecio
	},
	add: function()
	{
		this.save({cantidad: this.get("cantidad") + 1});
	},
	sub: function()
	{
		if(this.get("cantidad") == 1)
		{
			this.destroy();
		}
		else
		{
			this.save({cantidad: this.get("cantidad") - 1});
		}
	}
});


var ProductosCollection = Backbone.Collection.extend({
	model: ProductoPrecio,
	//localStorage: new Backbone.LocalStorage("puntoVentaProductosBackbone"),
	byDescripcion: function(desc) {
		filtered = this.filter(function(productPrecio) {
			var filter = new RegExp(desc, "i");
			return filter.test(productPrecio.get('producto').descripcion);
		});
		return new ProductosCollection(filtered);
	},
	byDescripcionPrecio: function(test) {
		filtered = this.filter(function(productPrecio) {
			var filterDesc = new RegExp(test, "i");
			return filterDesc.test(productPrecio.get('producto').descripcion) || parseFloat(productPrecio.get('precio')) == parseFloat(test);
		});
		return new ProductosCollection(filtered);
	}
});

var CarritoCollection = Backbone.Collection.extend({
	model: ProductoCompra,
	//localStorage: new Backbone.LocalStorage("puntoVentaComprasBackbone"),
	agregarProd: function(productoPrecio)
	{
		filtered = this.filter(function(item) {
			return item.get("item").idProductoPrecio == productoPrecio.get("idProductoPrecio");
		});
		if(filtered.length > 0)
		{
			filtered[0].set("cantidad", (filtered[0].get("cantidad") + 1));
		}
		else
		{
			this.add({cantidad: 1, item: productoPrecio.toJSON()});
		}
	}
});

var productos = new ProductosCollection();
var productosCompra = new CarritoCollection(productosCompraJSON);
var descuentos = new DescuentosCollection();
var descuentosCompra = new DescuentosCollection();

var ProductoView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#productoTemplate').html()),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.el["data-id"] = this.model.get('idProducto');
		this.$el.html(this.template(this.model.toJSON()));
		this.btnAdd = this.$("button");

		return this;
	},
	events: {
		'click button'	: 'add'
	},
	add: function(){
		productosCompra.agregarProd(this.model);
	}
})

var ProductoCompraView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#productoCompraTemplate').html()),
	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
		this.render();
	},
	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		this.btnRemove = this.$("button");

		return this;
	},
	events: {
		'click button.agregar'	: 'add',
		'click button.quitar'	: 'minus'
	},
	add: function(){
		var cantidad = this.model.get("cantidad");
		this.model.set("cantidad", ++cantidad);
	},
	minus: function(){
		var cantidad = this.model.get("cantidad");
		if(cantidad > 1)
		{
			this.model.set("cantidad", --cantidad);
		}
		else
		{
			this.model.destroy();
			this.remove();
		}
	}
})

var DescuentoView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#descuentoTemplate').html()),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.el["data-id"] = this.model.get('idDescuento');
		this.$el.html(this.template(this.model.toJSON()));
		this.btnAdd = this.$("button");

		return this;
	},
	events: {
		'click button'	: 'add'
	},
	add: function(){
		if(descuentosCompra.length == 0)
		{
			descuentosCompra.add(this.model.clone());
		}
		else
		{
			alert('Ya hay un descuento seleccionado.');
		}
	}
})

var DescuentoCompraView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#descuentoCompraTemplate').html()),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.el["data-id"] = this.model.get('idDescuento');
		this.$el.html(this.template(this.model.toJSON()));
		this.btnAdd = this.$("button");

		return this;
	},
	events: {
		'click button'	: 'minus'
	},
	minus: function(){
		this.model.destroy();
		this.remove();
	}
})


var CarritoView = Backbone.View.extend({
	el: $("#carrito"),
	events: {
	 "keyup input.search":  "filtrarProductos"
	},
	initialize: function() {			
		this.productosDisponibles = this.$(".productosDisponibles");
		this.productosSeleccionados = this.$(".productosSeleccionados");
		this.descuentoSeleccionado = null;
		this.importe = this.$("#total");
		this.subTotal = $("#mdr_puntoventabundle_pedidos_subTotal");
		this.total = $("#mdr_puntoventabundle_pedidos_totalPago");
		this.tipoPago = $("#mdr_puntoventabundle_pedidos_pago");
		this.gastosEnvio = $("#mdr_puntoventabundle_pedidos_gastosEnvio");
		this.render();
		this.search = this.$("input.search");
		this.clienteInfo = $("#clienteInfoPanel");
		this.shortInfoCliente = $("#shortInfoCliente");
		this.direccionInfo = $("#direccionInfoPanel");
		this.clienteInfoTemplate = _.template($('#datosClienteTemplate').html());
		this.direccionInfoTemplate = _.template($('#datosDireccionTemplate').html());

		this.listenTo(productos, 'add', this.addProducto);
		this.listenTo(productos, 'reset', this.reset);
		this.listenTo(productosCompra, 'add', this.addProductoCompra);
		this.listenTo(productosCompra, 'remove', this.totalCompra);
		this.listenTo(productosCompra, 'reset', this.reset);
		this.listenTo(productosCompra, 'change', this.totalCompra);
		this.listenTo(descuentosCompra, 'add', this.addDescuentoCompra);
		this.listenTo(descuentosCompra, 'remove', this.minusDescuentoCompra);
	},

	render: function() {
		var childs = this.productosDisponibles.children();
		for (var i = childs.length - 1; i >= 0; i--) {
			childs[i].remove();
		};
		childs = this.productosSeleccionados.children();
		for (var i = childs.length - 1; i >= 0; i--) {
			childs[i].remove();
		};

		this.addAllProductos(productos);
		this.addAllProductosCompra(productosCompra);
		this.addAllDescuentos(descuentos);
	},
	showCliente: function(cliente)
	{
		var htmlContent = '';
		if(cliente != null)
		{
			htmlContent = this.clienteInfoTemplate(cliente);
			$(this.shortInfoCliente).html(cliente.toString);
			$("#editarCliente").show();
		}
		else
		{
			$(this.shortInfoCliente).html("");
			$("#editarCliente").hide();
		}
		$(this.clienteInfo).html(htmlContent);
	},
	showDireccion: function(direccion)
	{
		var htmlContent = '';
		if(direccion != null)
		{
			htmlContent = this.direccionInfoTemplate(direccion);
			$("#editarDireccion").show();
		}
		else
		{
			$("#editarDireccion").hide();
		}
		$(this.direccionInfo).html(htmlContent);
	},
	addProducto: function(item)
	{
		var agregar = false;
		var tipoPago = this.tipoPago.val();
		var esEntrante = "";
		if(tipoPago == 1)
		{
			agregar = true;
		}
		else if(tipoPago == item.get('tipoPago').idTipoPago)
		{
			agregar = true;
		}
		if(item.get('tipoPromocion') != 1 && agregar)
		{
			agregar = false;
			esEntrante = document.getElementById("mdr_puntoventabundle_pedidos_esEntrante").value;
			if(esEntrante != "")
			{
				if(esEntrante == 0)
				{
					agregar = true;
				}
			}
		}
		if(agregar)
		{
			var view = new ProductoView({model: item});
	  		this.productosDisponibles.append(view.render().el);
		}
	},
	addDescuento: function(item)
	{
		var agregar = false;
		var tipoPago = this.tipoPago.val();
		if(tipoPago == item.get('tipoPago').idTipoPago)
		{
			agregar = true;
		}
		if(agregar)
		{
			var view = new DescuentoView({model: item});
  			this.productosDisponibles.append(view.render().el);
		}
	},
	addAllProductos: function(items)
	{
		items.each(this.addProducto, this);
	},
	addProductoCompra: function(item)
	{
		var view = new ProductoCompraView({model: item});
  		this.productosSeleccionados.append(view.render().el);
  		this.totalCompra();
	},
	addAllProductosCompra: function(items)
	{
		items.each(this.addProductoCompra, this);
	},
	addAllDescuentos: function(items)
	{
		items.each(this.addDescuento, this);
	},
	getSubTotal: function()
	{
		var subTotal = 0;
		productosCompra.each(function(item){
			subTotal += item.get("cantidad") * item.get("item").precio;
		}, productosCompra)	
		return subTotal;
	},
	getDescuentoTotal: function()
	{
		var totalDescuento = 0;
		var self = this;
		descuentosCompra.each(function(item){
			if(self.validarDescuento(item))
			{
				totalDescuento += item.get("importe");
			}
			else
			{
				item.destroy();
				self.render();
			}
		}, descuentosCompra)
		return totalDescuento;
	},
	getTotal: function()
	{
		var total = this.getSubTotal() - this.getDescuentoTotal();
		total += getGastosEnvioSelected();

		return total;
	},
	totalCompra: function()
	{
		var descuentoTotal = this.getDescuentoTotal();
		var subTotal = this.getSubTotal() - descuentoTotal;
		var total = this.getTotal();
		
		this.importe.text(accounting.formatMoney(subTotal));
		this.subTotal.val(accounting.formatMoney(subTotal));
		this.total.val(accounting.formatMoney(total));
		validarMaxTotal();
	},
	validarDescuento: function(item)
	{
		var subTotal = this.getSubTotal();
		return item.get('desde') <= subTotal;
	},
	vaciarCarrito: function()
	{
		descuentosCompra.reset();
		productosCompra.reset();
	},
	addDescuentoCompra: function(item)
	{
		if(this.validarDescuento(item))
		{
			if(this.tipoPago.children("[value=" + item.get("tipoPago").idTipoPago + "]").length == 1)
			{
		  		//this.tipoPago.attr("disabled", "");
				var view = new DescuentoCompraView({model: item});
		  		this.productosSeleccionados.prepend(view.render().el);
		  		this.totalCompra();
			}
			else
			{
				item.destroy();
				alert("No cuenta con forma de pago: " + item.get("tipoPago").descripcion + " para este pedido.");
			}
		}
		else
		{
			item.destroy();
			alert('Descuento no válido.');
		}
	},
	minusDescuentoCompra: function()
	{
		this.tipoPago.removeAttr("disabled");
		this.totalCompra();
	},
	reset: function()
	{
		this.render();
		this.totalCompra();
	},

	filtrarProductos: function()
	{
		var text = this.search.val();
		var items;
		var nodes;

		productos = new ProductosCollection(productosDid);
		if(text.length > 0)
		{
			productos = productos.byDescripcionPrecio(text);
		}
		this.render()
	}

});
