<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('apellidoPaterno',null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('apellidoMaterno', null, array(
                'required' => false,
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('fechaNacimiento', 'date', array(
                "read_only" => false,                
                "required" => false,                
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('onkeyup'=>'return makedate(event)',"pleaceholder" => "DD/MM/AAAA")
                ))
            ->add('sexo', 'choice', array(
                'choices'     => array(1 => 'Masculino', 0 => 'Femenino'),
                'empty_value' => '----------'
                ))
            ->add('email', 'email', array(
                'required' => false,
                'attr' => array('maxlength' => 50,'onkeyup'=>'javascript:this.value=this.value.toUpperCase();'),
                ))
            ->add('tipoRequest', 'hidden', array(
                'mapped'    => false,
            ))
            ->add('telefono', 'hidden', array(
                'mapped'    => false,
                'required'  => false,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'MDR\PuntoVentaBundle\Entity\Clientes',
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_clientes';
    }
}
