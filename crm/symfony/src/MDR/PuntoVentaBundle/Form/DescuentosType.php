<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DescuentosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave')
            ->add('desde')
            ->add('hasta')
            ->add('importe')
			//->add('activo')
			//->add('origenes', null, array('label' => 'Oficina de Ventas'))     
			->add('tipoPago');  
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\Descuentos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_descuentos';
    }
}
