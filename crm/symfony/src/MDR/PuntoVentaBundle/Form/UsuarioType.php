<?php

namespace MDR\PuntoVentaBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uClave', null, array(
                'label' => 'Usuario',
                'required' => true,
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();'),
                ))
            //->add('uPassword')
            ->add('uNombre', null, array(
                'label' => 'Nombre',
                'required' => true,
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('uApPaterno', null, array(
                'label' => 'A. Paterno',
                'required' => true,
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('uApMaterno', null, array(
                'label' => 'A. Materno',
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','onkeypress' => 'return sololetras(event)'),
                ))
            ->add('urfc', null, array(
                'label' => 'RFC',
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();'),
                ))
            ->add('horario', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Horarios',
                    'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                            ->where("e.activo IN(:estatusIds)")
                            ->setParameter('estatusIds', [1]);
                            },
                    'required' => true,
                    'empty_value' => "-----------",
                ))
            ->add('uFechaNac', 'date', array('label' => 'Fecha de Nacimiento',
                "read_only" => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'))
            /*
            ->add('ufechaalta', 'hidden', array(
                "read_only" => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'))
            ->add('ufechamodificacion', 'hidden', array(
                "read_only" => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'))
            ->add('unumerosupervisor')
            ->add('ufechaalta')
            ->add('idCampana')
            ->add('idCentro')
            ->add('activo')
            ->add('eUser')
            ->add('ePass')
            ->add('eAgent')*/
            ->add('email', null, array('label' => 'Email','required' => false,))
            ->add('tipoUsuario', null, array('label' => 'Tipo de Usuario',
                'required' => true,
                'empty_value' => '-----------',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.valor', 'ASC');
                },
            ))
           // ->add('pedidosRecuperacion')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_usuario';
    }
}
