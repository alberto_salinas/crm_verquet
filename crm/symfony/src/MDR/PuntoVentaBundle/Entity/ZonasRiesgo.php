<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ZonasRiesgo
 */
class ZonasRiesgo implements JsonSerializable
{
    /**
     * @var string
     */
    private $Municipio;

    /**
     * @var string
     */
    private $CP;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idZonaRiesgo;


    /**
     * Set Municipio
     *
     * @param string $municipio
     * @return ZonasRiesgo
     */
    public function setMunicipio($municipio)
    {
        $this->Municipio = $municipio;

        return $this;
    }

    /**
     * Get Municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->Municipio;
    }

    /**
     * Set CP
     *
     * @param string $cP
     * @return ZonasRiesgo
     */
    public function setCP($cP)
    {
        $this->CP = $cP;

        return $this;
    }

    /**
     * Get CP
     *
     * @return string 
     */
    public function getCP()
    {
        return $this->CP;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return ZonasRiesgo
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idZonaRiesgo
     *
     * @return integer 
     */
    public function getIdZonaRiesgo()
    {
        return $this->idZonaRiesgo;
    }
    
    public function jsonSerialize()
    {
        return array(

            'Municipio'     => $this->Municipio,
            'CP'            => $this->CP,
            'Activo'        => $this->Activo,
            'idZonaRiesgo'  => $this->idZonaRiesgo,
                       
        );

    }
}
