<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetMenus
 */
class IntranetMenus implements JsonSerializable
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaEdicion;

    /**
     * @var integer
     */
    private $idMenu;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $menuHijos;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuarioEdita;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuarioCrea;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetContenidos
     */
    private $contenido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetMenus
     */
    private $menuPadre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menuHijos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set url
     *
     * @param string $url
     * @return IntranetMenus
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return IntranetMenus
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return IntranetMenus
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return IntranetMenus
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;

        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }

    /**
	* Get idMenu
     *
     * @return integer 
     */
    public function getIdMenu()
    {
        return $this->idMenu;
    }

	/**
     * Get idMenu
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->idMenu;
    }

    /**
     * Add menuHijos
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetMenus $menuHijos
     * @return IntranetMenus
     */
    public function addMenuHijo(\MDR\PuntoVentaBundle\Entity\IntranetMenus $menuHijos)
    {
        $this->menuHijos[] = $menuHijos;

        return $this;
    }

    /**
     * Remove menuHijos
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetMenus $menuHijos
     */
    public function removeMenuHijo(\MDR\PuntoVentaBundle\Entity\IntranetMenus $menuHijos)
    {
        $this->menuHijos->removeElement($menuHijos);
    }

    /**
     * Get menuHijos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMenuHijos()
    {
        return $this->menuHijos;
    }

    /**
     * Set usuarioEdita
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuarioEdita
     * @return IntranetMenus
     */
    public function setUsuarioEdita(\MDR\PuntoVentaBundle\Entity\Usuario $usuarioEdita = null)
    {
        $this->usuarioEdita = $usuarioEdita;

        return $this;
    }

    /**
     * Get usuarioEdita
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuarioEdita()
    {
        return $this->usuarioEdita;
    }

    /**
     * Set usuarioCrea
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuarioCrea
     * @return IntranetMenus
     */
    public function setUsuarioCrea(\MDR\PuntoVentaBundle\Entity\Usuario $usuarioCrea = null)
    {
        $this->usuarioCrea = $usuarioCrea;

        return $this;
    }

    /**
     * Get usuarioCrea
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuarioCrea()
    {
        return $this->usuarioCrea;
    }

    /**
     * Set contenido
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetContenidos $contenido
     * @return IntranetMenus
     */
    public function setContenido(\MDR\PuntoVentaBundle\Entity\IntranetContenidos $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetContenidos 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set menuPadre
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetMenus $menuPadre
     * @return IntranetMenus
     */
    public function setMenuPadre(\MDR\PuntoVentaBundle\Entity\IntranetMenus $menuPadre = null)
    {
        $this->menuPadre = $menuPadre;

        return $this;
    }

    /**
     * Get menuPadre
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetMenus 
     */
    public function getMenuPadre()
    {
        return $this->menuPadre;
    }

    public function jsonSerialize()
    {
        $menuPadre = $this->getMenuPadre();
        $idMenuPadre = null;
        if($menuPadre != null)
        {
            $idMenuPadre = $menuPadre->getIdMenu();
		}
        return array(
            'idMenu'            => $this->idMenu,
            'idMenuPadre'       => $idMenuPadre,
            'url'               => $this->url,
            'nombre'            => $this->nombre,
            'fechaCreacion'     => $this->fechaCreacion,
            'fechaEdicion'      => $this->fechaEdicion,
            'usuarioCrea'       => $this->usuarioCrea,
            'usuarioEdita'      => $this->usuarioEdita,
            'contenido'         => $this->getContenido(),
        );
	}
}