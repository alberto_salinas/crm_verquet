<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrganizacionVentas
 */
class OrganizacionVentas
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $claveSAP;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrganizacionVentas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }
}
