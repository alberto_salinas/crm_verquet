<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetMensajeInbox
 */
class IntranetMensajeInbox implements JsonSerializable
{
    /**
     * @var string
     */
    private $Asunto;

    /**
     * @var string
     */
    private $Cuerpo;

    /**
     * @var \DateTime
     */
    private $Fecha;

    /**
     * @var integer
     */
    private $idMensaje;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $Emisor;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Inbox;

    /**
     * Set Asunto
     *
     * @param string $asunto
     * @return IntranetMensajeInbox
     */
    public function setAsunto($asunto)
    {
        $this->Asunto = $asunto;

        return $this;
    }

    /**
     * Get Asunto
     *
     * @return string 
     */
    public function getAsunto()
    {
        return $this->Asunto;
    }

    /**
     * Set Cuerpo
     *
     * @param string $cuerpo
     * @return IntranetMensajeInbox
     */
    public function setCuerpo($cuerpo)
    {
        $this->Cuerpo = $cuerpo;

        return $this;
    }

    /**
     * Get Cuerpo
     *
     * @return string 
     */
    public function getCuerpo()
    {
        return $this->Cuerpo;
    }

    /**
     * Set Fecha
     *
     * @param \DateTime $fecha
     * @return IntranetMensajeInbox
     */
    public function setFecha($fecha)
    {
        $this->Fecha = $fecha;

        return $this;
    }

    /**
     * Get Fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->Fecha;
    }

    /**
     * Get idMensaje
     *
     * @return integer 
     */
    public function getIdMensaje()
    {
        return $this->idMensaje;
    }

    /**
     * Set Emisor
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $emisor
     * @return IntranetMensajeInbox
     */
    public function setEmisor(\MDR\PuntoVentaBundle\Entity\Usuario $emisor = null)
    {
        $this->Emisor = $emisor;

        return $this;
    }

    /**
     * Get Emisor
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getEmisor()
    {
        return $this->Emisor;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Inbox = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add Inbox
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetInbox $inbox
     * @return IntranetMensajeInbox
     */
    public function addInbox(\MDR\PuntoVentaBundle\Entity\IntranetInbox $inbox)
    {
        $this->Inbox[] = $inbox;

        return $this;
    }

    /**
     * Remove Inbox
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetInbox $inbox
     */
    public function removeInbox(\MDR\PuntoVentaBundle\Entity\IntranetInbox $inbox)
    {
        $this->Inbox->removeElement($inbox);
    }

    /**
     * Get Inbox
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInbox()
    {
        return $this->Inbox;
    }

    public function __GregorianDateTime()
    {
        if($this->Fecha != null)
        {
            return date_format($this->Fecha,'d-m-Y  H:i:s');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y  H:i:s');
        }
    }
    
    public function jsonSerialize()
    {
        return array(

            'Asunto'            => $this->Asunto,
            'Cuerpo'            => $this->Cuerpo,
            'Fecha'             => $this->Fecha,
            'idMensaje'         => $this->idMensaje,
            'Emisor'            => $this->Emisor,
            'Inbox'             => $this->Inbox->toArray(),
            'fechaGregorian'    => $this->__GregorianDateTime(),
                       
        );

    }
    
}
