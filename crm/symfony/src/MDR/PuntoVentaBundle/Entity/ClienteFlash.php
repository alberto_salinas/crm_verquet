<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ClienteFlash
 */
class ClienteFlash implements JsonSerializable
{
    /**
     * @var \DateTime
     */
    private $Fecha;

    /**
     * @var integer
     */
    private $idFlash;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Telefonos
     */
    private $Telefono;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Clientes
     */
    private $Cliente;

    /**
     * @var string
     */
    private $Dnis;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var string
     */
    private $Comentarios;

    /**
     * Set Fecha
     *
     * @param \DateTime $fecha
     * @return ClienteFlash
     */
    public function setFecha($fecha)
    {
        $this->Fecha = $fecha;

        return $this;
    }

    /**
     * Get Fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->Fecha;
    }

    /**
     * Get idFlash
     *
     * @return integer 
     */
    public function getIdFlash()
    {
        return $this->idFlash;
    }

    /**
     * Set Telefono
     *
     * @param \MDR\PuntoVentaBundle\Entity\Telefonos $telefono
     * @return ClienteFlash
     */
    public function setTelefono(\MDR\PuntoVentaBundle\Entity\Telefonos $telefono = null)
    {
        $this->Telefono = $telefono;

        return $this;
    }

    /**
     * Get Telefono
     *
     * @return \MDR\PuntoVentaBundle\Entity\Telefonos 
     */
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set Cliente
     *
     * @param \MDR\PuntoVentaBundle\Entity\Clientes $cliente
     * @return ClienteFlash
     */
    public function setCliente(\MDR\PuntoVentaBundle\Entity\Clientes $cliente = null)
    {
        $this->Cliente = $cliente;

        return $this;
    }

    /**
     * Get Cliente
     *
     * @return \MDR\PuntoVentaBundle\Entity\Clientes 
     */
    public function getCliente()
    {
        return $this->Cliente;
    }

    /**
     * Set Dnis
     *
     * @param string $dnis
     * @return ClienteFlash
     */
    public function setDnis($dnis)
    {
        $this->Dnis = $dnis;

        return $this;
    }

    /**
     * Get Dnis
     *
     * @return string 
     */
    public function getDnis()
    {
        return $this->Dnis;
    }

    /**
     * Set Usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return ClienteFlash
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }
    


    /**
     * Set Comentarios
     *
     * @param string $comentarios
     * @return ClienteFlash
     */
    public function setComentarios($comentarios)
    {
        $this->Comentarios = $comentarios;

        return $this;
    }

    /**
     * Get Comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->Comentarios;
    }

    public function jsonSerialize()
    {
        return array(
            
            'Fecha'         => $this->Fecha,
            'idFlash'       => $this->idFlash,
            'Telefono'      => $this->Telefono,
            'Cliente'       => $this->Cliente,
            'Dnis'          => $this->Dnis,
            'Usuario'       => $this->Usuario,
            'Comentarios'   => $this->Comentarios,
                       
        );

    }
    


    
}


