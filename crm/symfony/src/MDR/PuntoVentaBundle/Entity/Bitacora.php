<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Bitacora
 */
class Bitacora implements JsonSerializable
{
    /**
     * @var \Date
     */
    private $fecha;

    /**
     * @var string
     */
    private $areaOperacion;

    /**
     * @var string
     */
    private $descripcionOperacion;

    /**
     * @var string
     */
    private $estatusOperacion;

    /**
     * @var integer
     */
    private $idOperacion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;


    /**
     * Set fecha
     *
     * @param \Date $fecha
     * @return Bitacora
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \Date 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set areaOperacion
     *
     * @param string $areaOperacion
     * @return Bitacora
     */
    public function setAreaOperacion($areaOperacion)
    {
        $this->areaOperacion = $areaOperacion;

        return $this;
    }

    /**
     * Get areaOperacion
     *
     * @return string 
     */
    public function getAreaOperacion()
    {
        return $this->areaOperacion;
    }

    /**
     * Set descripcionOperacion
     *
     * @param string $descripcionOperacion
     * @return Bitacora
     */
    public function setDescripcionOperacion($descripcionOperacion)
    {
        $this->descripcionOperacion = $descripcionOperacion;

        return $this;
    }

    /**
     * Get descripcionOperacion
     *
     * @return string 
     */
    public function getDescripcionOperacion()
    {
        return $this->descripcionOperacion;
    }

    /**
     * Set estatusOperacion
     *
     * @param string $estatusOperacion
     * @return Bitacora
     */
    public function setEstatusOperacion($estatusOperacion)
    {
        $this->estatusOperacion = $estatusOperacion;

        return $this;
    }

    /**
     * Get estatusOperacion
     *
     * @return string 
     */
    public function getEstatusOperacion()
    {
        return $this->estatusOperacion;
    }

    /**
     * Get idOperacion
     *
     * @return integer 
     */
    public function getIdOperacion()
    {
        return $this->idOperacion;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return Bitacora
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return Bitacora
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

     public function __GregorianDateTime()
    {
        return date_format($this->fecha,'d-m-Y  H:i:s');
    }

    public function jsonSerialize()
    {
        return array(

            'fecha'    				=> $this->fecha,            
            'fechaGregorian'        => $this->__GregorianDateTime(),
			'usuario'    			=> $this->usuario,
			'areaOperacion'    		=> $this->areaOperacion,
			'descripcionOperacion'  => $this->descripcionOperacion,
			'estatusOperacion'    	=> $this->estatusOperacion,
			'idOperacion'    		=> $this->idOperacion,
			'pedido'    			=> $this->pedido,
                       
        );

    }
}
