<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ListaNegraMontos
 */
class ListaNegraMontos implements JsonSerializable
{
    /**
     * @var string
     */
    private $NombreProducto;

    /**
     * @var integer
     */
    private $Monto;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idMonto;

    /**
     * @var string
     */
    private $ClaveProducto;


    /**
     * Set NombreProducto
     *
     * @param string $nombreProducto
     * @return ListaNegraMontos
     */
    public function setNombreProducto($nombreProducto)
    {
        $this->NombreProducto = $nombreProducto;

        return $this;
    }

    /**
     * Get NombreProducto
     *
     * @return string 
     */
    public function getNombreProducto()
    {
        return $this->NombreProducto;
    }

    /**
     * Set Monto
     *
     * @param integer $monto
     * @return ListaNegraMontos
     */
    public function setMonto($monto)
    {
        $this->Monto = $monto;

        return $this;
    }

    /**
     * Get Monto
     *
     * @return integer 
     */
    public function getMonto()
    {
        return $this->Monto;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return ListaNegraMontos
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idMonto
     *
     * @return integer 
     */
    public function getIdMonto()
    {
        return $this->idMonto;
    }    

    /**
     * Set ClaveProducto
     *
     * @param string $claveProducto
     * @return ListaNegraMontos
     */
    public function setClaveProducto($claveProducto)
    {
        $this->ClaveProducto = $claveProducto;

        return $this;
    }

    /**
     * Get ClaveProducto
     *
     * @return string 
     */
    public function getClaveProducto()
    {
        return $this->ClaveProducto;
    }


    public function jsonSerialize()
    {
        return array(

            'NombreProducto'    => $this->NombreProducto,
            'Monto'             => $this->Monto,
            'ClaveProducto'     => $this->ClaveProducto,
            'Activo'            => $this->Activo,
            'idMonto'           => $this->idMonto,
                       
        );

    }


}
