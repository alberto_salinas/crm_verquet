<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Kits
 */
class Kits
{
    /**
     * @var integer
     */
    private $cantidadBase;

    /**
     * @var string
     */
    private $uMBase;

    /**
     * @var integer
     */
    private $cantidadComponente;

    /**
     * @var string
     */
    private $uMComponente;

    /**
     * @var string
     */
    private $listaMateriales;

    /**
     * @var integer
     */
    private $idProductoPadre;

    /**
     * @var integer
     */
    private $idProductoHijo;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Productos
     */
    private $productoHijo;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Productos
     */
    private $productoPadre;


    /**
     * Set cantidadBase
     *
     * @param integer $cantidadBase
     * @return Kits
     */
    public function setCantidadBase($cantidadBase)
    {
        $this->cantidadBase = $cantidadBase;

        return $this;
    }

    /**
     * Get cantidadBase
     *
     * @return integer 
     */
    public function getCantidadBase()
    {
        return $this->cantidadBase;
    }

    /**
     * Set uMBase
     *
     * @param string $uMBase
     * @return Kits
     */
    public function setUMBase($uMBase)
    {
        $this->uMBase = $uMBase;

        return $this;
    }

    /**
     * Get uMBase
     *
     * @return string 
     */
    public function getUMBase()
    {
        return $this->uMBase;
    }

    /**
     * Set cantidadComponente
     *
     * @param integer $cantidadComponente
     * @return Kits
     */
    public function setCantidadComponente($cantidadComponente)
    {
        $this->cantidadComponente = $cantidadComponente;

        return $this;
    }

    /**
     * Get cantidadComponente
     *
     * @return integer 
     */
    public function getCantidadComponente()
    {
        return $this->cantidadComponente;
    }

    /**
     * Set uMComponente
     *
     * @param string $uMComponente
     * @return Kits
     */
    public function setUMComponente($uMComponente)
    {
        $this->uMComponente = $uMComponente;

        return $this;
    }

    /**
     * Get uMComponente
     *
     * @return string 
     */
    public function getUMComponente()
    {
        return $this->uMComponente;
    }

    /**
     * Set listaMateriales
     *
     * @param string $listaMateriales
     * @return Kits
     */
    public function setListaMateriales($listaMateriales)
    {
        $this->listaMateriales = $listaMateriales;

        return $this;
    }

    /**
     * Get listaMateriales
     *
     * @return string 
     */
    public function getListaMateriales()
    {
        return $this->listaMateriales;
    }

    /**
     * Set idProductoPadre
     *
     * @param integer $idProductoPadre
     * @return Kits
     */
    public function setIdProductoPadre($idProductoPadre)
    {
        $this->idProductoPadre = $idProductoPadre;

        return $this;
    }

    /**
     * Get idProductoPadre
     *
     * @return integer 
     */
    public function getIdProductoPadre()
    {
        return $this->idProductoPadre;
    }

    /**
     * Set idProductoHijo
     *
     * @param integer $idProductoHijo
     * @return Kits
     */
    public function setIdProductoHijo($idProductoHijo)
    {
        $this->idProductoHijo = $idProductoHijo;

        return $this;
    }

    /**
     * Get idProductoHijo
     *
     * @return integer 
     */
    public function getIdProductoHijo()
    {
        return $this->idProductoHijo;
    }

    /**
     * Set productoHijo
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productoHijo
     * @return Kits
     */
    public function setProductoHijo(\MDR\PuntoVentaBundle\Entity\Productos $productoHijo = null)
    {
        $this->productoHijo = $productoHijo;

        return $this;
    }

    /**
     * Get productoHijo
     *
     * @return \MDR\PuntoVentaBundle\Entity\Productos 
     */
    public function getProductoHijo()
    {
        return $this->productoHijo;
    }

    /**
     * Set productoPadre
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productoPadre
     * @return Kits
     */
    public function setProductoPadre(\MDR\PuntoVentaBundle\Entity\Productos $productoPadre = null)
    {
        $this->productoPadre = $productoPadre;

        return $this;
    }

    /**
     * Get productoPadre
     *
     * @return \MDR\PuntoVentaBundle\Entity\Productos 
     */
    public function getProductoPadre()
    {
        return $this->productoPadre;
    }
}
