<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescuentosOrigenVentas
 */
class DescuentosOrigenVentas
{
    /**
     * @var \MDR\PuntoVentaBundle\Entity\Oficinasventas
     */
    private $oficinaVentas;


    /**
     * @var \MDR\PuntoVentaBundle\Entity\CanalDistribucion
     */
    private $canalDistribucion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OrganizacionVentas
     */
    private $organizacionVentas;
	
    /**
     * @var \MDR\PuntoVentaBundle\Entity\Descuentos
     */
    private $descuento;
	

    /**
     * Set oficinaVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas
     * @return DescuentosOrigenVentas
     */
    public function setOficinaVentas(\MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas)
    {
        $this->oficinaVentas = $oficinaVentas;

        return $this;
    }

    /**
     * Get oficinaVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OficinasVentas 
     */
    public function getOficinaVentas()
    {
        return $this->oficinaVentas;
    }

    /**
     * Set canalDistribucion
     *
     * @param \MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion
     * @return DescuentosOrigenVentas
     */
    public function setCanalDistribucion(\MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion)
    {
        $this->canalDistribucion = $canalDistribucion;

        return $this;
    }

    /**
     * Get canalDistribucion
     *
     * @return \MDR\PuntoVentaBundle\Entity\CanalDistribucion 
     */
    public function getCanalDistribucion()
    {
        return $this->canalDistribucion;
    }

    /**
     * Set organizacionVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas
     * @return DescuentosOrigenVentas
     */
    public function setOrganizacionVentas(\MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas)
    {
        $this->organizacionVentas = $organizacionVentas;

        return $this;
    }

    /**
     * Get organizacionVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OrganizacionVentas 
     */
    public function getOrganizacionVentas()
    {
        return $this->organizacionVentas;
    }

    /**
     * Set descuento
     *
     * @param \MDR\PuntoVentaBundle\Entity\Descuentos $descuento
     * @return DescuentosOrigenVentas
     */
    public function setDescuento(\MDR\PuntoVentaBundle\Entity\Descuentos $descuento = null)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return \MDR\PuntoVentaBundle\Entity\Descuentos 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }
	
	
	    public function __toString()
    {
        return $this->oficinaVentas->getdescripcion();
    }
}
