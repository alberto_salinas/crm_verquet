<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Estatus
 */
class Estatus implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var integer
     */
    private $idEstatus;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Estatus
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set claveSAP
     *
     * @param string $claveSAP
     * @return Estatus
     */
    public function setClaveSAP($claveSAP)
    {
        $this->claveSAP = $claveSAP;

        return $this;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }

    /**
     * Get idEstatus
     *
     * @return integer 
     */
    public function getIdEstatus()
    {
        return $this->idEstatus;
    }

	public function __toString(){
		return $this->descripcion;
	}
    
    public function jsonSerialize()
    {
        return array(

            'descripcion'    => $this->descripcion,
			'idEstatus'    	=> $this->idEstatus,
                       
        );

    }
}
