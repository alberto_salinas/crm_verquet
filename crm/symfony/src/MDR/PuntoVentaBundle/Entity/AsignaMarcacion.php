<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * AsignaMarcacion
 */
class AsignaMarcacion implements JsonSerializable
{
    /**
     * @var string
     */
    private $usuario;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var string
     */
    private $telefono2;

    /**
     * @var string
     */
    private $producto;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $nombreCliente;

    /**
     * @var string
     */
    private $comentarios;

    /**
     * @var boolean
     */
    private $atendido;

    /**
     * @var integer
     */
    private $idAsigna;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OficinasVentas
     */
    private $oficinaVentas;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\CanalDistribucion
     */
    private $canalDistribucion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OrganizacionVentas
     */
    private $organizacionVentas;


    /**
     * Set usuario
     *
     * @param string $usuario
     * @return AsignaMarcacion
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return AsignaMarcacion
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set telefono2
     *
     * @param string $telefono2
     * @return AsignaMarcacion
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    /**
     * Get telefono2
     *
     * @return string 
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * Set producto
     *
     * @param string $producto
     * @return AsignaMarcacion
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return string 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return AsignaMarcacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set nombreCliente
     *
     * @param string $nombreCliente
     * @return AsignaMarcacion
     */
    public function setNombreCliente($nombreCliente)
    {
        $this->nombreCliente = $nombreCliente;

        return $this;
    }

    /**
     * Get nombreCliente
     *
     * @return string 
     */
    public function getNombreCliente()
    {
        return $this->nombreCliente;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     * @return AsignaMarcacion
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set atendido
     *
     * @param boolean $atendido
     * @return AsignaMarcacion
     */
    public function setAtendido($atendido)
    {
        $this->atendido = $atendido;

        return $this;
    }

    /**
     * Get atendido
     *
     * @return boolean 
     */
    public function getAtendido()
    {
        return $this->atendido;
    }

    /**
     * Get idAsigna
     *
     * @return integer 
     */
    public function getIdAsigna()
    {
        return $this->idAsigna;
    }

    /**
     * Set oficinaVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas
     * @return AsignaMarcacion
     */
    public function setOficinaVentas(\MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas = null)
    {
        $this->oficinaVentas = $oficinaVentas;

        return $this;
    }

    /**
     * Get oficinaVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OficinasVentas 
     */
    public function getOficinaVentas()
    {
        return $this->oficinaVentas;
    }

    /**
     * Set canalDistribucion
     *
     * @param \MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion
     * @return AsignaMarcacion
     */
    public function setCanalDistribucion(\MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion = null)
    {
        $this->canalDistribucion = $canalDistribucion;

        return $this;
    }

    /**
     * Get canalDistribucion
     *
     * @return \MDR\PuntoVentaBundle\Entity\CanalDistribucion 
     */
    public function getCanalDistribucion()
    {
        return $this->canalDistribucion;
    }

    /**
     * Set organizacionVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas
     * @return AsignaMarcacion
     */
    public function setOrganizacionVentas(\MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas = null)
    {
        $this->organizacionVentas = $organizacionVentas;

        return $this;
    }

    /**
     * Get organizacionVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OrganizacionVentas 
     */
    public function getOrganizacionVentas()
    {
        return $this->organizacionVentas;
    }

    public function jsonSerialize()
    {
        return array(
            'idAsigna'              => $this->idAsigna,
            'organizacionVentas'    => $this->organizacionVentas,
            'canalDistribucion'     => $this->canalDistribucion,
            'oficinaVentas'         => $this->oficinaVentas,
            'usuario'               => $this->usuario,
            'telefono'              => $this->telefono,
            'telefono2'             => $this->telefono2,
            'producto'              => $this->producto,
            'fecha'                 => $this->fecha,
            'nombreCliente'         => $this->nombreCliente,
            'comentarios'           => $this->comentarios,
            'atendido'              => $this->atendido,
        ); 

    }
}