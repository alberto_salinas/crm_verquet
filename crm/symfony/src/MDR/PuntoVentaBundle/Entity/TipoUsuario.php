<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * TipoUsuario
 */
class TipoUsuario implements RoleInterface, \Serializable, JsonSerializable
{
    /**
     * @var string
     */
    private $valor;

    /**
     * @var integer
     */
    private $tipo;

    /**
     * @var integer
     */
    private $seleccionador;

    /**
     * @var integer
     */
    private $idTipoUsuario;


    /**
     * Set valor
     *
     * @param string $valor
     * @return TipoUsuario
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return TipoUsuario
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set seleccionador
     *
     * @param integer $seleccionador
     * @return TipoUsuario
     */
    public function setSeleccionador($seleccionador)
    {
        $this->seleccionador = $seleccionador;

        return $this;
    }

    /**
     * Get seleccionador
     *
     * @return integer 
     */
    public function getSeleccionador()
    {
        return $this->seleccionador;
    }

    /**
     * Get idTipoUsuario
     *
     * @return integer 
     */
    public function getIdTipoUsuario()
    {
        return $this->idTipoUsuario;
    }

    /* Implementacióin de metodos de RoleInterface */

    public function getRole(){
        return 'ROLE_'.strtoupper(str_replace(' ', '', $this->valor));
    }

    /* Implementacióin de metodos de Serializable */
    
    public function serialize() {
        return serialize(array(
            $this->idTipoUsuario, $this->valor
            ));
    }

    public function unserialize($serialized) {
        list(
        $this->idTipoUsuario, $this->valor
        ) = unserialize($serialized);
    }
    
    public function __toString(){
        return ($this->valor);
    }


        public function jsonSerialize()
    {
        return array(
            'valor'             => $this->valor,
            'tipo'              => $this->tipo,
            'seleccionador'     => $this->seleccionador,
            'idTipoUsuario'     => $this->idTipoUsuario,           
                       
        );

    }
}
