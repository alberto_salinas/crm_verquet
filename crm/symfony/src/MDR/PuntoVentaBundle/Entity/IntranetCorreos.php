<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetCorreos
 */
class IntranetCorreos implements JsonSerializable
{
    /**
     * @var string
     */
    private $Area;

    /**
     * @var string
     */
    private $Correo;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idCorreo;


    /**
     * Set Area
     *
     * @param string $area
     * @return IntranetCorreos
     */
    public function setArea($area)
    {
        $this->Area = $area;

        return $this;
    }

    /**
     * Get Area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->Area;
    }

    /**
     * Set Correo
     *
     * @param string $correo
     * @return IntranetCorreos
     */
    public function setCorreo($correo)
    {
        $this->Correo = $correo;

        return $this;
    }

    /**
     * Get Correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->Correo;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return IntranetCorreos
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idCorreo
     *
     * @return integer 
     */
    public function getIdCorreo()
    {
        return $this->idCorreo;
    }

     public function __toString()
    {
        return $this->Area;
    }
    
    public function jsonSerialize()
    {
        return array(

            'Area'      => $this->Area,            
            'Correo'    => $this->Correo,
            'Activo'    => $this->Activo,
            'idCorreo'  => $this->idCorreo,
                       
        );

    }
}
