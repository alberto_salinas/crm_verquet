<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ReglasFraude
 */
class ReglasFraude implements JsonSerializable
{
    /**
     * @var string
     */
    private $Descripcion;

    /**
     * @var integer
     */
    private $Valor;

    /**
     * @var integer
     */
    private $idPago;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idReglas;


    /**
     * Set Descripcion
     *
     * @param string $descripcion
     * @return ReglasFraude
     */
    public function setDescripcion($descripcion)
    {
        $this->Descripcion = $descripcion;

        return $this;
    }

    /**
     * Get Descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Set Valor
     *
     * @param integer $valor
     * @return ReglasFraude
     */
    public function setValor($valor)
    {
        $this->Valor = $valor;

        return $this;
    }

    /**
     * Get Valor
     *
     * @return integer 
     */
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * Set idPago
     *
     * @param integer $idPago
     * @return ReglasFraude
     */
    public function setIdPago($idPago)
    {
        $this->idPago = $idPago;

        return $this;
    }

    /**
     * Get idPago
     *
     * @return integer 
     */
    public function getIdPago()
    {
        return $this->idPago;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return ReglasFraude
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idReglas
     *
     * @return integer 
     */
    public function getIdReglas()
    {
        return $this->idReglas;
    }
    
    public function jsonSerialize()
    {
        return array(

            'Descripcion'   => $this->Descripcion,
            'Valor'         => $this->Valor,
            'idPago'        => $this->idPago,
            'Activo'        => $this->Activo,
            'idReglas'      => $this->idReglas,
                       
        );

    }
}
