<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Pedidos
 * @Assert\Callback(methods={"estaCompleto"})
 */
class Pedidos implements JsonSerializable
{
    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $pedidoSAP;

    /**
     * @var string
     */
    private $numeroGuia;

    /**
     * @var float
     */
    private $subTotal;

    /**
     * @var float
     */
    private $totalPago;

    /**
     * @var string
     */
    private $rfc;

    /**
     * @var integer
     */
    private $factura;

    /**
     * @var string
     */
    private $dnis;

    /**
     * @var string
     */
    private $telefono;

    /**
     * @var boolean
     */
    private $esEntrante;

    /**
     * @var integer
     */
    private $idPedido;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pagosTarjeta;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productos; // RELACION TBL PedidosProductos

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cobros;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cancelacion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $descuentos;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\ModeloCalificador
     */
    private $modelo;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoPagos
     */
    private $pago;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Clientes
     */
    private $cliente;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Mensajeria
     */
    private $mensajeria;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Estatus
     */
    private $estatus;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Direcciones
     */
    private $direccion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\GastosEnvio
     */
    private $gastosEnvio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $indicadores; // INDICADORES DEL MODELO CALIFICADOR

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $usuariosRecuperacion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pagosTarjeta = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cobros = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cancelacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->descuentos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->indicadores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuariosRecuperacion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pedidos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set pedidoSAP
     *
     * @param string $pedidoSAP
     * @return Pedidos
     */
    public function setPedidoSAP($pedidoSAP)
    {
        $this->pedidoSAP = $pedidoSAP;

        return $this;
    }

    /**
     * Get pedidoSAP
     *
     * @return string 
     */
    public function getPedidoSAP()
    {
        return $this->pedidoSAP;
    }

    /**
     * Set numeroGuia
     *
     * @param string $numeroGuia
     * @return Pedidos
     */
    public function setNumeroGuia($numeroGuia)
    {
        $this->numeroGuia = $numeroGuia;

        return $this;
    }

    /**
     * Get numeroGuia
     *
     * @return string 
     */
    public function getNumeroGuia()
    {
        return $this->numeroGuia;
    }

    /**
     * Set subTotal
     *
     * @param float $subTotal
     * @return Pedidos
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;

        return $this;
    }

    /**
     * Get subTotal
     *
     * @return float 
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * Set totalPago
     *
     * @param float $totalPago
     * @return Pedidos
     */
    public function setTotalPago($totalPago)
    {
        $this->totalPago = $totalPago;

        return $this;
    }

    /**
     * Get totalPago
     *
     * @return float 
     */
    public function getTotalPago()
    {
        return $this->totalPago;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     * @return Pedidos
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string 
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set factura
     *
     * @param integer $factura
     * @return Pedidos
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return integer 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set dnis
     *
     * @param string $dnis
     * @return Pedidos
     */
    public function setDnis($dnis)
    {
        $this->dnis = $dnis;

        return $this;
    }

    /**
     * Get dnis
     *
     * @return string 
     */
    public function getDnis()
    {
        return $this->dnis;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Pedidos
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set esEntrante
     *
     * @param boolean $esEntrante
     * @return Pedidos
     */
    public function setEsEntrante($esEntrante)
    {
        $this->esEntrante = $esEntrante;

        return $this;
    }

    /**
     * Get esEntrante
     *
     * @return boolean 
     */
    public function getEsEntrante()
    {
        return $this->esEntrante;
    }

    /**
     * Get idPedido
     *
     * @return integer 
     */
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Add pagosTarjeta
     *
     * @param \MDR\PuntoVentaBundle\Entity\PagoTarjeta $pagosTarjeta
     * @return Pedidos
     */
    public function addPagosTarjetum(\MDR\PuntoVentaBundle\Entity\PagoTarjeta $pagosTarjeta)
    {
        $this->pagosTarjeta[] = $pagosTarjeta;

        return $this;
    }

    /**
     * Remove pagosTarjeta
     *
     * @param \MDR\PuntoVentaBundle\Entity\PagoTarjeta $pagosTarjeta
     */
    public function removePagosTarjetum(\MDR\PuntoVentaBundle\Entity\PagoTarjeta $pagosTarjeta)
    {
        $this->pagosTarjeta->removeElement($pagosTarjeta);
    }

    /**
     * Get pagosTarjeta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagosTarjeta()
    {
        return $this->pagosTarjeta;
    }

    /**
     * Add productos
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductosPedido $productos
     * @return Pedidos
     */
    public function addProducto(\MDR\PuntoVentaBundle\Entity\ProductosPedido $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductosPedido $productos
     */
    public function removeProducto(\MDR\PuntoVentaBundle\Entity\ProductosPedido $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Add cobros
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidosOrdenCobro $cobros
     * @return Pedidos
     */
    public function addCobro(\MDR\PuntoVentaBundle\Entity\PedidosOrdenCobro $cobros)
    {
        $this->cobros[] = $cobros;

        return $this;
    }

    /**
     * Remove cobros
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidosOrdenCobro $cobros
     */
    public function removeCobro(\MDR\PuntoVentaBundle\Entity\PedidosOrdenCobro $cobros)
    {
        $this->cobros->removeElement($cobros);
    }

    /**
     * Get cobros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobros()
    {
        return $this->cobros;
    }

    /**
     * Add cancelacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidosCancelados $cancelacion
     * @return Pedidos
     */
    public function addCancelacion(\MDR\PuntoVentaBundle\Entity\PedidosCancelados $cancelacion)
    {
        $this->cancelacion[] = $cancelacion;

        return $this;
    }

    /**
     * Remove cancelacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidosCancelados $cancelacion
     */
    public function removeCancelacion(\MDR\PuntoVentaBundle\Entity\PedidosCancelados $cancelacion)
    {
        $this->cancelacion->removeElement($cancelacion);
    }

    /**
     * Get cancelacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCancelacion()
    {
        return $this->cancelacion;
    }

    /**
     * Add descuentos
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidoDescuentos $descuentos
     * @return Pedidos
     */
    public function addDescuento(\MDR\PuntoVentaBundle\Entity\PedidoDescuentos $descuentos)
    {
        $this->descuentos[] = $descuentos;

        return $this;
    }

    /**
     * Remove descuentos
     *
     * @param \MDR\PuntoVentaBundle\Entity\PedidoDescuentos $descuentos
     */
    public function removeDescuento(\MDR\PuntoVentaBundle\Entity\PedidoDescuentos $descuentos)
    {
        $this->descuentos->removeElement($descuentos);
    }

    /**
     * Get descuentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDescuentos()
    {
        return $this->descuentos;
    }

    /**
     * Set modelo
     *
     * @param \MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo
     * @return Pedidos
     */
    public function setModelo(\MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return \MDR\PuntoVentaBundle\Entity\ModeloCalificador 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set pago
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoPagos $pago
     * @return Pedidos
     */
    public function setPago(\MDR\PuntoVentaBundle\Entity\TipoPagos $pago = null)
    {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoPagos 
     */
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Set cliente
     *
     * @param \MDR\PuntoVentaBundle\Entity\Clientes $cliente
     * @return Pedidos
     */
    public function setCliente(\MDR\PuntoVentaBundle\Entity\Clientes $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \MDR\PuntoVentaBundle\Entity\Clientes 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set mensajeria
     *
     * @param \MDR\PuntoVentaBundle\Entity\Mensajeria $mensajeria
     * @return Pedidos
     */
    public function setMensajeria(\MDR\PuntoVentaBundle\Entity\Mensajeria $mensajeria = null)
    {
        $this->mensajeria = $mensajeria;

        return $this;
    }

    /**
     * Get mensajeria
     *
     * @return \MDR\PuntoVentaBundle\Entity\Mensajeria 
     */
    public function getMensajeria()
    {
        return $this->mensajeria;
    }

    /**
     * Set estatus
     *
     * @param \MDR\PuntoVentaBundle\Entity\Estatus $estatus
     * @return Pedidos
     */
    public function setEstatus(\MDR\PuntoVentaBundle\Entity\Estatus $estatus = null)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get estatus
     *
     * @return \MDR\PuntoVentaBundle\Entity\Estatus 
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set direccion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Direcciones $direccion
     * @return Pedidos
     */
    public function setDireccion(\MDR\PuntoVentaBundle\Entity\Direcciones $direccion = null)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return \MDR\PuntoVentaBundle\Entity\Direcciones 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return Pedidos
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set gastosEnvio
     *
     * @param \MDR\PuntoVentaBundle\Entity\GastosEnvio $gastosEnvio
     * @return Pedidos
     */
    public function setGastosEnvio(\MDR\PuntoVentaBundle\Entity\GastosEnvio $gastosEnvio = null)
    {
        $this->gastosEnvio = $gastosEnvio;

        return $this;
    }

    /**
     * Get gastosEnvio
     *
     * @return \MDR\PuntoVentaBundle\Entity\GastosEnvio 
     */
    public function getGastosEnvio()
    {
        return $this->gastosEnvio;
    }

    /**
     * Add indicadores
     *
     * @param \MDR\PuntoVentaBundle\Entity\Indicadores $indicadores
     * @return Pedidos
     */
    public function addIndicadore(\MDR\PuntoVentaBundle\Entity\Indicadores $indicadores)
    {
        $this->indicadores[] = $indicadores;

        return $this;
    }

    /**
     * Remove indicadores
     *
     * @param \MDR\PuntoVentaBundle\Entity\Indicadores $indicadores
     */
    public function removeIndicadore(\MDR\PuntoVentaBundle\Entity\Indicadores $indicadores)
    {
        $this->indicadores->removeElement($indicadores);
    }

    /**
     * Get indicadores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndicadores()
    {
        return $this->indicadores;
    }

    /**
     * Add usuariosRecuperacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuariosRecuperacion
     * @return Pedidos
     */
    public function addUsuariosRecuperacion(\MDR\PuntoVentaBundle\Entity\Usuario $usuariosRecuperacion)
    {
        $this->usuariosRecuperacion[] = $usuariosRecuperacion;

        return $this;
    }

    /**
     * Remove usuariosRecuperacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuariosRecuperacion
     */
    public function removeUsuariosRecuperacion(\MDR\PuntoVentaBundle\Entity\Usuario $usuariosRecuperacion)
    {
        $this->usuariosRecuperacion->removeElement($usuariosRecuperacion);
    }

    /**
     * Get usuariosRecuperacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuariosRecuperacion()
    {
        return $this->usuariosRecuperacion;
    }

    public function estaCompleto(ExecutionContextInterface $context)
    {
        if(count($this->productos) <= 0 )
            $context->addViolationAt('productos', 'Debe seleccionar por lo menos un producto');
        if($this->mensajeria == null)
            $context->addViolationAt('mensajeria', 'Debe seleccionar la mensajeria');
        if($this->pago->getIdTipoPago() == 1)
        {
            if(count($this->pagosTarjeta) == 0)
            {
                $context->addViolationAt('pagosTarjeta', 'Debe llenar los datos del pago con TC');
            }
        }
    }

    public function calculaTotal()
    {
        $this->subTotal = $this->calculaSubTotal();
        $descuentoTotal = $this->calculaDescuento();
        
        $this->totalPago = $this->subTotal - $descuentoTotal + $this->gastosEnvio->getPrecio();
    }

    public function calculaSubTotal()
    {
        $subTotal = 0;
        foreach ($this->productos as $producto) {
            $subTotal += $producto->getCantidad() * $producto->getProductoPrecio()->getPrecio();
        }

        return $subTotal;
    }

    public function calculaDescuento()
    {
        $descuentoTotal = 0;
        foreach ($this->descuentos as $descuento) {
            $descuentoTotal += $descuento->getImporte();
        }

        return $descuentoTotal;
    }

    public function __toString()
    {
        return $this->idPedido.'';
    }

     public function __GregorianDateTime()
    {
        if($this->fecha != null)
        {
            return date_format($this->fecha,'d-m-Y  H:i:s');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y  H:i:s');
        }
    }

    public function jsonSerialize()
    {
        return array(
            'fecha'             => $this->fecha,
            'fechaGregorian'    => $this->__GregorianDateTime(),
            'pedidoSAP'         => $this->pedidoSAP,
            'numeroGuia'        => $this->numeroGuia,
            'gastosEnvio'       => $this->gastosEnvio,
            'subTotal'          => $this->subTotal,
            'totalPago'         => $this->totalPago,
            'rfc'               => $this->rfc,
            'factura'           => $this->factura,
            'esEntrante'        => $this->esEntrante,
            'idPedido'          => $this->idPedido,
            'modelo'            => $this->modelo,
            'pago'              => $this->pago,
            'cliente'           => $this->cliente,
            'mensajeria'        => $this->mensajeria,
            'estatus'           => $this->estatus,
            'direccion'         => $this->direccion,
            'dnis'              => $this->dnis,
            'telefono'          => $this->telefono,
            'usuario'           => $this->usuario,
            'detalles'          => $this->productos->toArray(),
            'productos'         => $this->productos->toArray(),
            'pagosTarjeta'      => $this->pagosTarjeta->toArray(),
            'cobros'            => $this->cobros->toArray(),
            'indicadores'       => $this->indicadores->toArray(),
            'descuentos'        => $this->descuentos->toArray(),
                       
        );

    }
}
