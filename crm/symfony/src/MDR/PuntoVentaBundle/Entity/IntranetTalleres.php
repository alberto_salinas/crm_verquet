<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetTalleres
 */
class IntranetTalleres implements JsonSerializable
{
    /**
     * @var string
     */
    private $Nombre;

    /**
     * @var string
     */
    private $Descripcion;

    /**
     * @var \DateTime
     */
    private $FechaInicio;

    /**
     * @var \DateTime
     */
    private $FechaFin;

    /**
     * @var integer
     */
    private $idTaller;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var boolean
     */
    private $Tipo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Registros;

    /**
     * Set Nombre
     *
     * @param string $nombre
     * @return IntranetTalleres
     */
    public function setNombre($nombre)
    {
        $this->Nombre = $nombre;

        return $this;
    }

    /**
     * Get Nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set Descripcion
     *
     * @param string $descripcion
     * @return IntranetTalleres
     */
    public function setDescripcion($descripcion)
    {
        $this->Descripcion = $descripcion;

        return $this;
    }

    /**
     * Get Descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Set FechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return IntranetTalleres
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->FechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get FechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->FechaInicio;
    }

    /**
     * Set FechaFin
     *
     * @param \DateTime $fechaFin
     * @return IntranetTalleres
     */
    public function setFechaFin($fechaFin)
    {
        $this->FechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get FechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->FechaFin;
    }

    /**
     * Get idTaller
     *
     * @return integer 
     */
    public function getIdTaller()
    {
        return $this->idTaller;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return IntranetTalleres
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set Tipo
     *
     * @param boolean $tipo
     * @return IntranetTalleres
     */
    public function setTipo($tipo)
    {
        $this->Tipo = $tipo;
        return $this;
    }

    /**
     * Get Tipo
     *
     * @return boolean 
     */
    public function getTipo()
    {
        return $this->Tipo;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Registros = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add Registros
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetRegistrosTalleres $registros
     * @return IntranetTalleres
     */
    public function addRegistro(\MDR\PuntoVentaBundle\Entity\IntranetRegistrosTalleres $registros)
    {
        $this->Registros[] = $registros;

        return $this;
    }

    /**
     * Remove Registros
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetRegistrosTalleres $registros
     */
    public function removeRegistro(\MDR\PuntoVentaBundle\Entity\IntranetRegistrosTalleres $registros)
    {
        $this->Registros->removeElement($registros);
    }

    /**
     * Get Registros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegistros()
    {
        return $this->Registros;
    }

    public function __GregorianDateTimeInicio()
    {
        if($this->FechaInicio != null)
        {
            return date_format($this->FechaInicio,'d/m/Y');
        }
        else
        {
            return date_format(new \DateTime(),'d/m/Y');
        }
    }

    public function __GregorianDateTimeFin()
    {
        if($this->FechaFin != null)
        {
            return date_format($this->FechaFin,'d/m/Y');
        }
        else
        {
            return date_format(new \DateTime(),'d/m/Y');
        }
    }
    
    public function __toString()
    {
        return $this->Nombre;
    }
            
    public function jsonSerialize()
    {
        return array(

            'Nombre'        => $this->Nombre,            
            'descripcion'   => $this->Descripcion,
            'FechaInicio'   => $this->FechaInicio,
            'FechaFin'      => $this->FechaFin,            
            'idTaller'      => $this->idTaller,
            'usuario'       => $this->usuario,
            'Tipo'          => $this->Tipo,
            'Registros'     => $this->Registros->toArray(),
            'InicioGregorian'   => $this->__GregorianDateTimeInicio(),
            'FinGregorian'      => $this->__GregorianDateTimeFin(), 

                       
        );

    }

}
