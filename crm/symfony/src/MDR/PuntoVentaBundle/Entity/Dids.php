<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Dids
 */
class Dids implements JsonSerializable
{
    /**
     * @var string
     */
    private $numero;

    /**
     * @var integer
     */
    private $idDid;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OficinasVentas
     */
    private $oficinaVentas;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OrganizacionVentas
     */
    private $organizacionVentas;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\CanalDistribucion
     */
    private $canalDistribucion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $especialidades;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->especialidades = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Dids
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Get idDid
     *
     * @return integer 
     */
    public function getIdDid()
    {
        return $this->idDid;
    }

    /**
     * Set oficinaVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas
     * @return Dids
     */
    public function setOficinaVentas(\MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVentas = null)
    {
        $this->oficinaVentas = $oficinaVentas;

        return $this;
    }

    /**
     * Get oficinaVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OficinasVentas 
     */
    public function getOficinaVentas()
    {
        return $this->oficinaVentas;
    }

    /**
     * Set organizacionVentas
     *
     * @param \MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas
     * @return Dids
     */
    public function setOrganizacionVentas(\MDR\PuntoVentaBundle\Entity\OrganizacionVentas $organizacionVentas = null)
    {
        $this->organizacionVentas = $organizacionVentas;

        return $this;
    }

    /**
     * Get organizacionVentas
     *
     * @return \MDR\PuntoVentaBundle\Entity\OrganizacionVentas 
     */
    public function getOrganizacionVentas()
    {
        return $this->organizacionVentas;
    }

    /**
     * Set canalDistribucion
     *
     * @param \MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion
     * @return Dids
     */
    public function setCanalDistribucion(\MDR\PuntoVentaBundle\Entity\CanalDistribucion $canalDistribucion = null)
    {
        $this->canalDistribucion = $canalDistribucion;

        return $this;
    }

    /**
     * Get canalDistribucion
     *
     * @return \MDR\PuntoVentaBundle\Entity\CanalDistribucion 
     */
    public function getCanalDistribucion()
    {
        return $this->canalDistribucion;
    }

    /**
     * Add especialidades
     *
     * @param \MDR\PuntoVentaBundle\Entity\Especialidades $especialidades
     * @return Dids
     */
    public function addEspecialidade(\MDR\PuntoVentaBundle\Entity\Especialidades $especialidades)
    {
        $this->especialidades[] = $especialidades;

        return $this;
    }

    /**
     * Remove especialidades
     *
     * @param \MDR\PuntoVentaBundle\Entity\Especialidades $especialidades
     */
    public function removeEspecialidade(\MDR\PuntoVentaBundle\Entity\Especialidades $especialidades)
    {
        $this->especialidades->removeElement($especialidades);
    }

    /**
     * Get especialidades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEspecialidades()
    {
        return $this->especialidades;
    }

    public function jsonSerialize()
    {
        return array(
            'idDid'                 => $this->idDid,
            'numero'                => $this->numero,
            'organizacionVentas'    => $this->organizacionVentas,
            'canalDistribucion'     => $this->canalDistribucion,
            'oficinaVentas'         => $this->oficinaVentas,
        ); 
    }	
}





