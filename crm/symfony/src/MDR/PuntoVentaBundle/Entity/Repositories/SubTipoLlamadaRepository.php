<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * SubTipoLlamadaRepository
 */
class SubTipoLlamadaRepository extends EntityRepository
{
        public function findAllOrder()
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT s,t FROM MDRPuntoVentaBundle:SubTipoLlamada s
                                INNER JOIN s.tipoLlamada t 
                                ORDER BY  t.descripcion ASC, s.descripcionSubTipo
                                ";

                $query = $em->createQuery($strQuery);
                $entities = $query->getResult();

                return $entities;
	}
}
