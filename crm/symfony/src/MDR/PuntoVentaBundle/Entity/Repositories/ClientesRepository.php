<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ClientesRepository
 */
class ClientesRepository extends EntityRepository
{
        public function findByTelefono($numero)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT c FROM MDRPuntoVentaBundle:Clientes c
                                LEFT JOIN c.direcciones d
                                LEFT JOIN c.telefonos t
                                WHERE d.telCasa like :numero or d.telOficina like :numero or d.telCel like :numero or t.numero like :numero
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('numero', '%'.$numero);
                $entities = $query->getResult();

                return $entities;
	}
}
