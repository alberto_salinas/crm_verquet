<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ListaNegraDireccionesRepository extends EntityRepository
{
    
        function findListaNegraDirecciones($cp, $municipio, $colonia){
                $em = $this->getEntityManager();
 
                        $strQuery = "SELECT lnd FROM MDRPuntoVentaBundle:ListaNegraDirecciones lnd";

                        if($cp || $municipio || $colonia){
                            $strQuery .= " WHERE ";
                        }
                        if($cp){
                            $strQuery .= "lnd.CP = :cp "; 
                        }
                        if($municipio){
                            if($cp){
                                $strQuery .= ' AND lnd.Municipio LIKE :municipio';
                            }else{
                                $strQuery .= ' lnd.Municipio LIKE :municipio';
                            }
                        }
                        if($colonia){
                            if($municipio || $cp){
                                $strQuery .= ' AND lnd.Colonia LIKE :colonia';
                            }else{
                                $strQuery .= ' lnd.Colonia LIKE :colonia';
                            }
                        }
                                                      
                        $query = $em->createQuery($strQuery);
                        if($cp){

                            $query->setParameter(':cp', $cp);

                        }
                        if($municipio){

                            $query->setParameter(':municipio', '%'.$municipio.'%');

                        }
                        if($colonia){

                            $query->setParameter(':colonia', '%'.$colonia.'%');

                        }
                        
                        $entities = $query->getResult();
                        return $entities;
                                                      
        }

        function findExistDir($cp, $municipio, $colonia){
                $em = $this->getEntityManager();
 
                        $strQuery = "SELECT lnd FROM MDRPuntoVentaBundle:ListaNegraDirecciones lnd";

                        if($cp || $municipio || $colonia){
                            $strQuery .= " WHERE ";
                        }
                        if($cp){
                            $strQuery .= "lnd.CP = :cp "; 
                        }
                        if($municipio){
                            if($cp){
                                $strQuery .= ' AND lnd.Municipio = :municipio';
                            }else{
                                $strQuery .= ' lnd.Municipio = :municipio';
                            }
                        }
                        if($colonia){
                            if($municipio || $cp){
                                $strQuery .= ' AND lnd.Colonia = :colonia';
                            }else{
                                $strQuery .= ' lnd.Colonia = :colonia';
                            }
                        }
                                                      
                        $query = $em->createQuery($strQuery);
                        if($cp){

                            $query->setParameter(':cp', $cp);

                        }
                        if($municipio){

                            $query->setParameter(':municipio', $municipio);

                        }
                        if($colonia){

                            $query->setParameter(':colonia', $colonia);

                        }
                        
                        $entities = $query->getResult();
                        return $entities;
                                                      
        }
}



