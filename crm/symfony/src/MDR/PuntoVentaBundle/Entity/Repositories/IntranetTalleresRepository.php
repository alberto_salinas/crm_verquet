<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class IntranetTalleresRepository extends EntityRepository
{
	

        public function findTalleresActivos(){
            
                $em = $this->getEntityManager();

                $hoy = new \DateTime();
                $hoy = new \DateTime($hoy->format('Ymd'));

                $strQuery = "SELECT it FROM MDRPuntoVentaBundle:IntranetTalleres it
                              WHERE it.FechaInicio = :hoy OR it.FechaInicio < :hoy AND it.FechaFin >= :hoy ORDER BY it.idTaller ";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':hoy', $hoy);
                $entities = $query->getResult();
                return $entities;

        }

        public function findTalleresInactivos(){
            
                $em = $this->getEntityManager();

                $hoy = new \DateTime();
                $hoy = new \DateTime($hoy->format('Ymd'));

                $strQuery = "SELECT it FROM MDRPuntoVentaBundle:IntranetTalleres it
                            WHERE it.FechaInicio < :hoy AND  it.FechaFin < :hoy ORDER BY it.idTaller ";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':hoy', $hoy);
                $entities = $query->getResult();
                return $entities;

        }

        public function findByesRegistrado($idTaller, $idUsuario){
            
                $em = $this->getEntityManager();               

                $strQuery = "SELECT irt FROM MDRPuntoVentaBundle:IntranetRegistrosTalleres irt
                              WHERE irt.usuario = :usuario AND irt.taller = :taller ";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':usuario', $idUsuario);
                $query->setParameter(':taller', $idTaller);
                $entities = $query->getResult();
                return $entities;

        }

        public function findTallerUpdate($idTaller){
            
                $em = $this->getEntityManager();

                $strQuery = "SELECT it FROM MDRPuntoVentaBundle:IntranetTalleres it
                              WHERE it.idTaller = :taller";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':taller', $idTaller);
                //\Doctrine\Common\Util\Debug::dump($strQuery); 
                $entities = $query->getResult();
                return $entities;

        }

        public function findAllRegistros($idTaller){
            
                $em = $this->getEntityManager();               

                $strQuery = "SELECT irt FROM MDRPuntoVentaBundle:IntranetRegistrosTalleres irt
                              WHERE irt.taller = :taller ";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':taller', $idTaller);
                $entities = $query->getResult();
                return $entities;

        }


}
