<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ComentariosPedidoRepository extends EntityRepository
{


    public function findAllCompleteByFechas($fechaInicio, $fechaFin)
    {
        $em = $this->getEntityManager();

        $fechaFin->modify('+1 day');

        $strQuery = 'SELECT cp, u, tp, ttp, p, pu FROM MDRPuntoVentaBundle:ComentariosPedido cp
                        JOIN cp.usuario u 
                        JOIN cp.tipoLlamada tp INNER JOIN tp.tipoLlamada ttp
                        INNER JOIN cp.pedido p INNER JOIN p.usuario pu
                        WHERE cp.fecha >= :fechaInicio AND cp.fecha <= :fechaFin ORDER BY cp.idComentario DESC
                        ';
        
        $query = $em->createQuery($strQuery);
        $query->setParameter('fechaInicio', $fechaInicio);
        $query->setParameter('fechaFin', $fechaFin);
        $entities = $query->getResult();

        return $entities;
    }
}
