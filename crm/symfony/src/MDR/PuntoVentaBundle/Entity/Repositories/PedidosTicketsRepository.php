<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PedidosTicketsRepository extends EntityRepository
{

		function findByTicketsPedido($idPedido){

			$em = $this->getEntityManager();
			$strQuery = "SELECT ptk FROM MDRPuntoVentaBundle:PedidosTickets ptk WHERE ptk.Pedido = :pedido ORDER BY ptk.idTicket";

			$query = $em->createQuery($strQuery);
            $query->setParameter(':pedido', $idPedido);
			$entities = $query->getResult();
            return $entities;


		}
}
