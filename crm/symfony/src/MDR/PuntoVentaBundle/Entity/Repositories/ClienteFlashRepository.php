<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ClienteFlashRepository extends EntityRepository
{      

        function findByLlamadas($fechaInicio, $fechaFin){
                $em = $this->getEntityManager();
                
                        $strQuery = "SELECT cf FROM MDRPuntoVentaBundle:ClienteFlash cf
                                    WHERE cf.Fecha BETWEEN :fechaInicio AND :fechaFin ORDER BY cf.idFlash";


            $query = $em->createQuery($strQuery);
            $query->setParameter(':fechaInicio', $fechaInicio);
            $query->setParameter(':fechaFin', $fechaFin);
            $entities = $query->getResult();
                    return $entities;


                       
        }


}
