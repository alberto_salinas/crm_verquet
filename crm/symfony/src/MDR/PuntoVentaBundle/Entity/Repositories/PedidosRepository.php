<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PedidosRepository extends EntityRepository
{


	public function findByFechasAndTerminal($fechaInicio, $fechaFin, $terminal, $OfiVentas)
	{
		$em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));
 
		//Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT p, g,tp,c,d,m,e,u,pt,tt,pro,b,t,pp,pr,pd,ov,co,tr FROM MDRPuntoVentaBundle:Pedidos p
        				JOIN p.gastosEnvio g JOIN p.pago tp JOIN p.cliente c JOIN p.direccion d
                        JOIN p.mensajeria m JOIN p.estatus e JOIN p.usuario u
                        JOIN p.pagosTarjeta pt JOIN pt.tipoTarjeta tt JOIN pt.promocion pro JOIN pt.banco b JOIN pt.terminal t
                        JOIN p.productos pp JOIN pp.productoPrecio pr JOIN pr.producto pd JOIN pr.oficinaVenta ov
                        LEFT JOIN p.cobros co LEFT JOIN co.respuesta tr
        				WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin AND p.estatus IN (17,10,1)";

        if($terminal)
        {
            $strQuery .= ' AND pt.terminal = :terminal';
        }
        if($OfiVentas)
        {
            $strQuery .= ' AND ov.claveSAP = :ofiVentas';
        }
        $strQuery .= ' ORDER BY co.fechaCobro';
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        if($terminal)
        {
            $query->setParameter(':terminal', $terminal);            
        }
        if($OfiVentas)
        {
            $query->setParameter(':ofiVentas', $OfiVentas); 
        }
        $entities = $query->getResult();
        
        //Pedidos que no pudiseron ser cobrados con el WS o estan posfechados
        
        $strQuery1 = "SELECT p, g,tp,c,d,m,e,u,pt,tt,pro,b,t,pp,pr,pd,ov,co,tr FROM MDRPuntoVentaBundle:Pedidos p
                        JOIN p.gastosEnvio g JOIN p.pago tp JOIN p.cliente c JOIN p.direccion d
                        JOIN p.mensajeria m JOIN p.estatus e JOIN p.usuario u
                        JOIN p.pagosTarjeta pt JOIN pt.tipoTarjeta tt JOIN pt.promocion pro JOIN pt.banco b JOIN pt.terminal t
                        JOIN p.productos pp JOIN pp.productoPrecio pr JOIN pr.producto pd JOIN pr.oficinaVenta ov
                        LEFT JOIN p.cobros co LEFT JOIN co.respuesta tr
                        WHERE pt.fechaCobro BETWEEN  :fechaInicio AND :fechaFin AND  p.estatus = 16";
        if($terminal)
        {
            $strQuery1 .= ' AND pt.terminal = :terminal';
        }
        if($OfiVentas)
        {
            $strQuery1 .= ' AND ov.claveSAP = :ofiVentas';
        }
        $strQuery1 .= ' ORDER BY co.fechaCobro';
        $query1 = $em->createQuery($strQuery1);
        $query1->setParameter(':fechaInicio', $fechaInicio);
        $query1->setParameter(':fechaFin', $fechaFin);
        if($terminal)
        {
            $query1->setParameter(':terminal', $terminal);            
        }
        if($OfiVentas)
        {
            $query1->setParameter(':ofiVentas', $OfiVentas); 
        }
        $entities=array_merge($entities,$query1->getResult());       
        return $entities;

    }

	public function findByFechasAndEstatus($fechaInicio, $fechaFin, $estatus, $terminal){

		$em = $this->getEntityManager();
        $idEstatus = $estatus->getIdEstatus();
        if($idEstatus == 4){
            $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(15);
        }
        $fechaFin->add(new \DateInterval('P1D'));
		//Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT p,po FROM MDRPuntoVentaBundle:Pedidos p
                    INNER JOIN p.pagosTarjeta pt INNER JOIN p.cobros po
                    WHERE po.fechaCobro >= :fechaInicio AND po.fechaCobro <= :fechaFin AND p.estatus = :estatus AND pt.terminal = :terminal";
       
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        $query->setParameter(':estatus', $estatus);
        $query->setParameter(':terminal', $terminal);
        $entities = $query->getResult();
       	return $entities;
	}

	public function findByPedidosCancelados($terminal){

		$em = $this->getEntityManager();
		$hoy = new \DateTime();
        $hoy = new \DateTime($hoy->format('Ymd'));
        $hoy1 =  clone $hoy;
        $hoy1->add(new \DateInterval('P1D')); 
        
		//Pedidos que fueron cobrados el dia de hoy
        $strQuery = "SELECT p,pt,e,po FROM MDRPuntoVentaBundle:Pedidos p
        				INNER JOIN p.pagosTarjeta pt INNER JOIN p.estatus e INNER JOIN p.cobros po
        				WHERE po.fechaCobro BETWEEN :hoy AND :hoy1 AND p.estatus = 2 AND p.pago=1 AND po.respuesta = 1";

        if($terminal)
        {
            $strQuery .= ' AND pt.terminal = :terminal';
        }             
        $query = $em->createQuery($strQuery);
        $query->setParameter(':hoy', $hoy);
        $query->setParameter(':hoy1', $hoy1);
        if($terminal)
        {
            $query->setParameter(':terminal', $terminal);            
        }
        $entities = $query->getResult();				
   
       	return $entities;

	}

	public function findAllComplete()
	{
		$em = $this->getEntityManager();

        $strQuery = "SELECT pp, p, ppr, pr, c, e, tp FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp
                        ";

        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();

        return $entities;
	}

    public function findAllCompleteByUser($user, $fechaInicio, $fechaFin)
    {
        $em = $this->getEntityManager();

        $fechaFin->modify('+1 day');

        $strQuery = 'SELECT pp, p, ppr, pr, c, e, tp FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp WHERE p.usuario = :user 
                        ';
        if($fechaInicio != null && $fechaFin != null)
        {
            $strQuery .= ' AND p.fecha >= :fechaInicio AND p.fecha <= :fechaFin ';
        }
        $strQuery .= ' ORDER BY p.idPedido DESC';
        $query = $em->createQuery($strQuery);
        $query->setParameter('user', $user);
        if($fechaInicio != null && $fechaFin != null)
        {
            $query->setParameter('fechaInicio', $fechaInicio);
            $query->setParameter('fechaFin', $fechaFin);
        }
        $entities = $query->getResult();

        return $entities;
    }

    public function findAllCompleteByFechas($fechaInicio, $fechaFin)
    {
        $em = $this->getEntityManager();

        $fechaFin->modify('+1 day');

        $strQuery = 'SELECT pp, p, ppr, pr, c, e, tp, ge, ov, u,co,tr,d FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN p.gastosEnvio ge INNER JOIN ppr.oficinaVenta ov
                        INNER JOIN p.usuario u LEFT JOIN p.cobros co Left JOIN co.respuesta tr LEFT JOIN p.descuentos d
                        WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin ORDER BY p.idPedido DESC
                        ';
        
        $query = $em->createQuery($strQuery);
        $query->setParameter('fechaInicio', $fechaInicio);
        $query->setParameter('fechaFin', $fechaFin);
        $entities = $query->getResult();

        return $entities;
    }

    public function findByFechaCobradoComplete($fechaInicio, $fechaFin)
    {
        $em = $this->getEntityManager();

        $fechaFin->modify('+1 day');

        $strQuery = 'SELECT pp, p, ppr, pr, c, e, tp, ge, ov, u,co,tr FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN p.gastosEnvio ge INNER JOIN ppr.oficinaVenta ov
                        INNER JOIN p.usuario u INNER JOIN p.cobros co INNER JOIN co.respuesta tr
                        WHERE co.fechaCobro >= :fechaInicio AND co.fechaCobro <= :fechaFin AND tr.idTipoRespuesta = 1 ORDER BY p.idPedido DESC
                        ';
        
        $query = $em->createQuery($strQuery);
        $query->setParameter('fechaInicio', $fechaInicio);
        $query->setParameter('fechaFin', $fechaFin);
        $entities = $query->getResult();

        return $entities;
    }

	public function findByRecuperacionComplete()
	{
        $em = $this->getEntityManager();

        $strQuery = "SELECT pp, p, ppr, pr, c, e, tp FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN p.telefono t LEFT JOIN p.usuariosRecuperacion ur
                            WHERE p.fecha >= :fechaMin AND p.fecha <= :fechaMax AND p.estatus IN (8, 10, 21)
                                AND t.esListaNegra = 0 AND ur is null
                        ";

        $fechaMin = new \DateTime();
        $fechaMin = $fechaMin->sub(new \DateInterval('P180D'));
        $fechaMax = new \DateTime();
        $fechaMax = $fechaMax->sub(new \DateInterval('P30D'));

        $query = $em->createQuery($strQuery);
        $query->setParameter('fechaMin', $fechaMin);
        $query->setParameter('fechaMax', $fechaMax);
        $entities = $query->getResult();

        return $entities;
	}

    public function findByUsuarioAsignadoComplete($usuario)
    {
        $em = $this->getEntityManager();

        $strQuery = "SELECT pp, p, ppr, pr, c, e, tp FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN p.telefono t INNER JOIN p.usuariosRecuperacion ur
                            WHERE p.fecha >= :fechaMin AND p.fecha <= :fechaMax AND p.estatus IN (10, 8)
                                AND t.esListaNegra = 0 AND ur = :usuario
                        ";

        $fechaMin = new \DateTime();
        $fechaMin = $fechaMin->sub(new \DateInterval('P180D'));
        $fechaMax = new \DateTime();
        $fechaMax = $fechaMax->sub(new \DateInterval('P30D'));

        $query = $em->createQuery($strQuery);
        $query->setParameter('fechaMin', $fechaMin);
        $query->setParameter('fechaMax', $fechaMax);
        $query->setParameter('usuario', $usuario);
        $entities = $query->getResult();

        return $entities;
    }

    public function findCompleteByIds($ids)
    {
        $em = $this->getEntityManager();

        $strQuery = "SELECT p, pp, ppr, pr, c, d, ge, m, e, tp, u, pt, co, te, tt, pm FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN c.direcciones d INNER JOIN p.gastosEnvio ge INNER JOIN p.mensajeria m
                        INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN p.usuario u
                        LEFT JOIN p.pagosTarjeta pt LEFT JOIN p.cobros co LEFT JOIN pt.terminal te LEFT JOIN pt.tipoTarjeta tt LEFT JOIN pt.promocion pm
                    WHERE p IN (:ids)
                        ";

        $query = $em->createQuery($strQuery);
        $query->setParameter('ids', $ids);
        $entities = $query->getResult();

        return $entities;
    }

    public function findByPedidosCliente($Ncliente,$APP,$APM,$idPedido,$NTarjeta,$idTipoPago,$NumGuia,$fecha1,$fecha2)
    {
        $em = $this->getEntityManager();        
        $queryPlus =  " WHERE ";
        
        $strQuery = "SELECT p,tp,c,pt FROM MDRPuntoVentaBundle:Pedidos p
                    INNER JOIN p.pago tp INNER JOIN p.cliente c LEFT JOIN p.pagosTarjeta pt";

       if($Ncliente){
                   
                $queryPlus .= 'c.nombre LIKE :Nombre';
       }

       if($APP){
        
            if($Ncliente){
                $queryPlus .= ' AND c.apellidoPaterno LIKE :apellidoP';
            }else{
                $queryPlus .= ' c.apellidoPaterno LIKE :apellidoP';
            }            
       }

       if($APM){
        
            if($Ncliente||$APP){
                $queryPlus .= ' AND c.apellidoMaterno LIKE :apellidoM';
            }else{
                $queryPlus .= ' c.apellidoMaterno LIKE :apellidoM';
            }            
       }

        if($idPedido != 0){
            
            if($Ncliente||$APP||$APM){
                $queryPlus .= ' AND p.idPedido = :Pedido';
            }else{
                $queryPlus .= 'p.idPedido = :Pedido';
            }            
       }
        if($idTipoPago != 0){
            
            if($Ncliente||$APP||$APM||$idPedido){
                $queryPlus .= ' AND tp.idTipoPago = :Tipopago';
            }else{
                $queryPlus .= ' tp.idTipoPago = :Tipopago';
            }            
       }
        if($NumGuia != ""){
            
            if($Ncliente||$APP||$APM||$idPedido||$idTipoPago){
                $queryPlus .= ' AND p.numeroGuia = :NumGuia';
            }else{
                $queryPlus .= ' p.numeroGuia = :NumGuia';
            }            
       }


        if($NTarjeta != ""){
            
            if($Ncliente||$APP||$APM||$idPedido||$idTipoPago||$NumGuia){
                $queryPlus .= ' AND pt.numeroTarjeta = :NumTarjeta';
            }else{
                $queryPlus .= ' pt.numeroTarjeta = :NumTarjeta';
            }            
       }
        if($fecha1 && $fecha2){

            $fecha1 = new \Datetime($fecha1);
            $fecha2 = new \Datetime($fecha2);
            
            $fecha2->add(new \DateInterval('P1D'));

            if($Ncliente||$APP||$APM||$idPedido||$idTipoPago||$NumGuia||$NTarjeta){
                $queryPlus .= ' AND p.fecha BETWEEN :fecha1 AND :fecha2';
            }else{
                $queryPlus .= ' p.fecha BETWEEN :fecha1 AND :fecha2';
            } 
       }



        $strQuery .= $queryPlus;
        $query = $em->createQuery($strQuery);
        if($Ncliente){
            $Ncliente .= "%";
            $query->setParameter('Nombre', $Ncliente);
        }
        if($APP){
            $APP .= "%";
            $query->setParameter('apellidoP', $APP);
        }
        if($APM){
            $APM .= "%";
            $query->setParameter('apellidoM', $APM);
        }
        if($idPedido){
            $query->setParameter('Pedido', $idPedido);
        }
        if($idTipoPago){
            $query->setParameter('Tipopago', $idTipoPago);
        }
        if($NumGuia){
            $query->setParameter('NumGuia', $NumGuia);
        }
        if($NTarjeta){
            $query->setParameter('NumTarjeta', $NTarjeta);
        }
        if($fecha1 && $fecha2){
            $query->setParameter('fecha1', $fecha1);
            $query->setParameter('fecha2', $fecha2);
        }
        $entities = $query->getResult();

        return $entities;    
    }

    public function findByClienteNotEstatus($cliente, $estatus)
    {
        $em = $this->getEntityManager();

        $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Pedidos p
                        WHERE p.cliente = :cliente AND p.estatus NOT IN(:estatus)
                        ";

        $query = $em->createQuery($strQuery);
        $query->setParameter('cliente', $cliente);
        $query->setParameter('estatus', $estatus);
        $entities = $query->getResult();

        return $entities;
    }

    public function PediosSAPError($fechaInicio, $fechaFin, $estatus){
        $em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));

        //Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT p,pt FROM MDRPuntoVentaBundle:Pedidos p
                        LEFT JOIN p.pagosTarjeta pt 
                        WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin AND p.estatus = :estatus";
        
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        $query->setParameter(':estatus', $estatus);
        $entities = $query->getResult();

        return $entities;
    }

    public function findByRepPedidosCobrados($fechaInicio, $fechaFin){
        $em = $this->getEntityManager();
		$fechaFin->add(new \DateInterval('P1D'));
        //Pedidos que no han sido cobrado
        
		$strQuery = "SELECT p,pt,pm,co,es,us,t, pp,ppr,ov FROM MDRPuntoVentaBundle:Pedidos p
                    INNER JOIN p.pagosTarjeta pt INNER JOIN pt.promocion pm INNER JOIN p.cobros co
                    INNER JOIN p.estatus es INNER JOIN p.usuario us INNER JOIN pt.terminal t 
                    INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.oficinaVenta ov
                    WHERE  co.fechaCobro >= :fechaInicio AND co.fechaCobro <= :fechaFin AND co.respuesta = 1 AND p.estatus IN(2,3,15) ORDER BY p.idPedido,pt.fechaCobro";
				
                    
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        
        $entities = $query->getResult();
        return $entities;
    }

    public function findByRepPedidosCobradosIntentos($fechaInicio, $fechaFin){
        $em = $this->getEntityManager();
		$fechaFin->add(new \DateInterval('P1D'));
        //Pedidos que no han sido cobrado
        
		$strQuery = "SELECT p,pt,pm,co,es,us,t FROM MDRPuntoVentaBundle:Pedidos p
                    INNER JOIN p.pagosTarjeta pt INNER JOIN pt.promocion pm INNER JOIN p.cobros co
                    INNER JOIN p.estatus es INNER JOIN p.usuario us INNER JOIN pt.terminal t 
                    WHERE  pt.fechaCobro >= :fechaInicio AND pt.fechaCobro <= :fechaFin  AND p.estatus IN(2,3,15) ORDER BY p.idPedido,pt.fechaCobro";
				
                    
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        
        $entities = $query->getResult();
        return $entities;
    }




        public function findByRepPedidosCancelados($fechaInicio, $fechaFin){
        $em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));

        //Pedidos que no han sido cobrado
        $strQuery = "SELECT pc,p,es,cl,us, tc, pp,ppr,ov,tp FROM MDRPuntoVentaBundle:PedidosCancelados pc 
                    INNER JOIN pc.pedido p INNER JOIN p.estatus es INNER JOIN pc.tipoCancelacion tc
                    INNER JOIN p.usuario us INNER JOIN p.cliente cl
                    INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.oficinaVenta ov INNER JOIN p.pago tp
                    WHERE p.fecha BETWEEN  :fechaInicio AND :fechaFin AND p.estatus = 5 ORDER BY p.idPedido,p.fecha";

                    
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        
        $entities = $query->getResult();

        return $entities;
    }

    public function findByRepEfectividad($fechaInicio, $fechaFin, $OfiVentas){
        $em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));

        //Pedidos que no han sido cobrado
        $strQuery = "SELECT pp, p, ppr, pr, ov, c, u, e, tp, pt, co FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.usuario u INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN ppr.oficinaVenta ov
                        LEFT JOIN p.pagosTarjeta pt LEFT JOIN p.cobros co
                    WHERE p.fecha BETWEEN  :fechaInicio AND :fechaFin AND p.estatus = 15";

        if($OfiVentas){
            $strQuery .= ' AND ppr.oficinaVenta = :OficinaVentas ';
        }
                          
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        if($OfiVentas){
            $query->setParameter(':OficinaVentas', $OfiVentas);
        }
        
        $entities = $query->getResult();

        return $entities;
    }


    public function findByPedidosPautaSeca($fechaInicio,$fechaFin){
        $em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));

        //Pedidos en estatus pauta seca
        $strQuery = "SELECT pp, p, ppr, pr, ov, c, u, e, tp, pt, co FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.usuario u INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN ppr.oficinaVenta ov
                        LEFT JOIN p.pagosTarjeta pt LEFT JOIN p.cobros co
                    WHERE p.fecha BETWEEN  :fechaInicio AND :fechaFin AND p.estatus = 25";

       
                          
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        
        $entities = $query->getResult();

        return $entities;
    }


    public function findByPedidosInvestigacion($fechaInicio,$fechaFin){
        $em = $this->getEntityManager();
        $fechaFin->add(new \DateInterval('P1D'));

        //Pedidos en estatus pauta seca
        $strQuery = "SELECT pp, p, ppr, pr, ov, c, u, e, tp, pt, co FROM MDRPuntoVentaBundle:Pedidos p
                        INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppr INNER JOIN ppr.producto pr INNER JOIN p.cliente c
                        INNER JOIN p.usuario u INNER JOIN p.estatus e INNER JOIN p.pago tp INNER JOIN ppr.oficinaVenta ov
                        LEFT JOIN p.pagosTarjeta pt LEFT JOIN p.cobros co
                    WHERE p.fecha BETWEEN  :fechaInicio AND :fechaFin AND p.estatus = 9";

       
                          
        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        
        $entities = $query->getResult();

        return $entities;
    }    

}
