<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PedidosOrdenCobroRepository extends EntityRepository
{
	function findByExistCobro($pedido){
		$em = $this->getEntityManager();
 
        $strQuery = "SELECT po FROM MDRPuntoVentaBundle:PedidosOrdenCobro po
        	WHERE po.pedido = :pedido AND po.numeroAutorizacion != '' ";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':pedido', $pedido);
        $entities = $query->getResult();
        return $entities;
	}

	function findByOrdenPedido($pedido){
		$em = $this->getEntityManager();
 
        $strQuery = "SELECT po FROM MDRPuntoVentaBundle:PedidosOrdenCobro po
        	WHERE po.pedido = :pedido AND po.numeroAutorizacion != '' AND po.respuesta = 1 ";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':pedido', $pedido);
        $entities = $query->getResult();
        return $entities;
	}

        function findByIntentosCobro($pedido,$numTarjeta){
                $em = $this->getEntityManager();
                $fecha1 = new \DateTime();
                date_time_set($fecha1, 00, 00, 00);
                $fecha2 = new \DateTime();
                date_time_set($fecha2, 00, 00, 00);
                $fecha2->add(new \DateInterval('P1D'));
               

                $strQuery = "SELECT po FROM MDRPuntoVentaBundle:PedidosOrdenCobro po
                        WHERE po.pedido= :pedido AND po.numeroTarjeta = :tarjeta AND po.fechaCobro BETWEEN :fecha1 AND :fecha2 ";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':pedido', $pedido);
                $query->setParameter(':tarjeta', $numTarjeta);
                $query->setParameter(':fecha1', $fecha1);
                $query->setParameter(':fecha2', $fecha2);
                $entities = $query->getResult();
                return $entities;
        }
}
