<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class IntranetInboxRepository extends EntityRepository
{
	
        function getInbox($usuario){

                $em = $this->getEntityManager();

                        $strQuery = "SELECT iin FROM MDRPuntoVentaBundle:IntranetInbox iin
                                     WHERE iin.Receptor = :usuario AND iin.Estatus = 1  ORDER BY iin.idInbox";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $entities = $query->getResult();
                        return $entities;
        }

        function getInboxToDelete($idMensaje,$usuario){

                        $em = $this->getEntityManager();

                        $strQuery = "SELECT iin FROM MDRPuntoVentaBundle:IntranetInbox iin
                                     WHERE iin.Receptor = :usuario AND iin.mensaje = :mensaje";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $query->setParameter(':mensaje', $idMensaje);
                        $entities = $query->getResult();
                        return $entities;
        }

        function getInboxsend($usuario,$fechaInicio,$fechaFin){

                        $em = $this->getEntityManager();
                        $fechaFin->add(new \DateInterval('P1D'));

                        $strQuery = "SELECT imsj FROM MDRPuntoVentaBundle:IntranetMensajeInbox imsj
                                    WHERE imsj.Emisor = :usuario AND imsj.Fecha BETWEEN :Inicio AND :Fin ORDER BY imsj.idMensaje";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $query->setParameter(':Inicio', $fechaInicio);
                        $query->setParameter(':Fin', $fechaFin);
                        $entities = $query->getResult();
                        return $entities;
        }

        function getInboxdelete($usuario,$fechaInicio,$fechaFin){

                        $em = $this->getEntityManager();
                        $fechaFin->add(new \DateInterval('P1D'));

                        $strQuery = "SELECT iin FROM MDRPuntoVentaBundle:IntranetInbox iin
                                     WHERE iin.Receptor = :usuario AND iin.Estatus = 3 AND iin.FechaEliminado BETWEEN :Inicio AND :Fin  ORDER BY iin.idInbox  ";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $query->setParameter(':Inicio', $fechaInicio);
                        $query->setParameter(':Fin', $fechaFin);
                        $entities = $query->getResult();
                        return $entities;
        }

        function getInboxRead($usuario,$fechaInicio,$fechaFin){

                        $em = $this->getEntityManager();
                        $fechaFin->add(new \DateInterval('P1D'));

                        $strQuery = "SELECT ims,iin FROM MDRPuntoVentaBundle:IntranetMensajeInbox ims
                                    INNER JOIN ims.Inbox iin
                                    WHERE iin.Receptor = :usuario AND iin.Estatus = 2 AND ims.Fecha BETWEEN :Inicio AND :Fin ORDER BY ims.idMensaje ";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $query->setParameter(':Inicio', $fechaInicio);
                        $query->setParameter(':Fin', $fechaFin);
                        $entities = $query->getResult();
                        return $entities;
        }

       function getCCRespuesta($idMensaje,$usuario){

                        $em = $this->getEntityManager();

                        $strQuery = "SELECT iin FROM MDRPuntoVentaBundle:IntranetInbox iin
                                     WHERE iin.Receptor != :usuario AND iin.mensaje = :mensaje";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $query->setParameter(':mensaje', $idMensaje);
                        $entities = $query->getResult();
                        return $entities;
       }

        function getTotalInbox($usuario){

                $em = $this->getEntityManager();

                        $strQuery = "SELECT ims,iin FROM MDRPuntoVentaBundle:IntranetMensajeInbox ims
                                    INNER JOIN ims.Inbox iin
                                    WHERE iin.Receptor = :usuario AND iin.Estatus = 1  ORDER BY iin.idInbox";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':usuario', $usuario);
                        $entities = $query->getResult();
                        return $entities;
        }


}
