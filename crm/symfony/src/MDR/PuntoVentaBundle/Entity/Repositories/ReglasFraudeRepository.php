<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use MDR\PuntoVentaBundle\Entity\ListaNegraTarjetas;
use MDR\PuntoVentaBundle\Entity\Telefonos;
use MDR\PuntoVentaBundle\Entity\Pedidos;
use MDR\PuntoVentaBundle\Entity\ListaNegraClientes;
use MDR\PuntoVentaBundle\Entity\ListaNegraDirecciones;
use MDR\PuntoVentaBundle\Entity\DireccionesUsuarios;
use MDR\PuntoVentaBundle\Entity\ZonasRiesgo;
use MDR\PuntoVentaBundle\Entity\ListaNegraMontos;


class ReglasFraudeRepository extends EntityRepository
{
	function findReglasActivas($activo){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT rf FROM MDRPuntoVentaBundle:ReglasFraude rf WHERE rf.Activo = :activo AND rf.idReglas != 9 AND rf.idReglas != 10 AND rf.idReglas != 11 "; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':activo', $activo);
                $entities = $query->getResult();
                return $entities; 
        }

        function findLnTarjetas($numTarjeta){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT lnt FROM MDRPuntoVentaBundle:ListaNegraTarjetas lnt WHERE lnt.numeroTarjeta = :tarjeta AND lnt.activo = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':tarjeta', $numTarjeta);
                $entities = $query->getResult();
                return $entities; 
        }

        function findLnTelefonos($Telefono){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Telefonos t WHERE t.numero = :tel AND t.esListaNegra = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':tel', $Telefono);
                $entities = $query->getResult();
                return $entities; 
        }

        function findPedidosDia($idCliente){
                $em = $this->getEntityManager();
                $fechaInicio = new \DateTime();
                $fechaInicio = new \DateTime($fechaInicio->format('Ymd'));
                $fechaFin = clone $fechaInicio;
                $fechaFin->add(new \DateInterval('P1D')); 


                $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Pedidos p WHERE p.cliente = :cliente AND p.fecha BETWEEN  :fechaInicio AND :fechaFin"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':cliente', $idCliente);
                $query->setParameter(':fechaInicio', $fechaInicio);
                $query->setParameter(':fechaFin', $fechaFin);
                $entities = $query->getResult();
                return $entities; 
        }

        function findLnClientes($Cliente){
                $em = $this->getEntityManager();
                $strQuery = "SELECT lnc FROM MDRPuntoVentaBundle:ListaNegraClientes lnc WHERE lnc.NombreCompleto = :nombre AND lnc.Activo = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':nombre', $Cliente);
                $entities = $query->getResult();
                return $entities; 
        }

        function findLnDirecciones($cp,$municipio,$colonia){

                $em = $this->getEntityManager();
                $strQuery = "SELECT lnd FROM MDRPuntoVentaBundle:ListaNegraDirecciones lnd WHERE lnd.CP = :cp AND lnd.Municipio = :municipio AND lnd.Colonia = :colonia AND lnd.Activo = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':cp', $cp);
                $query->setParameter(':municipio', utf8_decode($municipio));
                $query->setParameter(':colonia', utf8_decode($colonia));
                $entities = $query->getResult();
                return $entities; 
        }

        function findStaffDirecciones($cp,$municipio,$colonia){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT du FROM MDRPuntoVentaBundle:DireccionesUsuarios du WHERE du.cp = :cp AND du.municipio = :municipio AND du.colonia = :colonia"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':cp', $cp);
                $query->setParameter(':municipio', utf8_decode($municipio));
                $query->setParameter(':colonia', utf8_decode($colonia));
                $entities = $query->getResult();
                return $entities; 
        }
        
        function findZonaRiesgo($cp,$municipio){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT zr FROM MDRPuntoVentaBundle:ZonasRiesgo zr WHERE zr.CP = :cp AND zr.Municipio = :municipio AND zr.Activo = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':cp', $cp);
                $query->setParameter(':municipio', utf8_decode($municipio));
                $entities = $query->getResult();
                return $entities; 
        }
        
        function findClaveProducto($clave){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT lnm FROM MDRPuntoVentaBundle:ListaNegraMontos lnm WHERE lnm.ClaveProducto = :clave AND lnm.Activo = 1"; 
                $query = $em->createQuery($strQuery);
                $query->setParameter(':clave', $clave);
                $entities = $query->getResult();
                return $entities; 
        }




}

