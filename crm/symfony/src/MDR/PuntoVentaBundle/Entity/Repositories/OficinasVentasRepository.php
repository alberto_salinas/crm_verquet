<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ProductosRepository
 */
class OficinasVentasRepository extends EntityRepository
{
        public function findByPedido($idPedido)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT prod.idProducto FROM MDRPuntoVentaBundle:Pedidos p
                                INNER JOIN p.productos det INNER JOIN det.productoPrecio pp INNER JOIN pp.producto prod
                                WHERE p = :idPedido 
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('idPedido', $idPedido);
                $entities = $query->getResult();
                $idProductos = array();
                foreach ($entities as $entity) {
                        $idProductos[] = $entity['idProducto'];
                }

                $strQuery = "SELECT ov.claveSAP, count(distinct pp.producto) as cont FROM MDRPuntoVentaBundle:ProductoPrecios pp
                                INNER JOIN pp.producto p INNER JOIN pp.oficinaVenta ov
                                WHERE p IN (:idProductos) GROUP BY ov.claveSAP
                                ";
                $query = $em->createQuery($strQuery);
                $query->setParameter('idProductos', $idProductos);
                $entities = $query->getResult();

                $oficinasVentas = array();
                foreach ($entities as $entity) {
                        if((int)$entity['cont'] == count($idProductos))
                        {
                                $oficinasVentas[] = $entity['claveSAP'];
                        }
                }
                $strQuery = "SELECT ov FROM MDRPuntoVentaBundle:OficinasVentas ov WHERE ov.claveSAP IN (:clavesSAP)";
                $query = $em->createQuery($strQuery);
                $query->setParameter('clavesSAP', $oficinasVentas);
                $entities = $query->getResult();

                return $entities;
	}

        public function findByFiltroCanal($canal)
        {
                $em = $this->getEntityManager();

                $strQuery = 'SELECT ov FROM MDRPuntoVentaBundle:OficinasVentas ov 
                                INNER JOIN ov.productosPrecios pp WHERE pp.canalDistribucion = :canal ';
                $query = $em->createQuery($strQuery);
                $query->setParameter('canal', $canal);
                $entities = $query->getResult();

                return $entities;
        }
}
