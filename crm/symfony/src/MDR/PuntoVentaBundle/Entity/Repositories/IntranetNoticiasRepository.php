<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class IntranetNoticiasRepository extends EntityRepository
{
	function getNoticiasActivas(){

        $em = $this->getEntityManager();

        $hoy = new \DateTime();
        $hoy = new \DateTime($hoy->format('Ymd'));

        $strQuery = "SELECT inn FROM MDRPuntoVentaBundle:IntranetNoticias inn
                      WHERE inn.FechaInicio = :hoy OR inn.FechaInicio < :hoy AND inn.FechaFin >= :hoy ORDER BY inn.TipoNoticia ";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':hoy', $hoy);
        $entities = $query->getResult();
        return $entities;              
    } 

    function getNoticasInactivas(){

       $em = $this->getEntityManager();

        $hoy = new \DateTime();
        $hoy = new \DateTime($hoy->format('Ymd'));

        $strQuery = "SELECT inn FROM MDRPuntoVentaBundle:IntranetNoticias inn
                      WHERE inn.FechaInicio < :hoy AND  inn.FechaFin < :hoy ORDER BY inn.TipoNoticia ";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':hoy', $hoy);
        $entities = $query->getResult();
        return $entities;                      
    }      


}
