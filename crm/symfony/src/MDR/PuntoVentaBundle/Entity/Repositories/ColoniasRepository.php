<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ColoniasRepository
 */
class ColoniasRepository extends EntityRepository
{
	public function findByEstado($estado)
	{
		$em = $this->getEntityManager();

		$strQuery = "SELECT c FROM MDRPuntoVentaBundle:Colonias c
                        INNER JOIN c.municipio m
                        WHERE m.estado = :estado ORDER BY c.nombre ASC
                        ";

        $query = $em->createQuery($strQuery);
        $query->setParameter('estado', $estado);
        $entities = $query->getResult();

        return $entities;
	}
}