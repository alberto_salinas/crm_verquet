<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class PagoTarjetaRepository extends EntityRepository
{
	function findByFechaCobro($idpedido){
		$em = $this->getEntityManager();

        $strQuery1 = "SELECT pt.fechaCobro FROM MDRPuntoVentaBundle:PagoTarjeta pt
                       WHERE pt.pedido = :idPedido";

        $query1 = $em->createQuery($strQuery1);
        $query1->setParameter(':idPedido', $idpedido);               
        $entities = $query1->getResult();
        return $entities;
	}


}
