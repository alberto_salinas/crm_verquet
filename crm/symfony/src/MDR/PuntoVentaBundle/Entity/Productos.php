<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Productos
 */
class Productos implements JsonSerializable
{
    /**
     * @var string
     */
    private $codigoSAP;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $peso;

    /**
     * @var string
     */
    private $uMVentas;

    /**
     * @var integer
     */
    private $idProducto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productoPrecios;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productosHijo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $grupos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productoPrecios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productosHijo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set codigoSAP
     *
     * @param string $codigoSAP
     * @return Productos
     */
    public function setCodigoSAP($codigoSAP)
    {
        $this->codigoSAP = $codigoSAP;

        return $this;
    }

    /**
     * Get codigoSAP
     *
     * @return string 
     */
    public function getCodigoSAP()
    {
        return $this->codigoSAP;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Productos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set peso
     *
     * @param string $peso
     * @return Productos
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string 
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set uMVentas
     *
     * @param string $uMVentas
     * @return Productos
     */
    public function setUMVentas($uMVentas)
    {
        $this->uMVentas = $uMVentas;

        return $this;
    }

    /**
     * Get uMVentas
     *
     * @return string 
     */
    public function getUMVentas()
    {
        return $this->uMVentas;
    }

    /**
     * Get idProducto
     *
     * @return integer 
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Add productoPrecios
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecios
     * @return Productos
     */
    public function addProductoPrecio(\MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecios)
    {
        $this->productoPrecios[] = $productoPrecios;

        return $this;
    }

    /**
     * Remove productoPrecios
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecios
     */
    public function removeProductoPrecio(\MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecios)
    {
        $this->productoPrecios->removeElement($productoPrecios);
    }

    /**
     * Get productoPrecios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductoPrecios()
    {
        return $this->productoPrecios;
    }

    /**
     * Add productosHijo
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productosHijo
     * @return Productos
     */
    public function addProductosHijo(\MDR\PuntoVentaBundle\Entity\Productos $productosHijo)
    {
        $this->productosHijo[] = $productosHijo;

        return $this;
    }

    /**
     * Remove productosHijo
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productosHijo
     */
    public function removeProductosHijo(\MDR\PuntoVentaBundle\Entity\Productos $productosHijo)
    {
        $this->productosHijo->removeElement($productosHijo);
    }

    /**
     * Get productosHijo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosHijo()
    {
        return $this->productosHijo;
    }

    /**
     * Add grupos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Grupos $grupos
     * @return Productos
     */
    public function addGrupo(\MDR\PuntoVentaBundle\Entity\Grupos $grupos)
    {
        $this->grupos[] = $grupos;

        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Grupos $grupos
     */
    public function removeGrupo(\MDR\PuntoVentaBundle\Entity\Grupos $grupos)
    {
        $this->grupos->removeElement($grupos);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }

    public function jsonSerialize()
    {
        return array(
            'idProducto'    => $this->idProducto,
            'codigoSAP'     => $this->codigoSAP,
            'descripcion'   => $this->descripcion,
            'peso'          => $this->peso,
            'UMVentas'      => $this->uMVentas,
            'toString'      => $this->__toString(),
        );

    }
}
