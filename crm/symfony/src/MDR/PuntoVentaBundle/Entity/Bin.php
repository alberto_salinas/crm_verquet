<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bin
 */
class Bin
{
    /**
     * @var integer
     */
    private $idBin;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoTarjeta
     */
    private $tipoTarjeta;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Bancos
     */
    private $banco;


    /**
     * Get idBin
     *
     * @return integer 
     */
    public function getIdBin()
    {
        return $this->idBin;
    }

    /**
     * Set tipoTarjeta
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta
     * @return Bin
     */
    public function setTipoTarjeta(\MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta = null)
    {
        $this->tipoTarjeta = $tipoTarjeta;

        return $this;
    }

    /**
     * Get tipoTarjeta
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoTarjeta 
     */
    public function getTipoTarjeta()
    {
        return $this->tipoTarjeta;
    }

    /**
     * Set banco
     *
     * @param \MDR\PuntoVentaBundle\Entity\Bancos $banco
     * @return Bin
     */
    public function setBanco(\MDR\PuntoVentaBundle\Entity\Bancos $banco = null)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \MDR\PuntoVentaBundle\Entity\Bancos 
     */
    public function getBanco()
    {
        return $this->banco;
    }
}
