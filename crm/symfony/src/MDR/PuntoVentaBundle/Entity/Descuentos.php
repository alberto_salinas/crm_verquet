<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Descuentos
 */
class Descuentos implements JsonSerializable
{
    /**
     * @var string
     */
    private $clave;

    /**
     * @var float
     */
    private $desde;

    /**
     * @var float
     */
    private $hasta;

    /**
     * @var float
     */
    private $importe;

    /**
     * @var boolean
     */
    private $activo;

    /**
     * @var integer
     */
    private $idDescuento;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $origenes;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoPagos
     */
    private $tipoPago;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->origenes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Descuentos
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set desde
     *
     * @param float $desde
     * @return Descuentos
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;

        return $this;
    }

    /**
     * Get desde
     *
     * @return float 
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * Set hasta
     *
     * @param float $hasta
     * @return Descuentos
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;

        return $this;
    }

    /**
     * Get hasta
     *
     * @return float 
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Descuentos
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Descuentos
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get idDescuento
     *
     * @return integer 
     */
    public function getIdDescuento()
    {
        return $this->idDescuento;
    }

    /**
     * Add origenes
     *
     * @param \MDR\PuntoVentaBundle\Entity\DescuentosOrigenVentas $origenes
     * @return Descuentos
     */
    public function addOrigen(\MDR\PuntoVentaBundle\Entity\DescuentosOrigenVentas $origenes)
    {
        $this->origenes[] = $origenes;

        return $this;
    }

    /**
     * Remove origenes
     *
     * @param \MDR\PuntoVentaBundle\Entity\DescuentosOrigenVentas $origenes
     */
    public function removeOrigen(\MDR\PuntoVentaBundle\Entity\DescuentosOrigenVentas $origenes)
    {
        $this->origenes->removeElement($origenes);
    }

    /**
     * Get origenes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrigenes()
    {
        return $this->origenes;
    }

    /**
     * Set tipoPago
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago
     * @return Descuentos
     */
    public function setTipoPago(\MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago = null)
    {
        $this->tipoPago = $tipoPago;

        return $this;
    }

    /**
     * Get tipoPago
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoPagos 
     */
    public function getTipoPago()
    {
        return $this->tipoPago;
    }

    public function __toString()
    {
        return $this->clave.' $ '.$this->importe; 
}

    public function jsonSerialize()
    {
        return array(
            'idDescuento'   => $this->idDescuento,
            'clave'         => $this->clave,
            'desde'         => $this->desde,
            'hasta'         => $this->hasta,
            'importe'       => $this->importe,
            'activo'        => $this->activo,
            'tipoPago'      => $this->tipoPago,
        );

    }
}