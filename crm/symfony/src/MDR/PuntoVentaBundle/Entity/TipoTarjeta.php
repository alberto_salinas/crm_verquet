<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * TipoTarjeta
 */
class TipoTarjeta implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idTipoTarjeta;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoTarjeta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idTipoTarjeta
     *
     * @return integer 
     */
    public function getIdTipoTarjeta()
    {
        return $this->idTipoTarjeta;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(
            'idTipoTarjeta'    => $this->idTipoTarjeta,
            'descripcion'   => $this->descripcion,
            'toString'      => $this->__toString(),
        );

    }
}
