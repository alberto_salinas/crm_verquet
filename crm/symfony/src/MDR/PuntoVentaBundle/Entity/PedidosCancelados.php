<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * PedidosCancelados
 */
class PedidosCancelados implements JsonSerializable
{
    /**
     * @var string
     */
    private $comentariosCancelacion;

    /**
     * @var string
     */
    private $pedidoSAP;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TiposCancelacion
     */
    private $tipoCancelacion;


    /**
     * Set comentariosCancelacion
     *
     * @param string $comentariosCancelacion
     * @return PedidosCancelados
     */
    public function setComentariosCancelacion($comentariosCancelacion)
    {
        $this->comentariosCancelacion = $comentariosCancelacion;

        return $this;
    }

    /**
     * Get comentariosCancelacion
     *
     * @return string 
     */
    public function getComentariosCancelacion()
    {
        return $this->comentariosCancelacion;
    }

    /**
     * Set pedidoSAP
     *
     * @param string $pedidoSAP
     * @return PedidosCancelados
     */
    public function setPedidoSAP($pedidoSAP)
    {
        $this->pedidoSAP = $pedidoSAP;

        return $this;
    }

    /**
     * Get pedidoSAP
     *
     * @return string 
     */
    public function getPedidoSAP()
    {
        return $this->pedidoSAP;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PedidosCancelados
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set tipoCancelacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\TiposCancelacion $tipoCancelacion
     * @return PedidosCancelados
     */
    public function setTipoCancelacion(\MDR\PuntoVentaBundle\Entity\TiposCancelacion $tipoCancelacion = null)
    {
        $this->tipoCancelacion = $tipoCancelacion;

        return $this;
    }

    /**
     * Get tipoCancelacion
     *
     * @return \MDR\PuntoVentaBundle\Entity\TiposCancelacion 
     */
    public function getTipoCancelacion()
    {
        return $this->tipoCancelacion;
    }

     public function jsonSerialize()
    {
        return array(
            'comentariosCancelacion'        => $this->comentariosCancelacion,
            'pedidoSAP'                     => $this->pedidoSAP,
            'pedido'                        => $this->pedido,
            'tipoCancelacion'               => $this->tipoCancelacion,
                       
        );

    }
}
