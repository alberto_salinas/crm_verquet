<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * GastosEnvio
 */
class GastosEnvio implements JsonSerializable
{
    /**
     * @var float
     */
    private $precio;

    /**
     * @var integer
     */
    private $idGastosEnvio;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoPagos
     */
    private $tipoPago;
	
	    /** Revisar
     * @var integer
     */
    private $idTipoPago;

    /**
     * @var boolean
     */
    private $activo;


    /**
     * Set precio
     *
     * @param float $precio
     * @return GastosEnvio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
		return floatval($this->precio);
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecioSinIva()
    {
        $precio = floatval($this->precio);
        if($precio == 0) return 0;
        return number_format(($precio/1.16), 5, '.', '');
    }

    /**
     * Get idGastosEnvio
     *
     * @return integer 
     */
    public function getIdGastosEnvio()
    {
        return $this->idGastosEnvio;
    }

    /**
     * Set tipoPago
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago
     * @return GastosEnvio
     */
    public function setTipoPago(\MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago = null)
    {
        $this->tipoPago = $tipoPago;

        return $this;
    }

    /**
     * Get tipoPago
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoPagos 
     */
    public function getTipoPago()
    {
        return $this->tipoPago;
    }

    public function __toString()
    {
        return $this->getPrecio().'';
    }
	
	    /** Revisar
     * Get tipoPago
     *
     * @return integer 
     */
    public function getIdTipoPago()
    {
        return $this->idTipoPago;
    }

     /**
     * Set activo
     *
     * @param boolean $activo
     * @return GastosEnvio
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    public function jsonSerialize()
    {
        return array(
            'idGastosEnvio'     => $this->idGastosEnvio,
            'precio'            => $this->getPrecio(),
            'tipoPago'          => $this->tipoPago,
            'toString'          => $this->__toString(),
            'activo'            => $this->activo,
        );
    }



   
}
