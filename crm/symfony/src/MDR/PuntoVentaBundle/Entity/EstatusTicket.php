<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * EstatusTicket
 */
class EstatusTicket implements JsonSerializable
{
    /**
     * @var string
     */
    private $Descripcion;

    /**
     * @var integer
     */
    private $idEstatusTicket;


    /**
     * Set Descripcion
     *
     * @param string $descripcion
     * @return EstatusTicket
     */
    public function setDescripcion($descripcion)
    {
        $this->Descripcion = $descripcion;

        return $this;
    }

    /**
     * Get Descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Get idEstatusTicket
     *
     * @return integer 
     */
    public function getIdEstatusTicket()
    {
        return $this->idEstatusTicket;
    }

    public function jsonSerialize()
    {
        return array(

            'Descripcion'       => $this->Descripcion,
            'idEstatusTicket'   => $this->idEstatusTicket,
                       
        );

    }
}
