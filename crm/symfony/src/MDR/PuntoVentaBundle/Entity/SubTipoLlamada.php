<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * SubTipoLlamada
 */
class SubTipoLlamada implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcionSubTipo;

    /**
     * @var integer
     */
    private $idSubTipo;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoLlamada
     */
    private $tipoLlamada;


    /**
     * Set descripcionSubTipo
     *
     * @param string $descripcionSubTipo
     * @return SubTipoLlamada
     */
    public function setDescripcionSubTipo($descripcionSubTipo)
    {
        $this->descripcionSubTipo = $descripcionSubTipo;

        return $this;
    }

    /**
     * Get descripcionSubTipo
     *
     * @return string 
     */
    public function getDescripcionSubTipo()
    {
        return $this->descripcionSubTipo;
    }

    /**
     * Get idSubTipo
     *
     * @return integer 
     */
    public function getIdSubTipo()
    {
        return $this->idSubTipo;
    }

    /**
     * Set tipoLlamada
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoLlamada $tipoLlamada
     * @return SubTipoLlamada
     */
    public function setTipoLlamada(\MDR\PuntoVentaBundle\Entity\TipoLlamada $tipoLlamada = null)
    {
        $this->tipoLlamada = $tipoLlamada;

        return $this;
    }

    /**
     * Get tipoLlamada
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoLlamada 
     */
    public function getTipoLlamada()
    {
        return $this->tipoLlamada;
    }

	public function __toString()
    {
        return $this->descripcionSubTipo; 
    }


    public function jsonSerialize()
    {
        return array(

            'idSubTipo'             => $this->idSubTipo,
            'tipoLlamada'           => $this->tipoLlamada,                       
            'descripcionSubTipo'    => $this->descripcionSubTipo,
        );

    }
}
