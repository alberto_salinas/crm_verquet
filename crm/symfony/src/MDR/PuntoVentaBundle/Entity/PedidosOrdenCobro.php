<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * PedidosOrdenCobro
 */
class PedidosOrdenCobro implements JsonSerializable
{
    /**
     * @var string
     */
    private $numeroAutorizacion;

    /**
     * @var string
     */
    private $comentarios;

    /**
     * @var \DateTime
     */
    private $fechaCobro;

    /**
     * @var string
     */
    private $numeroTarjeta;

    /**
     * @var integer
     */
    private $idPedidoCobro;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoRespuesta
     */
    private $respuesta;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Set numeroAutorizacion
     *
     * @param string $numeroAutorizacion
     * @return PedidosOrdenCobro
     */
    public function setNumeroAutorizacion($numeroAutorizacion)
    {
        $this->numeroAutorizacion = $numeroAutorizacion;

        return $this;
    }

    /**
     * Get numeroAutorizacion
     *
     * @return string 
     */
    public function getNumeroAutorizacion()
    {
        return $this->numeroAutorizacion;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     * @return PedidosOrdenCobro
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set fechaCobro
     *
     * @param \DateTime $fechaCobro
     * @return PedidosOrdenCobro
     */
    public function setFechaCobro($fechaCobro)
    {
        $this->fechaCobro = $fechaCobro;

        return $this;
    }

    /**
     * Get fechaCobro
     *
     * @return \DateTime 
     */
    public function getFechaCobro()
    {
        return $this->fechaCobro;
    }

    /**
     * Set numeroTarjeta
     *
     * @param string $numeroTarjeta
     * @return PedidosOrdenCobro
     */
    public function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;

        return $this;
    }

    /**
     * Get numeroTarjeta
     *
     * @return string 
     */
    public function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    /**
     * Get idPedidoCobro
     *
     * @return integer 
     */
    public function getIdPedidoCobro()
    {
        return $this->idPedidoCobro;
    }

    /**
     * Set respuesta
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoRespuesta $respuesta
     * @return PedidosOrdenCobro
     */
    public function setRespuesta(\MDR\PuntoVentaBundle\Entity\TipoRespuesta $respuesta = null)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoRespuesta 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PedidosOrdenCobro
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return PedidosOrdenCobro
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function __GregorianDateTime()
    {
        if($this->fechaCobro != null)
        {
            return date_format($this->fechaCobro,'d-m-Y  H:i:s');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y  H:i:s');
        }
    }

	public function jsonSerialize()
	{
	     return array(

	        'numeroAutorizacion'   	=> $this->numeroAutorizacion,
	        'comentarios'  		 	=> $this->comentarios,
	        'fechaCobro'   			=> $this->fechaCobro,
            'fechaCobroGregorian'   => $this->__GregorianDateTime(),
	        'idPedidoCobro'   		=> $this->idPedidoCobro,
	        'respuesta'   			=> $this->respuesta,
            'numeroTarjeta'         => $this->numeroTarjeta,
            'usuario'               => $this->usuario,
				
	     );

	 }
}
