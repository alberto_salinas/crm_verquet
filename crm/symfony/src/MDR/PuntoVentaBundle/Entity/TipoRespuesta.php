<?php
namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * TipoRespuesta
 */
class TipoRespuesta implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idTipoRespuesta;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoRespuesta
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idTipoRespuesta
     *
     * @return integer 
     */
    public function getIdTipoRespuesta()
    {
        return $this->idTipoRespuesta;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(
            'descripcion'    	=> $this->descripcion,
            'idTipoRespuesta'   => $this->idTipoRespuesta,
        );

    }
}
