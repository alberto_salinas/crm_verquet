<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ListaNegraTarjetas
 */
class ListaNegraTarjetas implements JsonSerializable
{
    /**
     * @var string
     */
    private $numeroTarjeta;

    /**
     * @var boolean
     */
    private $activo;

    /**
     * @var integer
     */
    private $idListaNegra;


    /**
     * Set numeroTarjeta
     *
     * @param string $numeroTarjeta
     * @return ListaNegraTarjetas
     */
    public function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;

        return $this;
    }

    /**
     * Get numeroTarjeta
     *
     * @return string 
     */
    public function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return ListaNegraTarjetas
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get idListaNegra
     *
     * @return integer 
     */
    public function getIdListaNegra()
    {
        return $this->idListaNegra;
    }


    public function jsonSerialize()
    {
        return array(

            'numeroTarjeta'    	=> $this->numeroTarjeta,
			'idListaNegra'    	=> $this->idListaNegra,
                       
        );
    }
}
