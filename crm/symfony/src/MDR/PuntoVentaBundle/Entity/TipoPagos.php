<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * TipoPagos
 */
class TipoPagos implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var integer
     */
    private $idTipoPago;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoPagos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set claveSAP
     *
     * @param string $claveSAP
     * @return TipoPagos
     */
    public function setClaveSAP($claveSAP)
    {
        $this->claveSAP = $claveSAP;

        return $this;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }

    /**
     * Get idTipoPago
     *
     * @return integer 
     */
    public function getIdTipoPago()
    {
        return $this->idTipoPago;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(

            'descripcion'    	=> $this->descripcion,
			'idTipoPago'    	=> $this->idTipoPago,
            'id'                => $this->idTipoPago,
            'text'              => $this->descripcion,
			
        );

    }
}
