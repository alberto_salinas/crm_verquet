//validar fechas    
    function vd_date(){
        
        var date1 = createdate($('#form_fechaInicio').val());
        var date2 = createdate($('#form_fechaFin').val());

        if (date1 <= date2) {
            
            diferencia = restaFechas($('#form_fechaInicio').val(),$('#form_fechaFin').val());
            if(diferencia<8){
                $('#form_Consultar').attr("disabled", false);   
            }else{
                alert("No debe de haber mas de 7 dias de diferencia entre la fecha de inicio y la de fin");
                $('#form_Consultar').attr("disabled", true);
            }
            
        }else if (date1 > date2){
            alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
            $('#form_Consultar').attr("disabled", true);    
        }

    }


//diferencia entre 2 fechas (restriccion de solo 5 dias a la busqueda en bitacoras)

    function restaFechas(f1,f2)
     {
         var aFecha1 = f1.split('/'); 
         var aFecha2 = f2.split('/'); 
         var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
         var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
         var dif = fFecha2 - fFecha1;
         var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
         return dias;
     }
//Loading para consulta de operaciones
var img = new Image();
img.src = "../../vendor/img/bubbles_load.gif";

function loadPedidos(){
	showLoading("Cargando Bitacora ...");
}

		$(document).ready(function() {
        	var table = $('#tbl_operaciones').DataTable({
        					"language":DataTables.languaje.es					
        				});

        	$( "select" ).addClass( "form-control" );
        	$( "input[type='text']" ).addClass( "form-control" );        	
			$( "input[type='search']").addClass( "form-control" );
        	$( "#tbl_operaciones_length" ).addClass( "form-inline" ); 
        	$( "#tbl_operaciones_filter" ).addClass( "form-inline" );
        	$( "#tbl_operaciones_filter>label" ).html('Buscar: <input type="text" id="search_pedido" placeholder="Numero de pedido" class="form-control">');				
 

				$('#search_pedido').on( 'keyup', function () {
						    table
						        .columns( 3 )
						        .search( this.value )
						        .draw();
						} );
			table.order( [ 0, 'desc' ] ).draw();	


        	$('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                    	$target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });	      
    	});

