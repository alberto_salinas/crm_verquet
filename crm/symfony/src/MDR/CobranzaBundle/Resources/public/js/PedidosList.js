var PedidoTarjeta = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		posFechar: 0,
		fechaCobro: ''
	}
});


var Productos = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		Descripcion: 0
	}
});

var ProductoPedido = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		cantidad: 0,
		precio: 0,
		item: new Productos
	}
});

var ProductoPedidoCollection = Backbone.Collection.extend({
 model: ProductoPedido,
});

var PedidoCobro = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		idPedido: 0,
		fechaPedido: '',
		gastosEnvio:0,		
		subTotal:0,
		totalPago: 0,
		item: new PedidoTarjeta,
		productos: new ProductoPedidoCollection
	}
});

var PedidosCobroCollection = Backbone.Collection.extend({
 model: PedidoCobro,
});

var PedidoView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#pedidoTemplate').html()),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	},
	events: {

	}
})

var pedidos = new PedidosCobroCollection(pedidosJSON);

var PedidosView = Backbone.View.extend({
	el: $("#pedidos_cobro"),
	events: {
	},
	initialize: function() {			
		this.render();
		//this.listenTo(pedidosCobro, 'change', this.totalCompra);
	},

	render: function() {
		this.addAllPedidos(pedidos);
	},

	addPedidos: function(item)
	{
		var view = new PedidoView({model: item});
  		this.$el.append(view.render().$el);
	},
	addAllPedidos: function(items)
	{
		this.$el.html(" ");
		items.each(this.addPedidos, this);
	}
});

var pedidosView = new PedidosView;