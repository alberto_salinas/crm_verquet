//validar fechas	
	function vd_date(){
		
		var date1 = createdate($('#form_fechaInicio').val());
		var date2 = createdate($('#form_fechaFin').val());

		if (date1 <= date2) {
			$('#form_Consultar').attr("disabled", false);			
		}else if (date1 > date2){
			alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
			$('#form_Consultar').attr("disabled", true);	
		}

	}

//Loading para consulta de operaciones
var img = new Image();
img.src = "../../vendor/img/bubbles_load.gif";


function loadPedidos(){
	var terminal = $('#form_terminal').val();
	var status = $('#form_estatus').val();
	if(terminal != '' && status != ''){
		showLoading("Cargando Pedidos ...");
	}
}

		$(document).ready(function() {
        	table = $('#tbl_cierre').DataTable({
        					"language":DataTables.languaje.es 					
        				}); 

        	$( "select" ).addClass( "form-control" );
        	$( "input[type='text']" ).addClass( "form-control" );        	
			$( "input[type='search']").addClass( "form-control" );
        	$( "#tbl_cierre_length" ).addClass( "form-inline" ); 
        	$( "#tbl_cierre_filter" ).addClass( "form-inline" );
        	$( "#tbl_cierre_filter>label" ).html('Buscar: <input type="text" id="search_pedido" placeholder="Numero de pedido" class="form-control">');				

				$('#search_pedido').on( 'keyup', function () {
						    table
						        .columns(1)
						        .search( this.value )
						        .draw();
						} );       

        		$('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                    	$target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });	
    	});
//Formato Gregoriano de fecha
function gregoriandate(date){
	var annio = date.substring(0,4);
	date = date.substr(5);
	var mes = date.substr(2,4);
	mes = mes.substr(1);
	var dia = date.substr(0,2);
	date = mes+"-"+dia+"-"+annio;
	return date;
}
// CREACION DE LA TABLA CON LOS RESULTADOS DE LA CONSULTA
var table_cierres;
var btn_operaciones;
var factura;
var total_gral = 0;
var ApedidosCCR = new Array();	
var fechacobro ="";
var autorizacion="";

		if(cierreJSON.length != 0){

				var tam_cierreJSON = cierreJSON.length-1;
				var next_row=0;

			for(var x=0;x<=cierreJSON[next_row].cobros.length-1;){
					if(cierreJSON[next_row].cobros[x].numeroAutorizacion && cierreJSON[next_row].cobros[x].respuesta.idTipoRespuesta == 1 ){
						//REQUIERE FACTURA
						if(cierreJSON[next_row].factura == 1){
							factura = "SI";
						}else{
							factura = "NO";
						}

						total_gral = total_gral+parseFloat(cierreJSON[next_row].totalPago);

						table_cierres = table_cierres+"<tr><td><input type='checkbox' class='check_cr' onclick='Checkonebyone("+cierreJSON[next_row].idPedido+")'/></td>"+
										"<td>"+cierreJSON[next_row].idPedido+"</td>"+
										"<td>"+cierreJSON[next_row].pago.descripcion+"</td>"+
										"<td>"+factura+"</td>";
							
									fechacobro = cierreJSON[next_row].cobros[x].fechaCobroGregorian;
									autorizacion = cierreJSON[next_row].cobros[x].numeroAutorizacion;

						table_cierres = table_cierres+"<td>"+fechacobro+"</td><td>"+cierreJSON[next_row].pagosTarjeta[0].terminal.descripcion+"</td><td>"+autorizacion+"</td><td style='text-align:center;'>"+parseFloat(cierreJSON[next_row].totalPago)+"</td></tr>";
					}

					if(x==cierreJSON[next_row].cobros.length-1 && next_row != tam_cierreJSON){
						next_row++;
						x=0;
					}else{
						x++;
					}	
					
					
										
			}

			//PLANCHADO DE INFORMACION EN PANTALLA
						if(next_row == cierreJSON.length-1){
							$('#foot_tbl_cierre').html("<tr><td colspan='7'>TOTAL GENERAL</td><td>"+total_gral+"</td></tr>");
							$('#body_tbl_cierre').html(table_cierres);
							//BOTONES OPERACIONALES
								if(cierreJSON[0].estatus.descripcion == "COBRADO"){
									btn_operaciones = '<button type="button" onclick=marcar(":checkbox") class="btn btn-primary" id="btn_all"><span class="icon-checked" id="icon_btn"></span>  Marcar todos</button>&nbsp;&nbsp;<button type="button" onclick=desmarcar(":checkbox") class="btn btn-danger" id="btn_none"> <span class="icon-marquee" id="icon_btn"></span>  Desmarcar</button>&nbsp;&nbsp;<button type="button" class="btn btn-success" onclick=sendCCR("cierre") id="btn_wscobro"> <span class="icon-signup" id="icon_btn"></span> Cierre</button>';
								}else if(cierreJSON[0].estatus.descripcion == "EN CIERRE"){
									btn_operaciones = '<button type="button" onclick=marcar(":checkbox") class="btn btn-primary" id="btn_all"><span class="icon-checked" id="icon_btn"></span>  Marcar todos</button>&nbsp;&nbsp;<button type="button" onclick=desmarcar(":checkbox") class="btn btn-danger" id="btn_none"> <span class="icon-marquee" id="icon_btn"></span>  Desmarcar</button>&nbsp;&nbsp;<button type="button" class="btn btn-success" onclick=sendCCR("conciliacion") id="btn_wscobro"> <span class="icon-signup" id="icon_btn"></span> Conciliación</button>';
								}
							$('#btns_oper').html(btn_operaciones);
						}	



		}



//Botones de seleccion todos o deselecionar todos

$(function(){
	marcar = function(elemento){
	elemento = $(elemento);
	elemento.prop("checked", true);
	ApedidosCCR.length = 0;
	Checkall();
}

desmarcar = function(elemento){
	elemento = $(elemento);
	elemento.prop("checked", false);
	ApedidosCCR.length = 0;
}
});

//Funcion para checkear todos los pedidos

function Checkall(){
	for(var x=0;x<=cierreJSON.length-1;x++){
		ApedidosCCR.push(cierreJSON[x].idPedido);
	}	
}

//Check individual

function Checkonebyone(numpedido){
	var posicion = ApedidosCCR.indexOf(numpedido);
		if (posicion == -1) {
			ApedidosCCR.push(numpedido);
		}else{
			ApedidosCCR.splice(posicion,1);
		}
}


//CAMBIAR ESTATUS DEL PEDIDO
var ApedidosCCR1 = new Array();
var contador = 0;
var log_pedidosSAP = 0;
		function sendCCR(ident){
			if(ApedidosCCR.length > 0){
				if(ident == "cierre"){
					showLoading("Realizando CIERRE de Pedido(s) ..");
						CCDP(ApedidosCCR,ident,function(respuesta,x){
							contador++;
							if(contador == x){
								contador = 0;
								$('#txt_cobro').html("Actualizando pedidos...");
								$('#form_Consultar').click();
							}							
						});	
						
				}else if(ident == "conciliacion"){
					showLoading("Realizando CONCILIACIÓN de Pedido(s) ...");
						CCDP(ApedidosCCR,ident,function(respuesta,x){
							contador++;							

							if(respuesta[0].estatus == 0){
								log_pedidosSAP++;
							}
							
							if(contador == x){
									
									$('#txt_cobro').html("Actualizando pedidos...");
									table.clear().draw(); 
									hideLoading();

								if(log_pedidosSAP == contador){
									showAlert('danger','<strong>ERROR!!</strong> Ningún pedido se ha podido crear en SAP');
								}else if(log_pedidosSAP > 0 && log_pedidosSAP < contador){
									showAlert('warning','<strong>ATENCIÓN!!</strong> Uno o más pedidos no han sido creados en SAP');
								}else if(log_pedidosSAP == 0){
									showAlert('success','<strong>EXITO!!</strong> Todos los pedidos han sido creados con exito en SAP');
								}
									contador = 0;
									log_pedidosSAP =0;			
							}
						});	
				$("html, body").stop().animate({ scrollTop: 0 }, "slow");						
			}else{
				alert("No se a seleccionado ningun pedido");
			}	
			
		}
}		

function CCDP(ApedidosCCR,ident,callback){

	for(x=0;x<=ApedidosCCR.length-1;x++){
		ApedidosCCR1.length = 0;
		ApedidosCCR1.push({idPedido:ApedidosCCR[x],type:ident});
			
				$.post("../CCR",{
					ApedidosCCR1:ApedidosCCR1
				},function(respuesta){
					callback(respuesta,x);					
				});
			
	}		
}

function denegarPedido(idPedido){

showLoading("Actualizando el pedido "+idPedido);	
	denegar(idPedido,function(respuesta){		
		var res = respuesta[0].estatus;
				if(res == 1){
					alert("El pedido a pasado a un estatus de 'EN PROCESO DE COBRO'");
					$('#text_load').text("Actualizando Pedidos");
					$('#form_Consultar').click();
			  	}else{
					alert("Error"+respuesta[0].mensaje);
					$.unblockUI();
			   	}

	});

}


function denegar(pedido,callback){
	var ApedidoDenegado = new Array();
		ApedidoDenegado.push({idPedido:pedido});
	$.post("../DenegarPedido",{
				ApedidoDenegado:ApedidoDenegado
		},function(respuesta){
				callback(respuesta);
			});

}
