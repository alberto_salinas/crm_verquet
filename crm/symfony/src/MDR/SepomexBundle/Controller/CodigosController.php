<?php

namespace MDR\SepomexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Codigos controller.
 *
 * @Route("/Sepomex")
 */
class CodigosController extends Controller
{
    /**
     * @Route("/byCP", name="sepomex_by_cp")
     * @Method("POST")
     */
    public function byCPAction(Request $request)
    {
    	$cp = $request->get('cp');
    	$em = $this->getDoctrine()->getManager();
    	$entities = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($cp);

        $response = new Response(json_encode(array(
            'code'      => 1,
            'message'   => '',
            'entities'  => $entities,
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/delegacionesByEstado", name="delegaciones_by_estado"))
     * @Method({"POST", "GET"})
     */
    public function delegacionesByEstadoAction(Request $request)
    {
        $idEstado = $request->get('idEstado');
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findByEstado($idEstado, array('nombre' => 'ASC'));
        $colonias = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByEstado($idEstado);

        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => '',
            'entities' => $entities,
            'colonias' => $colonias,
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/coloniasByDelegacion", name="colonias_by_delegacion")
     * @Method({"POST", "GET"})
     */
    public function coloniasByDelegacionAction(Request $request)
    {
        $id = $request->get('idDelegacion');
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByMunicipio($id, array('nombre' => 'ASC'));

        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => '',
            'entities' => $entities
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
