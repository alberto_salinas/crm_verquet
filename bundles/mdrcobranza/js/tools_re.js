
var ATipoLlamada = new Array();
var ASubTipoLllamada = new Array();
var AEdos = new Array();
var AMunicipios = new Array();
var AColonias = new Array();
var ATiposPago = new Array();
var ATiposTarjeta = new Array();
var ABancos = new Array();
var APromociones = new Array();
var ATerminales = new Array();
var AnewTarjeta = new Array();
var newCard="";
var new_direccion_cliente =0;
var is_ready = 0;
var BIN = pedidoEditJSON.numeroTarjeta.substring(0,6);

$(document).ready(function() {

    showLoading("Cargando información");

    $.getJSON("../../EstadosJSON",function(result){
        AEdos = result;
      $.each(result, function(index, val) {
           $('#direcciones_estado').append('<option value="'+val.idEstado+'">'+val.nombre+'</option>');
        });
        datos_cargados();
    })

    $.getJSON("../../AllterminalesJSON",function(result){
           ATerminales = result;
        $('#slt_terminal').append('<option value="'+pedidoEditJSON.terminal.idTerminal+'">'+pedidoEditJSON.terminal.descripcion+'</option>');            
        $.each(result, function(index, val) {
           if(val.idTerminal != pedidoEditJSON.terminal.idTerminal && val.idTerminal != 7){
                    $('#slt_terminal').append('<option value="'+val.idTerminal+'">'+val.descripcion+'</option>');
                }
        });
        datos_cargados();        
    });

    $.getJSON("../../MunicipiosJSON",function(result){
           AMunicipios = result;
           datos_cargados();
    });

   
    
    

    $.getJSON("../../tipollamadasJSON",function(result){
      $.each(result, function(index, val) {
           $('#slt_tipollamada').append('<option value="'+val.idTipoLlamada+'">'+val.descripcion+'</option>');
        });
        datos_cargados();       
    });


    $.getJSON("../../subtipollamadasJSON",function(result){
           ASubTipoLllamada = result;
           datos_cargados(); 
    });

    $.getJSON("../../tiposTarjetaJSON",function(result){
            ATiposTarjeta = result; 
            $('#slt_tipotarjeta').append('<option value="'+pedidoEditJSON.tipoTarjeta.idTipoTarjeta+'">'+pedidoEditJSON.tipoTarjeta.descripcion+'</option>');
            $.each(result, function(index, val) {
                if(val.idTipoTarjeta != pedidoEditJSON.tipoTarjeta.idTipoTarjeta ){
                    $('#slt_tipotarjeta').append('<option value="'+val.idTipoTarjeta+'">'+val.descripcion+'</option>');
                }
            });
            datos_cargados(); 
                
    });

    $.getJSON("../../bancosJSON",function(result){
        ABancos = result;
        $('#slt_banco').append('<option value="'+pedidoEditJSON.banco.idBanco+'">'+pedidoEditJSON.banco.nombre+'</option>');
        $.each(result, function(index, val) {
            if(val.idTipoTarjeta != pedidoEditJSON.tipoTarjeta.idTipoTarjeta ){
                $('#slt_banco').append('<option value="'+val.idBanco+'">'+val.nombre+'</option>');
            }
        });
        datos_cargados(); 
    });

    $.post("../../promocionesBinJSON",{
          BIN:BIN
      },function(respuesta){
        if(respuesta.length-1 > 0){         
        APromociones = respuesta[0].promociones;
        }
         $('#slt_promo').append('<option value="'+pedidoEditJSON.promocion.idPromocion+'">'+pedidoEditJSON.promocion.descripcion+'</option>');
            if(APromociones.length-1 > 0){
                for(var promos =0;promos<= APromociones.length-1;promos++){
                    if(APromociones[promos].idPromocion != pedidoEditJSON.promocion.idPromocion){                   
                        $('#slt_promo').append('<option value="'+APromociones[promos].idPromocion+'">'+APromociones[promos].descripcion+'</option>');
                    }
                }
            }
        datos_cargados();
        
    });    


function datos_cargados(){
    is_ready++;
    if(is_ready == 8){
        hideLoading();
        is_ready=0;
    }

}


$('#space_pago').find('input, textarea, button, select').attr('disabled','disabled'); 
$('#exist_direccion').find('input, textarea, button, select').attr('disabled','disabled'); 

 $('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                        $target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });

    if(pedidoDireccionJSON){
       $('#direc_actual').html(pedidoDireccionJSON.direccion.toString);
       for(var x=0;x<=pedidoDireccionJSON.direcciones.length-1;x++){
            if(pedidoDireccionJSON.direcciones[x].idDireccion != pedidoDireccionJSON.direccion.idDireccion){
                $('#direcciones_exist').append('<option value="'+pedidoDireccionJSON.direcciones[x].idDireccion+'">'+pedidoDireccionJSON.direcciones[x].toString+'</option>');
            }
        }


        $('#txt_ntarjeta').val(pedidoEditJSON.numeroTarjeta);
        $('#txt_ntitular').val(pedidoEditJSON.titular);
        $('#txt_seguridad').val(pedidoEditJSON.codigoSeguridad);
        $('#lbl_total').html("<label>Total del Pago:</label>  $"+parseFloat(pedidoDireccionJSON.OriPedido.totalPago));

    }

            var fechcobro = gregoriandate(pedidoDireccionJSON.OriPedido.pagosTarjeta[0].fechaCobro.date.substring(0,10));
                  
            function gregoriandate(date){
                var annio = date.substring(0,4);
                date = date.substr(5);
                var mes = date.substr(2,4);
                mes = mes.substr(1);
                var dia = date.substr(0,2);
                date = mes+"/"+dia+"/"+annio;
                return date;
            }
            
            $("#txt_fcobro").val(fechcobro);

            $('option','#slt_estitular').remove();
            if(pedidoDireccionJSON.OriPedido.pagosTarjeta[0].esTitular == 1){
                $('#slt_estitular').append('<option value="1">SI</option>');
                $('#slt_estitular').append('<option value="0">NO</option>');
            }else{
                $('#slt_estitular').append('<option value="0">NO</option>');
                $('#slt_estitular').append('<option value="1">SI</option>');
            }
            //ANNIO VIGENCIA
            var vig_annio = parseInt(pedidoDireccionJSON.OriPedido.pagosTarjeta[0].vigencia.date.substring(0,4));
            $('option','#slt_annio').remove();
            $('#slt_annio').append('<option value="'+vig_annio+'">'+vig_annio+'</option>');
            
            var annio3 = vig_annio-1;
            var annio4 = vig_annio-9;
            for(annio4;annio4<=annio3;annio4++){
                $('#slt_annio').append('<option value="'+annio4+'">'+annio4+'</option>');
            }    


            var annio1 = vig_annio+9;
            var annio2 = vig_annio+1;
            for(annio2;annio2<=annio1;annio2++){
                $('#slt_annio').append('<option value="'+annio2+'">'+annio2+'</option>');
            }

            //MES VIGENCIA
            var vig_mes = pedidoDireccionJSON.OriPedido.pagosTarjeta[0].vigencia.date.substring(8,10);
            $('option','#slt_mes').remove();
            $('#slt_mes').append('<option value="'+vig_mes+'">'+vig_mes+'</option>');

            for(var mes=1;mes<=12;mes++){
                if(mes <= 9 && mes != vig_mes){
                    $('#slt_mes').append('<option value="0'+mes+'">0'+mes+'</option>');
                }else if(mes > 9 && mes != vig_mes){
                    $('#slt_mes').append('<option value="'+mes+'">'+mes+'</option>');
                }

            }

            $('option','#slt_tipopago').remove();
            $('#slt_tipopago').append('<option value="1">TDC</option>');



            
           


}); 



function open_form(){
            var tipo_edicion = $('#slt_edicion').val();
            switch(tipo_edicion){

                case "SO":
                    $('#space_pago').find('input, textarea, button, select').attr('disabled','disabled'); 
                    $('#exist_direccion').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#new_direccion').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#btn_PagoTarjeta').html("");
                    $('#btn_Direccion').html("");
                    $('#btn_PagoDireccion').html(""); 
                    break;

                case "D":
                    $('#space_pago').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#exist_direccion').find('input, textarea, button, select').removeAttr('disabled','disabled');
                    $('#new_direccion').find('input, textarea, button, select').removeAttr('disabled','disabled');
                        if($('#newdireccion').is(':checked')){    
                                $('#direcciones_exist').prop('disabled', 'disabled'); 
                                new_direccion_cliente =1;
                            }else{
                                $('#direcciones_exist').removeAttr('disabled','disabled');
                                new_direccion_cliente =0;
                            } 
                    $('#btn_PagoTarjeta').html("");
                    $('#btn_Direccion').html('<button type="button" class="btn btn-warning" id="btn_upDireccion" onclick="update_direccion()">Actualizar Dirección</button>');
                    $('#btn_PagoDireccion').html("");
                    break; 

                case "T":
                    $('#space_pago').find('input, textarea, button, select').removeAttr('disabled','disabled');
                    $('#txt_fcobro').attr('disabled','disabled'); 
                    $('#exist_direccion').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#new_direccion').find('input, textarea, button, select').attr('disabled','disabled');
                    $('#btn_PagoTarjeta').html('<button type="button" class="btn btn-success" onclick=update_tarjeta("V")>Actualizar Tarjeta</button>');
                    $('#btn_Direccion').html("");
                    $('#btn_PagoDireccion').html("");                    
                    makeManualCobro();
                    break;           
            }

        }



  function update_stllamada(){
    var tllamada = $('#slt_tipollamada').val();
    $('option','#slt_stipollamada').remove();
      for(var x =0;x<=ASubTipoLllamada.length-1;x++){
        if (ASubTipoLllamada[x].tipoLlamada.idTipoLlamada == tllamada) {
            $('#slt_stipollamada').append('<option value="'+ASubTipoLllamada[x].idSubTipo+'">'+ASubTipoLllamada[x].descripcionSubTipo+'</option>');

        }
      }
  }


function new_dirrecion(){
    if($('#newdireccion').is(':checked')){

        $("#new_direccion").css("display", "block");        
        $('#direcciones_exist').prop('disabled', 'disabled'); 
        new_direccion_cliente =1;

    }else{

        $("#new_direccion").css("display", "none");
        $('#direcciones_exist').removeAttr('disabled','disabled');
        new_direccion_cliente =0;
    }
}        



function show_direccion(){
    var CP = $('#direcciones_cp').val();
    var TP = "C";

    
    
    if(CP){
        showLoading("Verificando CP");
        $.post("../../ColoniasJSON",{
          CP:CP,
          TP:TP
      },function(respuesta){
          if(respuesta[0].estatus == 1){
            loadDirection(respuesta[0].colonias,TP);
          }else if(respuesta[0].estatus == 0 && respuesta[0].mensaje == "Sin resultados"){
            alert("Sin resultados con este codigo postal");
            hideLoading();
          }else if(respuesta[0].estatus == 0){
            alert(respuesta[0].mensaje);
            hideLoading();
          }
        });
    }else{
        $(':input','#new_direccion').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        $('option','#direcciones_estado').remove();
      for(var x =0;x<=AEdos.length-1;x++){   
                $('#direcciones_estado').append('<option value="'+AEdos[x].idEstado+'">'+AEdos[x].nombre+'</option>');
        }
        $('option','#direcciones_colonia').remove();
        $('option','#direcciones_municipio').remove();
    }
}  

function loadDirection(args,ident){

    if(ident == "C"){
        var edo;

        //Limpiamos y cargamos las colonias
        $('option','#direcciones_colonia').remove();
        $('#direcciones_colonia').append('<option>--------------------------------</option>');
      for(var x =0;x<=args.length-1;x++){        
            $('#direcciones_colonia').append('<option value="'+args[x].idColonia+'" name="'+args[x].cp+'">'+args[x].nombre+'-'+args[x].cp+'</option>');
        }
        //Limpiamos y mostramos el municipio al que pertenece el CP
         $('option','#direcciones_municipio').remove();
      for(var x =0;x<=AMunicipios.length-1;x++){
            if(AMunicipios[x].idMunicipio==args[0].municipio.idMunicipio){       
                $('#direcciones_municipio').append('<option value="'+AMunicipios[x].idMunicipio+'">'+AMunicipios[x].nombre+'</option>');
                edo=AMunicipios[x].estado.idEstado;
            }            
        }
        //Limpiamos y mostramos Estados
         $('option','#direcciones_estado').remove();
      for(var x =0;x<=AEdos.length-1;x++){
            if(AEdos[x].idEstado==edo){       
                $('#direcciones_estado').append('<option value="'+AEdos[x].idEstado+'">'+AEdos[x].nombre+'</option>');
            }
        }
      for(var x =0;x<=AEdos.length-1;x++){
            if(AEdos[x].idEstado!=edo){       
                $('#direcciones_estado').append('<option value="'+AEdos[x].idEstado+'">'+AEdos[x].nombre+'</option>');
            }
        }
        //Cargamos todos los Municipios pertenecientes al Estado
      for(var x =0;x<=AMunicipios.length-1;x++){
            if(AMunicipios[x].estado.idEstado==edo && AMunicipios[x].idMunicipio!=args[0].municipio.idMunicipio){       
                $('#direcciones_municipio').append('<option value="'+AMunicipios[x].idMunicipio+'">'+AMunicipios[x].nombre+'</option>');
            }            
        }  
        hideLoading();
    }else if(ident == "E"){
        $('option','#direcciones_colonia').remove();
        $('option','#direcciones_municipio').remove();
        var Estado_select = $('#direcciones_estado').val();
        $('#direcciones_cp').val("");
         $('option','#direcciones_municipio').remove();
      for(var x =0;x<=AMunicipios.length-1;x++){
            if(AMunicipios[x].estado.idEstado==Estado_select){       
                $('#direcciones_municipio').append('<option value="'+AMunicipios[x].idMunicipio+'">'+AMunicipios[x].nombre+'</option>');
            }            
        }
    }else if(ident == "M"){
        $('#direcciones_cp').val("");
        var MS = $('#direcciones_municipio').val();
        var TP = "M";
        showLoading("Cargando Colonias");
        $.post("../../ColoniasJSON",{
          MS:MS,
          TP:TP
      },function(respuesta){
          if(respuesta[0].estatus == 1){
                    $('option','#direcciones_colonia').remove();
                    $('#direcciones_colonia').append('<option>--------------------------------</option>');
                  for(var x =0;x<=respuesta[0].colonias.length-1;x++){        
                        $('#direcciones_colonia').append('<option value="'+respuesta[0].colonias[x].idColonia+'" name="'+respuesta[0].colonias[x].cp+'">'+respuesta[0].colonias[x].nombre+'-'+respuesta[0].colonias[x].cp+'</option>');
                    }
                    hideLoading();
          }else if(respuesta[0].estatus == 0 && respuesta[0].mensaje == "Sin resultados"){
            alert("Sin resultados con este codigo postal");
            hideLoading();
          }else if(respuesta[0].estatus == 0){
            alert(respuesta[0].mensaje);
            hideLoading();
          }
        });
    }if(ident == "CL"){
        var RCP = $('#direcciones_colonia :selected').attr("name");
        $('#direcciones_cp').val(RCP);
    }
} 


function update_direccion(){

    var area = "Cobranza";
    var stipollamada = $('#slt_stipollamada').val();  
    var comentarios = $('#txt_comentarios').val();

    if(stipollamada && comentarios){  


        if($('#newdireccion').is(':checked')){
            var d_pais = $('#direcciones_pais').val();
            var d_cp = $('#direcciones_cp').val();
            var d_edo = $('#direcciones_estado').val();
            var d_mun = $('#direcciones_municipio').val();
            var d_col = $('#direcciones_colonia').val();
            var d_calle = $('#direcciones_calle').val();
            var d_ecalles = $('#direcciones_entreCalles').val();
            var d_noExt = $('#direcciones_noExt').val();
            var d_noInt = $('#direcciones_noInt').val();
            var d_ref = $('#direcciones_referencia').val();
            var d_casa = $('#direcciones_telCasa').val();
            var d_ofi = $('#direcciones_telOficina').val();
            var d_cel = $('#direcciones_telCel').val();
            var funcion = "ND";
            var cliente = pedidoDireccionJSON.OriPedido.cliente.idCliente;
            var pedido = pedidoDireccionJSON.OriPedido.idPedido;

            if(d_cp && d_edo && d_mun && d_col && d_calle && d_ecalles && d_noExt && d_ref && d_casa){
                confirmar=confirm("¿Esta seguro de cambiar la dirección?"); 
                if(confirmar){
                    showLoading("Actualizando Dirección del pedido "+pedido);
                     $.post("../../UpdateDireccionC",{
                            d_pais:d_pais,
                            d_cp:d_cp,
                            d_edo:d_edo,
                            d_mun:d_mun,
                            d_col:d_col,
                            d_calle:d_calle,
                            d_ecalles:d_ecalles,
                            d_noExt:d_noExt,
                            d_noInt:d_noInt,
                            d_ref:d_ref,
                            d_casa:d_casa,
                            d_ofi:d_ofi,
                            d_cel:d_cel,
                            funcion:funcion,
                            cliente:cliente,
                            pedido:pedido,
                            area:area,
                            stipollamada:stipollamada,
                            comentarios:comentarios
                        },function(respuesta){
                      if(respuesta[0].estatus == 1){
                        alert(respuesta[0].mensaje);
                        $('#text_load').html("Actualizando información...");
                        location.reload();
                      }else if(respuesta[0].estatus == 0){
                        alert(respuesta[0].mensaje);
                        hideLoading();
                      }
                    });
                }
            }else{
                alert("Debe indicar:\n->Codigo Postal\n->Estado\n->Municipio\n->Colonia\n->Calle\n->Entre calles\n->No. Ext\n->Referencias\n->Telefono de casa");
            }
            

        }else{
            var direccion = $('#direcciones_exist').val();  
            if(direccion){

                confirmar=confirm("¿Esta seguro de cambiar la dirección?"); 
                if(confirmar){

                    var pedido = pedidoDireccionJSON.OriPedido.idPedido;
                    showLoading("Actualizando Dirección del pedido "+pedido);                              
                    var funcion = "D";


                    $.post("../../UpdateDireccionC",{
                            direccion:direccion,
                            pedido:pedido,
                            funcion:funcion,
                            area:area,
                            stipollamada:stipollamada,
                            comentarios:comentarios
                        },function(respuesta){
                      if(respuesta[0].estatus == 1){
                        alert(respuesta[0].mensaje);
                        $('#text_load').html("Actualizando información...");
                        location.reload();
                      }else if(respuesta[0].estatus == 0){
                        alert(respuesta[0].mensaje);
                        hideLoading();
                      }
                    });


                }
            }else{
                alert("Debe seleccionar una dirección");
            }
        }
    }else{
        alert("Debe indicar\n->Tipo de llamada\n->Subtipo de llamada\n->Comentarios");
    }    
}

function tipo_pago(){
    var TipoPago = $('#slt_tipopago').val();
    if(TipoPago == "2" || TipoPago == 2){
        $('#options_pago').find('input, textarea, button, select').attr('disabled','disabled');
    }else{
        $('#options_pago').find('input, textarea, button, select').removeAttr('disabled','disabled');
    } 
}

function posfechar_cobro(){
    var posfecho = $('#slt_pstfechar').val();
    if(posfecho == "1"){
        $('#txt_fcobro').removeAttr('disabled','disabled');
    }else if(posfecho != "1"){
        $('#txt_fcobro').attr('disabled','disabled'); 
    }
}

function update_tarjeta(ident){
    var TipoPago = $('#slt_tipopago').val();
    var idPedido = pedidoDireccionJSON.OriPedido.idPedido;
    var stipollamada = $('#slt_stipollamada').val();  
    var comentarios = $('#txt_comentarios').val();
    var AupTarjeta = new Array();

    if(stipollamada && comentarios){  
            if(TipoPago == "2" || TipoPago == 2){
                        confirmar=confirm("¿Esta seguro de cambiar el tipo de pago a COD?\n->Al cambiar la forma de pago ha COD se enviara de forma inmediata a SAP y no podra ser modificado el pedido"); 
                        if(confirmar){
                           showLoading("Actualizando Tipo de Pago"); 
                            $.post("../../UpdateTipodePago",{
                                    idPedido:idPedido,
                                    stipollamada:stipollamada,
                                    comentarios:comentarios
                                },function(respuesta){
                              if(respuesta[0].estatus == 1){
                                alert(respuesta[0].mensaje);
                                $('#text_load').html("Actualizando información...");
                                url = "../posfechados";
                                window.location.replace(url);
                                return false;
                              }else if(respuesta[0].estatus == 0){
                                alert(respuesta[0].mensaje);
                                hideLoading();
                              }
                            });
                        }

            }else{
                var NTarjeta = $('#txt_ntarjeta').val();
                var IsTitular = $('#slt_estitular').val();
                var NameTitular = $('#txt_ntitular').val();
                var Vigencia = $('#slt_annio').val()+"-"+$('#slt_mes').val()+"-"+"01";
                var CSeguridad = $('#txt_seguridad').val();
                var TTarjeta = $('#slt_tipotarjeta').val();
                var TerminalCobro = $('#slt_terminal').val();
                var Banco = $('#slt_banco').val();
                var Promocion = $('#slt_promo').val();
                var posfecho = $('#slt_pstfechar').val();
                var diaposfecho = $('#txt_fcobro').val();
                if(NTarjeta && Promocion && IsTitular && NameTitular && Vigencia && CSeguridad && TTarjeta && TerminalCobro && Banco){
                    if(ident == "V"){
                       var NumAutorizacion = "";

                       if(posfecho == "0"){
                        confirmar_cobro_virtual =confirm("Se realizara el cobro del pedido en este momento,¿Esta seguro(a) de querer seguir?");
                                if(confirmar_cobro_virtual){
                                    showLoading("Realizando Cobro"); 
                                    AupTarjeta.push({NTarjeta:NTarjeta,IsTitular:IsTitular,NameTitular:NameTitular,Vigencia:Vigencia,CSeguridad:CSeguridad,TTarjeta:TTarjeta,TerminalCobro:TerminalCobro,Banco:Banco,Promocion:Promocion,NumAutorizacion:NumAutorizacion,idPedido:idPedido,TipoLlamada:stipollamada,Comentarios:comentarios,Posfecho:diaposfecho});

                                    $.post(WSManual,{
                                            AupTarjeta:AupTarjeta
                                        },function(respuesta){
                                      if(respuesta[0].estatus == 1){
                                        
                                        hideLoading();
                                        showAlert('success',respuesta[0].mensaje);
                                        $('#space_coments').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_pago').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_direccion').find('input, textarea, button, select').attr('disabled','disabled');

                                      }else if(respuesta[0].estatus == 0){
                                        alert(respuesta[0].mensaje);
                                        hideLoading();
                                      }else if(respuesta[0].estatus == 2){
                                        hideLoading();
                                        showAlert('danger',respuesta[0].mensaje);

                                      }
                                    });
                                }
                        }else{
                            alert("El cobro del pedido se posfechara para el día:"+diaposfecho);
                            showLoading("Actualizando datos bancarios"); 
                                    AupTarjeta.push({NTarjeta:NTarjeta,IsTitular:IsTitular,NameTitular:NameTitular,Vigencia:Vigencia,CSeguridad:CSeguridad,TTarjeta:TTarjeta,TerminalCobro:TerminalCobro,Banco:Banco,Promocion:Promocion,NumAutorizacion:NumAutorizacion,idPedido:idPedido,TipoLlamada:stipollamada,Comentarios:comentarios,Posfecho:diaposfecho});

                                    $.post( WSVirtual,{
                                            AupTarjeta:AupTarjeta
                                        },function(respuesta){
                                      if(respuesta[0].estatus == 1){
                                        
                                        hideLoading();
                                        showAlert('success',respuesta[0].mensaje);
                                        //$('#space_coments').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_pago').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_direccion').find('input, textarea, button, select').attr('disabled','disabled');

                                      }else if(respuesta[0].estatus == 0){
                                        alert(respuesta[0].mensaje);
                                        hideLoading();
                                      }else if(respuesta[0].estatus == 2){

                                        hideLoading();
                                        showAlert('danger',respuesta[0].mensaje);

                                      }
                                    });
                        }        

                        $("html, body").stop().animate({ scrollTop: 0 }, "slow");
                                
                    }else if(ident == "M"){
                        var NumAutorizacion = $('#txt_autorizacion').val();

                        if(posfecho == "0"){

                                if(NumAutorizacion){
                                        confirmar_cobro_manual =confirm("¿Es correcto el numero de autorización?");
                                        if(confirmar_cobro_manual){
                                            showLoading("Actualizando Información Bancaria"); 
                                            AupTarjeta.push({NTarjeta:NTarjeta,IsTitular:IsTitular,NameTitular:NameTitular,Vigencia:Vigencia,CSeguridad:CSeguridad,TTarjeta:TTarjeta,TerminalCobro:TerminalCobro,Banco:Banco,Promocion:Promocion,NumAutorizacion:NumAutorizacion,idPedido:idPedido,TipoLlamada:stipollamada,Comentarios:comentarios,Posfecho:diaposfecho});

                                            $.post(WSManual ,{
                                                    AupTarjeta:AupTarjeta
                                                },function(respuesta){
                                              if(respuesta[0].estatus == 1){
                                                alert(respuesta[0].mensaje);
                                                $('#text_load').html("Actualizando información...");
                                                url = "../posfechados";
                                                window.location.replace(url);
                                                return false;
                                              }else if(respuesta[0].estatus == 0){
                                                alert(respuesta[0].mensaje);
                                                hideLoading();
                                              }
                                            });
                                        }
                                }else{
                                    alert("Ingrese el número de autorización que obtuvo de la terminal Fisica");
                                }

                        }else if(posfecho == "1" && TerminalCobro == 5){

                            alert("El cobro del pedido se posfechara para el día:"+diaposfecho);
                            showLoading("Actualizando datos bancarios"); 
                                    AupTarjeta.push({NTarjeta:NTarjeta,IsTitular:IsTitular,NameTitular:NameTitular,Vigencia:Vigencia,CSeguridad:CSeguridad,TTarjeta:TTarjeta,TerminalCobro:TerminalCobro,Banco:Banco,Promocion:Promocion,NumAutorizacion:NumAutorizacion,idPedido:idPedido,TipoLlamada:stipollamada,Comentarios:comentarios,Posfecho:diaposfecho});

                                    $.post(WSVirtual,{
                                            AupTarjeta:AupTarjeta
                                        },function(respuesta){
                                      if(respuesta[0].estatus == 1){
                                        
                                        hideLoading();
                                        showAlert('success',respuesta[0].mensaje);
                                        //$('#space_coments').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_pago').find('input, textarea, button, select').attr('disabled','disabled');
                                        $('#space_direccion').find('input, textarea, button, select').attr('disabled','disabled');

                                      }else if(respuesta[0].estatus == 0){
                                        alert(respuesta[0].mensaje);
                                        hideLoading();
                                      }else if(respuesta[0].estatus == 2){

                                        hideLoading();
                                        showAlert('danger',respuesta[0].mensaje);

                                      }
                                    });
                        } 
                    }
                }else{
                    alert("Existen campos vaciós");
                }    
            }
    }else{
        alert("Debe indicar\n->Tipo de llamada\n->Subtipo de llamada\n->Comentarios");
    }        
}

function VerificBlackList(){

    var Numero_tarjeta = $('#txt_ntarjeta').val();
    
    if(Numero_tarjeta && Numero_tarjeta != pedidoEditJSON.numeroTarjeta && Numero_tarjeta != newCard ){
        if ((Numero_tarjeta.length==15) || (Numero_tarjeta.length==16)) {

            showLoading("Verificando número de tarjeta");
            newCard = Numero_tarjeta;
                var  isValid = valid_credit_card(Numero_tarjeta);
                
                if(isValid == true){
                        
                    $.post("../../ExistListaNegra",{
                            Numero_tarjeta:Numero_tarjeta
                        },function(respuesta){
                        if(respuesta[0].estatus == 1){
                            $('#options_pago').find('input, textarea, button, select').removeAttr('disabled','disabled');
                            $('#txt_fcobro').attr('disabled','disabled');
                            posfechar_cobro();
                            $("#div_autorizacion").css("display", "none");
                            loadInformacion(respuesta[0].dTarjetas);
                        }else if(respuesta[0].estatus == 0){
                            alert(respuesta[0].mensaje);
                            $('#options_pago').find('input, textarea, button, select').attr('disabled','disabled');
                            $('#txt_ntarjeta').removeAttr('disabled','disabled');
                            $("#div_autorizacion").css("display", "none"); 
                            hideLoading();
                            
                        }
                    });

                }else{
                    alert("El número de tarjeta es invalido");
                    hideLoading();
                }

        }else{
            alert("El número de dígitos en la tarjeta es incorrecto.");
        }        
    
    }else{
        if(!Numero_tarjeta ){
            alert("Ingrese un número de tarjeta");
        }else if(Numero_tarjeta == pedidoEditJSON.numeroTarjeta){
            alert("El numero de tarjeta ya se ha ingresado antes");
        }else if(Numero_tarjeta != newCard){
            alert("El numero de tarjeta ya se ha ingresado antes");
        }
    }
}

function makeManualCobro(){
    var Terminal = $('#slt_terminal').val();
    if(Terminal == 5){
         $("#div_autorizacion").css("display", "block");
         $('#slt_terminal').prop('disabled', 'disabled');
         $('#btn_PagoTarjeta').html('<button type="button" class="btn btn-success" onclick=update_tarjeta("M")>Actualizar Tarjeta</button>');    
         $('option','#slt_pstfechar').remove();
         $('#slt_pstfechar').append('<option value="0">NO</option>');
         $('#slt_pstfechar').append('<option value="1">SI</option>');
    }else  if(Terminal == 7){
         $("#div_autorizacion").css("display", "block");
         $('#slt_terminal').prop('disabled', 'disabled');
         $('#btn_PagoTarjeta').html('<button type="button" class="btn btn-success" onclick=update_tarjeta("M")>Actualizar Tarjeta</button>');    
        $('option','#slt_pstfechar').remove();
        $('#slt_pstfechar').append('<option value="0">NO</option>');
    }else if(Terminal > 4){
        $("#div_autorizacion").css("display", "block");
        $('#slt_terminal').removeAttr('disabled','disabled');
         $('#btn_PagoTarjeta').html('<button type="button" class="btn btn-success" onclick=update_tarjeta("M")>Actualizar Tarjeta</button>');     
         $('option','#slt_pstfechar').remove();
         $('#slt_pstfechar').append('<option value="0">NO</option>');
    }else if(Terminal <= 4){
        $("#div_autorizacion").css("display", "none");
        $('#slt_terminal').removeAttr('disabled','disabled');
         $('#btn_PagoTarjeta').html('<button type="button" class="btn btn-success" onclick=update_tarjeta("V")>Actualizar Tarjeta</button>');
         $('option','#slt_pstfechar').remove();
         $('#slt_pstfechar').append('<option value="0">NO</option>');
         $('#slt_pstfechar').append('<option value="1">SI</option>');
    }
}

function cancelar_edicion(){
    showLoading("Cancelando edición"); 
    url = "../posfechados";
    window.location.replace(url);
    return false;
}

function loadInformacion(DatosTarjeta){
    $('#text_load').html("Actualizando información...");
    AnewTarjeta = DatosTarjeta;

    //Cargamos tipo de tarjeta
        $('option','#slt_tipotarjeta').remove();
        $('#slt_tipotarjeta').append('<option value="'+AnewTarjeta.tipoTarjeta.idTipoTarjeta+'">'+AnewTarjeta.tipoTarjeta.descripcion+'</option>');
        for(var x=0;x<=ATiposTarjeta.length-1;x++){
            if(ATiposTarjeta[x].idTipoTarjeta != AnewTarjeta.tipoTarjeta.idTipoTarjeta ){
                $('#slt_tipotarjeta').append('<option value="'+ATiposTarjeta[x].idTipoTarjeta+'">'+ATiposTarjeta[x].descripcion+'</option>');               
            }                
        }

    //Cargamos terminal por default y las demas terminales  
    //IF si la terminal por default de la tarjeta es AMEX o American Express no puede cambiar la terminal
        if(AnewTarjeta.terminal.idTerminal == 5 || AnewTarjeta.terminal.idTerminal == 7){
            $('option','#slt_terminal').remove();
            $('#slt_terminal').append('<option value="'+AnewTarjeta.terminal.idTerminal+'">'+AnewTarjeta.terminal.descripcion+'</option>');
            $('#slt_terminal').prop('disabled', 'disabled');
            makeManualCobro();
        }else{
            $('option','#slt_terminal').remove();
            $('#slt_terminal').append('<option value="'+AnewTarjeta.terminal.idTerminal+'">'+AnewTarjeta.terminal.descripcion+'</option>');
            $('#slt_terminal').removeAttr('disabled','disabled');
            for(var t=0;t<=ATerminales.length-1;t++){
                if(ATerminales[t].idTerminal != AnewTarjeta.terminal.idTerminal && ATerminales[t].idTerminal !=5 && ATerminales[t].idTerminal != 7 ){
                    $('#slt_terminal').append('<option value="'+ATerminales[t].idTerminal+'">'+ATerminales[t].descripcion+'</option>');
                }
            }
            makeManualCobro();
        }

    //Cargamos el Banco
        
        if(AnewTarjeta.banco){
            $('option','#slt_banco').remove();
            $('#slt_banco').append('<option value="'+AnewTarjeta.banco.idBanco+'">'+AnewTarjeta.banco.nombre+'</option>');
            for(var x=0;x<=ABancos.length-1;x++){
                if(ABancos[x].idBanco !=AnewTarjeta.banco.idBanco){
                    $('#slt_banco').append('<option value="'+ABancos[x].idBanco+'">'+ABancos[x].nombre+'</option>');                    
                }
            }
        }else{
            $('option','#slt_banco').remove();
            $('#slt_banco').append('<option value="">--------------------</option>');
            for(var x=0;x<=ABancos.length-1;x++){
                $('#slt_banco').append('<option value="'+ABancos[x].idBanco+'">'+ABancos[x].nombre+'</option>');
            }
        }


    //CArgamos las promociones        
        $('option','#slt_promo').remove();
        $('#slt_promo').append('<option value="">--------------------------------</option>');
            for(var p=0;p<=AnewTarjeta.promociones.length-1;p++){
                $('#slt_promo').append('<option value="'+AnewTarjeta.promociones[p].idPromocion+'">'+AnewTarjeta.promociones[p].descripcion+'</option>');
            }

    posfechar_cobro();               
    hideLoading();    
}