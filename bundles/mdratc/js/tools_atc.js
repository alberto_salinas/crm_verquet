  var AClientes = new Array();
    var AFormasPago = new Array();
    var ApedidoConsulta = new Array();
    var ATipoLlamada = new Array();
    var ASubTipoLllamada = new Array();
    var AresultadosBusqueda = new Array();
    var AdireccionesCliente = new Array();

  $(document).ready(function() { 


    $.getJSON("../formaspagoJSON",function(result){
        $.each(result, function(index, val) {
           AFormasPago.push({id:val.id,text:val.text});
        });
    });

    $.getJSON("../tipollamadasJSON",function(result){
      $.each(result, function(index, val) {
           ATipoLlamada.push({id:val.idTipoLlamada,text:val.descripcion});
        });
    });


    $.getJSON("../subtipollamadasJSON",function(result){
           ASubTipoLllamada = result;
    });


    $('#txt_pedido').keypress(function(e) {
      if(e.which == 13) {
        consulta_pedido();
      }
    });

    $('#txt_tarjeta').keypress(function(e) {
      if(e.which == 13) {
        consulta_pedido();
      }
    });       

    $('#txt_tipocompra').change(function(){
      if($('#txt_tipocompra').val()){
        consulta_pedido();
      }
    })

    $('#txt_guia').keypress(function(e) {
      if(e.which == 13) {
        consulta_pedido();
      }
    });





    $("#txt_cliente").select2({
      placeholder: "Escriba el nombre del cliente",
      allowClear: true,
      data:AClientes,
      minimumInputLength: 1
    });

    $("#txt_tipocompra").select2({
      placeholder: "Tipo de compra",
      allowClear: true,
      data:AFormasPago
    });


                 

    table  = $('#tbl_consultas_atc').DataTable({
                      "language":DataTables.languaje.es          
                    });


        $( "select" ).addClass( "form-control" );
        $( "input[type='text']" ).addClass( "form-control" );
        $( "input[type='search']").addClass( "form-control" );
        $( "#tbl_consultas_atc_length" ).addClass( "form-inline" );
        $( "#tbl_consultas_atc_filter" ).addClass( "form-inline" );             

  });



function UpdateDireccion(idDireccion,callback){
AupdateDirec = new Array();
AupdateDirec.push({idPedido:detallePP,Direccion:idDireccion});
  $.post("../UpdateDireccion",{
          AupdateDirec:AupdateDirec
      },function(respuesta){
          callback(respuesta);
        });

}

var html_flujo="";
var exist_flujo=1;
var Estatus_SAP="";

function clean_edit(){
  html_flujo="";
  exist_flujo=1;
  Estatus_SAP="";
}

function visualizarFlujoPedido(pedido,idPedido)
{   

    var template = _.template(document.getElementById("flujoPedidoTemplate").textContent);
    var html_flujo = template(pedido);
    $("#flujoPedidoNumeros").html(pedido.idPedido + " - SAP: " + pedido.pedidoSAP);
    hideLoading();
    open_popup(idPedido);
    comentarios_pedido(idPedido);
    tickets_pedido(idPedido);
    $("#body_flujo_SAP").html(html_flujo);
}

function getFlujoPedido(idPedido,callback, args)
{
  showLoading("Cagando información del pedido  "+idPedido);
  $.post(urls.sap.flujoPedido, {idPedido: idPedido}, function(data, textStatus, xhr) {
      
    if(data.code == 1 ){
      pedido = data.pedido;
      pedido.flujo = data.flujo;
      if(data.estatusDesc){

        Estatus_SAP =data.estatusDesc;

        if(Estatus_SAP == "ENTREGA"){
          Estatus_SAP="EN TRANSITO";
        }else if(Estatus_SAP == "CONFIRMACION MENSAJERIA"){
          Estatus_SAP="ENTREGADO";            
        }else if(Estatus_SAP == "DEVOLUCION"){
          Estatus_SAP="EN ESPERA DE DEVOLUCION";            
        }else if(Estatus_SAP == "ENTRADA POR DEVOLUCION"){
          Estatus_SAP="DEVUELTO";            
        }
      }      
      callback(pedido,idPedido); 
    }
    else
    {
      hideLoading();
      exist_flujo=0;
      open_popup(idPedido);
      comentarios_pedido(idPedido);
      tickets_pedido(idPedido);
      if(data.message !="Sin flujo en SAP"){
        Estatus_SAP=data.message;
      }
      $("#body_flujo_SAP").html("<h3>"+data.message+"</h3>");
    }
  });
}

  function gregoriandate(date){
      var annio = date.substring(0,4);
      date = date.substr(5);
      var mes = date.substr(2,4);
      mes = mes.substr(1);
      var dia = date.substr(0,2);
      date = mes+"-"+dia+"-"+annio;
      return date;
  }


  function consulta_pedido(){
    table.clear().draw(); 
     var ncliente = $('#txt_ncliente').val();
     var appcliente = $('#txt_appcliente').val();
     var apmcliente = $('#txt_apmcliente').val();
     var pedido = $('#txt_pedido').val();
     var ntarjeta = $('#txt_tarjeta').val();
     var tcompra = $('#txt_tipocompra').val();
     var nguia = $('#txt_guia').val();
     var fecha1 = $('#txt_fecha1').val();
     var fecha2 = $('#txt_fecha2').val();  

     if(fecha1 && fecha2){
        fecha1 = formatISO($('#txt_fecha1').val());
        fecha2 = formatISO($('#txt_fecha2').val());
      }


     if(ncliente||appcliente||apmcliente||pedido||nguia||ntarjeta){
                 

                  showLoading("Cargando Pedidos ...");
                    consultar(ncliente,appcliente,apmcliente,pedido,ntarjeta,tcompra,nguia,fecha1,fecha2,function(respuesta){
                      if(respuesta.estatus !=0){
                        AresultadosBusqueda = respuesta.pedido;
                        make_table(AresultadosBusqueda);
                      }else{
                        alert("No hay resultados");
                        hideLoading();
                      }
                      
                    }); 

            

     }else{
        alert("Es necesario indicar uno de los siguientes puntos:</br>->Nombre de cliente</br>->Apellido Paterno</br>->Apellido Materno</br>->Número de pedido</br>->Número de guía</br>->Número de Tarjeta");
     }

  }

  function consultar(ncliente,appcliente,apmcliente,pedido,ntarjeta,tcompra,nguia,fecha1,fecha2,callback){

    ApedidoConsulta.length = 0;
    ApedidoConsulta.push({Ncliente:ncliente,App:appcliente,Apm:apmcliente,idPedido:pedido,NTarjeta:ntarjeta,Tipo:tcompra,Nguia:nguia,date1:fecha1,date2:fecha2});

        $.post("../pedidoConsulta",{
          ApedidoConsulta:ApedidoConsulta
      },function(respuesta){
          callback(respuesta);
        });

  }


  function make_table(AresultadosBusqueda){
    var bodytable=new Array();
    var Productos="";

//NÚMERO DEL PEDIDO, NOMBRE DEL CLIENTE Y FECHA DEL PEDIDO
    for(var i =0;i<= AresultadosBusqueda.length - 1;i++){

      bodytable.length=0;
      Productos="";

      bodytable.push(AresultadosBusqueda[i].idPedido);
      bodytable.push(AresultadosBusqueda[i].cliente.toString);
      bodytable.push(AresultadosBusqueda[i].fechaGregorian);
 
//TIPO DE PAGO Y NUMERO DE TARJETA
        bodytable.push(AresultadosBusqueda[i].pago.descripcion);
        if(AresultadosBusqueda[i].pagosTarjeta.length > 0){
        bodytable.push(hidenumber(AresultadosBusqueda[i].pagosTarjeta[0].numeroTarjeta));
        }
        else
        {
        bodytable.push("");
        }

//PRODUCTOS DEL PEDIDO

              for(var z=0;z<=AresultadosBusqueda[i].productos.length-1;z++){
                Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].productos[z].productoPrecio.producto.descripcion+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].productos[z].cantidad+'</td></tr>';
              }
          bodytable.push("<table>"+Productos+"</table>");                  
//GASTOS ENVIO, SUBTOTAL, TOTAL
        bodytable.push("$"+parseFloat(AresultadosBusqueda[i].gastosEnvio.precio));
        bodytable.push("$"+parseFloat(AresultadosBusqueda[i].subTotal));
        bodytable.push("$"+parseFloat(AresultadosBusqueda[i].totalPago));
//FACTURA
            if(AresultadosBusqueda[i].factura == 1){
              bodytable.push('CON FACTURA');
            }else{
              bodytable.push('SIN FACTURA');
            }
//VALIDAMOS EL ESTATUS            
       bodytable.push('<button type="button" class="btn btn-info" onclick=getFlujoPedido('+AresultadosBusqueda[i].idPedido+',visualizarFlujoPedido);><span class="icon-pencil"></span></button>');
    
            
           
      table.row.add(bodytable).draw();    
    }
    $.unblockUI();
  }

var Adatos;
  function open_popup(pedido){

  //Selects TipoLlamada
    var StipoLlamada="<option>-------------------------</option>";
    for(var x=0;x<=ATipoLlamada.length-1;x++){
      StipoLlamada = StipoLlamada+"<option value='"+ATipoLlamada[x].id+"'>"+ATipoLlamada[x].text+"</option>";
      //console.log(StipoLlamada);
    }
    Adatos = info_cliente(pedido);
    GetDirecciones(pedido);
  //creacion del cuerpo del popup
    $('#title_atc').html('ATC: Pedido '+pedido+'<label style="float: right;">Número Origen: '+Adatos[0].CasaC+'</label>');
          var cuerpo= '<div class="row" style="padding-top: 15px;">'+
                        '<div class="col-md-6">'+
                          '<div class="panel panel-success">'+
                            '<div class="panel-heading">Resúmen del pedido: '+pedido+'<label style="float: right;">Numero de Pedido SAP: '+Adatos[0].SAP+'</label><p>Estatus del Pedido: <label id="lbl_estatus">';
                     if(Estatus_SAP){
                      cuerpo=cuerpo+Estatus_SAP;
                     }else{
                      cuerpo=cuerpo+Adatos[0].DescEstatus;
                     }   

              cuerpo=cuerpo+'</label></p>'+
                              '</div>'+
                            '<div class="panel-body">'+
                              '<div class="col-md-12">'+

                                '<div class="col-md-6"><label>Fecha de Alta:</label> '+Adatos[0].FCreado+'</div>'+
                                '<div class="col-md-6"><label>Agente:</label> '+Adatos[0].POperador+'</div>'+ 
                                '<div class="col-md-6"><label>Fecha de Cobro:</label> '+Adatos[0].FCobrado+'</div>'+ 
                                '<div class="col-md-6"><label>Cobranza:</label> '+Adatos[0].PCobranza+'</div>'+ 
                                '<div class="col-md-12"><label># Autorizacion:</label> '+Adatos[0].Auto+'</div>'+ 
                                  '<div class="col-md-3" style="font-weight: 700;">Cliente:</div><div class="col-md-9">'+Adatos[0].NombreC+'</div>'+
                                  '<div class="col-md-3" style="font-weight: 700;">Dirección:</div><div class="col-md-9" id="desc_direccion">';
  if(exist_flujo == 0 && Adatos[0].EstatusP==16 && Adatos[0].Tpago == 1){
      cuerpo=cuerpo+'<a href="#" id="txt_direccion" onclick=AproveChanges("D","txt_direccion",'+pedido+')>'+Adatos[0].DireccionC+'</a>&nbsp;<button type="button" id="btn_up_direcciones" class="btn btn-warning" style="display:none;" onclick=UpdateDirecciones("'+pedido+'","D","txt_direccion")><span class="icon-loop"></span></button>&nbsp;<button type="button" onclick=newDireccion() style="display:none;" id="btn_new_address" class="btn btn-success"><span class="icon-file-add"></span></button>';
  }else if(Estatus_SAP == "DEVUELTO"){
      cuerpo=cuerpo+'<a href="#" id="txt_direccion" onclick=AproveChanges("D","txt_direccion",'+pedido+')>'+Adatos[0].DireccionC+'</a>&nbsp;<button type="button" id="btn_up_direcciones" class="btn btn-warning" style="display:none;" onclick=UpdateDirecciones("'+pedido+'","D","txt_direccion")><span class="icon-loop"></span></button>&nbsp;<button type="button" onclick=newDireccion() style="display:none;" id="btn_new_address" class="btn btn-success"><span class="icon-file-add"></span></button>';
  }else{
      cuerpo=cuerpo+Adatos[0].DireccionC;
  }      
          cuerpo=cuerpo+'</div>'+
                            '<div class="col-md-3" style="font-weight: 700;">Entre Calles:</div><div class="col-md-9">'+Adatos[0].EntreC+'</div>'+
                            '<div class="col-md-3" style="font-weight: 700;">Referencia:</div><div class="col-md-9">'+Adatos[0].ReferenciasC+'</div>'+                            
                            '<div class="col-md-3" style="font-weight: 700;">E-mail:</div><div class="col-md-9">';
              if(Adatos[0].Mail){
                cuerpo=cuerpo+'<a href="mailto:'+Adatos[0].Mail+'">'+Adatos[0].Mail+'</a>';
              }else{
                cuerpo=cuerpo+"SIN CORREO";
              }                    
                  cuerpo=cuerpo+'</div>'+
                                  '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+           
                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Casa:</div>'+
                                      '<div class="col-md-12">'+Adatos[0].CasaC+'</div>'+
                                  '</div>'+ 
                                  '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+     
                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Oficina:</div>'+
                                      '<div class="col-md-12">'+Adatos[0].OficinaC+'</div>'+
                                  '</div>'+   
                                  '<div class="col-md-4" style="padding: 0px !important;margin-bottom: 5px;">'+  
                                      '<div class="col-md-12" style="font-weight: 700;">Tel. Celular:</div>'+
                                      '<div class="col-md-12">'+Adatos[0].CelC+'</div>'+
                                  '</div>'+     
                                  '<div class="col-md-12" style="margin:5px 0px 5px 0px;font-weight: 700;">Productos:</div>'+
                                  '<div class="col-md-12">'+tbl_productos_pedido(pedido,Adatos[0].EstatusP,Adatos[0].Tpago)+'</div>'+
                                  '<div class="col-md-3" style="font-weight: 700;">Tipo de Pago</div>'+
                                  '<div class="col-md-3">'+Adatos[0].PagoC+'</div>'+
                                  '<div class="col-md-3" style="font-weight: 700;">Promoción</div>'+
                                  '<div class="col-md-3">'+Adatos[0].PromocionC+'</div>'+
                                  '<div class="col-md-12" style="text-align: end;margin-top: 20px;"><label>Descargar PDF:&nbsp;&nbsp;&nbsp;</label><button style="width: 8%;border-radius: 100%;padding: 0px;" type="button" class="btn btn-danger" onclick=made_pdf('+pedido+')><img src='+img_pdf.src+' style="width: 103%;"  ></button></div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<div class="col-md-4" style="margin-bottom: 10px;font-weight: 700;">*Tipo Llamada:</div>'+
                              '<div class="col-md-8" style="margin-bottom: 10px;"><select id="s_tllamada" class="form-control" onchange="update_stllamada()">'+StipoLlamada+'</select></div>'+
                          '</div>'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<div class="col-md-4" style="margin-bottom: 10px;font-weight: 700;">*Sub llamada:</div>'+
                              '<div class="col-md-8" style="margin-bottom: 10px;"><select id="s_stllamada" class="form-control"></select></div>'+
                          '</div>'+
                          '<div class="col-md-12" style="padding: 0px !important;" id="operaciones_atc_select">'+
                          operaciones_ATC(1,Adatos[0].Tpago)+
                          '</div>'+
                          '<div class="col-md-12" style="margin-bottom: 10px;font-weight: 700;" id="desc_options"></div>'+
                          '<div class="col-md-12" id="more_options" style="margin-bottom: 10px;"></div>'+
                          '<div class="col-md-12" style="margin-top: 15px;font-weight: 700;">*Comentarios:</div>'+
                          '<div class="col-md-12"><textarea id="txt_comentarios" maxlength="300" style="resize:none;height: 200px;" class="form-control"></textarea></div>'+
                          '<div class="col-md-12" style="text-align:center;  margin-top:5px;">'+
                          '<button type="button" class="btn btn-success" onclick="end_Call('+pedido+')">Guardar Comentarios</button>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" onclick="clean_edit(),canceledit();">Cancelar</button>'+
                          '</div>'+
                          '<div class="col-md-12" style="text-align:center;  margin-top:50px;">'+
                            '<button type="button" style="width: 20%;"  class="btn btn-warning" onclick=openMensajeria("Estafeta")><img src='+logo_estafeta.src+' style="width: 100%;" ></button>  <button type="button" style="width: 20%;" class="btn btn-info" onclick=openMensajeria("Skymex")><img src='+logo_skymex.src+' style="width: 100%;"></button> <button type="button" style="width: 20%;" class="btn btn-primary" onclick=openMensajeria("Fedex")><img src='+logo_fedex.src+' style="width: 100%;"></button>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-12" style="margin-top:5px;">'+
                        '<div class="panel-group" id="accordion">'+
                          '<div class="panel panel-default">'+
                            '<div class="panel-heading">'+
                              '<h4 class="panel-title">'+
                                '<a data-toggle="collapse" data-parent="#accordion" href="#SAP">'+
                                  'Estatus del pedido SAP'+
                                  '<button type="button" id="btn_up_sap" class="btn btn-warning" onclick=UpdateSAP('+pedido+',UpdatevisualizarFlujoPedido,'+Adatos[0].Tpago+')><span class="icon-loop"></span></button>'+
                                '</a>'+
                              '</h4>'+
                            '</div>'+
                            '<div id="SAP" class="table-responsive panel-collapse collapse" >'+
                              '<div class="panel-body" id="body_flujo_SAP" style="text-align: center;">'+
                                '<img src='+img.src+' style="width: 4%;"><h3>Cargando</h3>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="panel panel-default">'+
                            '<div class="panel-heading">'+
                              '<h4 class="panel-title">'+
                                '<a data-toggle="collapse" data-parent="#accordion" href="#ComentariosPedido">'+
                                  'Comentarios Previos'+
                                '</a>'+
                              '</h4>'+
                            '</div>'+
                            '<div id="ComentariosPedido" class="panel-collapse collapse">'+
                              '<div class="panel-body" id="body_comentarios_pedido" style="margin: 11px 2px 11px 26px;">'+
                                  '<img src='+img.src+' style="width: 4%;"><h3>Cargando</h3>'+
                              '</div>'+
                            '</div>'+                            
                          '</div> ' +
                          '<div class="panel panel-default">'+
                            '<div class="panel-heading">'+
                              '<h4 class="panel-title">'+
                                '<a data-toggle="collapse" data-parent="#accordion" href="#TicketsPedido">'+
                                  'Tickets de Servicio'+
                                '</a>'+
                              '</h4>'+
                            '</div>'+
                            '<div id="TicketsPedido" class="panel-collapse collapse">'+
                              '<div class="panel-body" id="body_tickets_pedido" style="margin: 11px 2px 11px 26px;">'+
                                  '<img src='+img.src+' style="width: 4%;"><h3>Cargando</h3>'+
                              '</div>'+
                            '</div>'+                            
                          '</div> ' +
                         '</div>' +
                        '</div>'+  
                      '</div>';
                      
    $('#body_atc').html(cuerpo);
    //comentarios_btn
    //<button type="button" class="btn btn-warning" onclick="comentarios_pedido('+pedido+')"><span class="icon-search"></span></button>&nbsp;&nbsp;



    $('#popup_atc').modal({
      backdrop: 'static',
      keyboard: false
    });
    

    $("#txt_direccion").change(function(){

      var idDireccion = $("#txt_direccion").val();
      if(idDireccion){
        showLoading("Actualizando Direccion");
      UpdateDireccion(idDireccion,function(respuesta){
          if(respuesta.estatus == 1){
            alert("Direccion Actualizada");
            hideLoading();
          }else{
            alert("Error:\n"+respuesta.mensaje);
            hideLoading();
          }
      }); 
     }else{
      alert("Debe seleccionar una dirección");
     } 
});

$('#popup_atc').modal('show');
  
  }

function operaciones_ATC(load,Tpago){

  if(Tpago != "COD"){
      
                              if(exist_flujo == 1 && Estatus_SAP == "ENTREGADO" ){

 return '<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion" class="form-control">'+
                                '<option value="SO">---------------</option>'+
                                '<option value="C">Aviso de Cambio Fisico</option>'+
                                '<option value="R">Aviso de Reembolso</option>'+
                                '</select></div>'; 

                              }else if(exist_flujo == 1 && Estatus_SAP == "DEVUELTO" ){

 return '<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion"  class="form-control">'+
                                '<option value="SO">---------------</option>'+                                
                                '<option value="R">Aviso de Reembolso</option>'+                                
                                '<option value="C">Aviso de Cambio Fisico</option>'+
                                '<option value="E">Aviso de Reenvio</option>'+
                                '</select></div>'; 

                              }else{

 return '<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion" class="form-control">'+
                                '<option value="SO">---------------</option>'+
                              '</select></div>'; 

                              }
  }else if(Tpago == "COD"){
                              if(exist_flujo == 1 && Estatus_SAP == "ENTREGADO" ){

 $('#operaciones_atc_select').html('<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion" class="form-control">'+
                                '<option value="SO">---------------</option>'+
                                '<option value="C">Aviso de Cambio Fisico</option>'+
                                '<option value="R">Aviso de Reembolso</option>'+
                                '</select></div>'); 

                              }else if(exist_flujo == 1 && Estatus_SAP == "DEVUELTO" ){

$('#operaciones_atc_select').html('<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion"  class="form-control">'+
                                '<option value="SO">---------------</option>'+
                                '<option value="E">Aviso de Reenvio</option>'+                                
                                '<option value="C">Aviso de Cambio Fisico</option>'+                                
                                '</select></div>'); 

                              }else{

$('#operaciones_atc_select').html('<div class="col-md-3" style="margin-bottom: 10px;font-weight: 700;">Avisos:</div>'+
                              '<div class="col-md-9" style="margin-bottom: 10px;"><select id="s_operacion" class="form-control">'+
                                '<option value="SO">---------------</option>'+
                              '</select></div>'); 

                              }
  }                  
}



  function update_stllamada(){
    var tllamada = $('#s_tllamada').val();
    $('option','#s_stllamada').remove();
      for(var x =0;x<=ASubTipoLllamada.length-1;x++){
        if (ASubTipoLllamada[x].tipoLlamada.idTipoLlamada == tllamada) {
            $('#s_stllamada').append('<option value="'+ASubTipoLllamada[x].idSubTipo+'" selected="selected">'+ASubTipoLllamada[x].descripcionSubTipo+'</option>');

        }
      }  

  }


  function hidenumber(num){
    var nnum ="";
    for(x=0;x<=num.length-1;x++){

      if(x>3 && x<12){
        nnum = nnum + '&#8226;';
      }else{
        nnum = nnum+num[x];
      }      
    }
    return nnum;
  }

  function end_Call(pedido){
    var Tllamada = $('#s_tllamada').val();
    var Tsllamada = $('#s_stllamada').val();
    var Toperacion = $('#s_operacion').val();
    var Comentarios = $('#txt_comentarios').val();
    var ApedidoATC = new Array();

      if(Tllamada&&Tsllamada&&Comentarios){
        showLoading("Actualizando información");
        ApedidoATC.length=0;
          if(Toperacion == "SO"||Toperacion == "EC"||Toperacion == "ER"){
            ApedidoATC.push({idPedido:pedido,Tllamada:Tllamada,Tsllamada:Tsllamada,Operacion:Toperacion,Comentarios:Comentarios});
              Operacion_ATC(ApedidoATC,function(respuesta){
                  if(respuesta.code == 1){
                    alert("Registro exitoso\n PEDIDO SAP: "+respuesta.pedidoSAP);
                    $('#btn_consulta').click();
                    $('#popup_atc').modal('hide');                    
                    getFlujoPedido(pedido,visualizarFlujoPedido);
                  }else{
                    hideLoading();
                    alert(respuesta.message);
                  }
              });
          }else{
             ApedidoATC.push({idPedido:pedido,Tllamada:Tllamada,Tsllamada:Tsllamada,Operacion:Toperacion,Comentarios:Comentarios});
              Operacion_ATC(ApedidoATC,function(respuesta){
                  if(respuesta.code == 1){
                    alert("Registro exitoso\n PEDIDO SAP: "+respuesta.pedidoSAP);
                    $('#btn_consulta').click();
                    $('#popup_atc').modal('hide');
                    getFlujoPedido(pedido,visualizarFlujoPedido);
                  }else{
                    hideLoading();
                    alert(respuesta.message);
                  }
              });
          }


      }else{
        alert("Es necesario indicar:\n->Tipo de Llamada\n->Tipo de sub llamada\n->Comentarios");
      }
  }

  function Operacion_ATC(ApedidoATC,callback){

    if(ApedidoATC[0].Operacion == "SO" || ApedidoATC[0].Operacion == "EC" || ApedidoATC[0].Operacion == "ER"){
      $.post("../ComentariosPedidoATC",{
          ApedidoATC:ApedidoATC
      },function(respuesta){
          callback(respuesta);
        });
    }else if(ApedidoATC[0].Operacion == "C" || ApedidoATC[0].Operacion == "R" || ApedidoATC[0].Operacion == "E"){
      $.post("../crearPedidoCambioFisicoSAP",{
          ApedidoATC:ApedidoATC
      },function(respuesta){
          callback(respuesta);
        });
    }

  }

  function comentarios_pedido(pedido){
    obtener_comentarios(pedido,function(respuesta){
      $('#body_comentarios_pedido').html(made_tbl_comentarios(respuesta));
    });
  }

  function obtener_comentarios(pedido,callback){

    $.post("../GetComentariosPedido",{
          pedido:pedido
      },function(respuesta){
          callback(respuesta);
        });
  }

  function tickets_pedido(pedido){
    obtener_tickets(pedido,function(respuesta){
      $('#body_tickets_pedido').html(made_tbl_tickets(respuesta));
    });
  }

  function obtener_tickets(pedido,callback){

    $.post("../GetTicketsPedido",{
          pedido:pedido
      },function(respuesta){
          callback(respuesta);
        });
  }
  function made_tbl_comentarios(respuesta){

    if(respuesta == 0){
      var tbl_comentarios ='<h3>Sin Comentarios</h3>';

    }else{
    var tbl_comentarios ='<table class="table table-condensed table-bordered" id="tbl_comentarios" style="width:95%">'+
                            '<thead>'+
                              '<tr style="font-weight: 700;font-size: 13px;">'+
                                '<td>Fecha</td>'+
                                '<td>Tipo Llamada</td>'+
                                '<td>Subtipo Llamada</td>'+
                                '<td>Comentarios</td>'+
                                '<td>Usuario</td>'+
                              '</tr>'+    
                            '</thead>'+
                          '<tbody>';
        for(x=0;x<=respuesta.length-1;x++){
          tbl_comentarios = tbl_comentarios +'<tr style="font-size: 13px;text-align:center">'+
                                                '<td>'+respuesta[x].fechaGregorian+'</td>'+
                                                '<td>'+respuesta[x].tipoLlamada.tipoLlamada.descripcion+'</td>'+
                                                '<td>'+respuesta[x].tipoLlamada.descripcionSubTipo+'</td>'+
                                                '<td>'+respuesta[x].comentario+'</td>'+
                                                '<td>'+respuesta[x].usuario.fullname+'</td>'+
                                              '</tr>';
            if(x == respuesta.length-1){
                  tbl_comentarios = tbl_comentarios + '</tbody></table>'
            }                                    
        }                  
    }
   return tbl_comentarios;                       
  }


    function made_tbl_tickets(respuesta){

    if(respuesta == 0){
      var tbl_tickets ='<h3>Sin TICKETS</h3>';

    }else{
    var tbl_tickets ='<table class="table table-condensed table-bordered" id="tbl_tickets" style="width:95%">'+
                            '<thead>'+
                              '<tr style="font-weight: 700;font-size: 13px;">'+
                                '<td># Ticket</td>'+
                                '<td>Fecha</td>'+
                                '<td>Motivos</td>'+
                                '<td>Estatus del Ticket</td>'+
                                '<td>Usuario</td>'+
                              '</tr>'+    
                            '</thead>'+
                          '<tbody>';
        for(x=0;x<=respuesta.length-1;x++){
          tbl_tickets = tbl_tickets +'<tr style="font-size: 13px;text-align:center">'+
                                                '<td>'+respuesta[x].idTicket+'</td>'+
                                                '<td>'+respuesta[x].FechaGregorian+'</td>'+
                                                '<td>'+respuesta[x].Comentarios+'</td>'+
                                                '<td>'+respuesta[x].EstatusTicket.Descripcion+'</td>'+
                                                '<td>'+respuesta[x].Usuario.fullname+'</td>'+
                                              '</tr>';
            if(x == respuesta.length-1){
                  tbl_tickets = tbl_tickets + '</tbody></table>'
            }                                    
        }                  
    }
    console.log(tbl_tickets);
   return tbl_tickets;                       
  }


  function info_cliente(pedido){
    result = new Array();
    for(x=0;x<=AresultadosBusqueda.length-1;x++){
      if(AresultadosBusqueda[x].idPedido == pedido){
          cobrado = 'S/C';
          cobranza = 'S/C';
          autorizacion = 'S/N';
          nombre = AresultadosBusqueda[x].cliente.toString;
          direccion = AresultadosBusqueda[x].direccion.toString;
          referencia = AresultadosBusqueda[x].direccion.referencia;
          entre = AresultadosBusqueda[x].direccion.entreCalles;
          casa = AresultadosBusqueda[x].direccion.telCasa;
          cel = AresultadosBusqueda[x].direccion.telCel; 
          oficina = AresultadosBusqueda[x].direccion.telOficina;
          pago = AresultadosBusqueda[x].pago.descripcion;
          estatus = AresultadosBusqueda[x].estatus.idEstatus;
          Destatus = AresultadosBusqueda[x].estatus.descripcion;
          telefonoorigen = AresultadosBusqueda[x].telefono;
          t_pago=AresultadosBusqueda[x].pago.idTipoPago;
          p_sap=AresultadosBusqueda[x].pedidoSAP;
          email=AresultadosBusqueda[x].cliente.email;
          creado = AresultadosBusqueda[x].fechaGregorian.substr(0,10);
          operador = AresultadosBusqueda[x].usuario.uClave;

          if(t_pago == 1){
              //Informacion si esta cobrado el pedido
              if(AresultadosBusqueda[x].cobros.length > 0){
                for(var y=0; y<=AresultadosBusqueda[x].cobros.length-1;y++){
                  if(AresultadosBusqueda[x].cobros[y].respuesta.idTipoRespuesta == 1){
                    cobrado =AresultadosBusqueda[x].cobros[y].fechaCobroGregorian.substr(0,10);                    
                    if(AresultadosBusqueda[x].cobros[y].usuario){
                      cobranza = AresultadosBusqueda[x].cobros[y].usuario.uClave;
                    }
                    autorizacion = AresultadosBusqueda[x].cobros[y].numeroAutorizacion;                    

                  }
                }
              }
          }else{
            cobrado = 'COD';
            cobranza = 'COD';
            autorizacion = 'COD';
          }

          if(AresultadosBusqueda[x].pagosTarjeta.length > 0 ){
          promocion = AresultadosBusqueda[x].pagosTarjeta[0].promocion.descripcion;          
          }
          else
          {
          promocion = "Sin Promoción";          
          }
        
      }
      if(x == AresultadosBusqueda.length-1){
            result.length =0;
            result.push({NombreC:nombre,DireccionC:direccion,ReferenciasC:referencia,EntreC:entre,CasaC:casa,CelC:cel,OficinaC:oficina,PagoC:pago,PromocionC:promocion,EstatusP:estatus,DescEstatus:Destatus,Origen:telefonoorigen,Tpago:t_pago,SAP:p_sap,Mail:email,FCreado:creado,POperador:operador,FCobrado:cobrado,PCobranza:cobranza,Auto:autorizacion});
            return result;
        }
    } 
  }

  function tbl_productos_pedido(pedido,estatus,tpago){

    var table_dpedido='<table style="width:100%;" class="table table-bordered table-hover"><thead style="text-align: center;font-weight: 700;font-size: 14px;"><tr><td>Cantidad</td><td>Producto</td><td>PU</td><td>Total</td></tr></thead><tbody>';
    for(x=0;x<=AresultadosBusqueda.length-1;x++){
      if(AresultadosBusqueda[x].idPedido == pedido){
        for(z=0;z<=AresultadosBusqueda[x].productos.length-1;z++){
            
            table_dpedido=table_dpedido+'<tr style="text-align: center;font-size: 14px;">'+
                                          '<td>';
            if(exist_flujo == 0 && Adatos[0].EstatusP==16 && Adatos[0].Tpago == 1){
                table_dpedido=table_dpedido+'<a href="#" id="C'+AresultadosBusqueda[x].productos[z].idDetallePedido+'" onclick=AproveChanges("C","C'+AresultadosBusqueda[x].productos[z].idDetallePedido+'",'+AresultadosBusqueda[x].productos[z].idDetallePedido+')>'+AresultadosBusqueda[x].productos[z].cantidad+'</a>';
            }else{
                table_dpedido=table_dpedido+AresultadosBusqueda[x].productos[z].cantidad;
            }                                                                         

            table_dpedido=table_dpedido+'</td>'+
                                          '<td>'+AresultadosBusqueda[x].productos[z].productoPrecio.producto.toString+'</td>'+
                                          '<td>';
            if(exist_flujo == 0 && Adatos[0].EstatusP==16 && Adatos[0].Tpago == 1){
                table_dpedido=table_dpedido+'<a href="#" id="P'+AresultadosBusqueda[x].productos[z].idDetallePedido+'" onclick=getprices("'+AresultadosBusqueda[x].productos[z].productoPrecio.producto.idProducto+'","'+AresultadosBusqueda[x].productos[z].productoPrecio.oficinaVenta.claveSAP+'","'+AresultadosBusqueda[x].productos[z].productoPrecio.canalDistribucion+'","'+AresultadosBusqueda[x].productos[z].productoPrecio.organizacionVentas+'","'+AresultadosBusqueda[x].productos[z].idDetallePedido+'","'+AresultadosBusqueda[x].productos[z].productoPrecio.idProductoPrecio+'","P","P'+AresultadosBusqueda[x].productos[z].idDetallePedido+'")>$'+AresultadosBusqueda[x].productos[z].productoPrecio.precio+'</a>';
            }else{
                table_dpedido=table_dpedido+AresultadosBusqueda[x].productos[z].productoPrecio.precio;
            }                              
                                          
            table_dpedido=table_dpedido+'</td>'+
                                          '<td id="PTP'+AresultadosBusqueda[x].productos[z].idDetallePedido+'">$'+parseFloat(AresultadosBusqueda[x].productos[z].productoPrecio.precio)*AresultadosBusqueda[x].productos[z].cantidad+'</td>'+
                                        '</tr>';

            if(z == AresultadosBusqueda[x].productos.length-1){
            
              table_dpedido=table_dpedido+'<tr>'+
                                              '<td colspan="2" rowspan="4" style="text-align: center;"><img src="'+img_MDR.src+'" style="width: 24%;margin: 10px;"></td>'+                                             
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">SubTotal</td>'+
                                              '<td style="text-align: left;font-size: 14px;font-weight: 700;" id="camp_subtotal">$'+parseFloat(AresultadosBusqueda[x].subTotal)+'</td>'+ 
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">Envio</td>'+
                                              '<td style="text-align: left;font-size: 14px;" id="camp_envio">$'+parseFloat(AresultadosBusqueda[x].gastosEnvio.precio)+'</td>'+
                                          '</tr>'+
                                          '<tr>'+
                                              '<td style="text-align: left;font-size: 14px;">Total</td>'+
                                              '<td style="text-align: left;font-size: 14px;font-weight: 700;" id="camp_total">$'+parseFloat(AresultadosBusqueda[x].totalPago)+'</td>'+
                                          '</tr></tbody></table>';

            }
        }
      }

    }
    return table_dpedido;
  }

 var clave = "";
 var idedit = "";
 var detallePP = "";
  //Obtener direcciones del cliente
  function GetDirecciones(pedido){

      var idCliente;
      AdireccionesCliente.length=0;
    for(var x=0;x<=AresultadosBusqueda.length-1;x++){
      if(AresultadosBusqueda[x].idPedido == pedido){
          idCliente = AresultadosBusqueda[x].cliente.id;
      }
    }

    $.post("../GetDirreccionesCliente",{
        idCliente:idCliente
      },function(respuesta){
        AdireccionesCliente = respuesta;
          
      });
  }

  function newDireccion(){
      url = "../direcciones/new";
      window.open(url, '_blank');
      return false;
  }

  function openMensajeria(mensajeria){

      if(mensajeria == "Fedex"){

        url = "http://www.fedex.com/mx/";
        window.open(url, '_blank');
        return false;

      }else if(mensajeria == "Estafeta"){

        url = "http://www.estafeta.com/";
        window.open(url, '_blank');
        return false;

      }else if(mensajeria == "Skymex"){

        url = "http://www.skymex.com.mx/";
        window.open(url, '_blank');
        return false;

      }      
  }

  function UpdateDirecciones(pedido,ident,id){
    document.getElementById('btn_up_direcciones').style.display = 'none';
    showLoading("Actualizando direcciones");
    var idCliente;
      AdireccionesCliente.length=0;
    for(var x=0;x<=AresultadosBusqueda.length-1;x++){
      if(AresultadosBusqueda[x].idPedido == pedido){
          idCliente = AresultadosBusqueda[x].cliente.id;
      }
    }
try{
    $.post("../GetDirreccionesCliente",{
        idCliente:idCliente
      },function(respuesta){
        AdireccionesCliente = respuesta;
        hideLoading();
        edit(ident,id);  
      });
  }catch(err){
    alert(err);
  }
  }

function UpdatevisualizarFlujoPedido(pedido,idPedido,args)
{   

    var template = _.template(document.getElementById("flujoPedidoTemplate").textContent);
    var html_flujo = template(pedido);
    $("#flujoPedidoNumeros").html(pedido.idPedido + " - SAP: " + pedido.pedidoSAP);    
    comentarios_pedido(idPedido);
    tickets_pedido(idPedido);
    operaciones_ATC(0,args);
    $("#body_flujo_SAP").html(html_flujo);
    hideLoading();
    
}


  function UpdateSAP(idPedido,callback,args){

      showLoading("Actualizando información del estatus SAP  "+idPedido);
  $.post(urls.sap.flujoPedido, {idPedido: idPedido}, function(data, textStatus, xhr) {
      
    if(data.code == 1 ){
      pedido = data.pedido;
      pedido.flujo = data.flujo;
      if(data.estatusDesc){

        Estatus_SAP =data.estatusDesc;

        if(Estatus_SAP == "ENTREGA"){
          Estatus_SAP="EN TRANSITO";
        }else if(Estatus_SAP == "CONFIRMACION MENSAJERIA"){
          Estatus_SAP="ENTREGADO";            
        }else if(Estatus_SAP == "DEVOLUCION"){
          Estatus_SAP="EN ESPERA DE DEVOLUCION";            
        }else if(Estatus_SAP == "ENTRADA POR DEVOLUCION"){
          Estatus_SAP="DEVUELTO";            
        }
        $('#lbl_estatus').html(Estatus_SAP);
      }      
      callback(pedido,idPedido,args); 
    }
    else
    {
      hideLoading();
      exist_flujo=0;
      comentarios_pedido(idPedido);
      tickets_pedido(idPedido);
      $("#body_flujo_SAP").html("<h3>"+data.message+"</h3>");
    }
  });

  }

  // Aprobar cambios clave del supervisor
$(document).ready(function(){
  $("#btn_aceptar_sup").click(function(){
    valclave(typeedit,idedit);
  });
});

//el parametro detalle es para el cambio de precio es el detalle de la relacion pedido/producto
function AproveChanges(ident,id,detalle){
  
  if(clave == 1 && idedit == id){
          edit(ident,id); 
  }else{
      idedit = id;
    typeedit = ident;
    detallePP = detalle;
    $('#pf_supervisor').modal('show');
    
 }
}

function valclave(ident,id){
  usuario = $('#user_sup').val();
  contrasena = $('#pass_sup').val();
  bioSupervisor = new Array();
  

  if (contrasena != "" && usuario != "") {
    $("#super_loading").html("<img src="+img_loding.src+">&nbsp;&nbsp;&nbsp;<label>Validando...</label>");
    $('#btn_cancel_sup').attr("disabled", true);
    $('#btn_aceptar_sup').attr("disabled", true);

    bioSupervisor.push({user:usuario, pass:contrasena});
    $.post("../Supervisor",{
      bioSupervisor:bioSupervisor
    },function(respuesta){
      if(respuesta[0].estatus == 1){
        clave = 1;
        $('#user_sup').val("");
        $('#pass_sup').val("");
        $("#super_loading").html("");
        alert("Usuario correcto");
        $('#btn_cancel_sup').attr("disabled", false); 
        $('#btn_aceptar_sup').attr("disabled", false);        
        $('#pf_supervisor').modal('hide');
        edit(ident,id);
      }else{
        $('#user_sup').val("");
        $('#pass_sup').val("");
        $("#super_loading").html("");       
        alert(respuesta[0].mensaje);
        $('#btn_cancel_sup').attr("disabled", false); 
        $('#btn_aceptar_sup').attr("disabled", false);  
      }
      }
    );
    
  }else{
    alert("Es necesario indicar Usuario y contraseña de Supervisor de area");
  }
}

function canceledit(){
  clave = "";
  idedit = "";
  detallePP = "";
}

function edit(ident,id){
    document.getElementById('btn_up_direcciones').style.display = 'none';
    document.getElementById('btn_new_address').style.display = 'none';
   if(ident == "C"){
        $("#"+id).editable({
          type: 'number',
          min:1,
          max:999,
        title: 'Cantidad',
        mode: 'inline', 
          validate: function(value){
            if(value == 0 || value == "Empty"){
              return "Este campo no puede estar vacio";
            }else{
              validatechange(ident,id,value,detallePP,function(res){
                  if(res == 1){
                    $("#"+id).css({ color: "#428bca"});
                    canceledit();
                    alert("Actualización exitosa");
                    $('#text_load').text("Actualizando Pedidos");
                    update_table('C',ApedidosUpdate,value); 
                  }else{
                    $("#"+id).css({ color: "#FF0000"});
                    alert("Error Actualizacion CANTIDAD DE PRODUCTO(s):\n"+res);
                    hideLoading();
                  }
              }); 
            }
        },
      }); 
   }else if(ident == "P"){
      $("#"+id).editable({
          type: 'select',
          title: 'Nuevo Precio',
          mode: 'inline', 
            source: Aprices.concat(),
            validate: function(value){
              if(value == null||value==0){
                return "No puede dejar vacio este campo"
              }else{
                validatechange(ident,id,value,detallePP,function(res){
                if(res == 1){
                  $("#"+id).css({ color: "#428bca"});
                  canceledit();
              precios_cagados=0;
                  alert("Actualización exitosa");
                  $('#text_load').text("Actualizando Pedidos");
                  update_table('P',ApedidosUpdate,value);                 
                }else{
                  
                  $("#"+id).css({ color: "#FF0000"});
                  alert("Error Actualizacion PRECIO:\n"+res[0].mensaje);                    
                  hideLoading();
                  
                }
              });
            }   
          }, 
    });
   }else if(ident == "D"){
    document.getElementById('btn_up_direcciones').style.display = 'initial';
    document.getElementById('btn_new_address').style.display = 'initial';
    ADirecciones = new Array(); 
        $.each(AdireccionesCliente, function(index, val) {
           ADirecciones.push({id:val.idDireccion,text:val.toString});
        });

      $("#txt_direccion").select2({
      placeholder: "Direcciones",
      allowClear: true,
      data:ADirecciones
    });
   } 
}

//CARGA DE LOS PRECIOS RELACIONADOS AL PRODUCTO
var precios_cagados = 0;
//Arreglo de precios
  var Aprices = new Array();

function getprices(producto,ventas,distribucion,organizacion,detalle,prodP,ident,ida){  
  
  if(precios_cagados == 0){
    Aprices.length=0;
    showLoading('Cargando precios del producto');
    loadprices(producto,ventas,distribucion,organizacion,prodP,function(res){
          if(res != 0){
                  Aprices=res.concat();
                  hideLoading();
                  precios_cagados = 1;
                  AproveChanges(ident,ida,detalle);
                }else{
                  Aprices.push({value: '0', text: 'No hay más precios para este producto'});
                  precios_cagados = 1;
                  hideLoading();
                  AproveChanges(ident,ida,detalle);                 
                }
    });
  }else{
    AproveChanges(ident,ida,detalle);
  }
  
}


function loadprices(producto,ventas,distribucion,organizacion,prodP,callback){
  var ApedidotoPrice = new Array();
  ApedidotoPrice.push({idProducto:producto,OfVtn:ventas,CanDistri:distribucion,OrgVtn:organizacion,Pdetalle:prodP});

  $.post("../preciosJSON",{
        ApedidotoPrice:ApedidotoPrice
      },function(respuesta){
        var res = respuesta[0].estatus;
          if(res == 1){
            res = respuesta[0].mensaje;
            callback(res);
          }else{
            callback(res);
          }
      });

}

var ApedidosUpdate = new Array();
function validatechange(idents,ids,values,detalle,callback){

  if(idents == "P"){
    
        
    ApedidosUpdate.length = 0;

    for(var x=0;x<=AresultadosBusqueda.length-1;x++){
      for(var z=0;z<=AresultadosBusqueda[x].productos.length-1;z++){
      if(AresultadosBusqueda[x].productos[z].idDetallePedido == detalle){
        showLoading('Actualizando Pedido '+AresultadosBusqueda[x].idPedido);
        

            ApedidosUpdate.push({Pedido:AresultadosBusqueda[x].idPedido,
                      idDetallePedido:detalle,
                      Aproducto:AresultadosBusqueda[x].productos[z].productoPrecio.idProductoPrecio,
                      Nproducto:values,
                      cantidad:AresultadosBusqueda[x].productos[z].cantidad,
                      descripcion:AresultadosBusqueda[x].productos[z].productoPrecio.producto.descripcion,area:"ATC"});
            
              $.post("../UpdatePrecioPP",{
                              ApedidosUpdate:ApedidosUpdate
                          },function(respuesta){
                                var res = respuesta[0].estatus;
                                if(res == 1){
                                  callback(res);
                                }else{
                                  res = respuesta;
                                  callback(res);
                                }
                           });
      }
      }

    } 
  }else if(idents == "C"){

    ApedidosUpdate.length = 0;
    var idDetallePedido = detalle;
    var Subtotal = 0;
    var gastosEnvio = 0;
    var Total = 0;
    var AntCant = 0;
    var NewCant = values;
    var PrecioU = 0;
    var idPedido = 0;

    for(var x=0;x<=AresultadosBusqueda.length-1;x++){
      for(var z=0;z<=AresultadosBusqueda[x].productos.length-1;z++){
        if(AresultadosBusqueda[x].productos[z].idDetallePedido == idDetallePedido){
          Subtotal =parseFloat(AresultadosBusqueda[x].subTotal);
          gastosEnvio = parseFloat(AresultadosBusqueda[x].gastosEnvio.precio);
          AntCant = AresultadosBusqueda[x].productos[z].cantidad;
          PrecioU = parseFloat(AresultadosBusqueda[x].productos[z].productoPrecio.precio);
          idPedido = AresultadosBusqueda[x].idPedido;
        }
      }
    }
    showLoading('Actualizando Pedido '+idPedido);


      Precioproducto = AntCant*PrecioU;
      Subtotal = Subtotal-Precioproducto;
      Subtotal = Subtotal + (NewCant*PrecioU);
      Total = Subtotal + gastosEnvio;

      
        ApedidosUpdate.push({idPedido:idPedido,Detalle:idDetallePedido,Sub:Subtotal,total:Total,Cant:NewCant,ACant:AntCant,area:"ATC"});

        $.post("../UpdateCantProductosPedido",{
              ApedidosUpdate:ApedidosUpdate
          },function(respuesta){
                var res = respuesta[0].estatus;
                if(res == 1){
                  callback(res);
                }else{
                  res = respuesta[0].mensaje;
                  callback(res);
                }
           }
          );
    
  }
}

function update_table(ident,AdatesUpdate,value){

  if(ident == "P"){
    var subTotal = document.getElementById("camp_subtotal").innerHTML;
    subTotal = parseFloat(subTotal.replace("$", ""));
    var Envio = document.getElementById("camp_envio").innerHTML;
    Envio = parseFloat(Envio.replace("$", ""));
    var Nprecio;
    var Aprecio;
    var Cantidad = document.getElementById("C"+AdatesUpdate[0].idDetallePedido).innerHTML;
    for(var x=0;x<=Aprices.length-1;x++){
      if(Aprices[x].value == value){
        Nprecio = Aprices[x].text;
        Nprecio = parseFloat(Nprecio.replace('$',''));
      }
    }

    for(var z=0;z<=AresultadosBusqueda.length-1;z++){
      for(var y=0;y<=AresultadosBusqueda[z].productos.length-1;y++){
        if(AresultadosBusqueda[z].productos[y].idDetallePedido == AdatesUpdate[0].idDetallePedido){
          Aprecio=parseFloat(AresultadosBusqueda[z].productos[y].productoPrecio.precio);
        }
      }
    }

    var totalproducto = Cantidad*Aprecio;
    subTotal = subTotal-totalproducto;
    totalproducto = Cantidad*Nprecio; //Este se muestra
    subTotal = subTotal+totalproducto; //Este se muestra
    var Total = subTotal+Envio; //Este se muestra
    $('#PTP'+AdatesUpdate[0].idDetallePedido).html("$"+totalproducto);
    $('#camp_subtotal').html("$"+subTotal);
    $('#camp_total').html("$"+Total);

  }else if(ident == "C"){
    var iddiv = String(AdatesUpdate[0].Detalle);
    var Aprecio = document.getElementById("P"+iddiv).innerHTML;
    Aprecio = parseFloat(Aprecio.replace("$", ""));
    var totalproducto = Aprecio*parseFloat(AdatesUpdate[0].Cant);
    $('#PTP'+AdatesUpdate[0].Detalle).html("$"+totalproducto);
    $('#camp_subtotal').html("$"+AdatesUpdate[0].Sub);
    $('#camp_total').html("$"+AdatesUpdate[0].total);
  }

  hideLoading();

}

function made_pdf(idPedido){

    url = "../reportes/ventas/resumen/"+idPedido+"/pdf";
    window.open(url, '_blank');
    return false;
}


function clean_date(){
  $('#txt_fecha1').val("");
  $('#txt_fecha2').val("");
}

//validar fechas  
  function vd_date(){
    
    var date1 = createdate($('#txt_fecha1').val());
    var date2 = createdate($('#txt_fecha2').val());

    if (date1 <= date2) {
      $('#btn_consulta').attr("disabled", false);     
    }else if (date1 > date2){
      alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
      $('#btn_consulta').attr("disabled", true);  
    }

  }

function formatISO(date){

    var day =date.substr(0, 2);
    var month=date.substr(3, 2);
    var year=date.substr(6, 9);
    
    var Ndate = year+"-"+month+"-"+day;
    return Ndate;
} 