var nCliente = document.getElementById("mdr_puntoventabundle_pedidos_cliente");
var nDireccion = document.getElementById("mdr_puntoventabundle_pedidos_direccion");
var nFactura = document.getElementById("mdr_puntoventabundle_pedidos_factura");
var nRfc = document.getElementById("mdr_puntoventabundle_pedidos_rfc");

var idCliente = null;
var direcciones = null;
var clientes = null;
var cliente = null;
var direccionCliente = null;

document.addEventListener('DOMContentLoaded', function () {
	nCliente.addEventListener("change", changeCliente);
	nDireccion.addEventListener("change", changeDireccion);
	nFactura.addEventListener("change", changeFactura);
	
	nRfc.parentNode.hidden = true;
});

function changeCliente()
{
	idCliente = nCliente.value;
	getDirecciones(idCliente);
	cliente = null;
	carrito.showCliente(cliente);
	for (var i = 0; i < clientes.length; i++) {
		if(clientes[i].idCliente == idCliente)
		{
			cliente = clientes[i];
			carrito.showCliente(cliente);
		}
	}
}
function changeDireccion()
{
	direccionCliente = null;
	carrito.showDireccion(direccionCliente);
	for (var i = 0; i < direcciones.length; i++) {
		if(direcciones[i].idDireccion == nDireccion.value)
		{
			direccionCliente = direcciones[i];
			carrito.showDireccion(direccionCliente);
			i = direcciones.length;
			setTiposPago(direccionCliente);
		}
	};
}
function changeFactura()
{
	nRfc.parentNode.hidden = (this.value == 0);
}

function getDirecciones(idCliente)
{
	$.post(urls.cliente.direcciones, {idCliente: idCliente}, function(data, textStatus, xhr) {
		if(data.code == 1)
		{
			clearSelect(nDireccion);
			direcciones = data.value;
			addOption("", "", nDireccion);
			for (var i = 0; i < direcciones.length; i++) {
				addOption(direcciones[i].toString, direcciones[i].idDireccion, nDireccion);
			};
			if(direccionCliente != null)
			{
				nDireccion.value = direccionCliente.idDireccion;
				fireEvent(nDireccion, "change");
			}
			if(!(nDireccion.value > 0) && direcciones.length == 1)
			{
				nDireccion.value = direcciones[0].idDireccion;
				fireEvent(nDireccion, "change");
			}
		}
	});
}